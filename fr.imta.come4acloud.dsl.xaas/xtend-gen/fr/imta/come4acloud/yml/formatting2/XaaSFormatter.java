/**
 * generated by Xtext 2.11.0
 */
package fr.imta.come4acloud.yml.formatting2;

import com.google.inject.Inject;
import fr.imta.come4acloud.yml.services.XaaSGrammarAccess;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.formatting2.AbstractFormatter2;
import org.eclipse.xtext.formatting2.IFormattableDocument;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.xbase.lib.Extension;
import xaas.Configuration;
import xaas.Node;
import xaas.Relationship;
import xaas.Topology;
import xaas.XaasYamlLikeTosca;

@SuppressWarnings("all")
public class XaaSFormatter extends AbstractFormatter2 {
  @Inject
  @Extension
  private XaaSGrammarAccess _xaaSGrammarAccess;
  
  protected void _format(final XaasYamlLikeTosca xaasYamlLikeTosca, @Extension final IFormattableDocument document) {
    document.<Topology>format(xaasYamlLikeTosca.getTopology());
    document.<Configuration>format(xaasYamlLikeTosca.getConfiguration());
  }
  
  protected void _format(final Configuration configuration, @Extension final IFormattableDocument document) {
    EList<Node> _nodes = configuration.getNodes();
    for (final Node node : _nodes) {
      document.<Node>format(node);
    }
    EList<Relationship> _relationships = configuration.getRelationships();
    for (final Relationship relationship : _relationships) {
      document.<Relationship>format(relationship);
    }
  }
  
  public void format(final Object configuration, final IFormattableDocument document) {
    if (configuration instanceof XtextResource) {
      _format((XtextResource)configuration, document);
      return;
    } else if (configuration instanceof Configuration) {
      _format((Configuration)configuration, document);
      return;
    } else if (configuration instanceof XaasYamlLikeTosca) {
      _format((XaasYamlLikeTosca)configuration, document);
      return;
    } else if (configuration instanceof EObject) {
      _format((EObject)configuration, document);
      return;
    } else if (configuration == null) {
      _format((Void)null, document);
      return;
    } else if (configuration != null) {
      _format(configuration, document);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(configuration, document).toString());
    }
  }
}

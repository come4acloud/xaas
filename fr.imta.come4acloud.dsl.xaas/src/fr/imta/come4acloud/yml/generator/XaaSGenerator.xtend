/*
 * generated by Xtext 2.11.0
 */
package fr.imta.come4acloud.yml.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import xaas.NodeType
import xaas.Topology
import xaas.RelationshipType
import xaas.Relationship
import xaas.AttributeType
import xaas.Constraint
import xaas.ComparisonOperator
import xaas.Expression
import xaas.BinaryExpression
import xaas.AggregationExpression
import xaas.NbConnectionExpression
import xaas.AttributeExpression
import xaas.CustomExpression
import xaas.AlgebraicOperator
import xaas.AggregationOperator
import xaas.DirectionKind
import xaas.IntegerValueExpression
import xaas.Configuration
import xaas.Node
import xaas.Attribute
import xaas.CalculatedAttributeType
import java.util.List
import java.util.ArrayList
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.ecore.EObject
import java.util.HashMap
import org.eclipse.emf.ecore.xmi.XMLResource
import java.util.Map

import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtext.resource.XtextResource
import javax.xml.parsers.DocumentBuilderFactory
import java.io.File
import org.w3c.dom.Document
import javax.xml.parsers.DocumentBuilder
import org.w3c.dom.NodeList
import org.eclipse.xtext.util.UriUtil
import org.eclipse.core.runtime.FileLocator
import xaas.impl.NodeImpl
import java.util.jar.Attributes
import xaas.impl.AttributeImpl
import xaas.impl.RelationshipTypeImpl
import xaas.impl.RelationshipImpl
import org.eclipse.core.runtime.IAdaptable
import org.eclipse.ui.PlatformUI
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.core.resources.IProject

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class XaaSGenerator extends AbstractGenerator {
	
	String DOT = '.'
	String FORWARD_SLASH = '/'
	String JAVA_EXTENTION = 'java'
	String XML_EXTENTION = 'xml'
	
	final String PACKAGE_STATE_NODE_FIXED = 'state.node'
	final String PACKAGE_VARIABLE_NODE_FIXED = 'variable.node'
	final String PACKAGE_STATE_ATTRIBUTE_FIXED = 'state.attribute'
	final String PACKAGE_VARIABLE_ATTRIBUTE_FIXED = 'variable.attribute'
	final String PACKAGE_ACTION_FIXED = 'actions'
	final String PACKAGE_CONFIGRATIONS_FIXED = 'configurations'
	
	String PACKAGE_ROOT = ''
	String PACKAGE_STATE_NODE
	String PACKAGE_VARIABLE_NODE
	String PACKAGE_STATE_ATTRIBUTE
	String PACKAGE_VARIABLE_ATTRIBUTE
	String PACKAGE_ACTION
	String PACKAGE_CONFIGRATIONS
	
	IFileSystemAccess2 fsa
	Resource resource
	IGeneratorContext context
	
	var id_exp = 0
	var configurationsID = 0

	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		System.out.println('Start Generation')
		this.fsa = fsa
		this.resource = resource
		this.context = context
		
		//genarate java code for Topology
		for (topology : resource.allContents.toIterable.filter(Topology)){		
			topology.doGenerate
		}
		
		//genarate xml file for Configration
		for (configuration : resource.allContents.toIterable.filter(Configuration)){		
			configuration.doGenerate
			resource.doGenerateXMI(configuration)
		}
		
		System.out.println('Finish Generation')
	}


	def doGenerateXMI(Resource resource, Configuration configuration){

		//get the new configuration information
//		var window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
//    	if (window != null)
//    	{
//        	var selection = window.getSelectionService().getSelection();
//        	var firstElement = (selection as IStructuredSelection).getFirstElement();
//        	if (firstElement instanceof IAdaptable)
//        	{
//        		var adaptable = firstElement as IAdaptable
//            	var project = adaptable.getAdapter(IProject) as IProject;
//            	var path = project.getFullPath();
//            	System.out.println("zzzzzzzzzz "+path);
//        	}
//    	}
		var uri_xml = fsa.getURI(PACKAGE_CONFIGRATIONS_FIXED + FORWARD_SLASH + configuration.identifier + (configurationsID+1) + '.xml')
		var fXmlFile = new File(uri_xml.path)
//		var fXmlFile = new File('/Users/zakarea.alsharamines-nantes.fr/Documents/runtime-xaas/xaasTest/src-gen/configurations/iaas/' + configuration.identifier + (configurationsID+1) + '.xml')
		if(!fXmlFile.exists){return}
		var dbFactory = DocumentBuilderFactory.newInstance();
		var dBuilder = dbFactory.newDocumentBuilder();	
		var doc = dBuilder.parse(fXmlFile);
		var newnNodes = doc.getElementsByTagName("NodeDescription");
		
		//reset relashionships
		for (relationship : resource.allContents.toIterable.filter(Relationship)){
			if(!relationship.constant){
				configuration.relationships.remove(relationship)
			}
		}
		
		//execlude removed nodes
		for(var ii = 0; ii < configuration.nodes.length; ii++)
		{
			var currentNode = configuration.nodes.get(ii)
			var notFound = true
			for(var i = 0 ; i < newnNodes.getLength(); i++){
				var newNode = newnNodes.item(i);
				var idnode = newNode.getAttributes().getNamedItem("idnode").getNodeValue()
				if(currentNode.name.equals(idnode)){
					notFound = false
				}
			}
				
			if(notFound){
				configuration.nodes.remove(currentNode)
			}
		}

		for(var i = 0 ; i < newnNodes.getLength(); i++){
			var newNode = newnNodes.item(i);
			var idnode = newNode.getAttributes().getNamedItem("idnode").getNodeValue()
//			System.out.println('idnode = ' + idnode)
//			System.out.println('currentNodes.length = ' + currentNodes.iterator.size)
			var notFound = true
			for(var ii = 0; ii < configuration.nodes.length; ii++)
			{
				var currentNode = configuration.nodes.get(ii)
//				System.out.println('current node = ' + currentNode.name)
				if(idnode.equals(currentNode.name)){//change the current node state to the new one
					notFound = false
					var newNodeChild = newNode.getChildNodes()
					for(var j = 0; j < newNodeChild.length; j++){
						var child = newNodeChild.item(j)
						if(child.getNodeName().equals("successors")){
							var successors= child.getChildNodes();
							for(var succ = 0 ; succ < successors.getLength(); succ++){
								var nsucc = successors.item(succ);
								if(nsucc.getNodeType() != org.w3c.dom.Node.TEXT_NODE){
									var nameSucc = nsucc.getAttributes().getNamedItem("idnode").getNodeValue()
								}
							}
						}
						else if(child.getNodeName().equals("predecessors")){
							System.out.println('predecessors = ' + child.getChildNodes().length)
							var predecessors= child.getChildNodes();
							for(var pred = 0 ; pred < predecessors.getLength(); pred++){
								var npred = predecessors.item(pred);
								if(npred.getNodeType() != org.w3c.dom.Node.TEXT_NODE){
									var namePred = npred.getAttributes().getNamedItem("idnode").getNodeValue()
									var relashionShip = new RelationshipImpl()
									relashionShip.name = idnode + '_To_' + namePred
									var namePredType = ''
									for(node : configuration.nodes)
									{
										if(node.name.equals(namePred))
										{
											namePredType = node.type.name
										}
									}
									var relashionShipTypeName = currentNode.type.name + '_To_' + namePredType
									System.out.println('Relashionship Type ' + relashionShipTypeName)
									//search for relationships types
									for (relationshipType : configuration.topology.relationshipTypes){
										if(relashionShipTypeName.equals(relationshipType.name)){
											relashionShip.type = relationshipType
										}
									}
									//add source
									relashionShip.source = currentNode
									//add target
									for (node : configuration.nodes){
										if(namePred.equals(node.name)){
											relashionShip.target = node
										}
									}
									
									System.out.println('add Relashionship ' + relashionShip.name)
									configuration.relationships.add(relashionShip)
								}
							}
						}
						else if(child.getNodeName().equals("attributes")){
							var attributs= child.getChildNodes();
							for(var att = 0 ; att < attributs.getLength(); att++){
								var natt = attributs.item(att);
								if(natt.getNodeType() != org.w3c.dom.Node.TEXT_NODE){
									var nameAtt = natt.getAttributes().getNamedItem("name").getNodeValue();
									var valueAtt = natt.getAttributes().getNamedItem("value").getNodeValue();
									for (catt : currentNode.attributes) {
										if (catt.name.equals(nameAtt)) {
											System.out.println('change attribute ' + nameAtt + ' from ' + catt.value + ' to ' +valueAtt)
											catt.value = valueAtt
										}
										if(nameAtt.equals('AttributeEnable')){
											if(valueAtt == 0) {currentNode.activated = 'no'}
											if(valueAtt == 1) {currentNode.activated = 'yes'}
										}
									}
								}
							}
						}
					}
				}
			}
			
			//add missing nodes
			if(notFound && idnode != 'RootProvider' && idnode != 'RootClient'){
				var addNode = new NodeImpl()
				addNode.name = idnode
				addNode.activated = 'yes'
				//add node type
				var type = newNode.getAttributes().getNamedItem("type").getNodeValue()
				type = type.substring(type.lastIndexOf('.') + 1)

				for (addNodeType : configuration.topology.nodeTypes){
					System.out.println('type = ' + type)
					System.out.println('nodeType.name = ' + addNodeType.name)
					if(type.equals(addNodeType.name)){
						addNode.type = addNodeType
					}
				}
	
				var newNodeChild = newNode.getChildNodes()
				for(var j = 0; j < newNodeChild.length; j++){
					var child = newNodeChild.item(j)
					System.out.println('child.getNodeName() = ' + child.getNodeName())
					if(child.getNodeName().equals("successors")){
						var successors= child.getChildNodes();
						for(var succ = 0 ; succ < successors.getLength(); succ++){
							var nsucc = successors.item(succ);
							if(nsucc.getNodeType() != org.w3c.dom.Node.TEXT_NODE){
								var nameSucc = nsucc.getAttributes().getNamedItem("idnode").getNodeValue()
							}
						}
					}
					else if(child.getNodeName().equals("predecessors")){
						System.out.println('predecessors = ' + child.getChildNodes().length)
						var predecessors= child.getChildNodes();
						for(var pred = 0 ; pred < predecessors.getLength(); pred++){
							var npred = predecessors.item(pred);
							if(npred.getNodeType() != org.w3c.dom.Node.TEXT_NODE){
								var namePred = npred.getAttributes().getNamedItem("idnode").getNodeValue()
								var relashionShip = new RelationshipImpl()
								relashionShip.name = idnode + '_To_' + namePred
								var namePredType = ''
								for(node : configuration.nodes)
								{
									if(node.name.equals(namePred))
									{
										namePredType = node.type.name
									}
								}
								var relashionShipTypeName = addNode.type.name + '_To_' + namePredType
								System.out.println('Relashionship Type ' + relashionShipTypeName)
								//search for relationships types
								for (relationshipType : configuration.topology.relationshipTypes){
									if(relashionShipTypeName.equals(relationshipType.name)){
										relashionShip.type = relationshipType
									}
								}
								//add source
								relashionShip.source = addNode
								//add target
								for (node : configuration.nodes){
									if(namePred.equals(node.name)){
										relashionShip.target = node
									}
								}
									
								System.out.println('add Relashionship ' + relashionShip.name)
								configuration.relationships.add(relashionShip)
							}
						}
					}
					else if(child.getNodeName().equals("attributes")){
						var attributs= child.getChildNodes();
						for(var att = 0 ; att < attributs.getLength(); att++){
							var natt = attributs.item(att);
							if(natt.getNodeType() != org.w3c.dom.Node.TEXT_NODE){
								var nameAtt = natt.getAttributes().getNamedItem("name").getNodeValue();
								var valueAtt = natt.getAttributes().getNamedItem("value").getNodeValue();
								var addAtt = new AttributeImpl()
								addAtt.name = nameAtt
								addAtt.value = valueAtt
								addNode.attributes.add(addAtt)
								
								if(nameAtt.equals('AttributeEnable')){
									if(valueAtt == 0) {addNode.activated = 'no'}
									if(valueAtt == 1) {addNode.activated = 'yes'}
								}
							}
						}
					}
				}
				
				configuration.nodes.add(addNode)
										
			}	
			
		}
		
		
		var uri = URI.createURI('platform:' + fsa.getURI('configurations/' + configuration.identifier + (configurationsID+1) + '.xmi').path, false)
		
		System.out.println('path = ' + uri)
	    
		var xmiResource = resource.resourceSet.createResource(uri);
		xmiResource.getContents().addAll(EcoreUtil.copyAll(resource.getContents()));
		xmiResource.save(null);
		resource.save(null)

		configurationsID++	
	}
	
	def doGenerate(Topology topology){
		System.out.println('Parsing topology: ' + topology.name)
		
		//initilize package names
		PACKAGE_ROOT = topology.name
		PACKAGE_STATE_NODE = PACKAGE_ROOT + DOT + PACKAGE_STATE_NODE_FIXED
		PACKAGE_VARIABLE_NODE = PACKAGE_ROOT + DOT + PACKAGE_VARIABLE_NODE_FIXED
		PACKAGE_STATE_ATTRIBUTE = PACKAGE_ROOT + DOT + PACKAGE_STATE_ATTRIBUTE_FIXED
		PACKAGE_VARIABLE_ATTRIBUTE = PACKAGE_ROOT + DOT + PACKAGE_VARIABLE_ATTRIBUTE_FIXED
		PACKAGE_ACTION = PACKAGE_ROOT + DOT + PACKAGE_ACTION_FIXED
		PACKAGE_CONFIGRATIONS = PACKAGE_CONFIGRATIONS_FIXED
		
		//constraint excepration ID counter
		id_exp = 0
		
		for(nodeType : topology.nodeTypes){
			if(nodeType.inheritedType != null){//we dont need t generate a code for abstract nood types
				nodeType.doGenerate
			}
		}
		
		//generate ConcreteActionFactory.java
		var uri = PACKAGE_ACTION.toPath + FORWARD_SLASH + 'ConcreteActionFactory.java'
		fsa.generateFile(uri, topology.compile)
	
		//generate node actions (enable, desable)
		for(nodeType : topology.nodeTypes){
			
				if(nodeType.impactOfEnabling != 0)
				{
					//specify java path for enable action node class
					uri = PACKAGE_ACTION.toPath + FORWARD_SLASH + "enable_" + nodeType.name + DOT + JAVA_EXTENTION
					//generate java code for enable action node class
					fsa.generateFile(uri, nodeType.compile("enable", nodeType.impactOfEnabling))
					
				}
				if(nodeType.impactOfDisabling != 0)
				{
					//specify java path for disable action node class
					uri = PACKAGE_ACTION.toPath + FORWARD_SLASH + "disable_" + nodeType.name + DOT + JAVA_EXTENTION
					//generate java code for enable action node class
					fsa.generateFile(uri, nodeType.compile("disable", nodeType.impactOfDisabling))
					
				}
		}
		
		//generate relathionship actions (link, unlink)
		for(relationshipType : topology.relationshipTypes){
			
			if(relationshipType.impactOfLinking != 0)
			{
				//specify java path for link action node class
				uri = PACKAGE_ACTION.toPath + FORWARD_SLASH + "link_" + relationshipType.source.name + DOT + JAVA_EXTENTION
				//generate java code for enable action node class
				fsa.generateFile(uri, relationshipType.source.compile("link", relationshipType.impactOfLinking))
			}
			if(relationshipType.impactOfUnlinking != 0)
			{
				//specify java path for unlink action node class
				uri = PACKAGE_ACTION.toPath + FORWARD_SLASH + "unlink_" + relationshipType.source.name + DOT + JAVA_EXTENTION
				//generate java code for enable action node class
				fsa.generateFile(uri, relationshipType.source.compile("unlink", relationshipType.impactOfUnlinking))				
			}
		}
		
		////generate update attrebute actions (update)
		for(attributeType : resource.allContents.toIterable.filter(AttributeType)){
			if(attributeType.impactOfUpdating != 0)
			{
				//specify java path for unlink action node class
				uri = PACKAGE_ACTION.toPath + FORWARD_SLASH + "Modify_" + attributeType.name + DOT + JAVA_EXTENTION
				//generate java code for enable action node class
				fsa.generateFile(uri, attributeType.compile())
			}
		}
		
	}
	
	def compile(AttributeType attributeType){
		'''
		package «PACKAGE_ACTION»;
		
		import abstractsystem.action.*;
		import abstractsystem.state.node.id.*;
		import java.io.BufferedReader;
		import java.io.InputStreamReader;
		
		public class Modify_«attributeType.name» extends DefaultModifyAttributeAction implements Action{
			
			public Modify_«attributeType.name»(IDNode idnode,String name_attribute, int old_value, int new_value) {
				super(idnode,name_attribute, old_value,new_value);
			}
			
			@Override
			public int getTimeCost(){
				return «attributeType.impactOfUpdating»;
			}
			
			@Override
			public void perform() {
				Runtime runtime = Runtime.getRuntime();
				Process process = null;
				String arg_commande = getIdNode().toString()+" "+getName_attribute()+" "+getOld_value()+" "+getNew_value();
				try{
					process = runtime.exec("cmd "+arg_commande);
					InputStreamReader reader = new InputStreamReader(process.getInputStream());
					BufferedReader br = new BufferedReader(reader);
					String ligne=null;
					while ( (ligne = br.readLine()) != null){
						System.out.println(ligne);
					}
				} catch(Exception err) {;}			
			}
		}
		'''
	}
	
	def compile(NodeType nodeType, String action, int cost){
		'''
		package «PACKAGE_ACTION»;
		
		import abstractsystem.action.*;
		import abstractsystem.state.node.id.*;
		import java.io.BufferedReader;
		import java.io.InputStreamReader;
		
		
		
		public class «action»_«nodeType.name» extends «IF action == 'disable'»DefaultDisableAction«ELSEIF action == 'enable'»DefaultEnableAction«ELSEIF action == 'link'»DefaultLinkAction«ELSEIF action == 'unlink'»DefaultUnlinkAction«ENDIF» implements Action{
			«IF action == 'disable' || action == 'enable'»
			public «action»_«nodeType.name»(IDNode idnode){
				super(idnode);
			}
			«ELSEIF action == 'link' || action == 'unlink'»
			public «action»_«nodeType.name»(IDNode idnodefrom, IDNode idnodeto){
				super(idnodefrom,idnodeto);
			}
			«ENDIF»
			
			@Override
			public int getTimeCost() {
				return «cost»;
			}
			
			@Override
			public void perform() {
				Runtime runtime = Runtime.getRuntime();
				Process process = null;
				«IF action == 'disable' || action == 'enable'»
				String arg_commande = getIdNode().toString();
				«ELSEIF action == 'link' || action == 'unlink'»
				String arg_commande = getIdNodeFrom().toString()+" "+getIdNodeTo().toString();
				«ENDIF»
				try{
					process = runtime.exec("cmd "+arg_commande);
					InputStreamReader reader = new InputStreamReader(process.getInputStream());
					BufferedReader br = new BufferedReader(reader);
					String ligne=null;
					while ( (ligne = br.readLine()) != null){
						System.out.println(ligne);
					}
				} catch(Exception err) {;}
			}
		}
		'''
	}
	
	def compile(Topology topology){
		'''
		package «PACKAGE_ACTION»;
		
		import abstractsystem.state.node.id.*;
		import controler.Knowledge;
		import abstractsystem.action.*;
		
		public class ConcreteActionFactory extends DefaultActionFactory implements ActionFactory{
			
			@Override
			public Action newEnableAction(IDNode idNode) {
				return super.newEnableAction(idNode);
			}	
			
			@Override
			public Action newDisableAction(IDNode idNode) {
				return super.newDisableAction(idNode);
			}
			
			@Override
			public Action newUnlinkAction(IDNode idnodefrom, IDNode idnodeto) {
				return super.newUnlinkAction(idnodefrom,idnodeto);
			}
			
			@Override
			public Action newLinkAction(IDNode idnodefrom, IDNode idnodeto) {
				return super.newLinkAction(idnodefrom,idnodeto);
			}
			
			@Override
			public Action newModifyAttributeAction(IDNode idNode, String name_attribute, int old_value, int new_value) {
				«FOR nodeType : topology.nodeTypes»
				«FOR attrebute : nodeType.attributeTypes»
				«IF attrebute.impactOfUpdating != 0»
				if(Knowledge.getInstance().getIDTypeFromIDNode(idNode).getValue().equals("«PACKAGE_STATE_NODE»«DOT»«nodeType.name»") && name_attribute.equals("«PACKAGE_STATE_ATTRIBUTE»«DOT»«attrebute.name»")){
					return new Modify_«attrebute.name»(idNode, name_attribute, old_value, new_value);
				}
				«ENDIF»
				«ENDFOR»
				«ENDFOR»
				return super.newModifyAttributeAction(idNode,name_attribute,old_value, new_value);
			}			
		}
		
		'''
	}
	
	def doGenerate(NodeType nodeType){
		
		//specify java path for state node class
		var uri = PACKAGE_STATE_NODE.toPath + FORWARD_SLASH + nodeType.name + DOT + JAVA_EXTENTION
		//generate java code for state node class
		fsa.generateFile(uri, nodeType.compileStateNode)
		
		
		//specify java path for variable node class
		uri = PACKAGE_VARIABLE_NODE.toPath + FORWARD_SLASH + nodeType.name + "Variable" + DOT + JAVA_EXTENTION
		//generate java code for variable node class
		fsa.generateFile(uri, nodeType.compileVariableNode)
		
		for(attributeType : nodeType.attributeTypes){
			//specify java path for state attribute class
			uri = PACKAGE_STATE_ATTRIBUTE.toPath + FORWARD_SLASH + attributeType.name + DOT + JAVA_EXTENTION
			fsa.generateFile(uri, attributeType.compileStateAttribute)
			
			//specify java path for variable attribute class
			uri = PACKAGE_VARIABLE_ATTRIBUTE.toPath + FORWARD_SLASH + attributeType.name + 'Variable' + DOT + JAVA_EXTENTION
			fsa.generateFile(uri, attributeType.compileVariableAttribute(nodeType))
		}
		 
	}
	
	def compileStateNode(NodeType nodeType){
		'''
		package «PACKAGE_STATE_NODE»;
		
		import abstractsystem.state.node.*;
		import abstractsystem.state.node.id.*;
		import java.util.*;
		import «PACKAGE_VARIABLE_NODE».*;
		import org.chocosolver.solver.*;
		
		public class «nodeType.name» extends «nodeType.inheritedType.name»{
			
			private «nodeType.name»Variable variable=null;
			
			public «nodeType.name»(IDNode idNode, IDType idtype, Set<IDNode> preds, Set<IDNode> succs, IDInternal idNodeInternal, Map<String,Object> atts) {
				super(idNode,idtype, preds, succs, idNodeInternal, atts);	
			}
			
			protected «nodeType.name»Variable get«nodeType.inheritedType.name»Variable(Model m, int nb_nodes){
				if(variable == null){
					variable= new «nodeType.name»Variable(this, m, nb_nodes);
				}
				
				return variable;
			}
		}
		'''
	}
	
	def compileVariableNode(NodeType nodeType){
		'''
		package «PACKAGE_VARIABLE_NODE»;
		
		import abstractsystem.variable.node.*;
		import abstractsystem.state.attribute.*;
		import abstractsystem.state.node.*;
		import abstractsystem.state.node.id.*;
		import enumerations.*;
		import «PACKAGE_STATE_NODE».*;
		import «PACKAGE_STATE_ATTRIBUTE».*;
		import org.chocosolver.solver.*;
		
		
		public class «nodeType.name»Variable extends «nodeType.inheritedType.name»Variable {
			
			public «nodeType.name»Variable(«nodeType.name» n, Model m, int nb_nodes) {
				super(n, m, nb_nodes);
			}
			
			//prepare link constraints (link constraint type & link constraint node)
			@Override
			protected void prepareLinkConstraints(){
				super.prepareLinkConstraints();

				«FOR relashionShipType : nodeType.topology.relationshipTypes»
				«relashionShipType.compile(nodeType)»
				«ENDFOR»
				
				«FOR relashionShip : resource.allContents.toIterable.filter(Relationship)»
				«relashionShip.compile(nodeType)»
				«ENDFOR»
			
«««				getLinkConstraintsDescriptor().setNBPredsMin(1);
«««				getLinkConstraintsDescriptor().setNBPredsMax(1);
«««				getLinkConstraintsDescriptor().setNBSuccsMin(1);
«««				getLinkConstraintsDescriptor().setNBSuccsMax(1);
				
			}
		}
		'''
	}
	
	def compile(RelationshipType relationshipType, NodeType nodeType){
		//link constraint type
		'''
		«IF relationshipType.source == nodeType»
		getLinkConstraintsDescriptor().addClasse(TypeLinkConstraint.mandatory, EQcomparator.equal, «relationshipType.target.name».class, Sens.to);
		«ENDIF»
		«IF relationshipType.target == nodeType»
		getLinkConstraintsDescriptor().addClasse(TypeLinkConstraint.mandatory, EQcomparator.equal, «relationshipType.source.name».class, Sens.from);
		«ENDIF»
		'''
	}
	
	def compile(Relationship relationship, NodeType nodeType){
		//generate java for state ID attribute class
		//NOTE: replace nameAttribute by inferring target node name
		if(relationship.constant == true && relationship.source.type == nodeType){
			var attributeTypename = 'ID_' + relationship.target.type.name
			var uri = PACKAGE_STATE_ATTRIBUTE.toPath + FORWARD_SLASH + attributeTypename + DOT + JAVA_EXTENTION
			fsa.generateFile(uri, attributeTypename.compileStateIDAttribute)
			'''
			getLinkConstraintsDescriptor().addIDNode(TypeLinkConstraint.mandatory, EQcomparator.equal, ((AttributeIDNode) node.getAttributeByTypeName(Attribute.getNameType(ID_«relationship.target.type.name».class))).getValue(), Sens.to); 
			'''
		}
//		'''
//		«IF relationship.constant == true»
//		«IF relationship.source.type == nodeType»
//		getLinkConstraintsDescriptor().addIDNode(TypeLinkConstraint.mandatory, EQcomparator.equal, ((AttributeIDNode) node.getAttributeByTypeName(Attribute.getNameType(ID_«relationship.target.name».class))).getValue(), Sens.to); 
//		«ENDIF»
//«««		«IF relationship.target.type == nodeType»
//«««		getLinkConstraintsDescriptor().addIDNode(TypeLinkConstraint.mandatory, EQcomparator.equal, ((AttributeIDNode) node.getAttributeByTypeName(Attribute.getNameType(ID_«relationship.target.name».class))).getValue(), Sens.from); 
//«««		«ENDIF»
//		«ENDIF»
//		'''
	}

	def compileStateAttribute(AttributeType attributeType){
		'''
		package «PACKAGE_STATE_ATTRIBUTE»;
		
		import abstractsystem.state.node.*;
		import abstractsystem.state.node.id.*;
		import abstractsystem.state.attribute.*;
		import org.chocosolver.solver.variables.*;
		import org.chocosolver.solver.*;
		import «PACKAGE_VARIABLE_ATTRIBUTE».*;
		
		public class «attributeType.name» extends «IF attributeType.type == 'integer'»«IF attributeType instanceof CalculatedAttributeType»AttributeInteger«ELSE»AttributeIntegerFixed«ENDIF»«ELSE»Attribute«attributeType.type»«ENDIF»{
			
			private «attributeType.name»Variable variable=null;
			
			public «attributeType.name»(Node n, «IF attributeType.type == 'integer'»Integer«ELSE»«attributeType.type»«ENDIF» i){
				super(n,i);
			}
			
			@Override
			public «attributeType.name»Variable getVariable(Model m, int nb_nodes) {
				if(variable == null){
					variable=new «attributeType.name»Variable(this, 0, IntVar.MAX_INT_BOUND, m);
				}
				
				return variable;
			}
		}
		'''
	}
	//ID_«relationship.target.name»
	def compileStateIDAttribute (String IDAttributeName){
		'''
		package «PACKAGE_STATE_ATTRIBUTE»;
		
		import abstractsystem.state.node.*;
		import abstractsystem.state.node.id.*;
		import abstractsystem.state.attribute.*;
		
		public class «IDAttributeName» extends AttributeIDNode{
			
			public «IDAttributeName»(Node n, IDNode i){
				super(n,i);
			}
		}
		'''
	}
	
	def compileVariableAttribute(AttributeType attributeType, NodeType nodeType){
		'''
		package «PACKAGE_VARIABLE_ATTRIBUTE»;
		
		import org.chocosolver.solver.*;
		import abstractsystem.variable.attribute.*;
		import java.util.*;
		import org.chocosolver.solver.constraints.*;
		import abstractsystem.state.node.*;
		import enumerations.*;
		import abstractsystem.constraints.*;
		import «PACKAGE_STATE_ATTRIBUTE».*;
		
		public  class «attributeType.name»Variable  extends «IF attributeType.type == 'integer'»«IF attributeType instanceof CalculatedAttributeType»AttributeInteger«ELSE»AttributeIntegerFixed«ENDIF»«ELSE»Attribute«attributeType.type»«ENDIF»Variable{
			public «attributeType.name»Variable(«attributeType.name» a, int min, int max, Model m) {
				«IF attributeType instanceof CalculatedAttributeType»
				super(a, min, max, m);
				«ELSE»
				super(a, m);
				«ENDIF»
			}
			
			@Override
			public List<Constraint> buildConstraints(Model m, List<Node> nodes){
				List<Constraint> constraints = new ArrayList<Constraint>();
				«IF attributeType instanceof CalculatedAttributeType»
				//last id is «id_exp++»
				AttributeConstraint exp«id_exp» = «attributeType.expression.compile»;
				constraints	.addAll(exp«id_exp».build(m,nodes));
				constraints.add(m.arithm(getValueVar(), "=", exp«id_exp».getValueVar()));
				«ENDIF»
				«FOR constraint : nodeType.constraints»
				«IF (constraint?.expressions.get(0) as AttributeExpression)?.attributeType.name == attributeType?.name»
				«constraint.compile»
				«ENDIF»
				«ENDFOR»
				return constraints;
			}
		}
		'''
	}

	def compile(Constraint constraint){
//		var id=id_exp++;
		'''
		//last id is «id_exp++»
		AttributeConstraint exp«id_exp» = «constraint.expressions.get(1).compile»;
		constraints.addAll(exp«id_exp».build(m,nodes));
		constraints.add(m.arithm(getValueVar(), "«constraint.operator.toSign»", exp«id_exp».getValueVar()));
		'''
	}

	def compile(Expression expressions){

			if( expressions instanceof BinaryExpression){
			'''			
			new BinnaryExpression(TypeOperatorBinnaryExpression.«(expressions as BinaryExpression).operator.toAlgebraicOperator», «(expressions as BinaryExpression).expressions.get(0).compile», «(expressions as BinaryExpression).expressions.get(1).compile»)'''
			}
			else if(expressions instanceof AggregationExpression){
			'''
			new AggregationExpression(this.getAttribute().getHost(), TypeAggregationOperator.«(expressions as AggregationExpression).operator.toAggregationOperator», "«PACKAGE_STATE_ATTRIBUTE».«(expressions as AggregationExpression).attributeType.name»", TypeSens.«(expressions as AggregationExpression).direction.toDirection»)'''
			}
			else if(expressions instanceof NbConnectionExpression){
			'''
			new NbLinkExpression(this.getAttribute().getHost(), TypeSens.«(expressions as NbConnectionExpression).direction.toDirection»)'''
			}
			else if(expressions instanceof AttributeExpression){
			'''
			new NameAttributeExpression(this.getAttribute().getHost(), "«PACKAGE_STATE_ATTRIBUTE».«(expressions as AttributeExpression).attributeType.name»")'''
			}
			else if(expressions instanceof IntegerValueExpression){
			'''
			new IntegerValueExpression(«(expressions as IntegerValueExpression).value»)'''
			}
			else if(expressions instanceof CustomExpression){
			'''
			//Custom expression not supported yet
			'''
			}
	}
	
	def String toPath(String packageName) {
		return packageName.replace(DOT, FORWARD_SLASH)
		}
		
	def String toSign(ComparisonOperator comparisonOperater){
		switch(comparisonOperater){
		case ComparisonOperator.LESSER_OR_EQUAL:
			return "<="
		case ComparisonOperator.LESSER:
			return "<"
		case ComparisonOperator.GREATER_OR_EQUAL:
			return ">="
		case ComparisonOperator.GREATER:
			return ">"
		case ComparisonOperator.EQUAL:
			return "="
		default:
			return ""
		}
	}
	
	def String toAlgebraicOperator(AlgebraicOperator operator){
		switch(operator)
		{
			case AlgebraicOperator.SUM:
			return "sum"
			case AlgebraicOperator.MINUS:
			return "minus"
			case AlgebraicOperator.PROD:
			return "prod"
			case AlgebraicOperator.DIV:
			return "div" 
		}
	}
	
	def String toAggregationOperator(AggregationOperator operator){
		switch(operator)
		{
			case AggregationOperator.SUM:
			return "sum"
			case AggregationOperator.MIN:
			return "min"
			case AggregationOperator.MAX:
			return "max" 
		}
	}
	
	def String toDirection(DirectionKind direction){
		switch(direction)
		{
			case DirectionKind.PREDECESSOR_SOURCE:
			return "pred"
			case DirectionKind.SUCCESSOR_TARGET:
			return "succ"
		}
	}

	def doGenerate(Configuration configuration){
		
		System.out.println('Parsing configuration: ' + configuration.identifier)
		
		var uri = PACKAGE_CONFIGRATIONS_FIXED + FORWARD_SLASH + configuration.identifier + configurationsID + DOT + XML_EXTENTION
		var grammarUri = PACKAGE_CONFIGRATIONS_FIXED + FORWARD_SLASH + 'ConfDescriptionGrammar.dtd'
		fsa.generateFile(uri, configuration.compile)
		fsa.generateFile(grammarUri, generateGrammar)
	}
	
	def generateGrammar(){
		'''
		<!ELEMENT ConfigurationDescription (NodeDescription)+ >
		<!ATTLIST ConfigurationDescription package CDATA #REQUIRED>
		<!ATTLIST ConfigurationDescription packageAction CDATA #REQUIRED>
		
		
		<!ELEMENT NodeDescription (predecessors?,successors?, attributes?) >
		<!ATTLIST NodeDescription idnode ID #REQUIRED>
		<!ATTLIST NodeDescription type CDATA #REQUIRED>
		
		<!ELEMENT predecessors (predecessor)+>
		
		<!ELEMENT predecessor EMPTY>
		<!ATTLIST predecessor idnode IDREF #REQUIRED>
		
		
		<!ELEMENT successors (successor)+>
		
		<!ELEMENT successor EMPTY>
		<!ATTLIST successor idnode IDREF #REQUIRED>
		
		
		<!ELEMENT attributes (attribute|attributeIDNode)+>
		
		<!ELEMENT attribute EMPTY>
		<!ATTLIST attribute name CDATA #REQUIRED>
		<!ATTLIST attribute value CDATA #REQUIRED>
		
		
		<!ELEMENT attributeIDNode EMPTY>
		<!ATTLIST attributeIDNode name CDATA #REQUIRED>
		<!ATTLIST attributeIDNode value IDREF #REQUIRED>
		'''
	}
	
	def compile(Configuration configuration){
		'''
		<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
		<!DOCTYPE ConfigurationDescription SYSTEM "ConfDescriptionGrammar.dtd">
		
		<ConfigurationDescription package="«configuration.topology.name»«DOT»state" packageAction="«configuration.topology.name»«DOT»actions">
		
			<NodeDescription idnode="RootProvider" type="abstractsystem.state.node.RootProvider">
				<attributes>
					<attribute name="SystemExpense" value="0"/>
				</attributes>
			</NodeDescription>			
			<NodeDescription idnode="RootClient" type="abstractsystem.state.node.RootClient">
				<attributes>
					<attribute name="SystemRevenue" value="0"/>
				</attributes>
			</NodeDescription>
			
			«FOR node : configuration.nodes»
			«node.compile(configuration)»
			«ENDFOR»
		</ConfigurationDescription>
		'''
	}
	
	def compile(Node node, Configuration configuration){
		'''
		<NodeDescription  idnode="«node.name»" type="«node.type.topology.name»«DOT»«PACKAGE_STATE_NODE_FIXED»«DOT»«node.type.name»">
«««		    check if has predecessors or successors
			«var List<String> predecessors = new ArrayList»
			«var List<String> successors = new ArrayList»
			«FOR relashionship : configuration.relationships»
			«IF node == relashionship.source»
			«var twerk = predecessors.add(relashionship.target.name)»
			«ELSEIF node == relashionship.target»
			«var twerk = successors.add(relashionship.source.name)»
			«ENDIF»
			«ENDFOR»
«««		    add predecessors
			«IF predecessors.size > 0»
			<predecessors>
			«FOR predecessor : predecessors»
				<predecessor idnode="«predecessor»"/>
			«ENDFOR»
			</predecessors>
			«ENDIF»
«««		    add successors
			«IF successors.size > 0»
			<successors>
			«FOR successor : successors»
                <successor idnode="«successor»"/>
			«ENDFOR»
			</successors>
			«ENDIF»
«««			add attributes
			<attributes>
				«FOR attribute : node.attributes»
				«attribute.compile»
				«ENDFOR»
«««				manage attribute id
				«FOR relashionship : configuration.relationships»
				«relashionship.compile(node)»
				«ENDFOR»
				«IF node.type.name == 'SLAPower'»
				<attribute name="TotalCost"  value="0"/>
				«ENDIF»
				«IF node.activated == 'yes'»
				<attribute name="AttributeEnable" value="1"/>
				«ELSE»
				<attribute name="AttributeEnable" value="0"/>
				«ENDIF»
			</attributes>
		</NodeDescription>
		
		'''
	}
	
	def compile(Attribute attribute){
		'''
		<attribute name="«attribute.name»" value="«attribute.value»"/>
		'''
	}
	
	def compile(Relationship relationship, Node node){
		//NOTE: replace nameAttribute with node name
		'''
		«IF relationship.constant == true»
		«IF relationship.source.type == node.type && relationship.source.name == node.name »
		<attributeIDNode name="ID_«relationship.target.type.name»" value="«relationship.target.name»"/>
		«ENDIF»
«««		«IF relationship.target.type == node.type»
«««		<attributeIDNode name="«relationship.target.name»" value="«relationship.source.name»"/>
«««		«ENDIF»
		«ENDIF»
		'''
	}
	
}

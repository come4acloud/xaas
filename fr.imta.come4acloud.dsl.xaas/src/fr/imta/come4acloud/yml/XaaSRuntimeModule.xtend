/*
 * generated by Xtext 2.11.0
 */
package fr.imta.come4acloud.yml


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class XaaSRuntimeModule extends AbstractXaaSRuntimeModule {
}

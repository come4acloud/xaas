package fr.imta.come4acloud.yml.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.imta.come4acloud.yml.services.XaaSGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXaaSParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Configuration'", "':'", "'identifier'", "'topology'", "'Topology'", "'node_types'", "'relationship_types'", "'.'", "'Node'", "'type'", "'activated'", "'properties'", "'Relationship'", "'constant'", "'source'", "'target'", "'derived_from'", "'impactOfEnabling'", "'impactOfDisabling'", "'constraints'", "'variable'", "'equal'", "'impactOfUpdating'", "'('", "','", "')'", "'NbLink'", "'CustomExpression'", "'-'", "'valid_source_types'", "'valid_target_types'", "'impactOfLinking'", "'impactOfUnlinking'", "'true'", "'false'", "'+'", "'*'", "'/'", "'Sum'", "'Min'", "'Max'", "'Pred'", "'Succ'", "'less_than'", "'greater_than'", "'less_or_equal'", "'greater_or_equal'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalXaaSParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXaaSParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXaaSParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXaaS.g"; }



     	private XaaSGrammarAccess grammarAccess;

        public InternalXaaSParser(TokenStream input, XaaSGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "XaasYamlLikeTosca";
       	}

       	@Override
       	protected XaaSGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleXaasYamlLikeTosca"
    // InternalXaaS.g:65:1: entryRuleXaasYamlLikeTosca returns [EObject current=null] : iv_ruleXaasYamlLikeTosca= ruleXaasYamlLikeTosca EOF ;
    public final EObject entryRuleXaasYamlLikeTosca() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXaasYamlLikeTosca = null;


        try {
            // InternalXaaS.g:65:58: (iv_ruleXaasYamlLikeTosca= ruleXaasYamlLikeTosca EOF )
            // InternalXaaS.g:66:2: iv_ruleXaasYamlLikeTosca= ruleXaasYamlLikeTosca EOF
            {
             newCompositeNode(grammarAccess.getXaasYamlLikeToscaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXaasYamlLikeTosca=ruleXaasYamlLikeTosca();

            state._fsp--;

             current =iv_ruleXaasYamlLikeTosca; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXaasYamlLikeTosca"


    // $ANTLR start "ruleXaasYamlLikeTosca"
    // InternalXaaS.g:72:1: ruleXaasYamlLikeTosca returns [EObject current=null] : ( ( (lv_topology_0_0= ruleTopology ) )? ( (lv_configuration_1_0= ruleConfiguration ) )? ) ;
    public final EObject ruleXaasYamlLikeTosca() throws RecognitionException {
        EObject current = null;

        EObject lv_topology_0_0 = null;

        EObject lv_configuration_1_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:78:2: ( ( ( (lv_topology_0_0= ruleTopology ) )? ( (lv_configuration_1_0= ruleConfiguration ) )? ) )
            // InternalXaaS.g:79:2: ( ( (lv_topology_0_0= ruleTopology ) )? ( (lv_configuration_1_0= ruleConfiguration ) )? )
            {
            // InternalXaaS.g:79:2: ( ( (lv_topology_0_0= ruleTopology ) )? ( (lv_configuration_1_0= ruleConfiguration ) )? )
            // InternalXaaS.g:80:3: ( (lv_topology_0_0= ruleTopology ) )? ( (lv_configuration_1_0= ruleConfiguration ) )?
            {
            // InternalXaaS.g:80:3: ( (lv_topology_0_0= ruleTopology ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==15) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalXaaS.g:81:4: (lv_topology_0_0= ruleTopology )
                    {
                    // InternalXaaS.g:81:4: (lv_topology_0_0= ruleTopology )
                    // InternalXaaS.g:82:5: lv_topology_0_0= ruleTopology
                    {

                    					newCompositeNode(grammarAccess.getXaasYamlLikeToscaAccess().getTopologyTopologyParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_topology_0_0=ruleTopology();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getXaasYamlLikeToscaRule());
                    					}
                    					set(
                    						current,
                    						"topology",
                    						lv_topology_0_0,
                    						"fr.imta.come4acloud.yml.XaaS.Topology");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalXaaS.g:99:3: ( (lv_configuration_1_0= ruleConfiguration ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalXaaS.g:100:4: (lv_configuration_1_0= ruleConfiguration )
                    {
                    // InternalXaaS.g:100:4: (lv_configuration_1_0= ruleConfiguration )
                    // InternalXaaS.g:101:5: lv_configuration_1_0= ruleConfiguration
                    {

                    					newCompositeNode(grammarAccess.getXaasYamlLikeToscaAccess().getConfigurationConfigurationParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_configuration_1_0=ruleConfiguration();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getXaasYamlLikeToscaRule());
                    					}
                    					set(
                    						current,
                    						"configuration",
                    						lv_configuration_1_0,
                    						"fr.imta.come4acloud.yml.XaaS.Configuration");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXaasYamlLikeTosca"


    // $ANTLR start "entryRuleAttributeType"
    // InternalXaaS.g:122:1: entryRuleAttributeType returns [EObject current=null] : iv_ruleAttributeType= ruleAttributeType EOF ;
    public final EObject entryRuleAttributeType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeType = null;


        try {
            // InternalXaaS.g:122:54: (iv_ruleAttributeType= ruleAttributeType EOF )
            // InternalXaaS.g:123:2: iv_ruleAttributeType= ruleAttributeType EOF
            {
             newCompositeNode(grammarAccess.getAttributeTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeType=ruleAttributeType();

            state._fsp--;

             current =iv_ruleAttributeType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeType"


    // $ANTLR start "ruleAttributeType"
    // InternalXaaS.g:129:1: ruleAttributeType returns [EObject current=null] : (this_ConstantAttributeType_0= ruleConstantAttributeType | this_CalculatedAttributeType_1= ruleCalculatedAttributeType ) ;
    public final EObject ruleAttributeType() throws RecognitionException {
        EObject current = null;

        EObject this_ConstantAttributeType_0 = null;

        EObject this_CalculatedAttributeType_1 = null;



        	enterRule();

        try {
            // InternalXaaS.g:135:2: ( (this_ConstantAttributeType_0= ruleConstantAttributeType | this_CalculatedAttributeType_1= ruleCalculatedAttributeType ) )
            // InternalXaaS.g:136:2: (this_ConstantAttributeType_0= ruleConstantAttributeType | this_CalculatedAttributeType_1= ruleCalculatedAttributeType )
            {
            // InternalXaaS.g:136:2: (this_ConstantAttributeType_0= ruleConstantAttributeType | this_CalculatedAttributeType_1= ruleCalculatedAttributeType )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==24) ) {
                alt3=1;
            }
            else if ( (LA3_0==31) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalXaaS.g:137:3: this_ConstantAttributeType_0= ruleConstantAttributeType
                    {

                    			newCompositeNode(grammarAccess.getAttributeTypeAccess().getConstantAttributeTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ConstantAttributeType_0=ruleConstantAttributeType();

                    state._fsp--;


                    			current = this_ConstantAttributeType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalXaaS.g:146:3: this_CalculatedAttributeType_1= ruleCalculatedAttributeType
                    {

                    			newCompositeNode(grammarAccess.getAttributeTypeAccess().getCalculatedAttributeTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_CalculatedAttributeType_1=ruleCalculatedAttributeType();

                    state._fsp--;


                    			current = this_CalculatedAttributeType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeType"


    // $ANTLR start "entryRuleExpression"
    // InternalXaaS.g:158:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalXaaS.g:158:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalXaaS.g:159:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalXaaS.g:165:1: ruleExpression returns [EObject current=null] : (this_IntegerValueExpression_0= ruleIntegerValueExpression | this_AttributeExpression_1= ruleAttributeExpression | this_BinaryExpression_2= ruleBinaryExpression | this_AggregationExpression_3= ruleAggregationExpression | this_NbConnectionExpression_4= ruleNbConnectionExpression | this_CustomExpression_5= ruleCustomExpression ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerValueExpression_0 = null;

        EObject this_AttributeExpression_1 = null;

        EObject this_BinaryExpression_2 = null;

        EObject this_AggregationExpression_3 = null;

        EObject this_NbConnectionExpression_4 = null;

        EObject this_CustomExpression_5 = null;



        	enterRule();

        try {
            // InternalXaaS.g:171:2: ( (this_IntegerValueExpression_0= ruleIntegerValueExpression | this_AttributeExpression_1= ruleAttributeExpression | this_BinaryExpression_2= ruleBinaryExpression | this_AggregationExpression_3= ruleAggregationExpression | this_NbConnectionExpression_4= ruleNbConnectionExpression | this_CustomExpression_5= ruleCustomExpression ) )
            // InternalXaaS.g:172:2: (this_IntegerValueExpression_0= ruleIntegerValueExpression | this_AttributeExpression_1= ruleAttributeExpression | this_BinaryExpression_2= ruleBinaryExpression | this_AggregationExpression_3= ruleAggregationExpression | this_NbConnectionExpression_4= ruleNbConnectionExpression | this_CustomExpression_5= ruleCustomExpression )
            {
            // InternalXaaS.g:172:2: (this_IntegerValueExpression_0= ruleIntegerValueExpression | this_AttributeExpression_1= ruleAttributeExpression | this_BinaryExpression_2= ruleBinaryExpression | this_AggregationExpression_3= ruleAggregationExpression | this_NbConnectionExpression_4= ruleNbConnectionExpression | this_CustomExpression_5= ruleCustomExpression )
            int alt4=6;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // InternalXaaS.g:173:3: this_IntegerValueExpression_0= ruleIntegerValueExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getIntegerValueExpressionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_IntegerValueExpression_0=ruleIntegerValueExpression();

                    state._fsp--;


                    			current = this_IntegerValueExpression_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalXaaS.g:182:3: this_AttributeExpression_1= ruleAttributeExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getAttributeExpressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AttributeExpression_1=ruleAttributeExpression();

                    state._fsp--;


                    			current = this_AttributeExpression_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalXaaS.g:191:3: this_BinaryExpression_2= ruleBinaryExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getBinaryExpressionParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_BinaryExpression_2=ruleBinaryExpression();

                    state._fsp--;


                    			current = this_BinaryExpression_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalXaaS.g:200:3: this_AggregationExpression_3= ruleAggregationExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getAggregationExpressionParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_AggregationExpression_3=ruleAggregationExpression();

                    state._fsp--;


                    			current = this_AggregationExpression_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalXaaS.g:209:3: this_NbConnectionExpression_4= ruleNbConnectionExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getNbConnectionExpressionParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_NbConnectionExpression_4=ruleNbConnectionExpression();

                    state._fsp--;


                    			current = this_NbConnectionExpression_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalXaaS.g:218:3: this_CustomExpression_5= ruleCustomExpression
                    {

                    			newCompositeNode(grammarAccess.getExpressionAccess().getCustomExpressionParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_CustomExpression_5=ruleCustomExpression();

                    state._fsp--;


                    			current = this_CustomExpression_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleSingelExpression"
    // InternalXaaS.g:230:1: entryRuleSingelExpression returns [EObject current=null] : iv_ruleSingelExpression= ruleSingelExpression EOF ;
    public final EObject entryRuleSingelExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingelExpression = null;


        try {
            // InternalXaaS.g:230:57: (iv_ruleSingelExpression= ruleSingelExpression EOF )
            // InternalXaaS.g:231:2: iv_ruleSingelExpression= ruleSingelExpression EOF
            {
             newCompositeNode(grammarAccess.getSingelExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSingelExpression=ruleSingelExpression();

            state._fsp--;

             current =iv_ruleSingelExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingelExpression"


    // $ANTLR start "ruleSingelExpression"
    // InternalXaaS.g:237:1: ruleSingelExpression returns [EObject current=null] : (this_IntegerValueExpression_0= ruleIntegerValueExpression | this_AttributeExpression_1= ruleAttributeExpression | this_AggregationExpression_2= ruleAggregationExpression | this_NbConnectionExpression_3= ruleNbConnectionExpression ) ;
    public final EObject ruleSingelExpression() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerValueExpression_0 = null;

        EObject this_AttributeExpression_1 = null;

        EObject this_AggregationExpression_2 = null;

        EObject this_NbConnectionExpression_3 = null;



        	enterRule();

        try {
            // InternalXaaS.g:243:2: ( (this_IntegerValueExpression_0= ruleIntegerValueExpression | this_AttributeExpression_1= ruleAttributeExpression | this_AggregationExpression_2= ruleAggregationExpression | this_NbConnectionExpression_3= ruleNbConnectionExpression ) )
            // InternalXaaS.g:244:2: (this_IntegerValueExpression_0= ruleIntegerValueExpression | this_AttributeExpression_1= ruleAttributeExpression | this_AggregationExpression_2= ruleAggregationExpression | this_NbConnectionExpression_3= ruleNbConnectionExpression )
            {
            // InternalXaaS.g:244:2: (this_IntegerValueExpression_0= ruleIntegerValueExpression | this_AttributeExpression_1= ruleAttributeExpression | this_AggregationExpression_2= ruleAggregationExpression | this_NbConnectionExpression_3= ruleNbConnectionExpression )
            int alt5=4;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case 39:
                {
                alt5=1;
                }
                break;
            case RULE_ID:
            case RULE_STRING:
                {
                alt5=2;
                }
                break;
            case 49:
            case 50:
            case 51:
                {
                alt5=3;
                }
                break;
            case 37:
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalXaaS.g:245:3: this_IntegerValueExpression_0= ruleIntegerValueExpression
                    {

                    			newCompositeNode(grammarAccess.getSingelExpressionAccess().getIntegerValueExpressionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_IntegerValueExpression_0=ruleIntegerValueExpression();

                    state._fsp--;


                    			current = this_IntegerValueExpression_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalXaaS.g:254:3: this_AttributeExpression_1= ruleAttributeExpression
                    {

                    			newCompositeNode(grammarAccess.getSingelExpressionAccess().getAttributeExpressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AttributeExpression_1=ruleAttributeExpression();

                    state._fsp--;


                    			current = this_AttributeExpression_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalXaaS.g:263:3: this_AggregationExpression_2= ruleAggregationExpression
                    {

                    			newCompositeNode(grammarAccess.getSingelExpressionAccess().getAggregationExpressionParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_AggregationExpression_2=ruleAggregationExpression();

                    state._fsp--;


                    			current = this_AggregationExpression_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalXaaS.g:272:3: this_NbConnectionExpression_3= ruleNbConnectionExpression
                    {

                    			newCompositeNode(grammarAccess.getSingelExpressionAccess().getNbConnectionExpressionParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_NbConnectionExpression_3=ruleNbConnectionExpression();

                    state._fsp--;


                    			current = this_NbConnectionExpression_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingelExpression"


    // $ANTLR start "entryRuleConfiguration"
    // InternalXaaS.g:284:1: entryRuleConfiguration returns [EObject current=null] : iv_ruleConfiguration= ruleConfiguration EOF ;
    public final EObject entryRuleConfiguration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConfiguration = null;


        try {
            // InternalXaaS.g:284:54: (iv_ruleConfiguration= ruleConfiguration EOF )
            // InternalXaaS.g:285:2: iv_ruleConfiguration= ruleConfiguration EOF
            {
             newCompositeNode(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConfiguration=ruleConfiguration();

            state._fsp--;

             current =iv_ruleConfiguration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalXaaS.g:291:1: ruleConfiguration returns [EObject current=null] : (otherlv_0= 'Configuration' otherlv_1= ':' otherlv_2= 'identifier' otherlv_3= ':' ( (lv_identifier_4_0= ruleEString ) ) otherlv_5= 'topology' otherlv_6= ':' ( ( ruleEString ) ) ( (lv_nodes_8_0= ruleNode ) )* ( (lv_relationships_9_0= ruleRelationship ) )* ) ;
    public final EObject ruleConfiguration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_identifier_4_0 = null;

        EObject lv_nodes_8_0 = null;

        EObject lv_relationships_9_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:297:2: ( (otherlv_0= 'Configuration' otherlv_1= ':' otherlv_2= 'identifier' otherlv_3= ':' ( (lv_identifier_4_0= ruleEString ) ) otherlv_5= 'topology' otherlv_6= ':' ( ( ruleEString ) ) ( (lv_nodes_8_0= ruleNode ) )* ( (lv_relationships_9_0= ruleRelationship ) )* ) )
            // InternalXaaS.g:298:2: (otherlv_0= 'Configuration' otherlv_1= ':' otherlv_2= 'identifier' otherlv_3= ':' ( (lv_identifier_4_0= ruleEString ) ) otherlv_5= 'topology' otherlv_6= ':' ( ( ruleEString ) ) ( (lv_nodes_8_0= ruleNode ) )* ( (lv_relationships_9_0= ruleRelationship ) )* )
            {
            // InternalXaaS.g:298:2: (otherlv_0= 'Configuration' otherlv_1= ':' otherlv_2= 'identifier' otherlv_3= ':' ( (lv_identifier_4_0= ruleEString ) ) otherlv_5= 'topology' otherlv_6= ':' ( ( ruleEString ) ) ( (lv_nodes_8_0= ruleNode ) )* ( (lv_relationships_9_0= ruleRelationship ) )* )
            // InternalXaaS.g:299:3: otherlv_0= 'Configuration' otherlv_1= ':' otherlv_2= 'identifier' otherlv_3= ':' ( (lv_identifier_4_0= ruleEString ) ) otherlv_5= 'topology' otherlv_6= ':' ( ( ruleEString ) ) ( (lv_nodes_8_0= ruleNode ) )* ( (lv_relationships_9_0= ruleRelationship ) )*
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getConfigurationAccess().getConfigurationKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getConfigurationAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_4); 

            			newLeafNode(otherlv_2, grammarAccess.getConfigurationAccess().getIdentifierKeyword_2());
            		
            otherlv_3=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getConfigurationAccess().getColonKeyword_3());
            		
            // InternalXaaS.g:315:3: ( (lv_identifier_4_0= ruleEString ) )
            // InternalXaaS.g:316:4: (lv_identifier_4_0= ruleEString )
            {
            // InternalXaaS.g:316:4: (lv_identifier_4_0= ruleEString )
            // InternalXaaS.g:317:5: lv_identifier_4_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getConfigurationAccess().getIdentifierEStringParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_7);
            lv_identifier_4_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            					}
            					set(
            						current,
            						"identifier",
            						lv_identifier_4_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_5, grammarAccess.getConfigurationAccess().getTopologyKeyword_5());
            		
            otherlv_6=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_6, grammarAccess.getConfigurationAccess().getColonKeyword_6());
            		
            // InternalXaaS.g:342:3: ( ( ruleEString ) )
            // InternalXaaS.g:343:4: ( ruleEString )
            {
            // InternalXaaS.g:343:4: ( ruleEString )
            // InternalXaaS.g:344:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConfigurationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getConfigurationAccess().getTopologyTopologyCrossReference_7_0());
            				
            pushFollow(FOLLOW_8);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXaaS.g:358:3: ( (lv_nodes_8_0= ruleNode ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==19) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalXaaS.g:359:4: (lv_nodes_8_0= ruleNode )
            	    {
            	    // InternalXaaS.g:359:4: (lv_nodes_8_0= ruleNode )
            	    // InternalXaaS.g:360:5: lv_nodes_8_0= ruleNode
            	    {

            	    					newCompositeNode(grammarAccess.getConfigurationAccess().getNodesNodeParserRuleCall_8_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_nodes_8_0=ruleNode();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"nodes",
            	    						lv_nodes_8_0,
            	    						"fr.imta.come4acloud.yml.XaaS.Node");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalXaaS.g:377:3: ( (lv_relationships_9_0= ruleRelationship ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==23) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalXaaS.g:378:4: (lv_relationships_9_0= ruleRelationship )
            	    {
            	    // InternalXaaS.g:378:4: (lv_relationships_9_0= ruleRelationship )
            	    // InternalXaaS.g:379:5: lv_relationships_9_0= ruleRelationship
            	    {

            	    					newCompositeNode(grammarAccess.getConfigurationAccess().getRelationshipsRelationshipParserRuleCall_9_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_relationships_9_0=ruleRelationship();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getConfigurationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"relationships",
            	    						lv_relationships_9_0,
            	    						"fr.imta.come4acloud.yml.XaaS.Relationship");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleTopology"
    // InternalXaaS.g:400:1: entryRuleTopology returns [EObject current=null] : iv_ruleTopology= ruleTopology EOF ;
    public final EObject entryRuleTopology() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTopology = null;


        try {
            // InternalXaaS.g:400:49: (iv_ruleTopology= ruleTopology EOF )
            // InternalXaaS.g:401:2: iv_ruleTopology= ruleTopology EOF
            {
             newCompositeNode(grammarAccess.getTopologyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTopology=ruleTopology();

            state._fsp--;

             current =iv_ruleTopology; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTopology"


    // $ANTLR start "ruleTopology"
    // InternalXaaS.g:407:1: ruleTopology returns [EObject current=null] : ( () otherlv_1= 'Topology' otherlv_2= ':' ( (lv_name_3_0= ruleEString ) ) otherlv_4= 'node_types' otherlv_5= ':' ( (lv_nodeTypes_6_0= ruleNodeType ) )* (otherlv_7= 'relationship_types' otherlv_8= ':' ( (lv_relationshipTypes_9_0= ruleRelationshipType ) )* )? ) ;
    public final EObject ruleTopology() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;

        EObject lv_nodeTypes_6_0 = null;

        EObject lv_relationshipTypes_9_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:413:2: ( ( () otherlv_1= 'Topology' otherlv_2= ':' ( (lv_name_3_0= ruleEString ) ) otherlv_4= 'node_types' otherlv_5= ':' ( (lv_nodeTypes_6_0= ruleNodeType ) )* (otherlv_7= 'relationship_types' otherlv_8= ':' ( (lv_relationshipTypes_9_0= ruleRelationshipType ) )* )? ) )
            // InternalXaaS.g:414:2: ( () otherlv_1= 'Topology' otherlv_2= ':' ( (lv_name_3_0= ruleEString ) ) otherlv_4= 'node_types' otherlv_5= ':' ( (lv_nodeTypes_6_0= ruleNodeType ) )* (otherlv_7= 'relationship_types' otherlv_8= ':' ( (lv_relationshipTypes_9_0= ruleRelationshipType ) )* )? )
            {
            // InternalXaaS.g:414:2: ( () otherlv_1= 'Topology' otherlv_2= ':' ( (lv_name_3_0= ruleEString ) ) otherlv_4= 'node_types' otherlv_5= ':' ( (lv_nodeTypes_6_0= ruleNodeType ) )* (otherlv_7= 'relationship_types' otherlv_8= ':' ( (lv_relationshipTypes_9_0= ruleRelationshipType ) )* )? )
            // InternalXaaS.g:415:3: () otherlv_1= 'Topology' otherlv_2= ':' ( (lv_name_3_0= ruleEString ) ) otherlv_4= 'node_types' otherlv_5= ':' ( (lv_nodeTypes_6_0= ruleNodeType ) )* (otherlv_7= 'relationship_types' otherlv_8= ':' ( (lv_relationshipTypes_9_0= ruleRelationshipType ) )* )?
            {
            // InternalXaaS.g:415:3: ()
            // InternalXaaS.g:416:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTopologyAccess().getTopologyAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,15,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getTopologyAccess().getTopologyKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getTopologyAccess().getColonKeyword_2());
            		
            // InternalXaaS.g:430:3: ( (lv_name_3_0= ruleEString ) )
            // InternalXaaS.g:431:4: (lv_name_3_0= ruleEString )
            {
            // InternalXaaS.g:431:4: (lv_name_3_0= ruleEString )
            // InternalXaaS.g:432:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getTopologyAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_10);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTopologyRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,16,FOLLOW_4); 

            			newLeafNode(otherlv_4, grammarAccess.getTopologyAccess().getNode_typesKeyword_4());
            		
            otherlv_5=(Token)match(input,12,FOLLOW_11); 

            			newLeafNode(otherlv_5, grammarAccess.getTopologyAccess().getColonKeyword_5());
            		
            // InternalXaaS.g:457:3: ( (lv_nodeTypes_6_0= ruleNodeType ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=RULE_ID && LA8_0<=RULE_STRING)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalXaaS.g:458:4: (lv_nodeTypes_6_0= ruleNodeType )
            	    {
            	    // InternalXaaS.g:458:4: (lv_nodeTypes_6_0= ruleNodeType )
            	    // InternalXaaS.g:459:5: lv_nodeTypes_6_0= ruleNodeType
            	    {

            	    					newCompositeNode(grammarAccess.getTopologyAccess().getNodeTypesNodeTypeParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_11);
            	    lv_nodeTypes_6_0=ruleNodeType();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTopologyRule());
            	    					}
            	    					add(
            	    						current,
            	    						"nodeTypes",
            	    						lv_nodeTypes_6_0,
            	    						"fr.imta.come4acloud.yml.XaaS.NodeType");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            // InternalXaaS.g:476:3: (otherlv_7= 'relationship_types' otherlv_8= ':' ( (lv_relationshipTypes_9_0= ruleRelationshipType ) )* )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==17) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalXaaS.g:477:4: otherlv_7= 'relationship_types' otherlv_8= ':' ( (lv_relationshipTypes_9_0= ruleRelationshipType ) )*
                    {
                    otherlv_7=(Token)match(input,17,FOLLOW_4); 

                    				newLeafNode(otherlv_7, grammarAccess.getTopologyAccess().getRelationship_typesKeyword_7_0());
                    			
                    otherlv_8=(Token)match(input,12,FOLLOW_12); 

                    				newLeafNode(otherlv_8, grammarAccess.getTopologyAccess().getColonKeyword_7_1());
                    			
                    // InternalXaaS.g:485:4: ( (lv_relationshipTypes_9_0= ruleRelationshipType ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( ((LA9_0>=RULE_ID && LA9_0<=RULE_STRING)) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalXaaS.g:486:5: (lv_relationshipTypes_9_0= ruleRelationshipType )
                    	    {
                    	    // InternalXaaS.g:486:5: (lv_relationshipTypes_9_0= ruleRelationshipType )
                    	    // InternalXaaS.g:487:6: lv_relationshipTypes_9_0= ruleRelationshipType
                    	    {

                    	    						newCompositeNode(grammarAccess.getTopologyAccess().getRelationshipTypesRelationshipTypeParserRuleCall_7_2_0());
                    	    					
                    	    pushFollow(FOLLOW_12);
                    	    lv_relationshipTypes_9_0=ruleRelationshipType();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getTopologyRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"relationshipTypes",
                    	    							lv_relationshipTypes_9_0,
                    	    							"fr.imta.come4acloud.yml.XaaS.RelationshipType");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTopology"


    // $ANTLR start "entryRuleEString"
    // InternalXaaS.g:509:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalXaaS.g:509:47: (iv_ruleEString= ruleEString EOF )
            // InternalXaaS.g:510:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalXaaS.g:516:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) | this_STRING_3= RULE_STRING ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;
        Token this_STRING_3=null;


        	enterRule();

        try {
            // InternalXaaS.g:522:2: ( ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) | this_STRING_3= RULE_STRING ) )
            // InternalXaaS.g:523:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) | this_STRING_3= RULE_STRING )
            {
            // InternalXaaS.g:523:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) | this_STRING_3= RULE_STRING )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_ID) ) {
                alt12=1;
            }
            else if ( (LA12_0==RULE_STRING) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalXaaS.g:524:3: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
                    {
                    // InternalXaaS.g:524:3: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
                    // InternalXaaS.g:525:4: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
                    {
                    this_ID_0=(Token)match(input,RULE_ID,FOLLOW_13); 

                    				current.merge(this_ID_0);
                    			

                    				newLeafNode(this_ID_0, grammarAccess.getEStringAccess().getIDTerminalRuleCall_0_0());
                    			
                    // InternalXaaS.g:532:4: (kw= '.' this_ID_2= RULE_ID )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==18) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalXaaS.g:533:5: kw= '.' this_ID_2= RULE_ID
                    	    {
                    	    kw=(Token)match(input,18,FOLLOW_14); 

                    	    					current.merge(kw);
                    	    					newLeafNode(kw, grammarAccess.getEStringAccess().getFullStopKeyword_0_1_0());
                    	    				
                    	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_13); 

                    	    					current.merge(this_ID_2);
                    	    				

                    	    					newLeafNode(this_ID_2, grammarAccess.getEStringAccess().getIDTerminalRuleCall_0_1_1());
                    	    				

                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:548:3: this_STRING_3= RULE_STRING
                    {
                    this_STRING_3=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_3);
                    		

                    			newLeafNode(this_STRING_3, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEValue"
    // InternalXaaS.g:559:1: entryRuleEValue returns [String current=null] : iv_ruleEValue= ruleEValue EOF ;
    public final String entryRuleEValue() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEValue = null;


        try {
            // InternalXaaS.g:559:46: (iv_ruleEValue= ruleEValue EOF )
            // InternalXaaS.g:560:2: iv_ruleEValue= ruleEValue EOF
            {
             newCompositeNode(grammarAccess.getEValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEValue=ruleEValue();

            state._fsp--;

             current =iv_ruleEValue.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEValue"


    // $ANTLR start "ruleEValue"
    // InternalXaaS.g:566:1: ruleEValue returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_EInt_1= ruleEInt ) ;
    public final AntlrDatatypeRuleToken ruleEValue() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        AntlrDatatypeRuleToken this_EInt_1 = null;



        	enterRule();

        try {
            // InternalXaaS.g:572:2: ( (this_STRING_0= RULE_STRING | this_EInt_1= ruleEInt ) )
            // InternalXaaS.g:573:2: (this_STRING_0= RULE_STRING | this_EInt_1= ruleEInt )
            {
            // InternalXaaS.g:573:2: (this_STRING_0= RULE_STRING | this_EInt_1= ruleEInt )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_STRING) ) {
                alt13=1;
            }
            else if ( (LA13_0==RULE_INT||LA13_0==39) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalXaaS.g:574:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEValueAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalXaaS.g:582:3: this_EInt_1= ruleEInt
                    {

                    			newCompositeNode(grammarAccess.getEValueAccess().getEIntParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_EInt_1=ruleEInt();

                    state._fsp--;


                    			current.merge(this_EInt_1);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEValue"


    // $ANTLR start "entryRuleNode"
    // InternalXaaS.g:596:1: entryRuleNode returns [EObject current=null] : iv_ruleNode= ruleNode EOF ;
    public final EObject entryRuleNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNode = null;


        try {
            // InternalXaaS.g:596:45: (iv_ruleNode= ruleNode EOF )
            // InternalXaaS.g:597:2: iv_ruleNode= ruleNode EOF
            {
             newCompositeNode(grammarAccess.getNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNode=ruleNode();

            state._fsp--;

             current =iv_ruleNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNode"


    // $ANTLR start "ruleNode"
    // InternalXaaS.g:603:1: ruleNode returns [EObject current=null] : (otherlv_0= 'Node' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) otherlv_6= 'activated' otherlv_7= ':' ( (lv_activated_8_0= ruleEString ) ) (otherlv_9= 'properties' otherlv_10= ':' ( (lv_attributes_11_0= ruleAttribute ) )* )? ) ;
    public final EObject ruleNode() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_activated_8_0 = null;

        EObject lv_attributes_11_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:609:2: ( (otherlv_0= 'Node' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) otherlv_6= 'activated' otherlv_7= ':' ( (lv_activated_8_0= ruleEString ) ) (otherlv_9= 'properties' otherlv_10= ':' ( (lv_attributes_11_0= ruleAttribute ) )* )? ) )
            // InternalXaaS.g:610:2: (otherlv_0= 'Node' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) otherlv_6= 'activated' otherlv_7= ':' ( (lv_activated_8_0= ruleEString ) ) (otherlv_9= 'properties' otherlv_10= ':' ( (lv_attributes_11_0= ruleAttribute ) )* )? )
            {
            // InternalXaaS.g:610:2: (otherlv_0= 'Node' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) otherlv_6= 'activated' otherlv_7= ':' ( (lv_activated_8_0= ruleEString ) ) (otherlv_9= 'properties' otherlv_10= ':' ( (lv_attributes_11_0= ruleAttribute ) )* )? )
            // InternalXaaS.g:611:3: otherlv_0= 'Node' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) otherlv_6= 'activated' otherlv_7= ':' ( (lv_activated_8_0= ruleEString ) ) (otherlv_9= 'properties' otherlv_10= ':' ( (lv_attributes_11_0= ruleAttribute ) )* )?
            {
            otherlv_0=(Token)match(input,19,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getNodeAccess().getNodeKeyword_0());
            		
            // InternalXaaS.g:615:3: ( (lv_name_1_0= ruleEString ) )
            // InternalXaaS.g:616:4: (lv_name_1_0= ruleEString )
            {
            // InternalXaaS.g:616:4: (lv_name_1_0= ruleEString )
            // InternalXaaS.g:617:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getNodeAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNodeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getNodeAccess().getColonKeyword_2());
            		
            otherlv_3=(Token)match(input,20,FOLLOW_4); 

            			newLeafNode(otherlv_3, grammarAccess.getNodeAccess().getTypeKeyword_3());
            		
            otherlv_4=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_4, grammarAccess.getNodeAccess().getColonKeyword_4());
            		
            // InternalXaaS.g:646:3: ( ( ruleEString ) )
            // InternalXaaS.g:647:4: ( ruleEString )
            {
            // InternalXaaS.g:647:4: ( ruleEString )
            // InternalXaaS.g:648:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getNodeRule());
            					}
            				

            					newCompositeNode(grammarAccess.getNodeAccess().getTypeNodeTypeCrossReference_5_0());
            				
            pushFollow(FOLLOW_16);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,21,FOLLOW_4); 

            			newLeafNode(otherlv_6, grammarAccess.getNodeAccess().getActivatedKeyword_6());
            		
            otherlv_7=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_7, grammarAccess.getNodeAccess().getColonKeyword_7());
            		
            // InternalXaaS.g:670:3: ( (lv_activated_8_0= ruleEString ) )
            // InternalXaaS.g:671:4: (lv_activated_8_0= ruleEString )
            {
            // InternalXaaS.g:671:4: (lv_activated_8_0= ruleEString )
            // InternalXaaS.g:672:5: lv_activated_8_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getNodeAccess().getActivatedEStringParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_17);
            lv_activated_8_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNodeRule());
            					}
            					set(
            						current,
            						"activated",
            						lv_activated_8_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXaaS.g:689:3: (otherlv_9= 'properties' otherlv_10= ':' ( (lv_attributes_11_0= ruleAttribute ) )* )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==22) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalXaaS.g:690:4: otherlv_9= 'properties' otherlv_10= ':' ( (lv_attributes_11_0= ruleAttribute ) )*
                    {
                    otherlv_9=(Token)match(input,22,FOLLOW_4); 

                    				newLeafNode(otherlv_9, grammarAccess.getNodeAccess().getPropertiesKeyword_9_0());
                    			
                    otherlv_10=(Token)match(input,12,FOLLOW_12); 

                    				newLeafNode(otherlv_10, grammarAccess.getNodeAccess().getColonKeyword_9_1());
                    			
                    // InternalXaaS.g:698:4: ( (lv_attributes_11_0= ruleAttribute ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( ((LA14_0>=RULE_ID && LA14_0<=RULE_STRING)) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalXaaS.g:699:5: (lv_attributes_11_0= ruleAttribute )
                    	    {
                    	    // InternalXaaS.g:699:5: (lv_attributes_11_0= ruleAttribute )
                    	    // InternalXaaS.g:700:6: lv_attributes_11_0= ruleAttribute
                    	    {

                    	    						newCompositeNode(grammarAccess.getNodeAccess().getAttributesAttributeParserRuleCall_9_2_0());
                    	    					
                    	    pushFollow(FOLLOW_12);
                    	    lv_attributes_11_0=ruleAttribute();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getNodeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"attributes",
                    	    							lv_attributes_11_0,
                    	    							"fr.imta.come4acloud.yml.XaaS.Attribute");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNode"


    // $ANTLR start "entryRuleRelationship"
    // InternalXaaS.g:722:1: entryRuleRelationship returns [EObject current=null] : iv_ruleRelationship= ruleRelationship EOF ;
    public final EObject entryRuleRelationship() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationship = null;


        try {
            // InternalXaaS.g:722:53: (iv_ruleRelationship= ruleRelationship EOF )
            // InternalXaaS.g:723:2: iv_ruleRelationship= ruleRelationship EOF
            {
             newCompositeNode(grammarAccess.getRelationshipRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRelationship=ruleRelationship();

            state._fsp--;

             current =iv_ruleRelationship; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationship"


    // $ANTLR start "ruleRelationship"
    // InternalXaaS.g:729:1: ruleRelationship returns [EObject current=null] : (otherlv_0= 'Relationship' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) (otherlv_6= 'constant' otherlv_7= ':' ( (lv_constant_8_0= ruleEBoolean ) ) )? (otherlv_9= 'source' otherlv_10= ':' ( ( ruleEString ) ) )? (otherlv_12= 'target' otherlv_13= ':' ( ( ruleEString ) ) )? ) ;
    public final EObject ruleRelationship() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_constant_8_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:735:2: ( (otherlv_0= 'Relationship' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) (otherlv_6= 'constant' otherlv_7= ':' ( (lv_constant_8_0= ruleEBoolean ) ) )? (otherlv_9= 'source' otherlv_10= ':' ( ( ruleEString ) ) )? (otherlv_12= 'target' otherlv_13= ':' ( ( ruleEString ) ) )? ) )
            // InternalXaaS.g:736:2: (otherlv_0= 'Relationship' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) (otherlv_6= 'constant' otherlv_7= ':' ( (lv_constant_8_0= ruleEBoolean ) ) )? (otherlv_9= 'source' otherlv_10= ':' ( ( ruleEString ) ) )? (otherlv_12= 'target' otherlv_13= ':' ( ( ruleEString ) ) )? )
            {
            // InternalXaaS.g:736:2: (otherlv_0= 'Relationship' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) (otherlv_6= 'constant' otherlv_7= ':' ( (lv_constant_8_0= ruleEBoolean ) ) )? (otherlv_9= 'source' otherlv_10= ':' ( ( ruleEString ) ) )? (otherlv_12= 'target' otherlv_13= ':' ( ( ruleEString ) ) )? )
            // InternalXaaS.g:737:3: otherlv_0= 'Relationship' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( ( ruleEString ) ) (otherlv_6= 'constant' otherlv_7= ':' ( (lv_constant_8_0= ruleEBoolean ) ) )? (otherlv_9= 'source' otherlv_10= ':' ( ( ruleEString ) ) )? (otherlv_12= 'target' otherlv_13= ':' ( ( ruleEString ) ) )?
            {
            otherlv_0=(Token)match(input,23,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getRelationshipAccess().getRelationshipKeyword_0());
            		
            // InternalXaaS.g:741:3: ( (lv_name_1_0= ruleEString ) )
            // InternalXaaS.g:742:4: (lv_name_1_0= ruleEString )
            {
            // InternalXaaS.g:742:4: (lv_name_1_0= ruleEString )
            // InternalXaaS.g:743:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getRelationshipAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRelationshipRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getRelationshipAccess().getColonKeyword_2());
            		
            otherlv_3=(Token)match(input,20,FOLLOW_4); 

            			newLeafNode(otherlv_3, grammarAccess.getRelationshipAccess().getTypeKeyword_3());
            		
            otherlv_4=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_4, grammarAccess.getRelationshipAccess().getColonKeyword_4());
            		
            // InternalXaaS.g:772:3: ( ( ruleEString ) )
            // InternalXaaS.g:773:4: ( ruleEString )
            {
            // InternalXaaS.g:773:4: ( ruleEString )
            // InternalXaaS.g:774:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRelationshipRule());
            					}
            				

            					newCompositeNode(grammarAccess.getRelationshipAccess().getTypeRelationshipTypeCrossReference_5_0());
            				
            pushFollow(FOLLOW_18);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXaaS.g:788:3: (otherlv_6= 'constant' otherlv_7= ':' ( (lv_constant_8_0= ruleEBoolean ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==24) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalXaaS.g:789:4: otherlv_6= 'constant' otherlv_7= ':' ( (lv_constant_8_0= ruleEBoolean ) )
                    {
                    otherlv_6=(Token)match(input,24,FOLLOW_4); 

                    				newLeafNode(otherlv_6, grammarAccess.getRelationshipAccess().getConstantKeyword_6_0());
                    			
                    otherlv_7=(Token)match(input,12,FOLLOW_19); 

                    				newLeafNode(otherlv_7, grammarAccess.getRelationshipAccess().getColonKeyword_6_1());
                    			
                    // InternalXaaS.g:797:4: ( (lv_constant_8_0= ruleEBoolean ) )
                    // InternalXaaS.g:798:5: (lv_constant_8_0= ruleEBoolean )
                    {
                    // InternalXaaS.g:798:5: (lv_constant_8_0= ruleEBoolean )
                    // InternalXaaS.g:799:6: lv_constant_8_0= ruleEBoolean
                    {

                    						newCompositeNode(grammarAccess.getRelationshipAccess().getConstantEBooleanParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_20);
                    lv_constant_8_0=ruleEBoolean();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRelationshipRule());
                    						}
                    						set(
                    							current,
                    							"constant",
                    							lv_constant_8_0,
                    							"fr.imta.come4acloud.yml.XaaS.EBoolean");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalXaaS.g:817:3: (otherlv_9= 'source' otherlv_10= ':' ( ( ruleEString ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==25) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalXaaS.g:818:4: otherlv_9= 'source' otherlv_10= ':' ( ( ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,25,FOLLOW_4); 

                    				newLeafNode(otherlv_9, grammarAccess.getRelationshipAccess().getSourceKeyword_7_0());
                    			
                    otherlv_10=(Token)match(input,12,FOLLOW_6); 

                    				newLeafNode(otherlv_10, grammarAccess.getRelationshipAccess().getColonKeyword_7_1());
                    			
                    // InternalXaaS.g:826:4: ( ( ruleEString ) )
                    // InternalXaaS.g:827:5: ( ruleEString )
                    {
                    // InternalXaaS.g:827:5: ( ruleEString )
                    // InternalXaaS.g:828:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRelationshipRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getRelationshipAccess().getSourceNodeCrossReference_7_2_0());
                    					
                    pushFollow(FOLLOW_21);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalXaaS.g:843:3: (otherlv_12= 'target' otherlv_13= ':' ( ( ruleEString ) ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==26) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalXaaS.g:844:4: otherlv_12= 'target' otherlv_13= ':' ( ( ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,26,FOLLOW_4); 

                    				newLeafNode(otherlv_12, grammarAccess.getRelationshipAccess().getTargetKeyword_8_0());
                    			
                    otherlv_13=(Token)match(input,12,FOLLOW_6); 

                    				newLeafNode(otherlv_13, grammarAccess.getRelationshipAccess().getColonKeyword_8_1());
                    			
                    // InternalXaaS.g:852:4: ( ( ruleEString ) )
                    // InternalXaaS.g:853:5: ( ruleEString )
                    {
                    // InternalXaaS.g:853:5: ( ruleEString )
                    // InternalXaaS.g:854:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRelationshipRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getRelationshipAccess().getTargetNodeCrossReference_8_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationship"


    // $ANTLR start "entryRuleNodeType"
    // InternalXaaS.g:873:1: entryRuleNodeType returns [EObject current=null] : iv_ruleNodeType= ruleNodeType EOF ;
    public final EObject entryRuleNodeType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNodeType = null;


        try {
            // InternalXaaS.g:873:49: (iv_ruleNodeType= ruleNodeType EOF )
            // InternalXaaS.g:874:2: iv_ruleNodeType= ruleNodeType EOF
            {
             newCompositeNode(grammarAccess.getNodeTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNodeType=ruleNodeType();

            state._fsp--;

             current =iv_ruleNodeType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNodeType"


    // $ANTLR start "ruleNodeType"
    // InternalXaaS.g:880:1: ruleNodeType returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' (otherlv_3= 'derived_from' otherlv_4= ':' ( ( ruleEString ) ) )? (otherlv_6= 'properties' otherlv_7= ':' ( (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )? (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )? ( (lv_attributeTypes_14_0= ruleAttributeType ) ) (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )? )* )? ) ;
    public final EObject ruleNodeType() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_impactOfEnabling_10_0 = null;

        AntlrDatatypeRuleToken lv_impactOfDisabling_13_0 = null;

        EObject lv_attributeTypes_14_0 = null;

        EObject lv_constraints_17_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:886:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' (otherlv_3= 'derived_from' otherlv_4= ':' ( ( ruleEString ) ) )? (otherlv_6= 'properties' otherlv_7= ':' ( (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )? (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )? ( (lv_attributeTypes_14_0= ruleAttributeType ) ) (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )? )* )? ) )
            // InternalXaaS.g:887:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' (otherlv_3= 'derived_from' otherlv_4= ':' ( ( ruleEString ) ) )? (otherlv_6= 'properties' otherlv_7= ':' ( (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )? (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )? ( (lv_attributeTypes_14_0= ruleAttributeType ) ) (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )? )* )? )
            {
            // InternalXaaS.g:887:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' (otherlv_3= 'derived_from' otherlv_4= ':' ( ( ruleEString ) ) )? (otherlv_6= 'properties' otherlv_7= ':' ( (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )? (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )? ( (lv_attributeTypes_14_0= ruleAttributeType ) ) (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )? )* )? )
            // InternalXaaS.g:888:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' (otherlv_3= 'derived_from' otherlv_4= ':' ( ( ruleEString ) ) )? (otherlv_6= 'properties' otherlv_7= ':' ( (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )? (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )? ( (lv_attributeTypes_14_0= ruleAttributeType ) ) (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )? )* )?
            {
            // InternalXaaS.g:888:3: ()
            // InternalXaaS.g:889:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getNodeTypeAccess().getNodeTypeAction_0(),
            					current);
            			

            }

            // InternalXaaS.g:895:3: ( (lv_name_1_0= ruleEString ) )
            // InternalXaaS.g:896:4: (lv_name_1_0= ruleEString )
            {
            // InternalXaaS.g:896:4: (lv_name_1_0= ruleEString )
            // InternalXaaS.g:897:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getNodeTypeAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNodeTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_22); 

            			newLeafNode(otherlv_2, grammarAccess.getNodeTypeAccess().getColonKeyword_2());
            		
            // InternalXaaS.g:918:3: (otherlv_3= 'derived_from' otherlv_4= ':' ( ( ruleEString ) ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==27) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalXaaS.g:919:4: otherlv_3= 'derived_from' otherlv_4= ':' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_4); 

                    				newLeafNode(otherlv_3, grammarAccess.getNodeTypeAccess().getDerived_fromKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,12,FOLLOW_6); 

                    				newLeafNode(otherlv_4, grammarAccess.getNodeTypeAccess().getColonKeyword_3_1());
                    			
                    // InternalXaaS.g:927:4: ( ( ruleEString ) )
                    // InternalXaaS.g:928:5: ( ruleEString )
                    {
                    // InternalXaaS.g:928:5: ( ruleEString )
                    // InternalXaaS.g:929:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getNodeTypeRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getNodeTypeAccess().getInheritedTypeNodeTypeCrossReference_3_2_0());
                    					
                    pushFollow(FOLLOW_17);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalXaaS.g:944:3: (otherlv_6= 'properties' otherlv_7= ':' ( (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )? (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )? ( (lv_attributeTypes_14_0= ruleAttributeType ) ) (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )? )* )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==22) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalXaaS.g:945:4: otherlv_6= 'properties' otherlv_7= ':' ( (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )? (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )? ( (lv_attributeTypes_14_0= ruleAttributeType ) ) (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )? )*
                    {
                    otherlv_6=(Token)match(input,22,FOLLOW_4); 

                    				newLeafNode(otherlv_6, grammarAccess.getNodeTypeAccess().getPropertiesKeyword_4_0());
                    			
                    otherlv_7=(Token)match(input,12,FOLLOW_23); 

                    				newLeafNode(otherlv_7, grammarAccess.getNodeTypeAccess().getColonKeyword_4_1());
                    			
                    // InternalXaaS.g:953:4: ( (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )? (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )? ( (lv_attributeTypes_14_0= ruleAttributeType ) ) (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )? )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( (LA24_0==24||(LA24_0>=28 && LA24_0<=29)||LA24_0==31) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // InternalXaaS.g:954:5: (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )? (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )? ( (lv_attributeTypes_14_0= ruleAttributeType ) ) (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )?
                    	    {
                    	    // InternalXaaS.g:954:5: (otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) ) )?
                    	    int alt20=2;
                    	    int LA20_0 = input.LA(1);

                    	    if ( (LA20_0==28) ) {
                    	        alt20=1;
                    	    }
                    	    switch (alt20) {
                    	        case 1 :
                    	            // InternalXaaS.g:955:6: otherlv_8= 'impactOfEnabling' otherlv_9= ':' ( (lv_impactOfEnabling_10_0= ruleEInt ) )
                    	            {
                    	            otherlv_8=(Token)match(input,28,FOLLOW_4); 

                    	            						newLeafNode(otherlv_8, grammarAccess.getNodeTypeAccess().getImpactOfEnablingKeyword_4_2_0_0());
                    	            					
                    	            otherlv_9=(Token)match(input,12,FOLLOW_24); 

                    	            						newLeafNode(otherlv_9, grammarAccess.getNodeTypeAccess().getColonKeyword_4_2_0_1());
                    	            					
                    	            // InternalXaaS.g:963:6: ( (lv_impactOfEnabling_10_0= ruleEInt ) )
                    	            // InternalXaaS.g:964:7: (lv_impactOfEnabling_10_0= ruleEInt )
                    	            {
                    	            // InternalXaaS.g:964:7: (lv_impactOfEnabling_10_0= ruleEInt )
                    	            // InternalXaaS.g:965:8: lv_impactOfEnabling_10_0= ruleEInt
                    	            {

                    	            								newCompositeNode(grammarAccess.getNodeTypeAccess().getImpactOfEnablingEIntParserRuleCall_4_2_0_2_0());
                    	            							
                    	            pushFollow(FOLLOW_25);
                    	            lv_impactOfEnabling_10_0=ruleEInt();

                    	            state._fsp--;


                    	            								if (current==null) {
                    	            									current = createModelElementForParent(grammarAccess.getNodeTypeRule());
                    	            								}
                    	            								set(
                    	            									current,
                    	            									"impactOfEnabling",
                    	            									lv_impactOfEnabling_10_0,
                    	            									"fr.imta.come4acloud.yml.XaaS.EInt");
                    	            								afterParserOrEnumRuleCall();
                    	            							

                    	            }


                    	            }


                    	            }
                    	            break;

                    	    }

                    	    // InternalXaaS.g:983:5: (otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) ) )?
                    	    int alt21=2;
                    	    int LA21_0 = input.LA(1);

                    	    if ( (LA21_0==29) ) {
                    	        alt21=1;
                    	    }
                    	    switch (alt21) {
                    	        case 1 :
                    	            // InternalXaaS.g:984:6: otherlv_11= 'impactOfDisabling' otherlv_12= ':' ( (lv_impactOfDisabling_13_0= ruleEInt ) )
                    	            {
                    	            otherlv_11=(Token)match(input,29,FOLLOW_4); 

                    	            						newLeafNode(otherlv_11, grammarAccess.getNodeTypeAccess().getImpactOfDisablingKeyword_4_2_1_0());
                    	            					
                    	            otherlv_12=(Token)match(input,12,FOLLOW_24); 

                    	            						newLeafNode(otherlv_12, grammarAccess.getNodeTypeAccess().getColonKeyword_4_2_1_1());
                    	            					
                    	            // InternalXaaS.g:992:6: ( (lv_impactOfDisabling_13_0= ruleEInt ) )
                    	            // InternalXaaS.g:993:7: (lv_impactOfDisabling_13_0= ruleEInt )
                    	            {
                    	            // InternalXaaS.g:993:7: (lv_impactOfDisabling_13_0= ruleEInt )
                    	            // InternalXaaS.g:994:8: lv_impactOfDisabling_13_0= ruleEInt
                    	            {

                    	            								newCompositeNode(grammarAccess.getNodeTypeAccess().getImpactOfDisablingEIntParserRuleCall_4_2_1_2_0());
                    	            							
                    	            pushFollow(FOLLOW_25);
                    	            lv_impactOfDisabling_13_0=ruleEInt();

                    	            state._fsp--;


                    	            								if (current==null) {
                    	            									current = createModelElementForParent(grammarAccess.getNodeTypeRule());
                    	            								}
                    	            								set(
                    	            									current,
                    	            									"impactOfDisabling",
                    	            									lv_impactOfDisabling_13_0,
                    	            									"fr.imta.come4acloud.yml.XaaS.EInt");
                    	            								afterParserOrEnumRuleCall();
                    	            							

                    	            }


                    	            }


                    	            }
                    	            break;

                    	    }

                    	    // InternalXaaS.g:1012:5: ( (lv_attributeTypes_14_0= ruleAttributeType ) )
                    	    // InternalXaaS.g:1013:6: (lv_attributeTypes_14_0= ruleAttributeType )
                    	    {
                    	    // InternalXaaS.g:1013:6: (lv_attributeTypes_14_0= ruleAttributeType )
                    	    // InternalXaaS.g:1014:7: lv_attributeTypes_14_0= ruleAttributeType
                    	    {

                    	    							newCompositeNode(grammarAccess.getNodeTypeAccess().getAttributeTypesAttributeTypeParserRuleCall_4_2_2_0());
                    	    						
                    	    pushFollow(FOLLOW_26);
                    	    lv_attributeTypes_14_0=ruleAttributeType();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getNodeTypeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"attributeTypes",
                    	    								lv_attributeTypes_14_0,
                    	    								"fr.imta.come4acloud.yml.XaaS.AttributeType");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }

                    	    // InternalXaaS.g:1031:5: (otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+ )?
                    	    int alt23=2;
                    	    int LA23_0 = input.LA(1);

                    	    if ( (LA23_0==30) ) {
                    	        alt23=1;
                    	    }
                    	    switch (alt23) {
                    	        case 1 :
                    	            // InternalXaaS.g:1032:6: otherlv_15= 'constraints' otherlv_16= ':' ( (lv_constraints_17_0= ruleConstraint ) )+
                    	            {
                    	            otherlv_15=(Token)match(input,30,FOLLOW_4); 

                    	            						newLeafNode(otherlv_15, grammarAccess.getNodeTypeAccess().getConstraintsKeyword_4_2_3_0());
                    	            					
                    	            otherlv_16=(Token)match(input,12,FOLLOW_6); 

                    	            						newLeafNode(otherlv_16, grammarAccess.getNodeTypeAccess().getColonKeyword_4_2_3_1());
                    	            					
                    	            // InternalXaaS.g:1040:6: ( (lv_constraints_17_0= ruleConstraint ) )+
                    	            int cnt22=0;
                    	            loop22:
                    	            do {
                    	                int alt22=2;
                    	                alt22 = dfa22.predict(input);
                    	                switch (alt22) {
                    	            	case 1 :
                    	            	    // InternalXaaS.g:1041:7: (lv_constraints_17_0= ruleConstraint )
                    	            	    {
                    	            	    // InternalXaaS.g:1041:7: (lv_constraints_17_0= ruleConstraint )
                    	            	    // InternalXaaS.g:1042:8: lv_constraints_17_0= ruleConstraint
                    	            	    {

                    	            	    								newCompositeNode(grammarAccess.getNodeTypeAccess().getConstraintsConstraintParserRuleCall_4_2_3_2_0());
                    	            	    							
                    	            	    pushFollow(FOLLOW_27);
                    	            	    lv_constraints_17_0=ruleConstraint();

                    	            	    state._fsp--;


                    	            	    								if (current==null) {
                    	            	    									current = createModelElementForParent(grammarAccess.getNodeTypeRule());
                    	            	    								}
                    	            	    								add(
                    	            	    									current,
                    	            	    									"constraints",
                    	            	    									lv_constraints_17_0,
                    	            	    									"fr.imta.come4acloud.yml.XaaS.Constraint");
                    	            	    								afterParserOrEnumRuleCall();
                    	            	    							

                    	            	    }


                    	            	    }
                    	            	    break;

                    	            	default :
                    	            	    if ( cnt22 >= 1 ) break loop22;
                    	                        EarlyExitException eee =
                    	                            new EarlyExitException(22, input);
                    	                        throw eee;
                    	                }
                    	                cnt22++;
                    	            } while (true);


                    	            }
                    	            break;

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNodeType"


    // $ANTLR start "entryRuleAttribute"
    // InternalXaaS.g:1066:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalXaaS.g:1066:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalXaaS.g:1067:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalXaaS.g:1073:1: ruleAttribute returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( (lv_value_2_0= ruleEValue ) ) ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        AntlrDatatypeRuleToken lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1079:2: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( (lv_value_2_0= ruleEValue ) ) ) )
            // InternalXaaS.g:1080:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( (lv_value_2_0= ruleEValue ) ) )
            {
            // InternalXaaS.g:1080:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( (lv_value_2_0= ruleEValue ) ) )
            // InternalXaaS.g:1081:3: ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( (lv_value_2_0= ruleEValue ) )
            {
            // InternalXaaS.g:1081:3: ( (lv_name_0_0= ruleEString ) )
            // InternalXaaS.g:1082:4: (lv_name_0_0= ruleEString )
            {
            // InternalXaaS.g:1082:4: (lv_name_0_0= ruleEString )
            // InternalXaaS.g:1083:5: lv_name_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAttributeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,12,FOLLOW_28); 

            			newLeafNode(otherlv_1, grammarAccess.getAttributeAccess().getColonKeyword_1());
            		
            // InternalXaaS.g:1104:3: ( (lv_value_2_0= ruleEValue ) )
            // InternalXaaS.g:1105:4: (lv_value_2_0= ruleEValue )
            {
            // InternalXaaS.g:1105:4: (lv_value_2_0= ruleEValue )
            // InternalXaaS.g:1106:5: lv_value_2_0= ruleEValue
            {

            					newCompositeNode(grammarAccess.getAttributeAccess().getValueEValueParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_2_0=ruleEValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAttributeRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_2_0,
            						"fr.imta.come4acloud.yml.XaaS.EValue");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleConstraint"
    // InternalXaaS.g:1127:1: entryRuleConstraint returns [EObject current=null] : iv_ruleConstraint= ruleConstraint EOF ;
    public final EObject entryRuleConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraint = null;


        try {
            // InternalXaaS.g:1127:51: (iv_ruleConstraint= ruleConstraint EOF )
            // InternalXaaS.g:1128:2: iv_ruleConstraint= ruleConstraint EOF
            {
             newCompositeNode(grammarAccess.getConstraintRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstraint=ruleConstraint();

            state._fsp--;

             current =iv_ruleConstraint; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // InternalXaaS.g:1134:1: ruleConstraint returns [EObject current=null] : ( ( (lv_expressions_0_0= ruleAttributeExpression ) ) ( (lv_operator_1_0= ruleComparisonOperator ) ) otherlv_2= ':' ( (lv_expressions_3_0= ruleExpression ) ) ) ;
    public final EObject ruleConstraint() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_expressions_0_0 = null;

        Enumerator lv_operator_1_0 = null;

        EObject lv_expressions_3_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1140:2: ( ( ( (lv_expressions_0_0= ruleAttributeExpression ) ) ( (lv_operator_1_0= ruleComparisonOperator ) ) otherlv_2= ':' ( (lv_expressions_3_0= ruleExpression ) ) ) )
            // InternalXaaS.g:1141:2: ( ( (lv_expressions_0_0= ruleAttributeExpression ) ) ( (lv_operator_1_0= ruleComparisonOperator ) ) otherlv_2= ':' ( (lv_expressions_3_0= ruleExpression ) ) )
            {
            // InternalXaaS.g:1141:2: ( ( (lv_expressions_0_0= ruleAttributeExpression ) ) ( (lv_operator_1_0= ruleComparisonOperator ) ) otherlv_2= ':' ( (lv_expressions_3_0= ruleExpression ) ) )
            // InternalXaaS.g:1142:3: ( (lv_expressions_0_0= ruleAttributeExpression ) ) ( (lv_operator_1_0= ruleComparisonOperator ) ) otherlv_2= ':' ( (lv_expressions_3_0= ruleExpression ) )
            {
            // InternalXaaS.g:1142:3: ( (lv_expressions_0_0= ruleAttributeExpression ) )
            // InternalXaaS.g:1143:4: (lv_expressions_0_0= ruleAttributeExpression )
            {
            // InternalXaaS.g:1143:4: (lv_expressions_0_0= ruleAttributeExpression )
            // InternalXaaS.g:1144:5: lv_expressions_0_0= ruleAttributeExpression
            {

            					newCompositeNode(grammarAccess.getConstraintAccess().getExpressionsAttributeExpressionParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_29);
            lv_expressions_0_0=ruleAttributeExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConstraintRule());
            					}
            					add(
            						current,
            						"expressions",
            						lv_expressions_0_0,
            						"fr.imta.come4acloud.yml.XaaS.AttributeExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXaaS.g:1161:3: ( (lv_operator_1_0= ruleComparisonOperator ) )
            // InternalXaaS.g:1162:4: (lv_operator_1_0= ruleComparisonOperator )
            {
            // InternalXaaS.g:1162:4: (lv_operator_1_0= ruleComparisonOperator )
            // InternalXaaS.g:1163:5: lv_operator_1_0= ruleComparisonOperator
            {

            					newCompositeNode(grammarAccess.getConstraintAccess().getOperatorComparisonOperatorEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_operator_1_0=ruleComparisonOperator();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConstraintRule());
            					}
            					set(
            						current,
            						"operator",
            						lv_operator_1_0,
            						"fr.imta.come4acloud.yml.XaaS.ComparisonOperator");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_30); 

            			newLeafNode(otherlv_2, grammarAccess.getConstraintAccess().getColonKeyword_2());
            		
            // InternalXaaS.g:1184:3: ( (lv_expressions_3_0= ruleExpression ) )
            // InternalXaaS.g:1185:4: (lv_expressions_3_0= ruleExpression )
            {
            // InternalXaaS.g:1185:4: (lv_expressions_3_0= ruleExpression )
            // InternalXaaS.g:1186:5: lv_expressions_3_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getConstraintAccess().getExpressionsExpressionParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_expressions_3_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConstraintRule());
            					}
            					add(
            						current,
            						"expressions",
            						lv_expressions_3_0,
            						"fr.imta.come4acloud.yml.XaaS.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleConstantAttributeType"
    // InternalXaaS.g:1207:1: entryRuleConstantAttributeType returns [EObject current=null] : iv_ruleConstantAttributeType= ruleConstantAttributeType EOF ;
    public final EObject entryRuleConstantAttributeType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstantAttributeType = null;


        try {
            // InternalXaaS.g:1207:62: (iv_ruleConstantAttributeType= ruleConstantAttributeType EOF )
            // InternalXaaS.g:1208:2: iv_ruleConstantAttributeType= ruleConstantAttributeType EOF
            {
             newCompositeNode(grammarAccess.getConstantAttributeTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstantAttributeType=ruleConstantAttributeType();

            state._fsp--;

             current =iv_ruleConstantAttributeType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstantAttributeType"


    // $ANTLR start "ruleConstantAttributeType"
    // InternalXaaS.g:1214:1: ruleConstantAttributeType returns [EObject current=null] : (otherlv_0= 'constant' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) ) ;
    public final EObject ruleConstantAttributeType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_type_5_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1220:2: ( (otherlv_0= 'constant' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) ) )
            // InternalXaaS.g:1221:2: (otherlv_0= 'constant' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) )
            {
            // InternalXaaS.g:1221:2: (otherlv_0= 'constant' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) )
            // InternalXaaS.g:1222:3: otherlv_0= 'constant' ( (lv_name_1_0= ruleEString ) ) otherlv_2= ':' otherlv_3= 'type' otherlv_4= ':' ( (lv_type_5_0= ruleEString ) )
            {
            otherlv_0=(Token)match(input,24,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getConstantAttributeTypeAccess().getConstantKeyword_0());
            		
            // InternalXaaS.g:1226:3: ( (lv_name_1_0= ruleEString ) )
            // InternalXaaS.g:1227:4: (lv_name_1_0= ruleEString )
            {
            // InternalXaaS.g:1227:4: (lv_name_1_0= ruleEString )
            // InternalXaaS.g:1228:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getConstantAttributeTypeAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConstantAttributeTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getConstantAttributeTypeAccess().getColonKeyword_2());
            		
            otherlv_3=(Token)match(input,20,FOLLOW_4); 

            			newLeafNode(otherlv_3, grammarAccess.getConstantAttributeTypeAccess().getTypeKeyword_3());
            		
            otherlv_4=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_4, grammarAccess.getConstantAttributeTypeAccess().getColonKeyword_4());
            		
            // InternalXaaS.g:1257:3: ( (lv_type_5_0= ruleEString ) )
            // InternalXaaS.g:1258:4: (lv_type_5_0= ruleEString )
            {
            // InternalXaaS.g:1258:4: (lv_type_5_0= ruleEString )
            // InternalXaaS.g:1259:5: lv_type_5_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getConstantAttributeTypeAccess().getTypeEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_2);
            lv_type_5_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConstantAttributeTypeRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_5_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstantAttributeType"


    // $ANTLR start "entryRuleCalculatedAttributeType"
    // InternalXaaS.g:1280:1: entryRuleCalculatedAttributeType returns [EObject current=null] : iv_ruleCalculatedAttributeType= ruleCalculatedAttributeType EOF ;
    public final EObject entryRuleCalculatedAttributeType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCalculatedAttributeType = null;


        try {
            // InternalXaaS.g:1280:64: (iv_ruleCalculatedAttributeType= ruleCalculatedAttributeType EOF )
            // InternalXaaS.g:1281:2: iv_ruleCalculatedAttributeType= ruleCalculatedAttributeType EOF
            {
             newCompositeNode(grammarAccess.getCalculatedAttributeTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCalculatedAttributeType=ruleCalculatedAttributeType();

            state._fsp--;

             current =iv_ruleCalculatedAttributeType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCalculatedAttributeType"


    // $ANTLR start "ruleCalculatedAttributeType"
    // InternalXaaS.g:1287:1: ruleCalculatedAttributeType returns [EObject current=null] : ( () otherlv_1= 'variable' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' otherlv_4= 'type' otherlv_5= ':' ( (lv_type_6_0= ruleEString ) ) otherlv_7= 'equal' otherlv_8= ':' ( (lv_expression_9_0= ruleExpression ) ) (otherlv_10= 'impactOfUpdating' otherlv_11= ':' ( (lv_impactOfUpdating_12_0= ruleEInt ) ) )? ) ;
    public final EObject ruleCalculatedAttributeType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_type_6_0 = null;

        EObject lv_expression_9_0 = null;

        AntlrDatatypeRuleToken lv_impactOfUpdating_12_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1293:2: ( ( () otherlv_1= 'variable' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' otherlv_4= 'type' otherlv_5= ':' ( (lv_type_6_0= ruleEString ) ) otherlv_7= 'equal' otherlv_8= ':' ( (lv_expression_9_0= ruleExpression ) ) (otherlv_10= 'impactOfUpdating' otherlv_11= ':' ( (lv_impactOfUpdating_12_0= ruleEInt ) ) )? ) )
            // InternalXaaS.g:1294:2: ( () otherlv_1= 'variable' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' otherlv_4= 'type' otherlv_5= ':' ( (lv_type_6_0= ruleEString ) ) otherlv_7= 'equal' otherlv_8= ':' ( (lv_expression_9_0= ruleExpression ) ) (otherlv_10= 'impactOfUpdating' otherlv_11= ':' ( (lv_impactOfUpdating_12_0= ruleEInt ) ) )? )
            {
            // InternalXaaS.g:1294:2: ( () otherlv_1= 'variable' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' otherlv_4= 'type' otherlv_5= ':' ( (lv_type_6_0= ruleEString ) ) otherlv_7= 'equal' otherlv_8= ':' ( (lv_expression_9_0= ruleExpression ) ) (otherlv_10= 'impactOfUpdating' otherlv_11= ':' ( (lv_impactOfUpdating_12_0= ruleEInt ) ) )? )
            // InternalXaaS.g:1295:3: () otherlv_1= 'variable' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' otherlv_4= 'type' otherlv_5= ':' ( (lv_type_6_0= ruleEString ) ) otherlv_7= 'equal' otherlv_8= ':' ( (lv_expression_9_0= ruleExpression ) ) (otherlv_10= 'impactOfUpdating' otherlv_11= ':' ( (lv_impactOfUpdating_12_0= ruleEInt ) ) )?
            {
            // InternalXaaS.g:1295:3: ()
            // InternalXaaS.g:1296:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCalculatedAttributeTypeAccess().getCalculatedAttributeTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,31,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getCalculatedAttributeTypeAccess().getVariableKeyword_1());
            		
            // InternalXaaS.g:1306:3: ( (lv_name_2_0= ruleEString ) )
            // InternalXaaS.g:1307:4: (lv_name_2_0= ruleEString )
            {
            // InternalXaaS.g:1307:4: (lv_name_2_0= ruleEString )
            // InternalXaaS.g:1308:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCalculatedAttributeTypeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCalculatedAttributeTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_15); 

            			newLeafNode(otherlv_3, grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_3());
            		
            otherlv_4=(Token)match(input,20,FOLLOW_4); 

            			newLeafNode(otherlv_4, grammarAccess.getCalculatedAttributeTypeAccess().getTypeKeyword_4());
            		
            otherlv_5=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_5, grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_5());
            		
            // InternalXaaS.g:1337:3: ( (lv_type_6_0= ruleEString ) )
            // InternalXaaS.g:1338:4: (lv_type_6_0= ruleEString )
            {
            // InternalXaaS.g:1338:4: (lv_type_6_0= ruleEString )
            // InternalXaaS.g:1339:5: lv_type_6_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCalculatedAttributeTypeAccess().getTypeEStringParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_31);
            lv_type_6_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCalculatedAttributeTypeRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_6_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,32,FOLLOW_4); 

            			newLeafNode(otherlv_7, grammarAccess.getCalculatedAttributeTypeAccess().getEqualKeyword_7());
            		
            otherlv_8=(Token)match(input,12,FOLLOW_30); 

            			newLeafNode(otherlv_8, grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_8());
            		
            // InternalXaaS.g:1364:3: ( (lv_expression_9_0= ruleExpression ) )
            // InternalXaaS.g:1365:4: (lv_expression_9_0= ruleExpression )
            {
            // InternalXaaS.g:1365:4: (lv_expression_9_0= ruleExpression )
            // InternalXaaS.g:1366:5: lv_expression_9_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getCalculatedAttributeTypeAccess().getExpressionExpressionParserRuleCall_9_0());
            				
            pushFollow(FOLLOW_32);
            lv_expression_9_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCalculatedAttributeTypeRule());
            					}
            					set(
            						current,
            						"expression",
            						lv_expression_9_0,
            						"fr.imta.come4acloud.yml.XaaS.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXaaS.g:1383:3: (otherlv_10= 'impactOfUpdating' otherlv_11= ':' ( (lv_impactOfUpdating_12_0= ruleEInt ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==33) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalXaaS.g:1384:4: otherlv_10= 'impactOfUpdating' otherlv_11= ':' ( (lv_impactOfUpdating_12_0= ruleEInt ) )
                    {
                    otherlv_10=(Token)match(input,33,FOLLOW_4); 

                    				newLeafNode(otherlv_10, grammarAccess.getCalculatedAttributeTypeAccess().getImpactOfUpdatingKeyword_10_0());
                    			
                    otherlv_11=(Token)match(input,12,FOLLOW_24); 

                    				newLeafNode(otherlv_11, grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_10_1());
                    			
                    // InternalXaaS.g:1392:4: ( (lv_impactOfUpdating_12_0= ruleEInt ) )
                    // InternalXaaS.g:1393:5: (lv_impactOfUpdating_12_0= ruleEInt )
                    {
                    // InternalXaaS.g:1393:5: (lv_impactOfUpdating_12_0= ruleEInt )
                    // InternalXaaS.g:1394:6: lv_impactOfUpdating_12_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getCalculatedAttributeTypeAccess().getImpactOfUpdatingEIntParserRuleCall_10_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_impactOfUpdating_12_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCalculatedAttributeTypeRule());
                    						}
                    						set(
                    							current,
                    							"impactOfUpdating",
                    							lv_impactOfUpdating_12_0,
                    							"fr.imta.come4acloud.yml.XaaS.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCalculatedAttributeType"


    // $ANTLR start "entryRuleIntegerValueExpression"
    // InternalXaaS.g:1416:1: entryRuleIntegerValueExpression returns [EObject current=null] : iv_ruleIntegerValueExpression= ruleIntegerValueExpression EOF ;
    public final EObject entryRuleIntegerValueExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValueExpression = null;


        try {
            // InternalXaaS.g:1416:63: (iv_ruleIntegerValueExpression= ruleIntegerValueExpression EOF )
            // InternalXaaS.g:1417:2: iv_ruleIntegerValueExpression= ruleIntegerValueExpression EOF
            {
             newCompositeNode(grammarAccess.getIntegerValueExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntegerValueExpression=ruleIntegerValueExpression();

            state._fsp--;

             current =iv_ruleIntegerValueExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValueExpression"


    // $ANTLR start "ruleIntegerValueExpression"
    // InternalXaaS.g:1423:1: ruleIntegerValueExpression returns [EObject current=null] : ( (lv_value_0_0= ruleEInt ) ) ;
    public final EObject ruleIntegerValueExpression() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1429:2: ( ( (lv_value_0_0= ruleEInt ) ) )
            // InternalXaaS.g:1430:2: ( (lv_value_0_0= ruleEInt ) )
            {
            // InternalXaaS.g:1430:2: ( (lv_value_0_0= ruleEInt ) )
            // InternalXaaS.g:1431:3: (lv_value_0_0= ruleEInt )
            {
            // InternalXaaS.g:1431:3: (lv_value_0_0= ruleEInt )
            // InternalXaaS.g:1432:4: lv_value_0_0= ruleEInt
            {

            				newCompositeNode(grammarAccess.getIntegerValueExpressionAccess().getValueEIntParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_value_0_0=ruleEInt();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getIntegerValueExpressionRule());
            				}
            				set(
            					current,
            					"value",
            					lv_value_0_0,
            					"fr.imta.come4acloud.yml.XaaS.EInt");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValueExpression"


    // $ANTLR start "entryRuleAttributeExpression"
    // InternalXaaS.g:1452:1: entryRuleAttributeExpression returns [EObject current=null] : iv_ruleAttributeExpression= ruleAttributeExpression EOF ;
    public final EObject entryRuleAttributeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeExpression = null;


        try {
            // InternalXaaS.g:1452:60: (iv_ruleAttributeExpression= ruleAttributeExpression EOF )
            // InternalXaaS.g:1453:2: iv_ruleAttributeExpression= ruleAttributeExpression EOF
            {
             newCompositeNode(grammarAccess.getAttributeExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttributeExpression=ruleAttributeExpression();

            state._fsp--;

             current =iv_ruleAttributeExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeExpression"


    // $ANTLR start "ruleAttributeExpression"
    // InternalXaaS.g:1459:1: ruleAttributeExpression returns [EObject current=null] : ( ( ruleEString ) ) ;
    public final EObject ruleAttributeExpression() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalXaaS.g:1465:2: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:1466:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:1466:2: ( ( ruleEString ) )
            // InternalXaaS.g:1467:3: ( ruleEString )
            {
            // InternalXaaS.g:1467:3: ( ruleEString )
            // InternalXaaS.g:1468:4: ruleEString
            {

            				if (current==null) {
            					current = createModelElement(grammarAccess.getAttributeExpressionRule());
            				}
            			

            				newCompositeNode(grammarAccess.getAttributeExpressionAccess().getAttributeTypeAttributeTypeCrossReference_0());
            			
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;


            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeExpression"


    // $ANTLR start "entryRuleBinaryExpression"
    // InternalXaaS.g:1485:1: entryRuleBinaryExpression returns [EObject current=null] : iv_ruleBinaryExpression= ruleBinaryExpression EOF ;
    public final EObject entryRuleBinaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinaryExpression = null;


        try {
            // InternalXaaS.g:1485:57: (iv_ruleBinaryExpression= ruleBinaryExpression EOF )
            // InternalXaaS.g:1486:2: iv_ruleBinaryExpression= ruleBinaryExpression EOF
            {
             newCompositeNode(grammarAccess.getBinaryExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBinaryExpression=ruleBinaryExpression();

            state._fsp--;

             current =iv_ruleBinaryExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryExpression"


    // $ANTLR start "ruleBinaryExpression"
    // InternalXaaS.g:1492:1: ruleBinaryExpression returns [EObject current=null] : ( ( (lv_expressions_0_0= ruleSingelExpression ) ) ( (lv_operator_1_0= ruleAlgebraicOperator ) ) ( (lv_expressions_2_0= ruleExpression ) ) ) ;
    public final EObject ruleBinaryExpression() throws RecognitionException {
        EObject current = null;

        EObject lv_expressions_0_0 = null;

        Enumerator lv_operator_1_0 = null;

        EObject lv_expressions_2_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1498:2: ( ( ( (lv_expressions_0_0= ruleSingelExpression ) ) ( (lv_operator_1_0= ruleAlgebraicOperator ) ) ( (lv_expressions_2_0= ruleExpression ) ) ) )
            // InternalXaaS.g:1499:2: ( ( (lv_expressions_0_0= ruleSingelExpression ) ) ( (lv_operator_1_0= ruleAlgebraicOperator ) ) ( (lv_expressions_2_0= ruleExpression ) ) )
            {
            // InternalXaaS.g:1499:2: ( ( (lv_expressions_0_0= ruleSingelExpression ) ) ( (lv_operator_1_0= ruleAlgebraicOperator ) ) ( (lv_expressions_2_0= ruleExpression ) ) )
            // InternalXaaS.g:1500:3: ( (lv_expressions_0_0= ruleSingelExpression ) ) ( (lv_operator_1_0= ruleAlgebraicOperator ) ) ( (lv_expressions_2_0= ruleExpression ) )
            {
            // InternalXaaS.g:1500:3: ( (lv_expressions_0_0= ruleSingelExpression ) )
            // InternalXaaS.g:1501:4: (lv_expressions_0_0= ruleSingelExpression )
            {
            // InternalXaaS.g:1501:4: (lv_expressions_0_0= ruleSingelExpression )
            // InternalXaaS.g:1502:5: lv_expressions_0_0= ruleSingelExpression
            {

            					newCompositeNode(grammarAccess.getBinaryExpressionAccess().getExpressionsSingelExpressionParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_33);
            lv_expressions_0_0=ruleSingelExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBinaryExpressionRule());
            					}
            					add(
            						current,
            						"expressions",
            						lv_expressions_0_0,
            						"fr.imta.come4acloud.yml.XaaS.SingelExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXaaS.g:1519:3: ( (lv_operator_1_0= ruleAlgebraicOperator ) )
            // InternalXaaS.g:1520:4: (lv_operator_1_0= ruleAlgebraicOperator )
            {
            // InternalXaaS.g:1520:4: (lv_operator_1_0= ruleAlgebraicOperator )
            // InternalXaaS.g:1521:5: lv_operator_1_0= ruleAlgebraicOperator
            {

            					newCompositeNode(grammarAccess.getBinaryExpressionAccess().getOperatorAlgebraicOperatorEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_30);
            lv_operator_1_0=ruleAlgebraicOperator();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBinaryExpressionRule());
            					}
            					set(
            						current,
            						"operator",
            						lv_operator_1_0,
            						"fr.imta.come4acloud.yml.XaaS.AlgebraicOperator");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXaaS.g:1538:3: ( (lv_expressions_2_0= ruleExpression ) )
            // InternalXaaS.g:1539:4: (lv_expressions_2_0= ruleExpression )
            {
            // InternalXaaS.g:1539:4: (lv_expressions_2_0= ruleExpression )
            // InternalXaaS.g:1540:5: lv_expressions_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getBinaryExpressionAccess().getExpressionsExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_expressions_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBinaryExpressionRule());
            					}
            					add(
            						current,
            						"expressions",
            						lv_expressions_2_0,
            						"fr.imta.come4acloud.yml.XaaS.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryExpression"


    // $ANTLR start "entryRuleAggregationExpression"
    // InternalXaaS.g:1561:1: entryRuleAggregationExpression returns [EObject current=null] : iv_ruleAggregationExpression= ruleAggregationExpression EOF ;
    public final EObject entryRuleAggregationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAggregationExpression = null;


        try {
            // InternalXaaS.g:1561:62: (iv_ruleAggregationExpression= ruleAggregationExpression EOF )
            // InternalXaaS.g:1562:2: iv_ruleAggregationExpression= ruleAggregationExpression EOF
            {
             newCompositeNode(grammarAccess.getAggregationExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAggregationExpression=ruleAggregationExpression();

            state._fsp--;

             current =iv_ruleAggregationExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAggregationExpression"


    // $ANTLR start "ruleAggregationExpression"
    // InternalXaaS.g:1568:1: ruleAggregationExpression returns [EObject current=null] : ( ( (lv_operator_0_0= ruleAggregationOperator ) ) otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ',' ( ( ruleEString ) ) otherlv_5= ')' ) ;
    public final EObject ruleAggregationExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Enumerator lv_operator_0_0 = null;

        Enumerator lv_direction_2_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1574:2: ( ( ( (lv_operator_0_0= ruleAggregationOperator ) ) otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ',' ( ( ruleEString ) ) otherlv_5= ')' ) )
            // InternalXaaS.g:1575:2: ( ( (lv_operator_0_0= ruleAggregationOperator ) ) otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ',' ( ( ruleEString ) ) otherlv_5= ')' )
            {
            // InternalXaaS.g:1575:2: ( ( (lv_operator_0_0= ruleAggregationOperator ) ) otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ',' ( ( ruleEString ) ) otherlv_5= ')' )
            // InternalXaaS.g:1576:3: ( (lv_operator_0_0= ruleAggregationOperator ) ) otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ',' ( ( ruleEString ) ) otherlv_5= ')'
            {
            // InternalXaaS.g:1576:3: ( (lv_operator_0_0= ruleAggregationOperator ) )
            // InternalXaaS.g:1577:4: (lv_operator_0_0= ruleAggregationOperator )
            {
            // InternalXaaS.g:1577:4: (lv_operator_0_0= ruleAggregationOperator )
            // InternalXaaS.g:1578:5: lv_operator_0_0= ruleAggregationOperator
            {

            					newCompositeNode(grammarAccess.getAggregationExpressionAccess().getOperatorAggregationOperatorEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_34);
            lv_operator_0_0=ruleAggregationOperator();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAggregationExpressionRule());
            					}
            					set(
            						current,
            						"operator",
            						lv_operator_0_0,
            						"fr.imta.come4acloud.yml.XaaS.AggregationOperator");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,34,FOLLOW_35); 

            			newLeafNode(otherlv_1, grammarAccess.getAggregationExpressionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalXaaS.g:1599:3: ( (lv_direction_2_0= ruleDirectionKind ) )
            // InternalXaaS.g:1600:4: (lv_direction_2_0= ruleDirectionKind )
            {
            // InternalXaaS.g:1600:4: (lv_direction_2_0= ruleDirectionKind )
            // InternalXaaS.g:1601:5: lv_direction_2_0= ruleDirectionKind
            {

            					newCompositeNode(grammarAccess.getAggregationExpressionAccess().getDirectionDirectionKindEnumRuleCall_2_0());
            				
            pushFollow(FOLLOW_36);
            lv_direction_2_0=ruleDirectionKind();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAggregationExpressionRule());
            					}
            					set(
            						current,
            						"direction",
            						lv_direction_2_0,
            						"fr.imta.come4acloud.yml.XaaS.DirectionKind");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,35,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getAggregationExpressionAccess().getCommaKeyword_3());
            		
            // InternalXaaS.g:1622:3: ( ( ruleEString ) )
            // InternalXaaS.g:1623:4: ( ruleEString )
            {
            // InternalXaaS.g:1623:4: ( ruleEString )
            // InternalXaaS.g:1624:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAggregationExpressionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAggregationExpressionAccess().getAttributeTypeAttributeTypeCrossReference_4_0());
            				
            pushFollow(FOLLOW_37);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,36,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getAggregationExpressionAccess().getRightParenthesisKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAggregationExpression"


    // $ANTLR start "entryRuleNbConnectionExpression"
    // InternalXaaS.g:1646:1: entryRuleNbConnectionExpression returns [EObject current=null] : iv_ruleNbConnectionExpression= ruleNbConnectionExpression EOF ;
    public final EObject entryRuleNbConnectionExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNbConnectionExpression = null;


        try {
            // InternalXaaS.g:1646:63: (iv_ruleNbConnectionExpression= ruleNbConnectionExpression EOF )
            // InternalXaaS.g:1647:2: iv_ruleNbConnectionExpression= ruleNbConnectionExpression EOF
            {
             newCompositeNode(grammarAccess.getNbConnectionExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNbConnectionExpression=ruleNbConnectionExpression();

            state._fsp--;

             current =iv_ruleNbConnectionExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNbConnectionExpression"


    // $ANTLR start "ruleNbConnectionExpression"
    // InternalXaaS.g:1653:1: ruleNbConnectionExpression returns [EObject current=null] : (otherlv_0= 'NbLink' otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ')' ) ;
    public final EObject ruleNbConnectionExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Enumerator lv_direction_2_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1659:2: ( (otherlv_0= 'NbLink' otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ')' ) )
            // InternalXaaS.g:1660:2: (otherlv_0= 'NbLink' otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ')' )
            {
            // InternalXaaS.g:1660:2: (otherlv_0= 'NbLink' otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ')' )
            // InternalXaaS.g:1661:3: otherlv_0= 'NbLink' otherlv_1= '(' ( (lv_direction_2_0= ruleDirectionKind ) ) otherlv_3= ')'
            {
            otherlv_0=(Token)match(input,37,FOLLOW_34); 

            			newLeafNode(otherlv_0, grammarAccess.getNbConnectionExpressionAccess().getNbLinkKeyword_0());
            		
            otherlv_1=(Token)match(input,34,FOLLOW_35); 

            			newLeafNode(otherlv_1, grammarAccess.getNbConnectionExpressionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalXaaS.g:1669:3: ( (lv_direction_2_0= ruleDirectionKind ) )
            // InternalXaaS.g:1670:4: (lv_direction_2_0= ruleDirectionKind )
            {
            // InternalXaaS.g:1670:4: (lv_direction_2_0= ruleDirectionKind )
            // InternalXaaS.g:1671:5: lv_direction_2_0= ruleDirectionKind
            {

            					newCompositeNode(grammarAccess.getNbConnectionExpressionAccess().getDirectionDirectionKindEnumRuleCall_2_0());
            				
            pushFollow(FOLLOW_37);
            lv_direction_2_0=ruleDirectionKind();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNbConnectionExpressionRule());
            					}
            					set(
            						current,
            						"direction",
            						lv_direction_2_0,
            						"fr.imta.come4acloud.yml.XaaS.DirectionKind");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,36,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getNbConnectionExpressionAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNbConnectionExpression"


    // $ANTLR start "entryRuleCustomExpression"
    // InternalXaaS.g:1696:1: entryRuleCustomExpression returns [EObject current=null] : iv_ruleCustomExpression= ruleCustomExpression EOF ;
    public final EObject entryRuleCustomExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCustomExpression = null;


        try {
            // InternalXaaS.g:1696:57: (iv_ruleCustomExpression= ruleCustomExpression EOF )
            // InternalXaaS.g:1697:2: iv_ruleCustomExpression= ruleCustomExpression EOF
            {
             newCompositeNode(grammarAccess.getCustomExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCustomExpression=ruleCustomExpression();

            state._fsp--;

             current =iv_ruleCustomExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCustomExpression"


    // $ANTLR start "ruleCustomExpression"
    // InternalXaaS.g:1703:1: ruleCustomExpression returns [EObject current=null] : (otherlv_0= 'CustomExpression' otherlv_1= ':' ( (lv_expression_2_0= ruleEString ) ) ) ;
    public final EObject ruleCustomExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_expression_2_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1709:2: ( (otherlv_0= 'CustomExpression' otherlv_1= ':' ( (lv_expression_2_0= ruleEString ) ) ) )
            // InternalXaaS.g:1710:2: (otherlv_0= 'CustomExpression' otherlv_1= ':' ( (lv_expression_2_0= ruleEString ) ) )
            {
            // InternalXaaS.g:1710:2: (otherlv_0= 'CustomExpression' otherlv_1= ':' ( (lv_expression_2_0= ruleEString ) ) )
            // InternalXaaS.g:1711:3: otherlv_0= 'CustomExpression' otherlv_1= ':' ( (lv_expression_2_0= ruleEString ) )
            {
            otherlv_0=(Token)match(input,38,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getCustomExpressionAccess().getCustomExpressionKeyword_0());
            		
            otherlv_1=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getCustomExpressionAccess().getColonKeyword_1());
            		
            // InternalXaaS.g:1719:3: ( (lv_expression_2_0= ruleEString ) )
            // InternalXaaS.g:1720:4: (lv_expression_2_0= ruleEString )
            {
            // InternalXaaS.g:1720:4: (lv_expression_2_0= ruleEString )
            // InternalXaaS.g:1721:5: lv_expression_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCustomExpressionAccess().getExpressionEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_expression_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCustomExpressionRule());
            					}
            					set(
            						current,
            						"expression",
            						lv_expression_2_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCustomExpression"


    // $ANTLR start "entryRuleEInt"
    // InternalXaaS.g:1742:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalXaaS.g:1742:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalXaaS.g:1743:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalXaaS.g:1749:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalXaaS.g:1755:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalXaaS.g:1756:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalXaaS.g:1756:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalXaaS.g:1757:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalXaaS.g:1757:3: (kw= '-' )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==39) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalXaaS.g:1758:4: kw= '-'
                    {
                    kw=(Token)match(input,39,FOLLOW_38); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleRelationshipType"
    // InternalXaaS.g:1775:1: entryRuleRelationshipType returns [EObject current=null] : iv_ruleRelationshipType= ruleRelationshipType EOF ;
    public final EObject entryRuleRelationshipType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRelationshipType = null;


        try {
            // InternalXaaS.g:1775:57: (iv_ruleRelationshipType= ruleRelationshipType EOF )
            // InternalXaaS.g:1776:2: iv_ruleRelationshipType= ruleRelationshipType EOF
            {
             newCompositeNode(grammarAccess.getRelationshipTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRelationshipType=ruleRelationshipType();

            state._fsp--;

             current =iv_ruleRelationshipType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRelationshipType"


    // $ANTLR start "ruleRelationshipType"
    // InternalXaaS.g:1782:1: ruleRelationshipType returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' otherlv_2= 'valid_source_types' otherlv_3= ':' ( ( ruleEString ) ) otherlv_5= 'valid_target_types' otherlv_6= ':' ( ( ruleEString ) ) (otherlv_8= 'constraints' otherlv_9= ':' ( (lv_constraints_10_0= ruleConstraint ) )* )? (otherlv_11= 'impactOfLinking' otherlv_12= ':' ( (lv_impactOfLinking_13_0= ruleEInt ) ) )? (otherlv_14= 'impactOfUnlinking' otherlv_15= ':' ( (lv_impactOfUnlinking_16_0= ruleEInt ) ) )? ) ;
    public final EObject ruleRelationshipType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        EObject lv_constraints_10_0 = null;

        AntlrDatatypeRuleToken lv_impactOfLinking_13_0 = null;

        AntlrDatatypeRuleToken lv_impactOfUnlinking_16_0 = null;



        	enterRule();

        try {
            // InternalXaaS.g:1788:2: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' otherlv_2= 'valid_source_types' otherlv_3= ':' ( ( ruleEString ) ) otherlv_5= 'valid_target_types' otherlv_6= ':' ( ( ruleEString ) ) (otherlv_8= 'constraints' otherlv_9= ':' ( (lv_constraints_10_0= ruleConstraint ) )* )? (otherlv_11= 'impactOfLinking' otherlv_12= ':' ( (lv_impactOfLinking_13_0= ruleEInt ) ) )? (otherlv_14= 'impactOfUnlinking' otherlv_15= ':' ( (lv_impactOfUnlinking_16_0= ruleEInt ) ) )? ) )
            // InternalXaaS.g:1789:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' otherlv_2= 'valid_source_types' otherlv_3= ':' ( ( ruleEString ) ) otherlv_5= 'valid_target_types' otherlv_6= ':' ( ( ruleEString ) ) (otherlv_8= 'constraints' otherlv_9= ':' ( (lv_constraints_10_0= ruleConstraint ) )* )? (otherlv_11= 'impactOfLinking' otherlv_12= ':' ( (lv_impactOfLinking_13_0= ruleEInt ) ) )? (otherlv_14= 'impactOfUnlinking' otherlv_15= ':' ( (lv_impactOfUnlinking_16_0= ruleEInt ) ) )? )
            {
            // InternalXaaS.g:1789:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' otherlv_2= 'valid_source_types' otherlv_3= ':' ( ( ruleEString ) ) otherlv_5= 'valid_target_types' otherlv_6= ':' ( ( ruleEString ) ) (otherlv_8= 'constraints' otherlv_9= ':' ( (lv_constraints_10_0= ruleConstraint ) )* )? (otherlv_11= 'impactOfLinking' otherlv_12= ':' ( (lv_impactOfLinking_13_0= ruleEInt ) ) )? (otherlv_14= 'impactOfUnlinking' otherlv_15= ':' ( (lv_impactOfUnlinking_16_0= ruleEInt ) ) )? )
            // InternalXaaS.g:1790:3: ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' otherlv_2= 'valid_source_types' otherlv_3= ':' ( ( ruleEString ) ) otherlv_5= 'valid_target_types' otherlv_6= ':' ( ( ruleEString ) ) (otherlv_8= 'constraints' otherlv_9= ':' ( (lv_constraints_10_0= ruleConstraint ) )* )? (otherlv_11= 'impactOfLinking' otherlv_12= ':' ( (lv_impactOfLinking_13_0= ruleEInt ) ) )? (otherlv_14= 'impactOfUnlinking' otherlv_15= ':' ( (lv_impactOfUnlinking_16_0= ruleEInt ) ) )?
            {
            // InternalXaaS.g:1790:3: ( (lv_name_0_0= ruleEString ) )
            // InternalXaaS.g:1791:4: (lv_name_0_0= ruleEString )
            {
            // InternalXaaS.g:1791:4: (lv_name_0_0= ruleEString )
            // InternalXaaS.g:1792:5: lv_name_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getRelationshipTypeAccess().getNameEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRelationshipTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"fr.imta.come4acloud.yml.XaaS.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,12,FOLLOW_39); 

            			newLeafNode(otherlv_1, grammarAccess.getRelationshipTypeAccess().getColonKeyword_1());
            		
            otherlv_2=(Token)match(input,40,FOLLOW_4); 

            			newLeafNode(otherlv_2, grammarAccess.getRelationshipTypeAccess().getValid_source_typesKeyword_2());
            		
            otherlv_3=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getRelationshipTypeAccess().getColonKeyword_3());
            		
            // InternalXaaS.g:1821:3: ( ( ruleEString ) )
            // InternalXaaS.g:1822:4: ( ruleEString )
            {
            // InternalXaaS.g:1822:4: ( ruleEString )
            // InternalXaaS.g:1823:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRelationshipTypeRule());
            					}
            				

            					newCompositeNode(grammarAccess.getRelationshipTypeAccess().getSourceNodeTypeCrossReference_4_0());
            				
            pushFollow(FOLLOW_40);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,41,FOLLOW_4); 

            			newLeafNode(otherlv_5, grammarAccess.getRelationshipTypeAccess().getValid_target_typesKeyword_5());
            		
            otherlv_6=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_6, grammarAccess.getRelationshipTypeAccess().getColonKeyword_6());
            		
            // InternalXaaS.g:1845:3: ( ( ruleEString ) )
            // InternalXaaS.g:1846:4: ( ruleEString )
            {
            // InternalXaaS.g:1846:4: ( ruleEString )
            // InternalXaaS.g:1847:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRelationshipTypeRule());
            					}
            				

            					newCompositeNode(grammarAccess.getRelationshipTypeAccess().getTargetNodeTypeCrossReference_7_0());
            				
            pushFollow(FOLLOW_41);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXaaS.g:1861:3: (otherlv_8= 'constraints' otherlv_9= ':' ( (lv_constraints_10_0= ruleConstraint ) )* )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==30) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalXaaS.g:1862:4: otherlv_8= 'constraints' otherlv_9= ':' ( (lv_constraints_10_0= ruleConstraint ) )*
                    {
                    otherlv_8=(Token)match(input,30,FOLLOW_4); 

                    				newLeafNode(otherlv_8, grammarAccess.getRelationshipTypeAccess().getConstraintsKeyword_8_0());
                    			
                    otherlv_9=(Token)match(input,12,FOLLOW_42); 

                    				newLeafNode(otherlv_9, grammarAccess.getRelationshipTypeAccess().getColonKeyword_8_1());
                    			
                    // InternalXaaS.g:1870:4: ( (lv_constraints_10_0= ruleConstraint ) )*
                    loop28:
                    do {
                        int alt28=2;
                        alt28 = dfa28.predict(input);
                        switch (alt28) {
                    	case 1 :
                    	    // InternalXaaS.g:1871:5: (lv_constraints_10_0= ruleConstraint )
                    	    {
                    	    // InternalXaaS.g:1871:5: (lv_constraints_10_0= ruleConstraint )
                    	    // InternalXaaS.g:1872:6: lv_constraints_10_0= ruleConstraint
                    	    {

                    	    						newCompositeNode(grammarAccess.getRelationshipTypeAccess().getConstraintsConstraintParserRuleCall_8_2_0());
                    	    					
                    	    pushFollow(FOLLOW_42);
                    	    lv_constraints_10_0=ruleConstraint();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRelationshipTypeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"constraints",
                    	    							lv_constraints_10_0,
                    	    							"fr.imta.come4acloud.yml.XaaS.Constraint");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop28;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalXaaS.g:1890:3: (otherlv_11= 'impactOfLinking' otherlv_12= ':' ( (lv_impactOfLinking_13_0= ruleEInt ) ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==42) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalXaaS.g:1891:4: otherlv_11= 'impactOfLinking' otherlv_12= ':' ( (lv_impactOfLinking_13_0= ruleEInt ) )
                    {
                    otherlv_11=(Token)match(input,42,FOLLOW_4); 

                    				newLeafNode(otherlv_11, grammarAccess.getRelationshipTypeAccess().getImpactOfLinkingKeyword_9_0());
                    			
                    otherlv_12=(Token)match(input,12,FOLLOW_24); 

                    				newLeafNode(otherlv_12, grammarAccess.getRelationshipTypeAccess().getColonKeyword_9_1());
                    			
                    // InternalXaaS.g:1899:4: ( (lv_impactOfLinking_13_0= ruleEInt ) )
                    // InternalXaaS.g:1900:5: (lv_impactOfLinking_13_0= ruleEInt )
                    {
                    // InternalXaaS.g:1900:5: (lv_impactOfLinking_13_0= ruleEInt )
                    // InternalXaaS.g:1901:6: lv_impactOfLinking_13_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getRelationshipTypeAccess().getImpactOfLinkingEIntParserRuleCall_9_2_0());
                    					
                    pushFollow(FOLLOW_43);
                    lv_impactOfLinking_13_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRelationshipTypeRule());
                    						}
                    						set(
                    							current,
                    							"impactOfLinking",
                    							lv_impactOfLinking_13_0,
                    							"fr.imta.come4acloud.yml.XaaS.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalXaaS.g:1919:3: (otherlv_14= 'impactOfUnlinking' otherlv_15= ':' ( (lv_impactOfUnlinking_16_0= ruleEInt ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==43) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalXaaS.g:1920:4: otherlv_14= 'impactOfUnlinking' otherlv_15= ':' ( (lv_impactOfUnlinking_16_0= ruleEInt ) )
                    {
                    otherlv_14=(Token)match(input,43,FOLLOW_4); 

                    				newLeafNode(otherlv_14, grammarAccess.getRelationshipTypeAccess().getImpactOfUnlinkingKeyword_10_0());
                    			
                    otherlv_15=(Token)match(input,12,FOLLOW_24); 

                    				newLeafNode(otherlv_15, grammarAccess.getRelationshipTypeAccess().getColonKeyword_10_1());
                    			
                    // InternalXaaS.g:1928:4: ( (lv_impactOfUnlinking_16_0= ruleEInt ) )
                    // InternalXaaS.g:1929:5: (lv_impactOfUnlinking_16_0= ruleEInt )
                    {
                    // InternalXaaS.g:1929:5: (lv_impactOfUnlinking_16_0= ruleEInt )
                    // InternalXaaS.g:1930:6: lv_impactOfUnlinking_16_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getRelationshipTypeAccess().getImpactOfUnlinkingEIntParserRuleCall_10_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_impactOfUnlinking_16_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRelationshipTypeRule());
                    						}
                    						set(
                    							current,
                    							"impactOfUnlinking",
                    							lv_impactOfUnlinking_16_0,
                    							"fr.imta.come4acloud.yml.XaaS.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRelationshipType"


    // $ANTLR start "entryRuleEBoolean"
    // InternalXaaS.g:1952:1: entryRuleEBoolean returns [String current=null] : iv_ruleEBoolean= ruleEBoolean EOF ;
    public final String entryRuleEBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEBoolean = null;


        try {
            // InternalXaaS.g:1952:48: (iv_ruleEBoolean= ruleEBoolean EOF )
            // InternalXaaS.g:1953:2: iv_ruleEBoolean= ruleEBoolean EOF
            {
             newCompositeNode(grammarAccess.getEBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEBoolean=ruleEBoolean();

            state._fsp--;

             current =iv_ruleEBoolean.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEBoolean"


    // $ANTLR start "ruleEBoolean"
    // InternalXaaS.g:1959:1: ruleEBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleEBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalXaaS.g:1965:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalXaaS.g:1966:2: (kw= 'true' | kw= 'false' )
            {
            // InternalXaaS.g:1966:2: (kw= 'true' | kw= 'false' )
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==44) ) {
                alt32=1;
            }
            else if ( (LA32_0==45) ) {
                alt32=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }
            switch (alt32) {
                case 1 :
                    // InternalXaaS.g:1967:3: kw= 'true'
                    {
                    kw=(Token)match(input,44,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getTrueKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalXaaS.g:1973:3: kw= 'false'
                    {
                    kw=(Token)match(input,45,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getFalseKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEBoolean"


    // $ANTLR start "ruleAlgebraicOperator"
    // InternalXaaS.g:1982:1: ruleAlgebraicOperator returns [Enumerator current=null] : ( (enumLiteral_0= '+' ) | (enumLiteral_1= '*' ) | (enumLiteral_2= '-' ) | (enumLiteral_3= '/' ) ) ;
    public final Enumerator ruleAlgebraicOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalXaaS.g:1988:2: ( ( (enumLiteral_0= '+' ) | (enumLiteral_1= '*' ) | (enumLiteral_2= '-' ) | (enumLiteral_3= '/' ) ) )
            // InternalXaaS.g:1989:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '*' ) | (enumLiteral_2= '-' ) | (enumLiteral_3= '/' ) )
            {
            // InternalXaaS.g:1989:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '*' ) | (enumLiteral_2= '-' ) | (enumLiteral_3= '/' ) )
            int alt33=4;
            switch ( input.LA(1) ) {
            case 46:
                {
                alt33=1;
                }
                break;
            case 47:
                {
                alt33=2;
                }
                break;
            case 39:
                {
                alt33=3;
                }
                break;
            case 48:
                {
                alt33=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }

            switch (alt33) {
                case 1 :
                    // InternalXaaS.g:1990:3: (enumLiteral_0= '+' )
                    {
                    // InternalXaaS.g:1990:3: (enumLiteral_0= '+' )
                    // InternalXaaS.g:1991:4: enumLiteral_0= '+'
                    {
                    enumLiteral_0=(Token)match(input,46,FOLLOW_2); 

                    				current = grammarAccess.getAlgebraicOperatorAccess().getSumEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getAlgebraicOperatorAccess().getSumEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:1998:3: (enumLiteral_1= '*' )
                    {
                    // InternalXaaS.g:1998:3: (enumLiteral_1= '*' )
                    // InternalXaaS.g:1999:4: enumLiteral_1= '*'
                    {
                    enumLiteral_1=(Token)match(input,47,FOLLOW_2); 

                    				current = grammarAccess.getAlgebraicOperatorAccess().getProdEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getAlgebraicOperatorAccess().getProdEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalXaaS.g:2006:3: (enumLiteral_2= '-' )
                    {
                    // InternalXaaS.g:2006:3: (enumLiteral_2= '-' )
                    // InternalXaaS.g:2007:4: enumLiteral_2= '-'
                    {
                    enumLiteral_2=(Token)match(input,39,FOLLOW_2); 

                    				current = grammarAccess.getAlgebraicOperatorAccess().getMinusEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getAlgebraicOperatorAccess().getMinusEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalXaaS.g:2014:3: (enumLiteral_3= '/' )
                    {
                    // InternalXaaS.g:2014:3: (enumLiteral_3= '/' )
                    // InternalXaaS.g:2015:4: enumLiteral_3= '/'
                    {
                    enumLiteral_3=(Token)match(input,48,FOLLOW_2); 

                    				current = grammarAccess.getAlgebraicOperatorAccess().getDivEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getAlgebraicOperatorAccess().getDivEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlgebraicOperator"


    // $ANTLR start "ruleAggregationOperator"
    // InternalXaaS.g:2025:1: ruleAggregationOperator returns [Enumerator current=null] : ( (enumLiteral_0= 'Sum' ) | (enumLiteral_1= 'Min' ) | (enumLiteral_2= 'Max' ) ) ;
    public final Enumerator ruleAggregationOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalXaaS.g:2031:2: ( ( (enumLiteral_0= 'Sum' ) | (enumLiteral_1= 'Min' ) | (enumLiteral_2= 'Max' ) ) )
            // InternalXaaS.g:2032:2: ( (enumLiteral_0= 'Sum' ) | (enumLiteral_1= 'Min' ) | (enumLiteral_2= 'Max' ) )
            {
            // InternalXaaS.g:2032:2: ( (enumLiteral_0= 'Sum' ) | (enumLiteral_1= 'Min' ) | (enumLiteral_2= 'Max' ) )
            int alt34=3;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt34=1;
                }
                break;
            case 50:
                {
                alt34=2;
                }
                break;
            case 51:
                {
                alt34=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }

            switch (alt34) {
                case 1 :
                    // InternalXaaS.g:2033:3: (enumLiteral_0= 'Sum' )
                    {
                    // InternalXaaS.g:2033:3: (enumLiteral_0= 'Sum' )
                    // InternalXaaS.g:2034:4: enumLiteral_0= 'Sum'
                    {
                    enumLiteral_0=(Token)match(input,49,FOLLOW_2); 

                    				current = grammarAccess.getAggregationOperatorAccess().getSumEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getAggregationOperatorAccess().getSumEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:2041:3: (enumLiteral_1= 'Min' )
                    {
                    // InternalXaaS.g:2041:3: (enumLiteral_1= 'Min' )
                    // InternalXaaS.g:2042:4: enumLiteral_1= 'Min'
                    {
                    enumLiteral_1=(Token)match(input,50,FOLLOW_2); 

                    				current = grammarAccess.getAggregationOperatorAccess().getMinEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getAggregationOperatorAccess().getMinEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalXaaS.g:2049:3: (enumLiteral_2= 'Max' )
                    {
                    // InternalXaaS.g:2049:3: (enumLiteral_2= 'Max' )
                    // InternalXaaS.g:2050:4: enumLiteral_2= 'Max'
                    {
                    enumLiteral_2=(Token)match(input,51,FOLLOW_2); 

                    				current = grammarAccess.getAggregationOperatorAccess().getMaxEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getAggregationOperatorAccess().getMaxEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAggregationOperator"


    // $ANTLR start "ruleDirectionKind"
    // InternalXaaS.g:2060:1: ruleDirectionKind returns [Enumerator current=null] : ( (enumLiteral_0= 'Pred' ) | (enumLiteral_1= 'Succ' ) ) ;
    public final Enumerator ruleDirectionKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalXaaS.g:2066:2: ( ( (enumLiteral_0= 'Pred' ) | (enumLiteral_1= 'Succ' ) ) )
            // InternalXaaS.g:2067:2: ( (enumLiteral_0= 'Pred' ) | (enumLiteral_1= 'Succ' ) )
            {
            // InternalXaaS.g:2067:2: ( (enumLiteral_0= 'Pred' ) | (enumLiteral_1= 'Succ' ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==52) ) {
                alt35=1;
            }
            else if ( (LA35_0==53) ) {
                alt35=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // InternalXaaS.g:2068:3: (enumLiteral_0= 'Pred' )
                    {
                    // InternalXaaS.g:2068:3: (enumLiteral_0= 'Pred' )
                    // InternalXaaS.g:2069:4: enumLiteral_0= 'Pred'
                    {
                    enumLiteral_0=(Token)match(input,52,FOLLOW_2); 

                    				current = grammarAccess.getDirectionKindAccess().getPredecessorSourceEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDirectionKindAccess().getPredecessorSourceEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:2076:3: (enumLiteral_1= 'Succ' )
                    {
                    // InternalXaaS.g:2076:3: (enumLiteral_1= 'Succ' )
                    // InternalXaaS.g:2077:4: enumLiteral_1= 'Succ'
                    {
                    enumLiteral_1=(Token)match(input,53,FOLLOW_2); 

                    				current = grammarAccess.getDirectionKindAccess().getSuccessorTargetEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDirectionKindAccess().getSuccessorTargetEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDirectionKind"


    // $ANTLR start "ruleComparisonOperator"
    // InternalXaaS.g:2087:1: ruleComparisonOperator returns [Enumerator current=null] : ( (enumLiteral_0= 'equal' ) | (enumLiteral_1= 'less_than' ) | (enumLiteral_2= 'greater_than' ) | (enumLiteral_3= 'less_or_equal' ) | (enumLiteral_4= 'greater_or_equal' ) ) ;
    public final Enumerator ruleComparisonOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;


        	enterRule();

        try {
            // InternalXaaS.g:2093:2: ( ( (enumLiteral_0= 'equal' ) | (enumLiteral_1= 'less_than' ) | (enumLiteral_2= 'greater_than' ) | (enumLiteral_3= 'less_or_equal' ) | (enumLiteral_4= 'greater_or_equal' ) ) )
            // InternalXaaS.g:2094:2: ( (enumLiteral_0= 'equal' ) | (enumLiteral_1= 'less_than' ) | (enumLiteral_2= 'greater_than' ) | (enumLiteral_3= 'less_or_equal' ) | (enumLiteral_4= 'greater_or_equal' ) )
            {
            // InternalXaaS.g:2094:2: ( (enumLiteral_0= 'equal' ) | (enumLiteral_1= 'less_than' ) | (enumLiteral_2= 'greater_than' ) | (enumLiteral_3= 'less_or_equal' ) | (enumLiteral_4= 'greater_or_equal' ) )
            int alt36=5;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt36=1;
                }
                break;
            case 54:
                {
                alt36=2;
                }
                break;
            case 55:
                {
                alt36=3;
                }
                break;
            case 56:
                {
                alt36=4;
                }
                break;
            case 57:
                {
                alt36=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }

            switch (alt36) {
                case 1 :
                    // InternalXaaS.g:2095:3: (enumLiteral_0= 'equal' )
                    {
                    // InternalXaaS.g:2095:3: (enumLiteral_0= 'equal' )
                    // InternalXaaS.g:2096:4: enumLiteral_0= 'equal'
                    {
                    enumLiteral_0=(Token)match(input,32,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getEqualEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getComparisonOperatorAccess().getEqualEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:2103:3: (enumLiteral_1= 'less_than' )
                    {
                    // InternalXaaS.g:2103:3: (enumLiteral_1= 'less_than' )
                    // InternalXaaS.g:2104:4: enumLiteral_1= 'less_than'
                    {
                    enumLiteral_1=(Token)match(input,54,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getLesserEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getComparisonOperatorAccess().getLesserEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalXaaS.g:2111:3: (enumLiteral_2= 'greater_than' )
                    {
                    // InternalXaaS.g:2111:3: (enumLiteral_2= 'greater_than' )
                    // InternalXaaS.g:2112:4: enumLiteral_2= 'greater_than'
                    {
                    enumLiteral_2=(Token)match(input,55,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getGreaterEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getComparisonOperatorAccess().getGreaterEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalXaaS.g:2119:3: (enumLiteral_3= 'less_or_equal' )
                    {
                    // InternalXaaS.g:2119:3: (enumLiteral_3= 'less_or_equal' )
                    // InternalXaaS.g:2120:4: enumLiteral_3= 'less_or_equal'
                    {
                    enumLiteral_3=(Token)match(input,56,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getLesserOrEqualEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getComparisonOperatorAccess().getLesserOrEqualEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalXaaS.g:2127:3: (enumLiteral_4= 'greater_or_equal' )
                    {
                    // InternalXaaS.g:2127:3: (enumLiteral_4= 'greater_or_equal' )
                    // InternalXaaS.g:2128:4: enumLiteral_4= 'greater_or_equal'
                    {
                    enumLiteral_4=(Token)match(input,57,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getGreaterOrEqualEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getComparisonOperatorAccess().getGreaterOrEqualEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparisonOperator"

    // Delegated rules


    protected DFA4 dfa4 = new DFA4(this);
    protected DFA22 dfa22 = new DFA22(this);
    protected DFA28 dfa28 = new DFA28(this);
    static final String dfa_1s = "\36\uffff";
    static final String dfa_2s = "\2\uffff\1\13\2\15\13\uffff\1\15\5\uffff\1\31\4\uffff\1\35\2\uffff";
    static final String dfa_3s = "\1\4\1\6\3\4\4\42\3\uffff\1\4\1\uffff\2\64\1\4\2\43\2\44\2\4\1\22\1\44\1\uffff\2\4\1\22\1\uffff";
    static final String dfa_4s = "\1\63\1\6\3\60\4\42\3\uffff\1\4\1\uffff\2\65\1\60\2\43\2\44\1\5\1\60\2\44\1\uffff\1\4\1\60\1\44\1\uffff";
    static final String dfa_5s = "\11\uffff\1\6\1\3\1\1\1\uffff\1\2\13\uffff\1\5\3\uffff\1\4";
    static final String dfa_6s = "\36\uffff}>";
    static final String[] dfa_7s = {
            "\1\3\1\4\1\2\36\uffff\1\10\1\11\1\1\11\uffff\1\5\1\6\1\7",
            "\1\2",
            "\2\13\5\uffff\1\13\5\uffff\1\13\6\uffff\1\13\3\uffff\4\13\1\uffff\1\13\5\uffff\1\12\2\uffff\2\13\2\uffff\3\12",
            "\2\15\5\uffff\1\15\5\uffff\1\15\1\14\5\uffff\1\15\3\uffff\4\15\1\uffff\1\15\5\uffff\1\12\2\uffff\2\15\2\uffff\3\12",
            "\2\15\5\uffff\1\15\5\uffff\1\15\6\uffff\1\15\3\uffff\4\15\1\uffff\1\15\5\uffff\1\12\2\uffff\2\15\2\uffff\3\12",
            "\1\16",
            "\1\16",
            "\1\16",
            "\1\17",
            "",
            "",
            "",
            "\1\20",
            "",
            "\1\21\1\22",
            "\1\23\1\24",
            "\2\15\5\uffff\1\15\5\uffff\1\15\1\14\5\uffff\1\15\3\uffff\4\15\1\uffff\1\15\5\uffff\1\12\2\uffff\2\15\2\uffff\3\12",
            "\1\25",
            "\1\25",
            "\1\26",
            "\1\26",
            "\1\27\1\30",
            "\2\31\5\uffff\1\31\5\uffff\1\31\6\uffff\1\31\3\uffff\4\31\1\uffff\1\31\5\uffff\1\12\2\uffff\2\31\2\uffff\3\12",
            "\1\32\21\uffff\1\33",
            "\1\33",
            "",
            "\1\34",
            "\2\35\5\uffff\1\35\5\uffff\1\35\6\uffff\1\35\3\uffff\4\35\1\uffff\1\35\5\uffff\1\12\2\uffff\2\35\2\uffff\3\12",
            "\1\32\21\uffff\1\33",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "172:2: (this_IntegerValueExpression_0= ruleIntegerValueExpression | this_AttributeExpression_1= ruleAttributeExpression | this_BinaryExpression_2= ruleBinaryExpression | this_AggregationExpression_3= ruleAggregationExpression | this_NbConnectionExpression_4= ruleNbConnectionExpression | this_CustomExpression_5= ruleCustomExpression )";
        }
    }
    static final String dfa_8s = "\7\uffff";
    static final String dfa_9s = "\1\1\6\uffff";
    static final String dfa_10s = "\1\4\1\uffff\2\14\1\4\1\uffff\1\14";
    static final String dfa_11s = "\1\37\1\uffff\2\71\1\4\1\uffff\1\71";
    static final String dfa_12s = "\1\uffff\1\2\3\uffff\1\1\1\uffff";
    static final String dfa_13s = "\7\uffff}>";
    static final String[] dfa_14s = {
            "\1\2\1\3\5\uffff\1\1\5\uffff\1\1\6\uffff\1\1\3\uffff\2\1\1\uffff\1\1",
            "",
            "\1\1\5\uffff\1\4\15\uffff\1\5\25\uffff\4\5",
            "\1\1\23\uffff\1\5\25\uffff\4\5",
            "\1\6",
            "",
            "\1\1\5\uffff\1\4\15\uffff\1\5\25\uffff\4\5"
    };

    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final char[] dfa_11 = DFA.unpackEncodedStringToUnsignedChars(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[] dfa_13 = DFA.unpackEncodedString(dfa_13s);
    static final short[][] dfa_14 = unpackEncodedStringArray(dfa_14s);

    class DFA22 extends DFA {

        public DFA22(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 22;
            this.eot = dfa_8;
            this.eof = dfa_9;
            this.min = dfa_10;
            this.max = dfa_11;
            this.accept = dfa_12;
            this.special = dfa_13;
            this.transition = dfa_14;
        }
        public String getDescription() {
            return "()+ loopback of 1040:6: ( (lv_constraints_17_0= ruleConstraint ) )+";
        }
    }
    static final String dfa_15s = "\1\53\1\uffff\2\71\1\4\1\uffff\1\71";
    static final String[] dfa_16s = {
            "\1\2\1\3\5\uffff\1\1\36\uffff\2\1",
            "",
            "\1\1\5\uffff\1\4\15\uffff\1\5\25\uffff\4\5",
            "\1\1\23\uffff\1\5\25\uffff\4\5",
            "\1\6",
            "",
            "\1\1\5\uffff\1\4\15\uffff\1\5\25\uffff\4\5"
    };
    static final char[] dfa_15 = DFA.unpackEncodedStringToUnsignedChars(dfa_15s);
    static final short[][] dfa_16 = unpackEncodedStringArray(dfa_16s);

    class DFA28 extends DFA {

        public DFA28(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 28;
            this.eot = dfa_8;
            this.eof = dfa_9;
            this.min = dfa_10;
            this.max = dfa_15;
            this.accept = dfa_12;
            this.special = dfa_13;
            this.transition = dfa_16;
        }
        public String getDescription() {
            return "()* loopback of 1870:4: ( (lv_constraints_10_0= ruleConstraint ) )*";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000880002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020032L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000007000002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000300000000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000006000002L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000008400002L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x00000000B1000002L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000008000000040L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x00000000B1000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x00000000F1000002L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x00000000B1000032L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000008000000060L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x03C0000100000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x000E00E000000070L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0001C08000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0030000000000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x00000C0040000002L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x00000C0000000032L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000080000000002L});

}
/*
 * generated by Xtext 2.11.0
 */
package fr.imta.come4acloud.yml.ide

import com.google.inject.Guice
import fr.imta.come4acloud.yml.XaaSRuntimeModule
import fr.imta.come4acloud.yml.XaaSStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class XaaSIdeSetup extends XaaSStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new XaaSRuntimeModule, new XaaSIdeModule))
	}
	
}

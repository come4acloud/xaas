package fr.imta.come4acloud.yml.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.imta.come4acloud.yml.services.XaaSGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXaaSParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'true'", "'false'", "'+'", "'*'", "'-'", "'/'", "'Sum'", "'Min'", "'Max'", "'Pred'", "'Succ'", "'equal'", "'less_than'", "'greater_than'", "'less_or_equal'", "'greater_or_equal'", "'Configuration'", "':'", "'identifier'", "'topology'", "'Topology'", "'node_types'", "'relationship_types'", "'.'", "'Node'", "'type'", "'activated'", "'properties'", "'Relationship'", "'constant'", "'source'", "'target'", "'derived_from'", "'impactOfEnabling'", "'impactOfDisabling'", "'constraints'", "'variable'", "'impactOfUpdating'", "'('", "','", "')'", "'NbLink'", "'CustomExpression'", "'valid_source_types'", "'valid_target_types'", "'impactOfLinking'", "'impactOfUnlinking'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalXaaSParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXaaSParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXaaSParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXaaS.g"; }


    	private XaaSGrammarAccess grammarAccess;

    	public void setGrammarAccess(XaaSGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleXaasYamlLikeTosca"
    // InternalXaaS.g:53:1: entryRuleXaasYamlLikeTosca : ruleXaasYamlLikeTosca EOF ;
    public final void entryRuleXaasYamlLikeTosca() throws RecognitionException {
        try {
            // InternalXaaS.g:54:1: ( ruleXaasYamlLikeTosca EOF )
            // InternalXaaS.g:55:1: ruleXaasYamlLikeTosca EOF
            {
             before(grammarAccess.getXaasYamlLikeToscaRule()); 
            pushFollow(FOLLOW_1);
            ruleXaasYamlLikeTosca();

            state._fsp--;

             after(grammarAccess.getXaasYamlLikeToscaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXaasYamlLikeTosca"


    // $ANTLR start "ruleXaasYamlLikeTosca"
    // InternalXaaS.g:62:1: ruleXaasYamlLikeTosca : ( ( rule__XaasYamlLikeTosca__Group__0 ) ) ;
    public final void ruleXaasYamlLikeTosca() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:66:2: ( ( ( rule__XaasYamlLikeTosca__Group__0 ) ) )
            // InternalXaaS.g:67:2: ( ( rule__XaasYamlLikeTosca__Group__0 ) )
            {
            // InternalXaaS.g:67:2: ( ( rule__XaasYamlLikeTosca__Group__0 ) )
            // InternalXaaS.g:68:3: ( rule__XaasYamlLikeTosca__Group__0 )
            {
             before(grammarAccess.getXaasYamlLikeToscaAccess().getGroup()); 
            // InternalXaaS.g:69:3: ( rule__XaasYamlLikeTosca__Group__0 )
            // InternalXaaS.g:69:4: rule__XaasYamlLikeTosca__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XaasYamlLikeTosca__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXaasYamlLikeToscaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXaasYamlLikeTosca"


    // $ANTLR start "entryRuleAttributeType"
    // InternalXaaS.g:78:1: entryRuleAttributeType : ruleAttributeType EOF ;
    public final void entryRuleAttributeType() throws RecognitionException {
        try {
            // InternalXaaS.g:79:1: ( ruleAttributeType EOF )
            // InternalXaaS.g:80:1: ruleAttributeType EOF
            {
             before(grammarAccess.getAttributeTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleAttributeType();

            state._fsp--;

             after(grammarAccess.getAttributeTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttributeType"


    // $ANTLR start "ruleAttributeType"
    // InternalXaaS.g:87:1: ruleAttributeType : ( ( rule__AttributeType__Alternatives ) ) ;
    public final void ruleAttributeType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:91:2: ( ( ( rule__AttributeType__Alternatives ) ) )
            // InternalXaaS.g:92:2: ( ( rule__AttributeType__Alternatives ) )
            {
            // InternalXaaS.g:92:2: ( ( rule__AttributeType__Alternatives ) )
            // InternalXaaS.g:93:3: ( rule__AttributeType__Alternatives )
            {
             before(grammarAccess.getAttributeTypeAccess().getAlternatives()); 
            // InternalXaaS.g:94:3: ( rule__AttributeType__Alternatives )
            // InternalXaaS.g:94:4: rule__AttributeType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AttributeType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAttributeTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttributeType"


    // $ANTLR start "entryRuleExpression"
    // InternalXaaS.g:103:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // InternalXaaS.g:104:1: ( ruleExpression EOF )
            // InternalXaaS.g:105:1: ruleExpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalXaaS.g:112:1: ruleExpression : ( ( rule__Expression__Alternatives ) ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:116:2: ( ( ( rule__Expression__Alternatives ) ) )
            // InternalXaaS.g:117:2: ( ( rule__Expression__Alternatives ) )
            {
            // InternalXaaS.g:117:2: ( ( rule__Expression__Alternatives ) )
            // InternalXaaS.g:118:3: ( rule__Expression__Alternatives )
            {
             before(grammarAccess.getExpressionAccess().getAlternatives()); 
            // InternalXaaS.g:119:3: ( rule__Expression__Alternatives )
            // InternalXaaS.g:119:4: rule__Expression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Expression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleSingelExpression"
    // InternalXaaS.g:128:1: entryRuleSingelExpression : ruleSingelExpression EOF ;
    public final void entryRuleSingelExpression() throws RecognitionException {
        try {
            // InternalXaaS.g:129:1: ( ruleSingelExpression EOF )
            // InternalXaaS.g:130:1: ruleSingelExpression EOF
            {
             before(grammarAccess.getSingelExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleSingelExpression();

            state._fsp--;

             after(grammarAccess.getSingelExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSingelExpression"


    // $ANTLR start "ruleSingelExpression"
    // InternalXaaS.g:137:1: ruleSingelExpression : ( ( rule__SingelExpression__Alternatives ) ) ;
    public final void ruleSingelExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:141:2: ( ( ( rule__SingelExpression__Alternatives ) ) )
            // InternalXaaS.g:142:2: ( ( rule__SingelExpression__Alternatives ) )
            {
            // InternalXaaS.g:142:2: ( ( rule__SingelExpression__Alternatives ) )
            // InternalXaaS.g:143:3: ( rule__SingelExpression__Alternatives )
            {
             before(grammarAccess.getSingelExpressionAccess().getAlternatives()); 
            // InternalXaaS.g:144:3: ( rule__SingelExpression__Alternatives )
            // InternalXaaS.g:144:4: rule__SingelExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SingelExpression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSingelExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSingelExpression"


    // $ANTLR start "entryRuleConfiguration"
    // InternalXaaS.g:153:1: entryRuleConfiguration : ruleConfiguration EOF ;
    public final void entryRuleConfiguration() throws RecognitionException {
        try {
            // InternalXaaS.g:154:1: ( ruleConfiguration EOF )
            // InternalXaaS.g:155:1: ruleConfiguration EOF
            {
             before(grammarAccess.getConfigurationRule()); 
            pushFollow(FOLLOW_1);
            ruleConfiguration();

            state._fsp--;

             after(grammarAccess.getConfigurationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConfiguration"


    // $ANTLR start "ruleConfiguration"
    // InternalXaaS.g:162:1: ruleConfiguration : ( ( rule__Configuration__Group__0 ) ) ;
    public final void ruleConfiguration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:166:2: ( ( ( rule__Configuration__Group__0 ) ) )
            // InternalXaaS.g:167:2: ( ( rule__Configuration__Group__0 ) )
            {
            // InternalXaaS.g:167:2: ( ( rule__Configuration__Group__0 ) )
            // InternalXaaS.g:168:3: ( rule__Configuration__Group__0 )
            {
             before(grammarAccess.getConfigurationAccess().getGroup()); 
            // InternalXaaS.g:169:3: ( rule__Configuration__Group__0 )
            // InternalXaaS.g:169:4: rule__Configuration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConfiguration"


    // $ANTLR start "entryRuleTopology"
    // InternalXaaS.g:178:1: entryRuleTopology : ruleTopology EOF ;
    public final void entryRuleTopology() throws RecognitionException {
        try {
            // InternalXaaS.g:179:1: ( ruleTopology EOF )
            // InternalXaaS.g:180:1: ruleTopology EOF
            {
             before(grammarAccess.getTopologyRule()); 
            pushFollow(FOLLOW_1);
            ruleTopology();

            state._fsp--;

             after(grammarAccess.getTopologyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTopology"


    // $ANTLR start "ruleTopology"
    // InternalXaaS.g:187:1: ruleTopology : ( ( rule__Topology__Group__0 ) ) ;
    public final void ruleTopology() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:191:2: ( ( ( rule__Topology__Group__0 ) ) )
            // InternalXaaS.g:192:2: ( ( rule__Topology__Group__0 ) )
            {
            // InternalXaaS.g:192:2: ( ( rule__Topology__Group__0 ) )
            // InternalXaaS.g:193:3: ( rule__Topology__Group__0 )
            {
             before(grammarAccess.getTopologyAccess().getGroup()); 
            // InternalXaaS.g:194:3: ( rule__Topology__Group__0 )
            // InternalXaaS.g:194:4: rule__Topology__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Topology__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTopologyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTopology"


    // $ANTLR start "entryRuleEString"
    // InternalXaaS.g:203:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalXaaS.g:204:1: ( ruleEString EOF )
            // InternalXaaS.g:205:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalXaaS.g:212:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:216:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalXaaS.g:217:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalXaaS.g:217:2: ( ( rule__EString__Alternatives ) )
            // InternalXaaS.g:218:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalXaaS.g:219:3: ( rule__EString__Alternatives )
            // InternalXaaS.g:219:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEValue"
    // InternalXaaS.g:228:1: entryRuleEValue : ruleEValue EOF ;
    public final void entryRuleEValue() throws RecognitionException {
        try {
            // InternalXaaS.g:229:1: ( ruleEValue EOF )
            // InternalXaaS.g:230:1: ruleEValue EOF
            {
             before(grammarAccess.getEValueRule()); 
            pushFollow(FOLLOW_1);
            ruleEValue();

            state._fsp--;

             after(grammarAccess.getEValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEValue"


    // $ANTLR start "ruleEValue"
    // InternalXaaS.g:237:1: ruleEValue : ( ( rule__EValue__Alternatives ) ) ;
    public final void ruleEValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:241:2: ( ( ( rule__EValue__Alternatives ) ) )
            // InternalXaaS.g:242:2: ( ( rule__EValue__Alternatives ) )
            {
            // InternalXaaS.g:242:2: ( ( rule__EValue__Alternatives ) )
            // InternalXaaS.g:243:3: ( rule__EValue__Alternatives )
            {
             before(grammarAccess.getEValueAccess().getAlternatives()); 
            // InternalXaaS.g:244:3: ( rule__EValue__Alternatives )
            // InternalXaaS.g:244:4: rule__EValue__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEValue"


    // $ANTLR start "entryRuleNode"
    // InternalXaaS.g:253:1: entryRuleNode : ruleNode EOF ;
    public final void entryRuleNode() throws RecognitionException {
        try {
            // InternalXaaS.g:254:1: ( ruleNode EOF )
            // InternalXaaS.g:255:1: ruleNode EOF
            {
             before(grammarAccess.getNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleNode();

            state._fsp--;

             after(grammarAccess.getNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNode"


    // $ANTLR start "ruleNode"
    // InternalXaaS.g:262:1: ruleNode : ( ( rule__Node__Group__0 ) ) ;
    public final void ruleNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:266:2: ( ( ( rule__Node__Group__0 ) ) )
            // InternalXaaS.g:267:2: ( ( rule__Node__Group__0 ) )
            {
            // InternalXaaS.g:267:2: ( ( rule__Node__Group__0 ) )
            // InternalXaaS.g:268:3: ( rule__Node__Group__0 )
            {
             before(grammarAccess.getNodeAccess().getGroup()); 
            // InternalXaaS.g:269:3: ( rule__Node__Group__0 )
            // InternalXaaS.g:269:4: rule__Node__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Node__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNodeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNode"


    // $ANTLR start "entryRuleRelationship"
    // InternalXaaS.g:278:1: entryRuleRelationship : ruleRelationship EOF ;
    public final void entryRuleRelationship() throws RecognitionException {
        try {
            // InternalXaaS.g:279:1: ( ruleRelationship EOF )
            // InternalXaaS.g:280:1: ruleRelationship EOF
            {
             before(grammarAccess.getRelationshipRule()); 
            pushFollow(FOLLOW_1);
            ruleRelationship();

            state._fsp--;

             after(grammarAccess.getRelationshipRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRelationship"


    // $ANTLR start "ruleRelationship"
    // InternalXaaS.g:287:1: ruleRelationship : ( ( rule__Relationship__Group__0 ) ) ;
    public final void ruleRelationship() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:291:2: ( ( ( rule__Relationship__Group__0 ) ) )
            // InternalXaaS.g:292:2: ( ( rule__Relationship__Group__0 ) )
            {
            // InternalXaaS.g:292:2: ( ( rule__Relationship__Group__0 ) )
            // InternalXaaS.g:293:3: ( rule__Relationship__Group__0 )
            {
             before(grammarAccess.getRelationshipAccess().getGroup()); 
            // InternalXaaS.g:294:3: ( rule__Relationship__Group__0 )
            // InternalXaaS.g:294:4: rule__Relationship__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRelationship"


    // $ANTLR start "entryRuleNodeType"
    // InternalXaaS.g:303:1: entryRuleNodeType : ruleNodeType EOF ;
    public final void entryRuleNodeType() throws RecognitionException {
        try {
            // InternalXaaS.g:304:1: ( ruleNodeType EOF )
            // InternalXaaS.g:305:1: ruleNodeType EOF
            {
             before(grammarAccess.getNodeTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleNodeType();

            state._fsp--;

             after(grammarAccess.getNodeTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNodeType"


    // $ANTLR start "ruleNodeType"
    // InternalXaaS.g:312:1: ruleNodeType : ( ( rule__NodeType__Group__0 ) ) ;
    public final void ruleNodeType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:316:2: ( ( ( rule__NodeType__Group__0 ) ) )
            // InternalXaaS.g:317:2: ( ( rule__NodeType__Group__0 ) )
            {
            // InternalXaaS.g:317:2: ( ( rule__NodeType__Group__0 ) )
            // InternalXaaS.g:318:3: ( rule__NodeType__Group__0 )
            {
             before(grammarAccess.getNodeTypeAccess().getGroup()); 
            // InternalXaaS.g:319:3: ( rule__NodeType__Group__0 )
            // InternalXaaS.g:319:4: rule__NodeType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNodeTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNodeType"


    // $ANTLR start "entryRuleAttribute"
    // InternalXaaS.g:328:1: entryRuleAttribute : ruleAttribute EOF ;
    public final void entryRuleAttribute() throws RecognitionException {
        try {
            // InternalXaaS.g:329:1: ( ruleAttribute EOF )
            // InternalXaaS.g:330:1: ruleAttribute EOF
            {
             before(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalXaaS.g:337:1: ruleAttribute : ( ( rule__Attribute__Group__0 ) ) ;
    public final void ruleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:341:2: ( ( ( rule__Attribute__Group__0 ) ) )
            // InternalXaaS.g:342:2: ( ( rule__Attribute__Group__0 ) )
            {
            // InternalXaaS.g:342:2: ( ( rule__Attribute__Group__0 ) )
            // InternalXaaS.g:343:3: ( rule__Attribute__Group__0 )
            {
             before(grammarAccess.getAttributeAccess().getGroup()); 
            // InternalXaaS.g:344:3: ( rule__Attribute__Group__0 )
            // InternalXaaS.g:344:4: rule__Attribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleConstraint"
    // InternalXaaS.g:353:1: entryRuleConstraint : ruleConstraint EOF ;
    public final void entryRuleConstraint() throws RecognitionException {
        try {
            // InternalXaaS.g:354:1: ( ruleConstraint EOF )
            // InternalXaaS.g:355:1: ruleConstraint EOF
            {
             before(grammarAccess.getConstraintRule()); 
            pushFollow(FOLLOW_1);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getConstraintRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // InternalXaaS.g:362:1: ruleConstraint : ( ( rule__Constraint__Group__0 ) ) ;
    public final void ruleConstraint() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:366:2: ( ( ( rule__Constraint__Group__0 ) ) )
            // InternalXaaS.g:367:2: ( ( rule__Constraint__Group__0 ) )
            {
            // InternalXaaS.g:367:2: ( ( rule__Constraint__Group__0 ) )
            // InternalXaaS.g:368:3: ( rule__Constraint__Group__0 )
            {
             before(grammarAccess.getConstraintAccess().getGroup()); 
            // InternalXaaS.g:369:3: ( rule__Constraint__Group__0 )
            // InternalXaaS.g:369:4: rule__Constraint__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleConstantAttributeType"
    // InternalXaaS.g:378:1: entryRuleConstantAttributeType : ruleConstantAttributeType EOF ;
    public final void entryRuleConstantAttributeType() throws RecognitionException {
        try {
            // InternalXaaS.g:379:1: ( ruleConstantAttributeType EOF )
            // InternalXaaS.g:380:1: ruleConstantAttributeType EOF
            {
             before(grammarAccess.getConstantAttributeTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleConstantAttributeType();

            state._fsp--;

             after(grammarAccess.getConstantAttributeTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstantAttributeType"


    // $ANTLR start "ruleConstantAttributeType"
    // InternalXaaS.g:387:1: ruleConstantAttributeType : ( ( rule__ConstantAttributeType__Group__0 ) ) ;
    public final void ruleConstantAttributeType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:391:2: ( ( ( rule__ConstantAttributeType__Group__0 ) ) )
            // InternalXaaS.g:392:2: ( ( rule__ConstantAttributeType__Group__0 ) )
            {
            // InternalXaaS.g:392:2: ( ( rule__ConstantAttributeType__Group__0 ) )
            // InternalXaaS.g:393:3: ( rule__ConstantAttributeType__Group__0 )
            {
             before(grammarAccess.getConstantAttributeTypeAccess().getGroup()); 
            // InternalXaaS.g:394:3: ( rule__ConstantAttributeType__Group__0 )
            // InternalXaaS.g:394:4: rule__ConstantAttributeType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ConstantAttributeType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConstantAttributeTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstantAttributeType"


    // $ANTLR start "entryRuleCalculatedAttributeType"
    // InternalXaaS.g:403:1: entryRuleCalculatedAttributeType : ruleCalculatedAttributeType EOF ;
    public final void entryRuleCalculatedAttributeType() throws RecognitionException {
        try {
            // InternalXaaS.g:404:1: ( ruleCalculatedAttributeType EOF )
            // InternalXaaS.g:405:1: ruleCalculatedAttributeType EOF
            {
             before(grammarAccess.getCalculatedAttributeTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleCalculatedAttributeType();

            state._fsp--;

             after(grammarAccess.getCalculatedAttributeTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCalculatedAttributeType"


    // $ANTLR start "ruleCalculatedAttributeType"
    // InternalXaaS.g:412:1: ruleCalculatedAttributeType : ( ( rule__CalculatedAttributeType__Group__0 ) ) ;
    public final void ruleCalculatedAttributeType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:416:2: ( ( ( rule__CalculatedAttributeType__Group__0 ) ) )
            // InternalXaaS.g:417:2: ( ( rule__CalculatedAttributeType__Group__0 ) )
            {
            // InternalXaaS.g:417:2: ( ( rule__CalculatedAttributeType__Group__0 ) )
            // InternalXaaS.g:418:3: ( rule__CalculatedAttributeType__Group__0 )
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getGroup()); 
            // InternalXaaS.g:419:3: ( rule__CalculatedAttributeType__Group__0 )
            // InternalXaaS.g:419:4: rule__CalculatedAttributeType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCalculatedAttributeTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCalculatedAttributeType"


    // $ANTLR start "entryRuleIntegerValueExpression"
    // InternalXaaS.g:428:1: entryRuleIntegerValueExpression : ruleIntegerValueExpression EOF ;
    public final void entryRuleIntegerValueExpression() throws RecognitionException {
        try {
            // InternalXaaS.g:429:1: ( ruleIntegerValueExpression EOF )
            // InternalXaaS.g:430:1: ruleIntegerValueExpression EOF
            {
             before(grammarAccess.getIntegerValueExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleIntegerValueExpression();

            state._fsp--;

             after(grammarAccess.getIntegerValueExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerValueExpression"


    // $ANTLR start "ruleIntegerValueExpression"
    // InternalXaaS.g:437:1: ruleIntegerValueExpression : ( ( rule__IntegerValueExpression__ValueAssignment ) ) ;
    public final void ruleIntegerValueExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:441:2: ( ( ( rule__IntegerValueExpression__ValueAssignment ) ) )
            // InternalXaaS.g:442:2: ( ( rule__IntegerValueExpression__ValueAssignment ) )
            {
            // InternalXaaS.g:442:2: ( ( rule__IntegerValueExpression__ValueAssignment ) )
            // InternalXaaS.g:443:3: ( rule__IntegerValueExpression__ValueAssignment )
            {
             before(grammarAccess.getIntegerValueExpressionAccess().getValueAssignment()); 
            // InternalXaaS.g:444:3: ( rule__IntegerValueExpression__ValueAssignment )
            // InternalXaaS.g:444:4: rule__IntegerValueExpression__ValueAssignment
            {
            pushFollow(FOLLOW_2);
            rule__IntegerValueExpression__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getIntegerValueExpressionAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerValueExpression"


    // $ANTLR start "entryRuleAttributeExpression"
    // InternalXaaS.g:453:1: entryRuleAttributeExpression : ruleAttributeExpression EOF ;
    public final void entryRuleAttributeExpression() throws RecognitionException {
        try {
            // InternalXaaS.g:454:1: ( ruleAttributeExpression EOF )
            // InternalXaaS.g:455:1: ruleAttributeExpression EOF
            {
             before(grammarAccess.getAttributeExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleAttributeExpression();

            state._fsp--;

             after(grammarAccess.getAttributeExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttributeExpression"


    // $ANTLR start "ruleAttributeExpression"
    // InternalXaaS.g:462:1: ruleAttributeExpression : ( ( rule__AttributeExpression__AttributeTypeAssignment ) ) ;
    public final void ruleAttributeExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:466:2: ( ( ( rule__AttributeExpression__AttributeTypeAssignment ) ) )
            // InternalXaaS.g:467:2: ( ( rule__AttributeExpression__AttributeTypeAssignment ) )
            {
            // InternalXaaS.g:467:2: ( ( rule__AttributeExpression__AttributeTypeAssignment ) )
            // InternalXaaS.g:468:3: ( rule__AttributeExpression__AttributeTypeAssignment )
            {
             before(grammarAccess.getAttributeExpressionAccess().getAttributeTypeAssignment()); 
            // InternalXaaS.g:469:3: ( rule__AttributeExpression__AttributeTypeAssignment )
            // InternalXaaS.g:469:4: rule__AttributeExpression__AttributeTypeAssignment
            {
            pushFollow(FOLLOW_2);
            rule__AttributeExpression__AttributeTypeAssignment();

            state._fsp--;


            }

             after(grammarAccess.getAttributeExpressionAccess().getAttributeTypeAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttributeExpression"


    // $ANTLR start "entryRuleBinaryExpression"
    // InternalXaaS.g:478:1: entryRuleBinaryExpression : ruleBinaryExpression EOF ;
    public final void entryRuleBinaryExpression() throws RecognitionException {
        try {
            // InternalXaaS.g:479:1: ( ruleBinaryExpression EOF )
            // InternalXaaS.g:480:1: ruleBinaryExpression EOF
            {
             before(grammarAccess.getBinaryExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleBinaryExpression();

            state._fsp--;

             after(grammarAccess.getBinaryExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinaryExpression"


    // $ANTLR start "ruleBinaryExpression"
    // InternalXaaS.g:487:1: ruleBinaryExpression : ( ( rule__BinaryExpression__Group__0 ) ) ;
    public final void ruleBinaryExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:491:2: ( ( ( rule__BinaryExpression__Group__0 ) ) )
            // InternalXaaS.g:492:2: ( ( rule__BinaryExpression__Group__0 ) )
            {
            // InternalXaaS.g:492:2: ( ( rule__BinaryExpression__Group__0 ) )
            // InternalXaaS.g:493:3: ( rule__BinaryExpression__Group__0 )
            {
             before(grammarAccess.getBinaryExpressionAccess().getGroup()); 
            // InternalXaaS.g:494:3: ( rule__BinaryExpression__Group__0 )
            // InternalXaaS.g:494:4: rule__BinaryExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BinaryExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBinaryExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinaryExpression"


    // $ANTLR start "entryRuleAggregationExpression"
    // InternalXaaS.g:503:1: entryRuleAggregationExpression : ruleAggregationExpression EOF ;
    public final void entryRuleAggregationExpression() throws RecognitionException {
        try {
            // InternalXaaS.g:504:1: ( ruleAggregationExpression EOF )
            // InternalXaaS.g:505:1: ruleAggregationExpression EOF
            {
             before(grammarAccess.getAggregationExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleAggregationExpression();

            state._fsp--;

             after(grammarAccess.getAggregationExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAggregationExpression"


    // $ANTLR start "ruleAggregationExpression"
    // InternalXaaS.g:512:1: ruleAggregationExpression : ( ( rule__AggregationExpression__Group__0 ) ) ;
    public final void ruleAggregationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:516:2: ( ( ( rule__AggregationExpression__Group__0 ) ) )
            // InternalXaaS.g:517:2: ( ( rule__AggregationExpression__Group__0 ) )
            {
            // InternalXaaS.g:517:2: ( ( rule__AggregationExpression__Group__0 ) )
            // InternalXaaS.g:518:3: ( rule__AggregationExpression__Group__0 )
            {
             before(grammarAccess.getAggregationExpressionAccess().getGroup()); 
            // InternalXaaS.g:519:3: ( rule__AggregationExpression__Group__0 )
            // InternalXaaS.g:519:4: rule__AggregationExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AggregationExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAggregationExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAggregationExpression"


    // $ANTLR start "entryRuleNbConnectionExpression"
    // InternalXaaS.g:528:1: entryRuleNbConnectionExpression : ruleNbConnectionExpression EOF ;
    public final void entryRuleNbConnectionExpression() throws RecognitionException {
        try {
            // InternalXaaS.g:529:1: ( ruleNbConnectionExpression EOF )
            // InternalXaaS.g:530:1: ruleNbConnectionExpression EOF
            {
             before(grammarAccess.getNbConnectionExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleNbConnectionExpression();

            state._fsp--;

             after(grammarAccess.getNbConnectionExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNbConnectionExpression"


    // $ANTLR start "ruleNbConnectionExpression"
    // InternalXaaS.g:537:1: ruleNbConnectionExpression : ( ( rule__NbConnectionExpression__Group__0 ) ) ;
    public final void ruleNbConnectionExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:541:2: ( ( ( rule__NbConnectionExpression__Group__0 ) ) )
            // InternalXaaS.g:542:2: ( ( rule__NbConnectionExpression__Group__0 ) )
            {
            // InternalXaaS.g:542:2: ( ( rule__NbConnectionExpression__Group__0 ) )
            // InternalXaaS.g:543:3: ( rule__NbConnectionExpression__Group__0 )
            {
             before(grammarAccess.getNbConnectionExpressionAccess().getGroup()); 
            // InternalXaaS.g:544:3: ( rule__NbConnectionExpression__Group__0 )
            // InternalXaaS.g:544:4: rule__NbConnectionExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NbConnectionExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNbConnectionExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNbConnectionExpression"


    // $ANTLR start "entryRuleCustomExpression"
    // InternalXaaS.g:553:1: entryRuleCustomExpression : ruleCustomExpression EOF ;
    public final void entryRuleCustomExpression() throws RecognitionException {
        try {
            // InternalXaaS.g:554:1: ( ruleCustomExpression EOF )
            // InternalXaaS.g:555:1: ruleCustomExpression EOF
            {
             before(grammarAccess.getCustomExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleCustomExpression();

            state._fsp--;

             after(grammarAccess.getCustomExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCustomExpression"


    // $ANTLR start "ruleCustomExpression"
    // InternalXaaS.g:562:1: ruleCustomExpression : ( ( rule__CustomExpression__Group__0 ) ) ;
    public final void ruleCustomExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:566:2: ( ( ( rule__CustomExpression__Group__0 ) ) )
            // InternalXaaS.g:567:2: ( ( rule__CustomExpression__Group__0 ) )
            {
            // InternalXaaS.g:567:2: ( ( rule__CustomExpression__Group__0 ) )
            // InternalXaaS.g:568:3: ( rule__CustomExpression__Group__0 )
            {
             before(grammarAccess.getCustomExpressionAccess().getGroup()); 
            // InternalXaaS.g:569:3: ( rule__CustomExpression__Group__0 )
            // InternalXaaS.g:569:4: rule__CustomExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CustomExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCustomExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCustomExpression"


    // $ANTLR start "entryRuleEInt"
    // InternalXaaS.g:578:1: entryRuleEInt : ruleEInt EOF ;
    public final void entryRuleEInt() throws RecognitionException {
        try {
            // InternalXaaS.g:579:1: ( ruleEInt EOF )
            // InternalXaaS.g:580:1: ruleEInt EOF
            {
             before(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getEIntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalXaaS.g:587:1: ruleEInt : ( ( rule__EInt__Group__0 ) ) ;
    public final void ruleEInt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:591:2: ( ( ( rule__EInt__Group__0 ) ) )
            // InternalXaaS.g:592:2: ( ( rule__EInt__Group__0 ) )
            {
            // InternalXaaS.g:592:2: ( ( rule__EInt__Group__0 ) )
            // InternalXaaS.g:593:3: ( rule__EInt__Group__0 )
            {
             before(grammarAccess.getEIntAccess().getGroup()); 
            // InternalXaaS.g:594:3: ( rule__EInt__Group__0 )
            // InternalXaaS.g:594:4: rule__EInt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEIntAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleRelationshipType"
    // InternalXaaS.g:603:1: entryRuleRelationshipType : ruleRelationshipType EOF ;
    public final void entryRuleRelationshipType() throws RecognitionException {
        try {
            // InternalXaaS.g:604:1: ( ruleRelationshipType EOF )
            // InternalXaaS.g:605:1: ruleRelationshipType EOF
            {
             before(grammarAccess.getRelationshipTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleRelationshipType();

            state._fsp--;

             after(grammarAccess.getRelationshipTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRelationshipType"


    // $ANTLR start "ruleRelationshipType"
    // InternalXaaS.g:612:1: ruleRelationshipType : ( ( rule__RelationshipType__Group__0 ) ) ;
    public final void ruleRelationshipType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:616:2: ( ( ( rule__RelationshipType__Group__0 ) ) )
            // InternalXaaS.g:617:2: ( ( rule__RelationshipType__Group__0 ) )
            {
            // InternalXaaS.g:617:2: ( ( rule__RelationshipType__Group__0 ) )
            // InternalXaaS.g:618:3: ( rule__RelationshipType__Group__0 )
            {
             before(grammarAccess.getRelationshipTypeAccess().getGroup()); 
            // InternalXaaS.g:619:3: ( rule__RelationshipType__Group__0 )
            // InternalXaaS.g:619:4: rule__RelationshipType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRelationshipType"


    // $ANTLR start "entryRuleEBoolean"
    // InternalXaaS.g:628:1: entryRuleEBoolean : ruleEBoolean EOF ;
    public final void entryRuleEBoolean() throws RecognitionException {
        try {
            // InternalXaaS.g:629:1: ( ruleEBoolean EOF )
            // InternalXaaS.g:630:1: ruleEBoolean EOF
            {
             before(grammarAccess.getEBooleanRule()); 
            pushFollow(FOLLOW_1);
            ruleEBoolean();

            state._fsp--;

             after(grammarAccess.getEBooleanRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEBoolean"


    // $ANTLR start "ruleEBoolean"
    // InternalXaaS.g:637:1: ruleEBoolean : ( ( rule__EBoolean__Alternatives ) ) ;
    public final void ruleEBoolean() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:641:2: ( ( ( rule__EBoolean__Alternatives ) ) )
            // InternalXaaS.g:642:2: ( ( rule__EBoolean__Alternatives ) )
            {
            // InternalXaaS.g:642:2: ( ( rule__EBoolean__Alternatives ) )
            // InternalXaaS.g:643:3: ( rule__EBoolean__Alternatives )
            {
             before(grammarAccess.getEBooleanAccess().getAlternatives()); 
            // InternalXaaS.g:644:3: ( rule__EBoolean__Alternatives )
            // InternalXaaS.g:644:4: rule__EBoolean__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EBoolean__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEBooleanAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEBoolean"


    // $ANTLR start "ruleAlgebraicOperator"
    // InternalXaaS.g:653:1: ruleAlgebraicOperator : ( ( rule__AlgebraicOperator__Alternatives ) ) ;
    public final void ruleAlgebraicOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:657:1: ( ( ( rule__AlgebraicOperator__Alternatives ) ) )
            // InternalXaaS.g:658:2: ( ( rule__AlgebraicOperator__Alternatives ) )
            {
            // InternalXaaS.g:658:2: ( ( rule__AlgebraicOperator__Alternatives ) )
            // InternalXaaS.g:659:3: ( rule__AlgebraicOperator__Alternatives )
            {
             before(grammarAccess.getAlgebraicOperatorAccess().getAlternatives()); 
            // InternalXaaS.g:660:3: ( rule__AlgebraicOperator__Alternatives )
            // InternalXaaS.g:660:4: rule__AlgebraicOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AlgebraicOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAlgebraicOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAlgebraicOperator"


    // $ANTLR start "ruleAggregationOperator"
    // InternalXaaS.g:669:1: ruleAggregationOperator : ( ( rule__AggregationOperator__Alternatives ) ) ;
    public final void ruleAggregationOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:673:1: ( ( ( rule__AggregationOperator__Alternatives ) ) )
            // InternalXaaS.g:674:2: ( ( rule__AggregationOperator__Alternatives ) )
            {
            // InternalXaaS.g:674:2: ( ( rule__AggregationOperator__Alternatives ) )
            // InternalXaaS.g:675:3: ( rule__AggregationOperator__Alternatives )
            {
             before(grammarAccess.getAggregationOperatorAccess().getAlternatives()); 
            // InternalXaaS.g:676:3: ( rule__AggregationOperator__Alternatives )
            // InternalXaaS.g:676:4: rule__AggregationOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AggregationOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAggregationOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAggregationOperator"


    // $ANTLR start "ruleDirectionKind"
    // InternalXaaS.g:685:1: ruleDirectionKind : ( ( rule__DirectionKind__Alternatives ) ) ;
    public final void ruleDirectionKind() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:689:1: ( ( ( rule__DirectionKind__Alternatives ) ) )
            // InternalXaaS.g:690:2: ( ( rule__DirectionKind__Alternatives ) )
            {
            // InternalXaaS.g:690:2: ( ( rule__DirectionKind__Alternatives ) )
            // InternalXaaS.g:691:3: ( rule__DirectionKind__Alternatives )
            {
             before(grammarAccess.getDirectionKindAccess().getAlternatives()); 
            // InternalXaaS.g:692:3: ( rule__DirectionKind__Alternatives )
            // InternalXaaS.g:692:4: rule__DirectionKind__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DirectionKind__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDirectionKindAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDirectionKind"


    // $ANTLR start "ruleComparisonOperator"
    // InternalXaaS.g:701:1: ruleComparisonOperator : ( ( rule__ComparisonOperator__Alternatives ) ) ;
    public final void ruleComparisonOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:705:1: ( ( ( rule__ComparisonOperator__Alternatives ) ) )
            // InternalXaaS.g:706:2: ( ( rule__ComparisonOperator__Alternatives ) )
            {
            // InternalXaaS.g:706:2: ( ( rule__ComparisonOperator__Alternatives ) )
            // InternalXaaS.g:707:3: ( rule__ComparisonOperator__Alternatives )
            {
             before(grammarAccess.getComparisonOperatorAccess().getAlternatives()); 
            // InternalXaaS.g:708:3: ( rule__ComparisonOperator__Alternatives )
            // InternalXaaS.g:708:4: rule__ComparisonOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ComparisonOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getComparisonOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComparisonOperator"


    // $ANTLR start "rule__AttributeType__Alternatives"
    // InternalXaaS.g:716:1: rule__AttributeType__Alternatives : ( ( ruleConstantAttributeType ) | ( ruleCalculatedAttributeType ) );
    public final void rule__AttributeType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:720:1: ( ( ruleConstantAttributeType ) | ( ruleCalculatedAttributeType ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==40) ) {
                alt1=1;
            }
            else if ( (LA1_0==47) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalXaaS.g:721:2: ( ruleConstantAttributeType )
                    {
                    // InternalXaaS.g:721:2: ( ruleConstantAttributeType )
                    // InternalXaaS.g:722:3: ruleConstantAttributeType
                    {
                     before(grammarAccess.getAttributeTypeAccess().getConstantAttributeTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleConstantAttributeType();

                    state._fsp--;

                     after(grammarAccess.getAttributeTypeAccess().getConstantAttributeTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:727:2: ( ruleCalculatedAttributeType )
                    {
                    // InternalXaaS.g:727:2: ( ruleCalculatedAttributeType )
                    // InternalXaaS.g:728:3: ruleCalculatedAttributeType
                    {
                     before(grammarAccess.getAttributeTypeAccess().getCalculatedAttributeTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleCalculatedAttributeType();

                    state._fsp--;

                     after(grammarAccess.getAttributeTypeAccess().getCalculatedAttributeTypeParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeType__Alternatives"


    // $ANTLR start "rule__Expression__Alternatives"
    // InternalXaaS.g:737:1: rule__Expression__Alternatives : ( ( ruleIntegerValueExpression ) | ( ruleAttributeExpression ) | ( ruleBinaryExpression ) | ( ruleAggregationExpression ) | ( ruleNbConnectionExpression ) | ( ruleCustomExpression ) );
    public final void rule__Expression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:741:1: ( ( ruleIntegerValueExpression ) | ( ruleAttributeExpression ) | ( ruleBinaryExpression ) | ( ruleAggregationExpression ) | ( ruleNbConnectionExpression ) | ( ruleCustomExpression ) )
            int alt2=6;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // InternalXaaS.g:742:2: ( ruleIntegerValueExpression )
                    {
                    // InternalXaaS.g:742:2: ( ruleIntegerValueExpression )
                    // InternalXaaS.g:743:3: ruleIntegerValueExpression
                    {
                     before(grammarAccess.getExpressionAccess().getIntegerValueExpressionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleIntegerValueExpression();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getIntegerValueExpressionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:748:2: ( ruleAttributeExpression )
                    {
                    // InternalXaaS.g:748:2: ( ruleAttributeExpression )
                    // InternalXaaS.g:749:3: ruleAttributeExpression
                    {
                     before(grammarAccess.getExpressionAccess().getAttributeExpressionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAttributeExpression();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getAttributeExpressionParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXaaS.g:754:2: ( ruleBinaryExpression )
                    {
                    // InternalXaaS.g:754:2: ( ruleBinaryExpression )
                    // InternalXaaS.g:755:3: ruleBinaryExpression
                    {
                     before(grammarAccess.getExpressionAccess().getBinaryExpressionParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleBinaryExpression();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getBinaryExpressionParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXaaS.g:760:2: ( ruleAggregationExpression )
                    {
                    // InternalXaaS.g:760:2: ( ruleAggregationExpression )
                    // InternalXaaS.g:761:3: ruleAggregationExpression
                    {
                     before(grammarAccess.getExpressionAccess().getAggregationExpressionParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleAggregationExpression();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getAggregationExpressionParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalXaaS.g:766:2: ( ruleNbConnectionExpression )
                    {
                    // InternalXaaS.g:766:2: ( ruleNbConnectionExpression )
                    // InternalXaaS.g:767:3: ruleNbConnectionExpression
                    {
                     before(grammarAccess.getExpressionAccess().getNbConnectionExpressionParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleNbConnectionExpression();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getNbConnectionExpressionParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalXaaS.g:772:2: ( ruleCustomExpression )
                    {
                    // InternalXaaS.g:772:2: ( ruleCustomExpression )
                    // InternalXaaS.g:773:3: ruleCustomExpression
                    {
                     before(grammarAccess.getExpressionAccess().getCustomExpressionParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleCustomExpression();

                    state._fsp--;

                     after(grammarAccess.getExpressionAccess().getCustomExpressionParserRuleCall_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Alternatives"


    // $ANTLR start "rule__SingelExpression__Alternatives"
    // InternalXaaS.g:782:1: rule__SingelExpression__Alternatives : ( ( ruleIntegerValueExpression ) | ( ruleAttributeExpression ) | ( ruleAggregationExpression ) | ( ruleNbConnectionExpression ) );
    public final void rule__SingelExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:786:1: ( ( ruleIntegerValueExpression ) | ( ruleAttributeExpression ) | ( ruleAggregationExpression ) | ( ruleNbConnectionExpression ) )
            int alt3=4;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case 15:
                {
                alt3=1;
                }
                break;
            case RULE_STRING:
            case RULE_ID:
                {
                alt3=2;
                }
                break;
            case 17:
            case 18:
            case 19:
                {
                alt3=3;
                }
                break;
            case 52:
                {
                alt3=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalXaaS.g:787:2: ( ruleIntegerValueExpression )
                    {
                    // InternalXaaS.g:787:2: ( ruleIntegerValueExpression )
                    // InternalXaaS.g:788:3: ruleIntegerValueExpression
                    {
                     before(grammarAccess.getSingelExpressionAccess().getIntegerValueExpressionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleIntegerValueExpression();

                    state._fsp--;

                     after(grammarAccess.getSingelExpressionAccess().getIntegerValueExpressionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:793:2: ( ruleAttributeExpression )
                    {
                    // InternalXaaS.g:793:2: ( ruleAttributeExpression )
                    // InternalXaaS.g:794:3: ruleAttributeExpression
                    {
                     before(grammarAccess.getSingelExpressionAccess().getAttributeExpressionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAttributeExpression();

                    state._fsp--;

                     after(grammarAccess.getSingelExpressionAccess().getAttributeExpressionParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXaaS.g:799:2: ( ruleAggregationExpression )
                    {
                    // InternalXaaS.g:799:2: ( ruleAggregationExpression )
                    // InternalXaaS.g:800:3: ruleAggregationExpression
                    {
                     before(grammarAccess.getSingelExpressionAccess().getAggregationExpressionParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleAggregationExpression();

                    state._fsp--;

                     after(grammarAccess.getSingelExpressionAccess().getAggregationExpressionParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXaaS.g:805:2: ( ruleNbConnectionExpression )
                    {
                    // InternalXaaS.g:805:2: ( ruleNbConnectionExpression )
                    // InternalXaaS.g:806:3: ruleNbConnectionExpression
                    {
                     before(grammarAccess.getSingelExpressionAccess().getNbConnectionExpressionParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleNbConnectionExpression();

                    state._fsp--;

                     after(grammarAccess.getSingelExpressionAccess().getNbConnectionExpressionParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingelExpression__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalXaaS.g:815:1: rule__EString__Alternatives : ( ( ( rule__EString__Group_0__0 ) ) | ( RULE_STRING ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:819:1: ( ( ( rule__EString__Group_0__0 ) ) | ( RULE_STRING ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_STRING) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalXaaS.g:820:2: ( ( rule__EString__Group_0__0 ) )
                    {
                    // InternalXaaS.g:820:2: ( ( rule__EString__Group_0__0 ) )
                    // InternalXaaS.g:821:3: ( rule__EString__Group_0__0 )
                    {
                     before(grammarAccess.getEStringAccess().getGroup_0()); 
                    // InternalXaaS.g:822:3: ( rule__EString__Group_0__0 )
                    // InternalXaaS.g:822:4: rule__EString__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__EString__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getEStringAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:826:2: ( RULE_STRING )
                    {
                    // InternalXaaS.g:826:2: ( RULE_STRING )
                    // InternalXaaS.g:827:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_1()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__EValue__Alternatives"
    // InternalXaaS.g:836:1: rule__EValue__Alternatives : ( ( RULE_STRING ) | ( ruleEInt ) );
    public final void rule__EValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:840:1: ( ( RULE_STRING ) | ( ruleEInt ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_STRING) ) {
                alt5=1;
            }
            else if ( (LA5_0==RULE_INT||LA5_0==15) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalXaaS.g:841:2: ( RULE_STRING )
                    {
                    // InternalXaaS.g:841:2: ( RULE_STRING )
                    // InternalXaaS.g:842:3: RULE_STRING
                    {
                     before(grammarAccess.getEValueAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEValueAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:847:2: ( ruleEInt )
                    {
                    // InternalXaaS.g:847:2: ( ruleEInt )
                    // InternalXaaS.g:848:3: ruleEInt
                    {
                     before(grammarAccess.getEValueAccess().getEIntParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleEInt();

                    state._fsp--;

                     after(grammarAccess.getEValueAccess().getEIntParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EValue__Alternatives"


    // $ANTLR start "rule__EBoolean__Alternatives"
    // InternalXaaS.g:857:1: rule__EBoolean__Alternatives : ( ( 'true' ) | ( 'false' ) );
    public final void rule__EBoolean__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:861:1: ( ( 'true' ) | ( 'false' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==11) ) {
                alt6=1;
            }
            else if ( (LA6_0==12) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalXaaS.g:862:2: ( 'true' )
                    {
                    // InternalXaaS.g:862:2: ( 'true' )
                    // InternalXaaS.g:863:3: 'true'
                    {
                     before(grammarAccess.getEBooleanAccess().getTrueKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getEBooleanAccess().getTrueKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:868:2: ( 'false' )
                    {
                    // InternalXaaS.g:868:2: ( 'false' )
                    // InternalXaaS.g:869:3: 'false'
                    {
                     before(grammarAccess.getEBooleanAccess().getFalseKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getEBooleanAccess().getFalseKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EBoolean__Alternatives"


    // $ANTLR start "rule__AlgebraicOperator__Alternatives"
    // InternalXaaS.g:878:1: rule__AlgebraicOperator__Alternatives : ( ( ( '+' ) ) | ( ( '*' ) ) | ( ( '-' ) ) | ( ( '/' ) ) );
    public final void rule__AlgebraicOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:882:1: ( ( ( '+' ) ) | ( ( '*' ) ) | ( ( '-' ) ) | ( ( '/' ) ) )
            int alt7=4;
            switch ( input.LA(1) ) {
            case 13:
                {
                alt7=1;
                }
                break;
            case 14:
                {
                alt7=2;
                }
                break;
            case 15:
                {
                alt7=3;
                }
                break;
            case 16:
                {
                alt7=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalXaaS.g:883:2: ( ( '+' ) )
                    {
                    // InternalXaaS.g:883:2: ( ( '+' ) )
                    // InternalXaaS.g:884:3: ( '+' )
                    {
                     before(grammarAccess.getAlgebraicOperatorAccess().getSumEnumLiteralDeclaration_0()); 
                    // InternalXaaS.g:885:3: ( '+' )
                    // InternalXaaS.g:885:4: '+'
                    {
                    match(input,13,FOLLOW_2); 

                    }

                     after(grammarAccess.getAlgebraicOperatorAccess().getSumEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:889:2: ( ( '*' ) )
                    {
                    // InternalXaaS.g:889:2: ( ( '*' ) )
                    // InternalXaaS.g:890:3: ( '*' )
                    {
                     before(grammarAccess.getAlgebraicOperatorAccess().getProdEnumLiteralDeclaration_1()); 
                    // InternalXaaS.g:891:3: ( '*' )
                    // InternalXaaS.g:891:4: '*'
                    {
                    match(input,14,FOLLOW_2); 

                    }

                     after(grammarAccess.getAlgebraicOperatorAccess().getProdEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXaaS.g:895:2: ( ( '-' ) )
                    {
                    // InternalXaaS.g:895:2: ( ( '-' ) )
                    // InternalXaaS.g:896:3: ( '-' )
                    {
                     before(grammarAccess.getAlgebraicOperatorAccess().getMinusEnumLiteralDeclaration_2()); 
                    // InternalXaaS.g:897:3: ( '-' )
                    // InternalXaaS.g:897:4: '-'
                    {
                    match(input,15,FOLLOW_2); 

                    }

                     after(grammarAccess.getAlgebraicOperatorAccess().getMinusEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXaaS.g:901:2: ( ( '/' ) )
                    {
                    // InternalXaaS.g:901:2: ( ( '/' ) )
                    // InternalXaaS.g:902:3: ( '/' )
                    {
                     before(grammarAccess.getAlgebraicOperatorAccess().getDivEnumLiteralDeclaration_3()); 
                    // InternalXaaS.g:903:3: ( '/' )
                    // InternalXaaS.g:903:4: '/'
                    {
                    match(input,16,FOLLOW_2); 

                    }

                     after(grammarAccess.getAlgebraicOperatorAccess().getDivEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AlgebraicOperator__Alternatives"


    // $ANTLR start "rule__AggregationOperator__Alternatives"
    // InternalXaaS.g:911:1: rule__AggregationOperator__Alternatives : ( ( ( 'Sum' ) ) | ( ( 'Min' ) ) | ( ( 'Max' ) ) );
    public final void rule__AggregationOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:915:1: ( ( ( 'Sum' ) ) | ( ( 'Min' ) ) | ( ( 'Max' ) ) )
            int alt8=3;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt8=1;
                }
                break;
            case 18:
                {
                alt8=2;
                }
                break;
            case 19:
                {
                alt8=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalXaaS.g:916:2: ( ( 'Sum' ) )
                    {
                    // InternalXaaS.g:916:2: ( ( 'Sum' ) )
                    // InternalXaaS.g:917:3: ( 'Sum' )
                    {
                     before(grammarAccess.getAggregationOperatorAccess().getSumEnumLiteralDeclaration_0()); 
                    // InternalXaaS.g:918:3: ( 'Sum' )
                    // InternalXaaS.g:918:4: 'Sum'
                    {
                    match(input,17,FOLLOW_2); 

                    }

                     after(grammarAccess.getAggregationOperatorAccess().getSumEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:922:2: ( ( 'Min' ) )
                    {
                    // InternalXaaS.g:922:2: ( ( 'Min' ) )
                    // InternalXaaS.g:923:3: ( 'Min' )
                    {
                     before(grammarAccess.getAggregationOperatorAccess().getMinEnumLiteralDeclaration_1()); 
                    // InternalXaaS.g:924:3: ( 'Min' )
                    // InternalXaaS.g:924:4: 'Min'
                    {
                    match(input,18,FOLLOW_2); 

                    }

                     after(grammarAccess.getAggregationOperatorAccess().getMinEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXaaS.g:928:2: ( ( 'Max' ) )
                    {
                    // InternalXaaS.g:928:2: ( ( 'Max' ) )
                    // InternalXaaS.g:929:3: ( 'Max' )
                    {
                     before(grammarAccess.getAggregationOperatorAccess().getMaxEnumLiteralDeclaration_2()); 
                    // InternalXaaS.g:930:3: ( 'Max' )
                    // InternalXaaS.g:930:4: 'Max'
                    {
                    match(input,19,FOLLOW_2); 

                    }

                     after(grammarAccess.getAggregationOperatorAccess().getMaxEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationOperator__Alternatives"


    // $ANTLR start "rule__DirectionKind__Alternatives"
    // InternalXaaS.g:938:1: rule__DirectionKind__Alternatives : ( ( ( 'Pred' ) ) | ( ( 'Succ' ) ) );
    public final void rule__DirectionKind__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:942:1: ( ( ( 'Pred' ) ) | ( ( 'Succ' ) ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==20) ) {
                alt9=1;
            }
            else if ( (LA9_0==21) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalXaaS.g:943:2: ( ( 'Pred' ) )
                    {
                    // InternalXaaS.g:943:2: ( ( 'Pred' ) )
                    // InternalXaaS.g:944:3: ( 'Pred' )
                    {
                     before(grammarAccess.getDirectionKindAccess().getPredecessorSourceEnumLiteralDeclaration_0()); 
                    // InternalXaaS.g:945:3: ( 'Pred' )
                    // InternalXaaS.g:945:4: 'Pred'
                    {
                    match(input,20,FOLLOW_2); 

                    }

                     after(grammarAccess.getDirectionKindAccess().getPredecessorSourceEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:949:2: ( ( 'Succ' ) )
                    {
                    // InternalXaaS.g:949:2: ( ( 'Succ' ) )
                    // InternalXaaS.g:950:3: ( 'Succ' )
                    {
                     before(grammarAccess.getDirectionKindAccess().getSuccessorTargetEnumLiteralDeclaration_1()); 
                    // InternalXaaS.g:951:3: ( 'Succ' )
                    // InternalXaaS.g:951:4: 'Succ'
                    {
                    match(input,21,FOLLOW_2); 

                    }

                     after(grammarAccess.getDirectionKindAccess().getSuccessorTargetEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DirectionKind__Alternatives"


    // $ANTLR start "rule__ComparisonOperator__Alternatives"
    // InternalXaaS.g:959:1: rule__ComparisonOperator__Alternatives : ( ( ( 'equal' ) ) | ( ( 'less_than' ) ) | ( ( 'greater_than' ) ) | ( ( 'less_or_equal' ) ) | ( ( 'greater_or_equal' ) ) );
    public final void rule__ComparisonOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:963:1: ( ( ( 'equal' ) ) | ( ( 'less_than' ) ) | ( ( 'greater_than' ) ) | ( ( 'less_or_equal' ) ) | ( ( 'greater_or_equal' ) ) )
            int alt10=5;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt10=1;
                }
                break;
            case 23:
                {
                alt10=2;
                }
                break;
            case 24:
                {
                alt10=3;
                }
                break;
            case 25:
                {
                alt10=4;
                }
                break;
            case 26:
                {
                alt10=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalXaaS.g:964:2: ( ( 'equal' ) )
                    {
                    // InternalXaaS.g:964:2: ( ( 'equal' ) )
                    // InternalXaaS.g:965:3: ( 'equal' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getEqualEnumLiteralDeclaration_0()); 
                    // InternalXaaS.g:966:3: ( 'equal' )
                    // InternalXaaS.g:966:4: 'equal'
                    {
                    match(input,22,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getEqualEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXaaS.g:970:2: ( ( 'less_than' ) )
                    {
                    // InternalXaaS.g:970:2: ( ( 'less_than' ) )
                    // InternalXaaS.g:971:3: ( 'less_than' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getLesserEnumLiteralDeclaration_1()); 
                    // InternalXaaS.g:972:3: ( 'less_than' )
                    // InternalXaaS.g:972:4: 'less_than'
                    {
                    match(input,23,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getLesserEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXaaS.g:976:2: ( ( 'greater_than' ) )
                    {
                    // InternalXaaS.g:976:2: ( ( 'greater_than' ) )
                    // InternalXaaS.g:977:3: ( 'greater_than' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getGreaterEnumLiteralDeclaration_2()); 
                    // InternalXaaS.g:978:3: ( 'greater_than' )
                    // InternalXaaS.g:978:4: 'greater_than'
                    {
                    match(input,24,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getGreaterEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXaaS.g:982:2: ( ( 'less_or_equal' ) )
                    {
                    // InternalXaaS.g:982:2: ( ( 'less_or_equal' ) )
                    // InternalXaaS.g:983:3: ( 'less_or_equal' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getLesserOrEqualEnumLiteralDeclaration_3()); 
                    // InternalXaaS.g:984:3: ( 'less_or_equal' )
                    // InternalXaaS.g:984:4: 'less_or_equal'
                    {
                    match(input,25,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getLesserOrEqualEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalXaaS.g:988:2: ( ( 'greater_or_equal' ) )
                    {
                    // InternalXaaS.g:988:2: ( ( 'greater_or_equal' ) )
                    // InternalXaaS.g:989:3: ( 'greater_or_equal' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getGreaterOrEqualEnumLiteralDeclaration_4()); 
                    // InternalXaaS.g:990:3: ( 'greater_or_equal' )
                    // InternalXaaS.g:990:4: 'greater_or_equal'
                    {
                    match(input,26,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getGreaterOrEqualEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonOperator__Alternatives"


    // $ANTLR start "rule__XaasYamlLikeTosca__Group__0"
    // InternalXaaS.g:998:1: rule__XaasYamlLikeTosca__Group__0 : rule__XaasYamlLikeTosca__Group__0__Impl rule__XaasYamlLikeTosca__Group__1 ;
    public final void rule__XaasYamlLikeTosca__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1002:1: ( rule__XaasYamlLikeTosca__Group__0__Impl rule__XaasYamlLikeTosca__Group__1 )
            // InternalXaaS.g:1003:2: rule__XaasYamlLikeTosca__Group__0__Impl rule__XaasYamlLikeTosca__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__XaasYamlLikeTosca__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XaasYamlLikeTosca__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XaasYamlLikeTosca__Group__0"


    // $ANTLR start "rule__XaasYamlLikeTosca__Group__0__Impl"
    // InternalXaaS.g:1010:1: rule__XaasYamlLikeTosca__Group__0__Impl : ( ( rule__XaasYamlLikeTosca__TopologyAssignment_0 ) ) ;
    public final void rule__XaasYamlLikeTosca__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1014:1: ( ( ( rule__XaasYamlLikeTosca__TopologyAssignment_0 ) ) )
            // InternalXaaS.g:1015:1: ( ( rule__XaasYamlLikeTosca__TopologyAssignment_0 ) )
            {
            // InternalXaaS.g:1015:1: ( ( rule__XaasYamlLikeTosca__TopologyAssignment_0 ) )
            // InternalXaaS.g:1016:2: ( rule__XaasYamlLikeTosca__TopologyAssignment_0 )
            {
             before(grammarAccess.getXaasYamlLikeToscaAccess().getTopologyAssignment_0()); 
            // InternalXaaS.g:1017:2: ( rule__XaasYamlLikeTosca__TopologyAssignment_0 )
            // InternalXaaS.g:1017:3: rule__XaasYamlLikeTosca__TopologyAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__XaasYamlLikeTosca__TopologyAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getXaasYamlLikeToscaAccess().getTopologyAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XaasYamlLikeTosca__Group__0__Impl"


    // $ANTLR start "rule__XaasYamlLikeTosca__Group__1"
    // InternalXaaS.g:1025:1: rule__XaasYamlLikeTosca__Group__1 : rule__XaasYamlLikeTosca__Group__1__Impl ;
    public final void rule__XaasYamlLikeTosca__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1029:1: ( rule__XaasYamlLikeTosca__Group__1__Impl )
            // InternalXaaS.g:1030:2: rule__XaasYamlLikeTosca__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XaasYamlLikeTosca__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XaasYamlLikeTosca__Group__1"


    // $ANTLR start "rule__XaasYamlLikeTosca__Group__1__Impl"
    // InternalXaaS.g:1036:1: rule__XaasYamlLikeTosca__Group__1__Impl : ( ( rule__XaasYamlLikeTosca__ConfigurationAssignment_1 ) ) ;
    public final void rule__XaasYamlLikeTosca__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1040:1: ( ( ( rule__XaasYamlLikeTosca__ConfigurationAssignment_1 ) ) )
            // InternalXaaS.g:1041:1: ( ( rule__XaasYamlLikeTosca__ConfigurationAssignment_1 ) )
            {
            // InternalXaaS.g:1041:1: ( ( rule__XaasYamlLikeTosca__ConfigurationAssignment_1 ) )
            // InternalXaaS.g:1042:2: ( rule__XaasYamlLikeTosca__ConfigurationAssignment_1 )
            {
             before(grammarAccess.getXaasYamlLikeToscaAccess().getConfigurationAssignment_1()); 
            // InternalXaaS.g:1043:2: ( rule__XaasYamlLikeTosca__ConfigurationAssignment_1 )
            // InternalXaaS.g:1043:3: rule__XaasYamlLikeTosca__ConfigurationAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__XaasYamlLikeTosca__ConfigurationAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getXaasYamlLikeToscaAccess().getConfigurationAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XaasYamlLikeTosca__Group__1__Impl"


    // $ANTLR start "rule__Configuration__Group__0"
    // InternalXaaS.g:1052:1: rule__Configuration__Group__0 : rule__Configuration__Group__0__Impl rule__Configuration__Group__1 ;
    public final void rule__Configuration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1056:1: ( rule__Configuration__Group__0__Impl rule__Configuration__Group__1 )
            // InternalXaaS.g:1057:2: rule__Configuration__Group__0__Impl rule__Configuration__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Configuration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0"


    // $ANTLR start "rule__Configuration__Group__0__Impl"
    // InternalXaaS.g:1064:1: rule__Configuration__Group__0__Impl : ( 'Configuration' ) ;
    public final void rule__Configuration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1068:1: ( ( 'Configuration' ) )
            // InternalXaaS.g:1069:1: ( 'Configuration' )
            {
            // InternalXaaS.g:1069:1: ( 'Configuration' )
            // InternalXaaS.g:1070:2: 'Configuration'
            {
             before(grammarAccess.getConfigurationAccess().getConfigurationKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getConfigurationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__0__Impl"


    // $ANTLR start "rule__Configuration__Group__1"
    // InternalXaaS.g:1079:1: rule__Configuration__Group__1 : rule__Configuration__Group__1__Impl rule__Configuration__Group__2 ;
    public final void rule__Configuration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1083:1: ( rule__Configuration__Group__1__Impl rule__Configuration__Group__2 )
            // InternalXaaS.g:1084:2: rule__Configuration__Group__1__Impl rule__Configuration__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Configuration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1"


    // $ANTLR start "rule__Configuration__Group__1__Impl"
    // InternalXaaS.g:1091:1: rule__Configuration__Group__1__Impl : ( ':' ) ;
    public final void rule__Configuration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1095:1: ( ( ':' ) )
            // InternalXaaS.g:1096:1: ( ':' )
            {
            // InternalXaaS.g:1096:1: ( ':' )
            // InternalXaaS.g:1097:2: ':'
            {
             before(grammarAccess.getConfigurationAccess().getColonKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__1__Impl"


    // $ANTLR start "rule__Configuration__Group__2"
    // InternalXaaS.g:1106:1: rule__Configuration__Group__2 : rule__Configuration__Group__2__Impl rule__Configuration__Group__3 ;
    public final void rule__Configuration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1110:1: ( rule__Configuration__Group__2__Impl rule__Configuration__Group__3 )
            // InternalXaaS.g:1111:2: rule__Configuration__Group__2__Impl rule__Configuration__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Configuration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2"


    // $ANTLR start "rule__Configuration__Group__2__Impl"
    // InternalXaaS.g:1118:1: rule__Configuration__Group__2__Impl : ( 'identifier' ) ;
    public final void rule__Configuration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1122:1: ( ( 'identifier' ) )
            // InternalXaaS.g:1123:1: ( 'identifier' )
            {
            // InternalXaaS.g:1123:1: ( 'identifier' )
            // InternalXaaS.g:1124:2: 'identifier'
            {
             before(grammarAccess.getConfigurationAccess().getIdentifierKeyword_2()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getIdentifierKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__2__Impl"


    // $ANTLR start "rule__Configuration__Group__3"
    // InternalXaaS.g:1133:1: rule__Configuration__Group__3 : rule__Configuration__Group__3__Impl rule__Configuration__Group__4 ;
    public final void rule__Configuration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1137:1: ( rule__Configuration__Group__3__Impl rule__Configuration__Group__4 )
            // InternalXaaS.g:1138:2: rule__Configuration__Group__3__Impl rule__Configuration__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Configuration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3"


    // $ANTLR start "rule__Configuration__Group__3__Impl"
    // InternalXaaS.g:1145:1: rule__Configuration__Group__3__Impl : ( ':' ) ;
    public final void rule__Configuration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1149:1: ( ( ':' ) )
            // InternalXaaS.g:1150:1: ( ':' )
            {
            // InternalXaaS.g:1150:1: ( ':' )
            // InternalXaaS.g:1151:2: ':'
            {
             before(grammarAccess.getConfigurationAccess().getColonKeyword_3()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__3__Impl"


    // $ANTLR start "rule__Configuration__Group__4"
    // InternalXaaS.g:1160:1: rule__Configuration__Group__4 : rule__Configuration__Group__4__Impl rule__Configuration__Group__5 ;
    public final void rule__Configuration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1164:1: ( rule__Configuration__Group__4__Impl rule__Configuration__Group__5 )
            // InternalXaaS.g:1165:2: rule__Configuration__Group__4__Impl rule__Configuration__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Configuration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4"


    // $ANTLR start "rule__Configuration__Group__4__Impl"
    // InternalXaaS.g:1172:1: rule__Configuration__Group__4__Impl : ( ( rule__Configuration__IdentifierAssignment_4 ) ) ;
    public final void rule__Configuration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1176:1: ( ( ( rule__Configuration__IdentifierAssignment_4 ) ) )
            // InternalXaaS.g:1177:1: ( ( rule__Configuration__IdentifierAssignment_4 ) )
            {
            // InternalXaaS.g:1177:1: ( ( rule__Configuration__IdentifierAssignment_4 ) )
            // InternalXaaS.g:1178:2: ( rule__Configuration__IdentifierAssignment_4 )
            {
             before(grammarAccess.getConfigurationAccess().getIdentifierAssignment_4()); 
            // InternalXaaS.g:1179:2: ( rule__Configuration__IdentifierAssignment_4 )
            // InternalXaaS.g:1179:3: rule__Configuration__IdentifierAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__IdentifierAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getIdentifierAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__4__Impl"


    // $ANTLR start "rule__Configuration__Group__5"
    // InternalXaaS.g:1187:1: rule__Configuration__Group__5 : rule__Configuration__Group__5__Impl rule__Configuration__Group__6 ;
    public final void rule__Configuration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1191:1: ( rule__Configuration__Group__5__Impl rule__Configuration__Group__6 )
            // InternalXaaS.g:1192:2: rule__Configuration__Group__5__Impl rule__Configuration__Group__6
            {
            pushFollow(FOLLOW_4);
            rule__Configuration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5"


    // $ANTLR start "rule__Configuration__Group__5__Impl"
    // InternalXaaS.g:1199:1: rule__Configuration__Group__5__Impl : ( 'topology' ) ;
    public final void rule__Configuration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1203:1: ( ( 'topology' ) )
            // InternalXaaS.g:1204:1: ( 'topology' )
            {
            // InternalXaaS.g:1204:1: ( 'topology' )
            // InternalXaaS.g:1205:2: 'topology'
            {
             before(grammarAccess.getConfigurationAccess().getTopologyKeyword_5()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getTopologyKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__5__Impl"


    // $ANTLR start "rule__Configuration__Group__6"
    // InternalXaaS.g:1214:1: rule__Configuration__Group__6 : rule__Configuration__Group__6__Impl rule__Configuration__Group__7 ;
    public final void rule__Configuration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1218:1: ( rule__Configuration__Group__6__Impl rule__Configuration__Group__7 )
            // InternalXaaS.g:1219:2: rule__Configuration__Group__6__Impl rule__Configuration__Group__7
            {
            pushFollow(FOLLOW_6);
            rule__Configuration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6"


    // $ANTLR start "rule__Configuration__Group__6__Impl"
    // InternalXaaS.g:1226:1: rule__Configuration__Group__6__Impl : ( ':' ) ;
    public final void rule__Configuration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1230:1: ( ( ':' ) )
            // InternalXaaS.g:1231:1: ( ':' )
            {
            // InternalXaaS.g:1231:1: ( ':' )
            // InternalXaaS.g:1232:2: ':'
            {
             before(grammarAccess.getConfigurationAccess().getColonKeyword_6()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getConfigurationAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__6__Impl"


    // $ANTLR start "rule__Configuration__Group__7"
    // InternalXaaS.g:1241:1: rule__Configuration__Group__7 : rule__Configuration__Group__7__Impl rule__Configuration__Group__8 ;
    public final void rule__Configuration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1245:1: ( rule__Configuration__Group__7__Impl rule__Configuration__Group__8 )
            // InternalXaaS.g:1246:2: rule__Configuration__Group__7__Impl rule__Configuration__Group__8
            {
            pushFollow(FOLLOW_8);
            rule__Configuration__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7"


    // $ANTLR start "rule__Configuration__Group__7__Impl"
    // InternalXaaS.g:1253:1: rule__Configuration__Group__7__Impl : ( ( rule__Configuration__TopologyAssignment_7 ) ) ;
    public final void rule__Configuration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1257:1: ( ( ( rule__Configuration__TopologyAssignment_7 ) ) )
            // InternalXaaS.g:1258:1: ( ( rule__Configuration__TopologyAssignment_7 ) )
            {
            // InternalXaaS.g:1258:1: ( ( rule__Configuration__TopologyAssignment_7 ) )
            // InternalXaaS.g:1259:2: ( rule__Configuration__TopologyAssignment_7 )
            {
             before(grammarAccess.getConfigurationAccess().getTopologyAssignment_7()); 
            // InternalXaaS.g:1260:2: ( rule__Configuration__TopologyAssignment_7 )
            // InternalXaaS.g:1260:3: rule__Configuration__TopologyAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__TopologyAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getConfigurationAccess().getTopologyAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__7__Impl"


    // $ANTLR start "rule__Configuration__Group__8"
    // InternalXaaS.g:1268:1: rule__Configuration__Group__8 : rule__Configuration__Group__8__Impl rule__Configuration__Group__9 ;
    public final void rule__Configuration__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1272:1: ( rule__Configuration__Group__8__Impl rule__Configuration__Group__9 )
            // InternalXaaS.g:1273:2: rule__Configuration__Group__8__Impl rule__Configuration__Group__9
            {
            pushFollow(FOLLOW_8);
            rule__Configuration__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Configuration__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__8"


    // $ANTLR start "rule__Configuration__Group__8__Impl"
    // InternalXaaS.g:1280:1: rule__Configuration__Group__8__Impl : ( ( rule__Configuration__NodesAssignment_8 )* ) ;
    public final void rule__Configuration__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1284:1: ( ( ( rule__Configuration__NodesAssignment_8 )* ) )
            // InternalXaaS.g:1285:1: ( ( rule__Configuration__NodesAssignment_8 )* )
            {
            // InternalXaaS.g:1285:1: ( ( rule__Configuration__NodesAssignment_8 )* )
            // InternalXaaS.g:1286:2: ( rule__Configuration__NodesAssignment_8 )*
            {
             before(grammarAccess.getConfigurationAccess().getNodesAssignment_8()); 
            // InternalXaaS.g:1287:2: ( rule__Configuration__NodesAssignment_8 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==35) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalXaaS.g:1287:3: rule__Configuration__NodesAssignment_8
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Configuration__NodesAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getNodesAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__8__Impl"


    // $ANTLR start "rule__Configuration__Group__9"
    // InternalXaaS.g:1295:1: rule__Configuration__Group__9 : rule__Configuration__Group__9__Impl ;
    public final void rule__Configuration__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1299:1: ( rule__Configuration__Group__9__Impl )
            // InternalXaaS.g:1300:2: rule__Configuration__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Configuration__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__9"


    // $ANTLR start "rule__Configuration__Group__9__Impl"
    // InternalXaaS.g:1306:1: rule__Configuration__Group__9__Impl : ( ( rule__Configuration__RelationshipsAssignment_9 )* ) ;
    public final void rule__Configuration__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1310:1: ( ( ( rule__Configuration__RelationshipsAssignment_9 )* ) )
            // InternalXaaS.g:1311:1: ( ( rule__Configuration__RelationshipsAssignment_9 )* )
            {
            // InternalXaaS.g:1311:1: ( ( rule__Configuration__RelationshipsAssignment_9 )* )
            // InternalXaaS.g:1312:2: ( rule__Configuration__RelationshipsAssignment_9 )*
            {
             before(grammarAccess.getConfigurationAccess().getRelationshipsAssignment_9()); 
            // InternalXaaS.g:1313:2: ( rule__Configuration__RelationshipsAssignment_9 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==39) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalXaaS.g:1313:3: rule__Configuration__RelationshipsAssignment_9
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__Configuration__RelationshipsAssignment_9();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getConfigurationAccess().getRelationshipsAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__Group__9__Impl"


    // $ANTLR start "rule__Topology__Group__0"
    // InternalXaaS.g:1322:1: rule__Topology__Group__0 : rule__Topology__Group__0__Impl rule__Topology__Group__1 ;
    public final void rule__Topology__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1326:1: ( rule__Topology__Group__0__Impl rule__Topology__Group__1 )
            // InternalXaaS.g:1327:2: rule__Topology__Group__0__Impl rule__Topology__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Topology__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Topology__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__0"


    // $ANTLR start "rule__Topology__Group__0__Impl"
    // InternalXaaS.g:1334:1: rule__Topology__Group__0__Impl : ( () ) ;
    public final void rule__Topology__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1338:1: ( ( () ) )
            // InternalXaaS.g:1339:1: ( () )
            {
            // InternalXaaS.g:1339:1: ( () )
            // InternalXaaS.g:1340:2: ()
            {
             before(grammarAccess.getTopologyAccess().getTopologyAction_0()); 
            // InternalXaaS.g:1341:2: ()
            // InternalXaaS.g:1341:3: 
            {
            }

             after(grammarAccess.getTopologyAccess().getTopologyAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__0__Impl"


    // $ANTLR start "rule__Topology__Group__1"
    // InternalXaaS.g:1349:1: rule__Topology__Group__1 : rule__Topology__Group__1__Impl rule__Topology__Group__2 ;
    public final void rule__Topology__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1353:1: ( rule__Topology__Group__1__Impl rule__Topology__Group__2 )
            // InternalXaaS.g:1354:2: rule__Topology__Group__1__Impl rule__Topology__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Topology__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Topology__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__1"


    // $ANTLR start "rule__Topology__Group__1__Impl"
    // InternalXaaS.g:1361:1: rule__Topology__Group__1__Impl : ( 'Topology' ) ;
    public final void rule__Topology__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1365:1: ( ( 'Topology' ) )
            // InternalXaaS.g:1366:1: ( 'Topology' )
            {
            // InternalXaaS.g:1366:1: ( 'Topology' )
            // InternalXaaS.g:1367:2: 'Topology'
            {
             before(grammarAccess.getTopologyAccess().getTopologyKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getTopologyAccess().getTopologyKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__1__Impl"


    // $ANTLR start "rule__Topology__Group__2"
    // InternalXaaS.g:1376:1: rule__Topology__Group__2 : rule__Topology__Group__2__Impl rule__Topology__Group__3 ;
    public final void rule__Topology__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1380:1: ( rule__Topology__Group__2__Impl rule__Topology__Group__3 )
            // InternalXaaS.g:1381:2: rule__Topology__Group__2__Impl rule__Topology__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Topology__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Topology__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__2"


    // $ANTLR start "rule__Topology__Group__2__Impl"
    // InternalXaaS.g:1388:1: rule__Topology__Group__2__Impl : ( ':' ) ;
    public final void rule__Topology__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1392:1: ( ( ':' ) )
            // InternalXaaS.g:1393:1: ( ':' )
            {
            // InternalXaaS.g:1393:1: ( ':' )
            // InternalXaaS.g:1394:2: ':'
            {
             before(grammarAccess.getTopologyAccess().getColonKeyword_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getTopologyAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__2__Impl"


    // $ANTLR start "rule__Topology__Group__3"
    // InternalXaaS.g:1403:1: rule__Topology__Group__3 : rule__Topology__Group__3__Impl rule__Topology__Group__4 ;
    public final void rule__Topology__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1407:1: ( rule__Topology__Group__3__Impl rule__Topology__Group__4 )
            // InternalXaaS.g:1408:2: rule__Topology__Group__3__Impl rule__Topology__Group__4
            {
            pushFollow(FOLLOW_12);
            rule__Topology__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Topology__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__3"


    // $ANTLR start "rule__Topology__Group__3__Impl"
    // InternalXaaS.g:1415:1: rule__Topology__Group__3__Impl : ( ( rule__Topology__NameAssignment_3 ) ) ;
    public final void rule__Topology__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1419:1: ( ( ( rule__Topology__NameAssignment_3 ) ) )
            // InternalXaaS.g:1420:1: ( ( rule__Topology__NameAssignment_3 ) )
            {
            // InternalXaaS.g:1420:1: ( ( rule__Topology__NameAssignment_3 ) )
            // InternalXaaS.g:1421:2: ( rule__Topology__NameAssignment_3 )
            {
             before(grammarAccess.getTopologyAccess().getNameAssignment_3()); 
            // InternalXaaS.g:1422:2: ( rule__Topology__NameAssignment_3 )
            // InternalXaaS.g:1422:3: rule__Topology__NameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Topology__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTopologyAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__3__Impl"


    // $ANTLR start "rule__Topology__Group__4"
    // InternalXaaS.g:1430:1: rule__Topology__Group__4 : rule__Topology__Group__4__Impl rule__Topology__Group__5 ;
    public final void rule__Topology__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1434:1: ( rule__Topology__Group__4__Impl rule__Topology__Group__5 )
            // InternalXaaS.g:1435:2: rule__Topology__Group__4__Impl rule__Topology__Group__5
            {
            pushFollow(FOLLOW_4);
            rule__Topology__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Topology__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__4"


    // $ANTLR start "rule__Topology__Group__4__Impl"
    // InternalXaaS.g:1442:1: rule__Topology__Group__4__Impl : ( 'node_types' ) ;
    public final void rule__Topology__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1446:1: ( ( 'node_types' ) )
            // InternalXaaS.g:1447:1: ( 'node_types' )
            {
            // InternalXaaS.g:1447:1: ( 'node_types' )
            // InternalXaaS.g:1448:2: 'node_types'
            {
             before(grammarAccess.getTopologyAccess().getNode_typesKeyword_4()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getTopologyAccess().getNode_typesKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__4__Impl"


    // $ANTLR start "rule__Topology__Group__5"
    // InternalXaaS.g:1457:1: rule__Topology__Group__5 : rule__Topology__Group__5__Impl rule__Topology__Group__6 ;
    public final void rule__Topology__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1461:1: ( rule__Topology__Group__5__Impl rule__Topology__Group__6 )
            // InternalXaaS.g:1462:2: rule__Topology__Group__5__Impl rule__Topology__Group__6
            {
            pushFollow(FOLLOW_13);
            rule__Topology__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Topology__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__5"


    // $ANTLR start "rule__Topology__Group__5__Impl"
    // InternalXaaS.g:1469:1: rule__Topology__Group__5__Impl : ( ':' ) ;
    public final void rule__Topology__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1473:1: ( ( ':' ) )
            // InternalXaaS.g:1474:1: ( ':' )
            {
            // InternalXaaS.g:1474:1: ( ':' )
            // InternalXaaS.g:1475:2: ':'
            {
             before(grammarAccess.getTopologyAccess().getColonKeyword_5()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getTopologyAccess().getColonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__5__Impl"


    // $ANTLR start "rule__Topology__Group__6"
    // InternalXaaS.g:1484:1: rule__Topology__Group__6 : rule__Topology__Group__6__Impl rule__Topology__Group__7 ;
    public final void rule__Topology__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1488:1: ( rule__Topology__Group__6__Impl rule__Topology__Group__7 )
            // InternalXaaS.g:1489:2: rule__Topology__Group__6__Impl rule__Topology__Group__7
            {
            pushFollow(FOLLOW_13);
            rule__Topology__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Topology__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__6"


    // $ANTLR start "rule__Topology__Group__6__Impl"
    // InternalXaaS.g:1496:1: rule__Topology__Group__6__Impl : ( ( rule__Topology__NodeTypesAssignment_6 )* ) ;
    public final void rule__Topology__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1500:1: ( ( ( rule__Topology__NodeTypesAssignment_6 )* ) )
            // InternalXaaS.g:1501:1: ( ( rule__Topology__NodeTypesAssignment_6 )* )
            {
            // InternalXaaS.g:1501:1: ( ( rule__Topology__NodeTypesAssignment_6 )* )
            // InternalXaaS.g:1502:2: ( rule__Topology__NodeTypesAssignment_6 )*
            {
             before(grammarAccess.getTopologyAccess().getNodeTypesAssignment_6()); 
            // InternalXaaS.g:1503:2: ( rule__Topology__NodeTypesAssignment_6 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>=RULE_STRING && LA13_0<=RULE_ID)) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalXaaS.g:1503:3: rule__Topology__NodeTypesAssignment_6
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Topology__NodeTypesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getTopologyAccess().getNodeTypesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__6__Impl"


    // $ANTLR start "rule__Topology__Group__7"
    // InternalXaaS.g:1511:1: rule__Topology__Group__7 : rule__Topology__Group__7__Impl ;
    public final void rule__Topology__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1515:1: ( rule__Topology__Group__7__Impl )
            // InternalXaaS.g:1516:2: rule__Topology__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Topology__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__7"


    // $ANTLR start "rule__Topology__Group__7__Impl"
    // InternalXaaS.g:1522:1: rule__Topology__Group__7__Impl : ( ( rule__Topology__Group_7__0 )? ) ;
    public final void rule__Topology__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1526:1: ( ( ( rule__Topology__Group_7__0 )? ) )
            // InternalXaaS.g:1527:1: ( ( rule__Topology__Group_7__0 )? )
            {
            // InternalXaaS.g:1527:1: ( ( rule__Topology__Group_7__0 )? )
            // InternalXaaS.g:1528:2: ( rule__Topology__Group_7__0 )?
            {
             before(grammarAccess.getTopologyAccess().getGroup_7()); 
            // InternalXaaS.g:1529:2: ( rule__Topology__Group_7__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==33) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalXaaS.g:1529:3: rule__Topology__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Topology__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTopologyAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group__7__Impl"


    // $ANTLR start "rule__Topology__Group_7__0"
    // InternalXaaS.g:1538:1: rule__Topology__Group_7__0 : rule__Topology__Group_7__0__Impl rule__Topology__Group_7__1 ;
    public final void rule__Topology__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1542:1: ( rule__Topology__Group_7__0__Impl rule__Topology__Group_7__1 )
            // InternalXaaS.g:1543:2: rule__Topology__Group_7__0__Impl rule__Topology__Group_7__1
            {
            pushFollow(FOLLOW_4);
            rule__Topology__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Topology__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group_7__0"


    // $ANTLR start "rule__Topology__Group_7__0__Impl"
    // InternalXaaS.g:1550:1: rule__Topology__Group_7__0__Impl : ( 'relationship_types' ) ;
    public final void rule__Topology__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1554:1: ( ( 'relationship_types' ) )
            // InternalXaaS.g:1555:1: ( 'relationship_types' )
            {
            // InternalXaaS.g:1555:1: ( 'relationship_types' )
            // InternalXaaS.g:1556:2: 'relationship_types'
            {
             before(grammarAccess.getTopologyAccess().getRelationship_typesKeyword_7_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getTopologyAccess().getRelationship_typesKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group_7__0__Impl"


    // $ANTLR start "rule__Topology__Group_7__1"
    // InternalXaaS.g:1565:1: rule__Topology__Group_7__1 : rule__Topology__Group_7__1__Impl rule__Topology__Group_7__2 ;
    public final void rule__Topology__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1569:1: ( rule__Topology__Group_7__1__Impl rule__Topology__Group_7__2 )
            // InternalXaaS.g:1570:2: rule__Topology__Group_7__1__Impl rule__Topology__Group_7__2
            {
            pushFollow(FOLLOW_6);
            rule__Topology__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Topology__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group_7__1"


    // $ANTLR start "rule__Topology__Group_7__1__Impl"
    // InternalXaaS.g:1577:1: rule__Topology__Group_7__1__Impl : ( ':' ) ;
    public final void rule__Topology__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1581:1: ( ( ':' ) )
            // InternalXaaS.g:1582:1: ( ':' )
            {
            // InternalXaaS.g:1582:1: ( ':' )
            // InternalXaaS.g:1583:2: ':'
            {
             before(grammarAccess.getTopologyAccess().getColonKeyword_7_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getTopologyAccess().getColonKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group_7__1__Impl"


    // $ANTLR start "rule__Topology__Group_7__2"
    // InternalXaaS.g:1592:1: rule__Topology__Group_7__2 : rule__Topology__Group_7__2__Impl ;
    public final void rule__Topology__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1596:1: ( rule__Topology__Group_7__2__Impl )
            // InternalXaaS.g:1597:2: rule__Topology__Group_7__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Topology__Group_7__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group_7__2"


    // $ANTLR start "rule__Topology__Group_7__2__Impl"
    // InternalXaaS.g:1603:1: rule__Topology__Group_7__2__Impl : ( ( rule__Topology__RelationshipTypesAssignment_7_2 )* ) ;
    public final void rule__Topology__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1607:1: ( ( ( rule__Topology__RelationshipTypesAssignment_7_2 )* ) )
            // InternalXaaS.g:1608:1: ( ( rule__Topology__RelationshipTypesAssignment_7_2 )* )
            {
            // InternalXaaS.g:1608:1: ( ( rule__Topology__RelationshipTypesAssignment_7_2 )* )
            // InternalXaaS.g:1609:2: ( rule__Topology__RelationshipTypesAssignment_7_2 )*
            {
             before(grammarAccess.getTopologyAccess().getRelationshipTypesAssignment_7_2()); 
            // InternalXaaS.g:1610:2: ( rule__Topology__RelationshipTypesAssignment_7_2 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>=RULE_STRING && LA15_0<=RULE_ID)) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalXaaS.g:1610:3: rule__Topology__RelationshipTypesAssignment_7_2
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Topology__RelationshipTypesAssignment_7_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getTopologyAccess().getRelationshipTypesAssignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__Group_7__2__Impl"


    // $ANTLR start "rule__EString__Group_0__0"
    // InternalXaaS.g:1619:1: rule__EString__Group_0__0 : rule__EString__Group_0__0__Impl rule__EString__Group_0__1 ;
    public final void rule__EString__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1623:1: ( rule__EString__Group_0__0__Impl rule__EString__Group_0__1 )
            // InternalXaaS.g:1624:2: rule__EString__Group_0__0__Impl rule__EString__Group_0__1
            {
            pushFollow(FOLLOW_15);
            rule__EString__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EString__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_0__0"


    // $ANTLR start "rule__EString__Group_0__0__Impl"
    // InternalXaaS.g:1631:1: rule__EString__Group_0__0__Impl : ( RULE_ID ) ;
    public final void rule__EString__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1635:1: ( ( RULE_ID ) )
            // InternalXaaS.g:1636:1: ( RULE_ID )
            {
            // InternalXaaS.g:1636:1: ( RULE_ID )
            // InternalXaaS.g:1637:2: RULE_ID
            {
             before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_0__0__Impl"


    // $ANTLR start "rule__EString__Group_0__1"
    // InternalXaaS.g:1646:1: rule__EString__Group_0__1 : rule__EString__Group_0__1__Impl ;
    public final void rule__EString__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1650:1: ( rule__EString__Group_0__1__Impl )
            // InternalXaaS.g:1651:2: rule__EString__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EString__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_0__1"


    // $ANTLR start "rule__EString__Group_0__1__Impl"
    // InternalXaaS.g:1657:1: rule__EString__Group_0__1__Impl : ( ( rule__EString__Group_0_1__0 )* ) ;
    public final void rule__EString__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1661:1: ( ( ( rule__EString__Group_0_1__0 )* ) )
            // InternalXaaS.g:1662:1: ( ( rule__EString__Group_0_1__0 )* )
            {
            // InternalXaaS.g:1662:1: ( ( rule__EString__Group_0_1__0 )* )
            // InternalXaaS.g:1663:2: ( rule__EString__Group_0_1__0 )*
            {
             before(grammarAccess.getEStringAccess().getGroup_0_1()); 
            // InternalXaaS.g:1664:2: ( rule__EString__Group_0_1__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==34) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalXaaS.g:1664:3: rule__EString__Group_0_1__0
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__EString__Group_0_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getEStringAccess().getGroup_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_0__1__Impl"


    // $ANTLR start "rule__EString__Group_0_1__0"
    // InternalXaaS.g:1673:1: rule__EString__Group_0_1__0 : rule__EString__Group_0_1__0__Impl rule__EString__Group_0_1__1 ;
    public final void rule__EString__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1677:1: ( rule__EString__Group_0_1__0__Impl rule__EString__Group_0_1__1 )
            // InternalXaaS.g:1678:2: rule__EString__Group_0_1__0__Impl rule__EString__Group_0_1__1
            {
            pushFollow(FOLLOW_17);
            rule__EString__Group_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EString__Group_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_0_1__0"


    // $ANTLR start "rule__EString__Group_0_1__0__Impl"
    // InternalXaaS.g:1685:1: rule__EString__Group_0_1__0__Impl : ( '.' ) ;
    public final void rule__EString__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1689:1: ( ( '.' ) )
            // InternalXaaS.g:1690:1: ( '.' )
            {
            // InternalXaaS.g:1690:1: ( '.' )
            // InternalXaaS.g:1691:2: '.'
            {
             before(grammarAccess.getEStringAccess().getFullStopKeyword_0_1_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getEStringAccess().getFullStopKeyword_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_0_1__0__Impl"


    // $ANTLR start "rule__EString__Group_0_1__1"
    // InternalXaaS.g:1700:1: rule__EString__Group_0_1__1 : rule__EString__Group_0_1__1__Impl ;
    public final void rule__EString__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1704:1: ( rule__EString__Group_0_1__1__Impl )
            // InternalXaaS.g:1705:2: rule__EString__Group_0_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EString__Group_0_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_0_1__1"


    // $ANTLR start "rule__EString__Group_0_1__1__Impl"
    // InternalXaaS.g:1711:1: rule__EString__Group_0_1__1__Impl : ( RULE_ID ) ;
    public final void rule__EString__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1715:1: ( ( RULE_ID ) )
            // InternalXaaS.g:1716:1: ( RULE_ID )
            {
            // InternalXaaS.g:1716:1: ( RULE_ID )
            // InternalXaaS.g:1717:2: RULE_ID
            {
             before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_0_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Group_0_1__1__Impl"


    // $ANTLR start "rule__Node__Group__0"
    // InternalXaaS.g:1727:1: rule__Node__Group__0 : rule__Node__Group__0__Impl rule__Node__Group__1 ;
    public final void rule__Node__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1731:1: ( rule__Node__Group__0__Impl rule__Node__Group__1 )
            // InternalXaaS.g:1732:2: rule__Node__Group__0__Impl rule__Node__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Node__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__0"


    // $ANTLR start "rule__Node__Group__0__Impl"
    // InternalXaaS.g:1739:1: rule__Node__Group__0__Impl : ( 'Node' ) ;
    public final void rule__Node__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1743:1: ( ( 'Node' ) )
            // InternalXaaS.g:1744:1: ( 'Node' )
            {
            // InternalXaaS.g:1744:1: ( 'Node' )
            // InternalXaaS.g:1745:2: 'Node'
            {
             before(grammarAccess.getNodeAccess().getNodeKeyword_0()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getNodeAccess().getNodeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__0__Impl"


    // $ANTLR start "rule__Node__Group__1"
    // InternalXaaS.g:1754:1: rule__Node__Group__1 : rule__Node__Group__1__Impl rule__Node__Group__2 ;
    public final void rule__Node__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1758:1: ( rule__Node__Group__1__Impl rule__Node__Group__2 )
            // InternalXaaS.g:1759:2: rule__Node__Group__1__Impl rule__Node__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Node__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__1"


    // $ANTLR start "rule__Node__Group__1__Impl"
    // InternalXaaS.g:1766:1: rule__Node__Group__1__Impl : ( ( rule__Node__NameAssignment_1 ) ) ;
    public final void rule__Node__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1770:1: ( ( ( rule__Node__NameAssignment_1 ) ) )
            // InternalXaaS.g:1771:1: ( ( rule__Node__NameAssignment_1 ) )
            {
            // InternalXaaS.g:1771:1: ( ( rule__Node__NameAssignment_1 ) )
            // InternalXaaS.g:1772:2: ( rule__Node__NameAssignment_1 )
            {
             before(grammarAccess.getNodeAccess().getNameAssignment_1()); 
            // InternalXaaS.g:1773:2: ( rule__Node__NameAssignment_1 )
            // InternalXaaS.g:1773:3: rule__Node__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Node__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getNodeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__1__Impl"


    // $ANTLR start "rule__Node__Group__2"
    // InternalXaaS.g:1781:1: rule__Node__Group__2 : rule__Node__Group__2__Impl rule__Node__Group__3 ;
    public final void rule__Node__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1785:1: ( rule__Node__Group__2__Impl rule__Node__Group__3 )
            // InternalXaaS.g:1786:2: rule__Node__Group__2__Impl rule__Node__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__Node__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__2"


    // $ANTLR start "rule__Node__Group__2__Impl"
    // InternalXaaS.g:1793:1: rule__Node__Group__2__Impl : ( ':' ) ;
    public final void rule__Node__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1797:1: ( ( ':' ) )
            // InternalXaaS.g:1798:1: ( ':' )
            {
            // InternalXaaS.g:1798:1: ( ':' )
            // InternalXaaS.g:1799:2: ':'
            {
             before(grammarAccess.getNodeAccess().getColonKeyword_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__2__Impl"


    // $ANTLR start "rule__Node__Group__3"
    // InternalXaaS.g:1808:1: rule__Node__Group__3 : rule__Node__Group__3__Impl rule__Node__Group__4 ;
    public final void rule__Node__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1812:1: ( rule__Node__Group__3__Impl rule__Node__Group__4 )
            // InternalXaaS.g:1813:2: rule__Node__Group__3__Impl rule__Node__Group__4
            {
            pushFollow(FOLLOW_4);
            rule__Node__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__3"


    // $ANTLR start "rule__Node__Group__3__Impl"
    // InternalXaaS.g:1820:1: rule__Node__Group__3__Impl : ( 'type' ) ;
    public final void rule__Node__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1824:1: ( ( 'type' ) )
            // InternalXaaS.g:1825:1: ( 'type' )
            {
            // InternalXaaS.g:1825:1: ( 'type' )
            // InternalXaaS.g:1826:2: 'type'
            {
             before(grammarAccess.getNodeAccess().getTypeKeyword_3()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getNodeAccess().getTypeKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__3__Impl"


    // $ANTLR start "rule__Node__Group__4"
    // InternalXaaS.g:1835:1: rule__Node__Group__4 : rule__Node__Group__4__Impl rule__Node__Group__5 ;
    public final void rule__Node__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1839:1: ( rule__Node__Group__4__Impl rule__Node__Group__5 )
            // InternalXaaS.g:1840:2: rule__Node__Group__4__Impl rule__Node__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Node__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__4"


    // $ANTLR start "rule__Node__Group__4__Impl"
    // InternalXaaS.g:1847:1: rule__Node__Group__4__Impl : ( ':' ) ;
    public final void rule__Node__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1851:1: ( ( ':' ) )
            // InternalXaaS.g:1852:1: ( ':' )
            {
            // InternalXaaS.g:1852:1: ( ':' )
            // InternalXaaS.g:1853:2: ':'
            {
             before(grammarAccess.getNodeAccess().getColonKeyword_4()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeAccess().getColonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__4__Impl"


    // $ANTLR start "rule__Node__Group__5"
    // InternalXaaS.g:1862:1: rule__Node__Group__5 : rule__Node__Group__5__Impl rule__Node__Group__6 ;
    public final void rule__Node__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1866:1: ( rule__Node__Group__5__Impl rule__Node__Group__6 )
            // InternalXaaS.g:1867:2: rule__Node__Group__5__Impl rule__Node__Group__6
            {
            pushFollow(FOLLOW_19);
            rule__Node__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__5"


    // $ANTLR start "rule__Node__Group__5__Impl"
    // InternalXaaS.g:1874:1: rule__Node__Group__5__Impl : ( ( rule__Node__TypeAssignment_5 ) ) ;
    public final void rule__Node__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1878:1: ( ( ( rule__Node__TypeAssignment_5 ) ) )
            // InternalXaaS.g:1879:1: ( ( rule__Node__TypeAssignment_5 ) )
            {
            // InternalXaaS.g:1879:1: ( ( rule__Node__TypeAssignment_5 ) )
            // InternalXaaS.g:1880:2: ( rule__Node__TypeAssignment_5 )
            {
             before(grammarAccess.getNodeAccess().getTypeAssignment_5()); 
            // InternalXaaS.g:1881:2: ( rule__Node__TypeAssignment_5 )
            // InternalXaaS.g:1881:3: rule__Node__TypeAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Node__TypeAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getNodeAccess().getTypeAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__5__Impl"


    // $ANTLR start "rule__Node__Group__6"
    // InternalXaaS.g:1889:1: rule__Node__Group__6 : rule__Node__Group__6__Impl rule__Node__Group__7 ;
    public final void rule__Node__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1893:1: ( rule__Node__Group__6__Impl rule__Node__Group__7 )
            // InternalXaaS.g:1894:2: rule__Node__Group__6__Impl rule__Node__Group__7
            {
            pushFollow(FOLLOW_4);
            rule__Node__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__6"


    // $ANTLR start "rule__Node__Group__6__Impl"
    // InternalXaaS.g:1901:1: rule__Node__Group__6__Impl : ( 'activated' ) ;
    public final void rule__Node__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1905:1: ( ( 'activated' ) )
            // InternalXaaS.g:1906:1: ( 'activated' )
            {
            // InternalXaaS.g:1906:1: ( 'activated' )
            // InternalXaaS.g:1907:2: 'activated'
            {
             before(grammarAccess.getNodeAccess().getActivatedKeyword_6()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getNodeAccess().getActivatedKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__6__Impl"


    // $ANTLR start "rule__Node__Group__7"
    // InternalXaaS.g:1916:1: rule__Node__Group__7 : rule__Node__Group__7__Impl rule__Node__Group__8 ;
    public final void rule__Node__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1920:1: ( rule__Node__Group__7__Impl rule__Node__Group__8 )
            // InternalXaaS.g:1921:2: rule__Node__Group__7__Impl rule__Node__Group__8
            {
            pushFollow(FOLLOW_6);
            rule__Node__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__7"


    // $ANTLR start "rule__Node__Group__7__Impl"
    // InternalXaaS.g:1928:1: rule__Node__Group__7__Impl : ( ':' ) ;
    public final void rule__Node__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1932:1: ( ( ':' ) )
            // InternalXaaS.g:1933:1: ( ':' )
            {
            // InternalXaaS.g:1933:1: ( ':' )
            // InternalXaaS.g:1934:2: ':'
            {
             before(grammarAccess.getNodeAccess().getColonKeyword_7()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeAccess().getColonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__7__Impl"


    // $ANTLR start "rule__Node__Group__8"
    // InternalXaaS.g:1943:1: rule__Node__Group__8 : rule__Node__Group__8__Impl rule__Node__Group__9 ;
    public final void rule__Node__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1947:1: ( rule__Node__Group__8__Impl rule__Node__Group__9 )
            // InternalXaaS.g:1948:2: rule__Node__Group__8__Impl rule__Node__Group__9
            {
            pushFollow(FOLLOW_20);
            rule__Node__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__8"


    // $ANTLR start "rule__Node__Group__8__Impl"
    // InternalXaaS.g:1955:1: rule__Node__Group__8__Impl : ( ( rule__Node__ActivatedAssignment_8 ) ) ;
    public final void rule__Node__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1959:1: ( ( ( rule__Node__ActivatedAssignment_8 ) ) )
            // InternalXaaS.g:1960:1: ( ( rule__Node__ActivatedAssignment_8 ) )
            {
            // InternalXaaS.g:1960:1: ( ( rule__Node__ActivatedAssignment_8 ) )
            // InternalXaaS.g:1961:2: ( rule__Node__ActivatedAssignment_8 )
            {
             before(grammarAccess.getNodeAccess().getActivatedAssignment_8()); 
            // InternalXaaS.g:1962:2: ( rule__Node__ActivatedAssignment_8 )
            // InternalXaaS.g:1962:3: rule__Node__ActivatedAssignment_8
            {
            pushFollow(FOLLOW_2);
            rule__Node__ActivatedAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getNodeAccess().getActivatedAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__8__Impl"


    // $ANTLR start "rule__Node__Group__9"
    // InternalXaaS.g:1970:1: rule__Node__Group__9 : rule__Node__Group__9__Impl ;
    public final void rule__Node__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1974:1: ( rule__Node__Group__9__Impl )
            // InternalXaaS.g:1975:2: rule__Node__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Node__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__9"


    // $ANTLR start "rule__Node__Group__9__Impl"
    // InternalXaaS.g:1981:1: rule__Node__Group__9__Impl : ( ( rule__Node__Group_9__0 )? ) ;
    public final void rule__Node__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:1985:1: ( ( ( rule__Node__Group_9__0 )? ) )
            // InternalXaaS.g:1986:1: ( ( rule__Node__Group_9__0 )? )
            {
            // InternalXaaS.g:1986:1: ( ( rule__Node__Group_9__0 )? )
            // InternalXaaS.g:1987:2: ( rule__Node__Group_9__0 )?
            {
             before(grammarAccess.getNodeAccess().getGroup_9()); 
            // InternalXaaS.g:1988:2: ( rule__Node__Group_9__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==38) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalXaaS.g:1988:3: rule__Node__Group_9__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Node__Group_9__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNodeAccess().getGroup_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group__9__Impl"


    // $ANTLR start "rule__Node__Group_9__0"
    // InternalXaaS.g:1997:1: rule__Node__Group_9__0 : rule__Node__Group_9__0__Impl rule__Node__Group_9__1 ;
    public final void rule__Node__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2001:1: ( rule__Node__Group_9__0__Impl rule__Node__Group_9__1 )
            // InternalXaaS.g:2002:2: rule__Node__Group_9__0__Impl rule__Node__Group_9__1
            {
            pushFollow(FOLLOW_4);
            rule__Node__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group_9__0"


    // $ANTLR start "rule__Node__Group_9__0__Impl"
    // InternalXaaS.g:2009:1: rule__Node__Group_9__0__Impl : ( 'properties' ) ;
    public final void rule__Node__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2013:1: ( ( 'properties' ) )
            // InternalXaaS.g:2014:1: ( 'properties' )
            {
            // InternalXaaS.g:2014:1: ( 'properties' )
            // InternalXaaS.g:2015:2: 'properties'
            {
             before(grammarAccess.getNodeAccess().getPropertiesKeyword_9_0()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getNodeAccess().getPropertiesKeyword_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group_9__0__Impl"


    // $ANTLR start "rule__Node__Group_9__1"
    // InternalXaaS.g:2024:1: rule__Node__Group_9__1 : rule__Node__Group_9__1__Impl rule__Node__Group_9__2 ;
    public final void rule__Node__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2028:1: ( rule__Node__Group_9__1__Impl rule__Node__Group_9__2 )
            // InternalXaaS.g:2029:2: rule__Node__Group_9__1__Impl rule__Node__Group_9__2
            {
            pushFollow(FOLLOW_6);
            rule__Node__Group_9__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Node__Group_9__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group_9__1"


    // $ANTLR start "rule__Node__Group_9__1__Impl"
    // InternalXaaS.g:2036:1: rule__Node__Group_9__1__Impl : ( ':' ) ;
    public final void rule__Node__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2040:1: ( ( ':' ) )
            // InternalXaaS.g:2041:1: ( ':' )
            {
            // InternalXaaS.g:2041:1: ( ':' )
            // InternalXaaS.g:2042:2: ':'
            {
             before(grammarAccess.getNodeAccess().getColonKeyword_9_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeAccess().getColonKeyword_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group_9__1__Impl"


    // $ANTLR start "rule__Node__Group_9__2"
    // InternalXaaS.g:2051:1: rule__Node__Group_9__2 : rule__Node__Group_9__2__Impl ;
    public final void rule__Node__Group_9__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2055:1: ( rule__Node__Group_9__2__Impl )
            // InternalXaaS.g:2056:2: rule__Node__Group_9__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Node__Group_9__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group_9__2"


    // $ANTLR start "rule__Node__Group_9__2__Impl"
    // InternalXaaS.g:2062:1: rule__Node__Group_9__2__Impl : ( ( rule__Node__AttributesAssignment_9_2 )* ) ;
    public final void rule__Node__Group_9__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2066:1: ( ( ( rule__Node__AttributesAssignment_9_2 )* ) )
            // InternalXaaS.g:2067:1: ( ( rule__Node__AttributesAssignment_9_2 )* )
            {
            // InternalXaaS.g:2067:1: ( ( rule__Node__AttributesAssignment_9_2 )* )
            // InternalXaaS.g:2068:2: ( rule__Node__AttributesAssignment_9_2 )*
            {
             before(grammarAccess.getNodeAccess().getAttributesAssignment_9_2()); 
            // InternalXaaS.g:2069:2: ( rule__Node__AttributesAssignment_9_2 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=RULE_STRING && LA18_0<=RULE_ID)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalXaaS.g:2069:3: rule__Node__AttributesAssignment_9_2
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Node__AttributesAssignment_9_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getNodeAccess().getAttributesAssignment_9_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Group_9__2__Impl"


    // $ANTLR start "rule__Relationship__Group__0"
    // InternalXaaS.g:2078:1: rule__Relationship__Group__0 : rule__Relationship__Group__0__Impl rule__Relationship__Group__1 ;
    public final void rule__Relationship__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2082:1: ( rule__Relationship__Group__0__Impl rule__Relationship__Group__1 )
            // InternalXaaS.g:2083:2: rule__Relationship__Group__0__Impl rule__Relationship__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Relationship__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__0"


    // $ANTLR start "rule__Relationship__Group__0__Impl"
    // InternalXaaS.g:2090:1: rule__Relationship__Group__0__Impl : ( 'Relationship' ) ;
    public final void rule__Relationship__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2094:1: ( ( 'Relationship' ) )
            // InternalXaaS.g:2095:1: ( 'Relationship' )
            {
            // InternalXaaS.g:2095:1: ( 'Relationship' )
            // InternalXaaS.g:2096:2: 'Relationship'
            {
             before(grammarAccess.getRelationshipAccess().getRelationshipKeyword_0()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getRelationshipKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__0__Impl"


    // $ANTLR start "rule__Relationship__Group__1"
    // InternalXaaS.g:2105:1: rule__Relationship__Group__1 : rule__Relationship__Group__1__Impl rule__Relationship__Group__2 ;
    public final void rule__Relationship__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2109:1: ( rule__Relationship__Group__1__Impl rule__Relationship__Group__2 )
            // InternalXaaS.g:2110:2: rule__Relationship__Group__1__Impl rule__Relationship__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Relationship__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__1"


    // $ANTLR start "rule__Relationship__Group__1__Impl"
    // InternalXaaS.g:2117:1: rule__Relationship__Group__1__Impl : ( ( rule__Relationship__NameAssignment_1 ) ) ;
    public final void rule__Relationship__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2121:1: ( ( ( rule__Relationship__NameAssignment_1 ) ) )
            // InternalXaaS.g:2122:1: ( ( rule__Relationship__NameAssignment_1 ) )
            {
            // InternalXaaS.g:2122:1: ( ( rule__Relationship__NameAssignment_1 ) )
            // InternalXaaS.g:2123:2: ( rule__Relationship__NameAssignment_1 )
            {
             before(grammarAccess.getRelationshipAccess().getNameAssignment_1()); 
            // InternalXaaS.g:2124:2: ( rule__Relationship__NameAssignment_1 )
            // InternalXaaS.g:2124:3: rule__Relationship__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__1__Impl"


    // $ANTLR start "rule__Relationship__Group__2"
    // InternalXaaS.g:2132:1: rule__Relationship__Group__2 : rule__Relationship__Group__2__Impl rule__Relationship__Group__3 ;
    public final void rule__Relationship__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2136:1: ( rule__Relationship__Group__2__Impl rule__Relationship__Group__3 )
            // InternalXaaS.g:2137:2: rule__Relationship__Group__2__Impl rule__Relationship__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__Relationship__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__2"


    // $ANTLR start "rule__Relationship__Group__2__Impl"
    // InternalXaaS.g:2144:1: rule__Relationship__Group__2__Impl : ( ':' ) ;
    public final void rule__Relationship__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2148:1: ( ( ':' ) )
            // InternalXaaS.g:2149:1: ( ':' )
            {
            // InternalXaaS.g:2149:1: ( ':' )
            // InternalXaaS.g:2150:2: ':'
            {
             before(grammarAccess.getRelationshipAccess().getColonKeyword_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__2__Impl"


    // $ANTLR start "rule__Relationship__Group__3"
    // InternalXaaS.g:2159:1: rule__Relationship__Group__3 : rule__Relationship__Group__3__Impl rule__Relationship__Group__4 ;
    public final void rule__Relationship__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2163:1: ( rule__Relationship__Group__3__Impl rule__Relationship__Group__4 )
            // InternalXaaS.g:2164:2: rule__Relationship__Group__3__Impl rule__Relationship__Group__4
            {
            pushFollow(FOLLOW_4);
            rule__Relationship__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__3"


    // $ANTLR start "rule__Relationship__Group__3__Impl"
    // InternalXaaS.g:2171:1: rule__Relationship__Group__3__Impl : ( 'type' ) ;
    public final void rule__Relationship__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2175:1: ( ( 'type' ) )
            // InternalXaaS.g:2176:1: ( 'type' )
            {
            // InternalXaaS.g:2176:1: ( 'type' )
            // InternalXaaS.g:2177:2: 'type'
            {
             before(grammarAccess.getRelationshipAccess().getTypeKeyword_3()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getTypeKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__3__Impl"


    // $ANTLR start "rule__Relationship__Group__4"
    // InternalXaaS.g:2186:1: rule__Relationship__Group__4 : rule__Relationship__Group__4__Impl rule__Relationship__Group__5 ;
    public final void rule__Relationship__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2190:1: ( rule__Relationship__Group__4__Impl rule__Relationship__Group__5 )
            // InternalXaaS.g:2191:2: rule__Relationship__Group__4__Impl rule__Relationship__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Relationship__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__4"


    // $ANTLR start "rule__Relationship__Group__4__Impl"
    // InternalXaaS.g:2198:1: rule__Relationship__Group__4__Impl : ( ':' ) ;
    public final void rule__Relationship__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2202:1: ( ( ':' ) )
            // InternalXaaS.g:2203:1: ( ':' )
            {
            // InternalXaaS.g:2203:1: ( ':' )
            // InternalXaaS.g:2204:2: ':'
            {
             before(grammarAccess.getRelationshipAccess().getColonKeyword_4()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getColonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__4__Impl"


    // $ANTLR start "rule__Relationship__Group__5"
    // InternalXaaS.g:2213:1: rule__Relationship__Group__5 : rule__Relationship__Group__5__Impl rule__Relationship__Group__6 ;
    public final void rule__Relationship__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2217:1: ( rule__Relationship__Group__5__Impl rule__Relationship__Group__6 )
            // InternalXaaS.g:2218:2: rule__Relationship__Group__5__Impl rule__Relationship__Group__6
            {
            pushFollow(FOLLOW_21);
            rule__Relationship__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__5"


    // $ANTLR start "rule__Relationship__Group__5__Impl"
    // InternalXaaS.g:2225:1: rule__Relationship__Group__5__Impl : ( ( rule__Relationship__TypeAssignment_5 ) ) ;
    public final void rule__Relationship__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2229:1: ( ( ( rule__Relationship__TypeAssignment_5 ) ) )
            // InternalXaaS.g:2230:1: ( ( rule__Relationship__TypeAssignment_5 ) )
            {
            // InternalXaaS.g:2230:1: ( ( rule__Relationship__TypeAssignment_5 ) )
            // InternalXaaS.g:2231:2: ( rule__Relationship__TypeAssignment_5 )
            {
             before(grammarAccess.getRelationshipAccess().getTypeAssignment_5()); 
            // InternalXaaS.g:2232:2: ( rule__Relationship__TypeAssignment_5 )
            // InternalXaaS.g:2232:3: rule__Relationship__TypeAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__TypeAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipAccess().getTypeAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__5__Impl"


    // $ANTLR start "rule__Relationship__Group__6"
    // InternalXaaS.g:2240:1: rule__Relationship__Group__6 : rule__Relationship__Group__6__Impl rule__Relationship__Group__7 ;
    public final void rule__Relationship__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2244:1: ( rule__Relationship__Group__6__Impl rule__Relationship__Group__7 )
            // InternalXaaS.g:2245:2: rule__Relationship__Group__6__Impl rule__Relationship__Group__7
            {
            pushFollow(FOLLOW_21);
            rule__Relationship__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__6"


    // $ANTLR start "rule__Relationship__Group__6__Impl"
    // InternalXaaS.g:2252:1: rule__Relationship__Group__6__Impl : ( ( rule__Relationship__Group_6__0 )? ) ;
    public final void rule__Relationship__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2256:1: ( ( ( rule__Relationship__Group_6__0 )? ) )
            // InternalXaaS.g:2257:1: ( ( rule__Relationship__Group_6__0 )? )
            {
            // InternalXaaS.g:2257:1: ( ( rule__Relationship__Group_6__0 )? )
            // InternalXaaS.g:2258:2: ( rule__Relationship__Group_6__0 )?
            {
             before(grammarAccess.getRelationshipAccess().getGroup_6()); 
            // InternalXaaS.g:2259:2: ( rule__Relationship__Group_6__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==40) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalXaaS.g:2259:3: rule__Relationship__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Relationship__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRelationshipAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__6__Impl"


    // $ANTLR start "rule__Relationship__Group__7"
    // InternalXaaS.g:2267:1: rule__Relationship__Group__7 : rule__Relationship__Group__7__Impl rule__Relationship__Group__8 ;
    public final void rule__Relationship__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2271:1: ( rule__Relationship__Group__7__Impl rule__Relationship__Group__8 )
            // InternalXaaS.g:2272:2: rule__Relationship__Group__7__Impl rule__Relationship__Group__8
            {
            pushFollow(FOLLOW_21);
            rule__Relationship__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__7"


    // $ANTLR start "rule__Relationship__Group__7__Impl"
    // InternalXaaS.g:2279:1: rule__Relationship__Group__7__Impl : ( ( rule__Relationship__Group_7__0 )? ) ;
    public final void rule__Relationship__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2283:1: ( ( ( rule__Relationship__Group_7__0 )? ) )
            // InternalXaaS.g:2284:1: ( ( rule__Relationship__Group_7__0 )? )
            {
            // InternalXaaS.g:2284:1: ( ( rule__Relationship__Group_7__0 )? )
            // InternalXaaS.g:2285:2: ( rule__Relationship__Group_7__0 )?
            {
             before(grammarAccess.getRelationshipAccess().getGroup_7()); 
            // InternalXaaS.g:2286:2: ( rule__Relationship__Group_7__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==41) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalXaaS.g:2286:3: rule__Relationship__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Relationship__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRelationshipAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__7__Impl"


    // $ANTLR start "rule__Relationship__Group__8"
    // InternalXaaS.g:2294:1: rule__Relationship__Group__8 : rule__Relationship__Group__8__Impl ;
    public final void rule__Relationship__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2298:1: ( rule__Relationship__Group__8__Impl )
            // InternalXaaS.g:2299:2: rule__Relationship__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__8"


    // $ANTLR start "rule__Relationship__Group__8__Impl"
    // InternalXaaS.g:2305:1: rule__Relationship__Group__8__Impl : ( ( rule__Relationship__Group_8__0 )? ) ;
    public final void rule__Relationship__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2309:1: ( ( ( rule__Relationship__Group_8__0 )? ) )
            // InternalXaaS.g:2310:1: ( ( rule__Relationship__Group_8__0 )? )
            {
            // InternalXaaS.g:2310:1: ( ( rule__Relationship__Group_8__0 )? )
            // InternalXaaS.g:2311:2: ( rule__Relationship__Group_8__0 )?
            {
             before(grammarAccess.getRelationshipAccess().getGroup_8()); 
            // InternalXaaS.g:2312:2: ( rule__Relationship__Group_8__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==42) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalXaaS.g:2312:3: rule__Relationship__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Relationship__Group_8__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRelationshipAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group__8__Impl"


    // $ANTLR start "rule__Relationship__Group_6__0"
    // InternalXaaS.g:2321:1: rule__Relationship__Group_6__0 : rule__Relationship__Group_6__0__Impl rule__Relationship__Group_6__1 ;
    public final void rule__Relationship__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2325:1: ( rule__Relationship__Group_6__0__Impl rule__Relationship__Group_6__1 )
            // InternalXaaS.g:2326:2: rule__Relationship__Group_6__0__Impl rule__Relationship__Group_6__1
            {
            pushFollow(FOLLOW_4);
            rule__Relationship__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_6__0"


    // $ANTLR start "rule__Relationship__Group_6__0__Impl"
    // InternalXaaS.g:2333:1: rule__Relationship__Group_6__0__Impl : ( 'constant' ) ;
    public final void rule__Relationship__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2337:1: ( ( 'constant' ) )
            // InternalXaaS.g:2338:1: ( 'constant' )
            {
            // InternalXaaS.g:2338:1: ( 'constant' )
            // InternalXaaS.g:2339:2: 'constant'
            {
             before(grammarAccess.getRelationshipAccess().getConstantKeyword_6_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getConstantKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_6__0__Impl"


    // $ANTLR start "rule__Relationship__Group_6__1"
    // InternalXaaS.g:2348:1: rule__Relationship__Group_6__1 : rule__Relationship__Group_6__1__Impl rule__Relationship__Group_6__2 ;
    public final void rule__Relationship__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2352:1: ( rule__Relationship__Group_6__1__Impl rule__Relationship__Group_6__2 )
            // InternalXaaS.g:2353:2: rule__Relationship__Group_6__1__Impl rule__Relationship__Group_6__2
            {
            pushFollow(FOLLOW_22);
            rule__Relationship__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_6__1"


    // $ANTLR start "rule__Relationship__Group_6__1__Impl"
    // InternalXaaS.g:2360:1: rule__Relationship__Group_6__1__Impl : ( ':' ) ;
    public final void rule__Relationship__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2364:1: ( ( ':' ) )
            // InternalXaaS.g:2365:1: ( ':' )
            {
            // InternalXaaS.g:2365:1: ( ':' )
            // InternalXaaS.g:2366:2: ':'
            {
             before(grammarAccess.getRelationshipAccess().getColonKeyword_6_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getColonKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_6__1__Impl"


    // $ANTLR start "rule__Relationship__Group_6__2"
    // InternalXaaS.g:2375:1: rule__Relationship__Group_6__2 : rule__Relationship__Group_6__2__Impl ;
    public final void rule__Relationship__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2379:1: ( rule__Relationship__Group_6__2__Impl )
            // InternalXaaS.g:2380:2: rule__Relationship__Group_6__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__Group_6__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_6__2"


    // $ANTLR start "rule__Relationship__Group_6__2__Impl"
    // InternalXaaS.g:2386:1: rule__Relationship__Group_6__2__Impl : ( ( rule__Relationship__ConstantAssignment_6_2 ) ) ;
    public final void rule__Relationship__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2390:1: ( ( ( rule__Relationship__ConstantAssignment_6_2 ) ) )
            // InternalXaaS.g:2391:1: ( ( rule__Relationship__ConstantAssignment_6_2 ) )
            {
            // InternalXaaS.g:2391:1: ( ( rule__Relationship__ConstantAssignment_6_2 ) )
            // InternalXaaS.g:2392:2: ( rule__Relationship__ConstantAssignment_6_2 )
            {
             before(grammarAccess.getRelationshipAccess().getConstantAssignment_6_2()); 
            // InternalXaaS.g:2393:2: ( rule__Relationship__ConstantAssignment_6_2 )
            // InternalXaaS.g:2393:3: rule__Relationship__ConstantAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__ConstantAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipAccess().getConstantAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_6__2__Impl"


    // $ANTLR start "rule__Relationship__Group_7__0"
    // InternalXaaS.g:2402:1: rule__Relationship__Group_7__0 : rule__Relationship__Group_7__0__Impl rule__Relationship__Group_7__1 ;
    public final void rule__Relationship__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2406:1: ( rule__Relationship__Group_7__0__Impl rule__Relationship__Group_7__1 )
            // InternalXaaS.g:2407:2: rule__Relationship__Group_7__0__Impl rule__Relationship__Group_7__1
            {
            pushFollow(FOLLOW_4);
            rule__Relationship__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_7__0"


    // $ANTLR start "rule__Relationship__Group_7__0__Impl"
    // InternalXaaS.g:2414:1: rule__Relationship__Group_7__0__Impl : ( 'source' ) ;
    public final void rule__Relationship__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2418:1: ( ( 'source' ) )
            // InternalXaaS.g:2419:1: ( 'source' )
            {
            // InternalXaaS.g:2419:1: ( 'source' )
            // InternalXaaS.g:2420:2: 'source'
            {
             before(grammarAccess.getRelationshipAccess().getSourceKeyword_7_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getSourceKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_7__0__Impl"


    // $ANTLR start "rule__Relationship__Group_7__1"
    // InternalXaaS.g:2429:1: rule__Relationship__Group_7__1 : rule__Relationship__Group_7__1__Impl rule__Relationship__Group_7__2 ;
    public final void rule__Relationship__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2433:1: ( rule__Relationship__Group_7__1__Impl rule__Relationship__Group_7__2 )
            // InternalXaaS.g:2434:2: rule__Relationship__Group_7__1__Impl rule__Relationship__Group_7__2
            {
            pushFollow(FOLLOW_6);
            rule__Relationship__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_7__1"


    // $ANTLR start "rule__Relationship__Group_7__1__Impl"
    // InternalXaaS.g:2441:1: rule__Relationship__Group_7__1__Impl : ( ':' ) ;
    public final void rule__Relationship__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2445:1: ( ( ':' ) )
            // InternalXaaS.g:2446:1: ( ':' )
            {
            // InternalXaaS.g:2446:1: ( ':' )
            // InternalXaaS.g:2447:2: ':'
            {
             before(grammarAccess.getRelationshipAccess().getColonKeyword_7_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getColonKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_7__1__Impl"


    // $ANTLR start "rule__Relationship__Group_7__2"
    // InternalXaaS.g:2456:1: rule__Relationship__Group_7__2 : rule__Relationship__Group_7__2__Impl ;
    public final void rule__Relationship__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2460:1: ( rule__Relationship__Group_7__2__Impl )
            // InternalXaaS.g:2461:2: rule__Relationship__Group_7__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__Group_7__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_7__2"


    // $ANTLR start "rule__Relationship__Group_7__2__Impl"
    // InternalXaaS.g:2467:1: rule__Relationship__Group_7__2__Impl : ( ( rule__Relationship__SourceAssignment_7_2 ) ) ;
    public final void rule__Relationship__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2471:1: ( ( ( rule__Relationship__SourceAssignment_7_2 ) ) )
            // InternalXaaS.g:2472:1: ( ( rule__Relationship__SourceAssignment_7_2 ) )
            {
            // InternalXaaS.g:2472:1: ( ( rule__Relationship__SourceAssignment_7_2 ) )
            // InternalXaaS.g:2473:2: ( rule__Relationship__SourceAssignment_7_2 )
            {
             before(grammarAccess.getRelationshipAccess().getSourceAssignment_7_2()); 
            // InternalXaaS.g:2474:2: ( rule__Relationship__SourceAssignment_7_2 )
            // InternalXaaS.g:2474:3: rule__Relationship__SourceAssignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__SourceAssignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipAccess().getSourceAssignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_7__2__Impl"


    // $ANTLR start "rule__Relationship__Group_8__0"
    // InternalXaaS.g:2483:1: rule__Relationship__Group_8__0 : rule__Relationship__Group_8__0__Impl rule__Relationship__Group_8__1 ;
    public final void rule__Relationship__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2487:1: ( rule__Relationship__Group_8__0__Impl rule__Relationship__Group_8__1 )
            // InternalXaaS.g:2488:2: rule__Relationship__Group_8__0__Impl rule__Relationship__Group_8__1
            {
            pushFollow(FOLLOW_4);
            rule__Relationship__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_8__0"


    // $ANTLR start "rule__Relationship__Group_8__0__Impl"
    // InternalXaaS.g:2495:1: rule__Relationship__Group_8__0__Impl : ( 'target' ) ;
    public final void rule__Relationship__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2499:1: ( ( 'target' ) )
            // InternalXaaS.g:2500:1: ( 'target' )
            {
            // InternalXaaS.g:2500:1: ( 'target' )
            // InternalXaaS.g:2501:2: 'target'
            {
             before(grammarAccess.getRelationshipAccess().getTargetKeyword_8_0()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getTargetKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_8__0__Impl"


    // $ANTLR start "rule__Relationship__Group_8__1"
    // InternalXaaS.g:2510:1: rule__Relationship__Group_8__1 : rule__Relationship__Group_8__1__Impl rule__Relationship__Group_8__2 ;
    public final void rule__Relationship__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2514:1: ( rule__Relationship__Group_8__1__Impl rule__Relationship__Group_8__2 )
            // InternalXaaS.g:2515:2: rule__Relationship__Group_8__1__Impl rule__Relationship__Group_8__2
            {
            pushFollow(FOLLOW_6);
            rule__Relationship__Group_8__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Relationship__Group_8__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_8__1"


    // $ANTLR start "rule__Relationship__Group_8__1__Impl"
    // InternalXaaS.g:2522:1: rule__Relationship__Group_8__1__Impl : ( ':' ) ;
    public final void rule__Relationship__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2526:1: ( ( ':' ) )
            // InternalXaaS.g:2527:1: ( ':' )
            {
            // InternalXaaS.g:2527:1: ( ':' )
            // InternalXaaS.g:2528:2: ':'
            {
             before(grammarAccess.getRelationshipAccess().getColonKeyword_8_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipAccess().getColonKeyword_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_8__1__Impl"


    // $ANTLR start "rule__Relationship__Group_8__2"
    // InternalXaaS.g:2537:1: rule__Relationship__Group_8__2 : rule__Relationship__Group_8__2__Impl ;
    public final void rule__Relationship__Group_8__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2541:1: ( rule__Relationship__Group_8__2__Impl )
            // InternalXaaS.g:2542:2: rule__Relationship__Group_8__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__Group_8__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_8__2"


    // $ANTLR start "rule__Relationship__Group_8__2__Impl"
    // InternalXaaS.g:2548:1: rule__Relationship__Group_8__2__Impl : ( ( rule__Relationship__TargetAssignment_8_2 ) ) ;
    public final void rule__Relationship__Group_8__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2552:1: ( ( ( rule__Relationship__TargetAssignment_8_2 ) ) )
            // InternalXaaS.g:2553:1: ( ( rule__Relationship__TargetAssignment_8_2 ) )
            {
            // InternalXaaS.g:2553:1: ( ( rule__Relationship__TargetAssignment_8_2 ) )
            // InternalXaaS.g:2554:2: ( rule__Relationship__TargetAssignment_8_2 )
            {
             before(grammarAccess.getRelationshipAccess().getTargetAssignment_8_2()); 
            // InternalXaaS.g:2555:2: ( rule__Relationship__TargetAssignment_8_2 )
            // InternalXaaS.g:2555:3: rule__Relationship__TargetAssignment_8_2
            {
            pushFollow(FOLLOW_2);
            rule__Relationship__TargetAssignment_8_2();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipAccess().getTargetAssignment_8_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__Group_8__2__Impl"


    // $ANTLR start "rule__NodeType__Group__0"
    // InternalXaaS.g:2564:1: rule__NodeType__Group__0 : rule__NodeType__Group__0__Impl rule__NodeType__Group__1 ;
    public final void rule__NodeType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2568:1: ( rule__NodeType__Group__0__Impl rule__NodeType__Group__1 )
            // InternalXaaS.g:2569:2: rule__NodeType__Group__0__Impl rule__NodeType__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__NodeType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__0"


    // $ANTLR start "rule__NodeType__Group__0__Impl"
    // InternalXaaS.g:2576:1: rule__NodeType__Group__0__Impl : ( () ) ;
    public final void rule__NodeType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2580:1: ( ( () ) )
            // InternalXaaS.g:2581:1: ( () )
            {
            // InternalXaaS.g:2581:1: ( () )
            // InternalXaaS.g:2582:2: ()
            {
             before(grammarAccess.getNodeTypeAccess().getNodeTypeAction_0()); 
            // InternalXaaS.g:2583:2: ()
            // InternalXaaS.g:2583:3: 
            {
            }

             after(grammarAccess.getNodeTypeAccess().getNodeTypeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__0__Impl"


    // $ANTLR start "rule__NodeType__Group__1"
    // InternalXaaS.g:2591:1: rule__NodeType__Group__1 : rule__NodeType__Group__1__Impl rule__NodeType__Group__2 ;
    public final void rule__NodeType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2595:1: ( rule__NodeType__Group__1__Impl rule__NodeType__Group__2 )
            // InternalXaaS.g:2596:2: rule__NodeType__Group__1__Impl rule__NodeType__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__NodeType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__1"


    // $ANTLR start "rule__NodeType__Group__1__Impl"
    // InternalXaaS.g:2603:1: rule__NodeType__Group__1__Impl : ( ( rule__NodeType__NameAssignment_1 ) ) ;
    public final void rule__NodeType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2607:1: ( ( ( rule__NodeType__NameAssignment_1 ) ) )
            // InternalXaaS.g:2608:1: ( ( rule__NodeType__NameAssignment_1 ) )
            {
            // InternalXaaS.g:2608:1: ( ( rule__NodeType__NameAssignment_1 ) )
            // InternalXaaS.g:2609:2: ( rule__NodeType__NameAssignment_1 )
            {
             before(grammarAccess.getNodeTypeAccess().getNameAssignment_1()); 
            // InternalXaaS.g:2610:2: ( rule__NodeType__NameAssignment_1 )
            // InternalXaaS.g:2610:3: rule__NodeType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getNodeTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__1__Impl"


    // $ANTLR start "rule__NodeType__Group__2"
    // InternalXaaS.g:2618:1: rule__NodeType__Group__2 : rule__NodeType__Group__2__Impl rule__NodeType__Group__3 ;
    public final void rule__NodeType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2622:1: ( rule__NodeType__Group__2__Impl rule__NodeType__Group__3 )
            // InternalXaaS.g:2623:2: rule__NodeType__Group__2__Impl rule__NodeType__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__NodeType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__2"


    // $ANTLR start "rule__NodeType__Group__2__Impl"
    // InternalXaaS.g:2630:1: rule__NodeType__Group__2__Impl : ( ':' ) ;
    public final void rule__NodeType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2634:1: ( ( ':' ) )
            // InternalXaaS.g:2635:1: ( ':' )
            {
            // InternalXaaS.g:2635:1: ( ':' )
            // InternalXaaS.g:2636:2: ':'
            {
             before(grammarAccess.getNodeTypeAccess().getColonKeyword_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__2__Impl"


    // $ANTLR start "rule__NodeType__Group__3"
    // InternalXaaS.g:2645:1: rule__NodeType__Group__3 : rule__NodeType__Group__3__Impl rule__NodeType__Group__4 ;
    public final void rule__NodeType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2649:1: ( rule__NodeType__Group__3__Impl rule__NodeType__Group__4 )
            // InternalXaaS.g:2650:2: rule__NodeType__Group__3__Impl rule__NodeType__Group__4
            {
            pushFollow(FOLLOW_23);
            rule__NodeType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__3"


    // $ANTLR start "rule__NodeType__Group__3__Impl"
    // InternalXaaS.g:2657:1: rule__NodeType__Group__3__Impl : ( ( rule__NodeType__Group_3__0 )? ) ;
    public final void rule__NodeType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2661:1: ( ( ( rule__NodeType__Group_3__0 )? ) )
            // InternalXaaS.g:2662:1: ( ( rule__NodeType__Group_3__0 )? )
            {
            // InternalXaaS.g:2662:1: ( ( rule__NodeType__Group_3__0 )? )
            // InternalXaaS.g:2663:2: ( rule__NodeType__Group_3__0 )?
            {
             before(grammarAccess.getNodeTypeAccess().getGroup_3()); 
            // InternalXaaS.g:2664:2: ( rule__NodeType__Group_3__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==43) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalXaaS.g:2664:3: rule__NodeType__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__NodeType__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNodeTypeAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__3__Impl"


    // $ANTLR start "rule__NodeType__Group__4"
    // InternalXaaS.g:2672:1: rule__NodeType__Group__4 : rule__NodeType__Group__4__Impl ;
    public final void rule__NodeType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2676:1: ( rule__NodeType__Group__4__Impl )
            // InternalXaaS.g:2677:2: rule__NodeType__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__4"


    // $ANTLR start "rule__NodeType__Group__4__Impl"
    // InternalXaaS.g:2683:1: rule__NodeType__Group__4__Impl : ( ( rule__NodeType__Group_4__0 )? ) ;
    public final void rule__NodeType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2687:1: ( ( ( rule__NodeType__Group_4__0 )? ) )
            // InternalXaaS.g:2688:1: ( ( rule__NodeType__Group_4__0 )? )
            {
            // InternalXaaS.g:2688:1: ( ( rule__NodeType__Group_4__0 )? )
            // InternalXaaS.g:2689:2: ( rule__NodeType__Group_4__0 )?
            {
             before(grammarAccess.getNodeTypeAccess().getGroup_4()); 
            // InternalXaaS.g:2690:2: ( rule__NodeType__Group_4__0 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==38) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalXaaS.g:2690:3: rule__NodeType__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__NodeType__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNodeTypeAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group__4__Impl"


    // $ANTLR start "rule__NodeType__Group_3__0"
    // InternalXaaS.g:2699:1: rule__NodeType__Group_3__0 : rule__NodeType__Group_3__0__Impl rule__NodeType__Group_3__1 ;
    public final void rule__NodeType__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2703:1: ( rule__NodeType__Group_3__0__Impl rule__NodeType__Group_3__1 )
            // InternalXaaS.g:2704:2: rule__NodeType__Group_3__0__Impl rule__NodeType__Group_3__1
            {
            pushFollow(FOLLOW_4);
            rule__NodeType__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_3__0"


    // $ANTLR start "rule__NodeType__Group_3__0__Impl"
    // InternalXaaS.g:2711:1: rule__NodeType__Group_3__0__Impl : ( 'derived_from' ) ;
    public final void rule__NodeType__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2715:1: ( ( 'derived_from' ) )
            // InternalXaaS.g:2716:1: ( 'derived_from' )
            {
            // InternalXaaS.g:2716:1: ( 'derived_from' )
            // InternalXaaS.g:2717:2: 'derived_from'
            {
             before(grammarAccess.getNodeTypeAccess().getDerived_fromKeyword_3_0()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getDerived_fromKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_3__0__Impl"


    // $ANTLR start "rule__NodeType__Group_3__1"
    // InternalXaaS.g:2726:1: rule__NodeType__Group_3__1 : rule__NodeType__Group_3__1__Impl rule__NodeType__Group_3__2 ;
    public final void rule__NodeType__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2730:1: ( rule__NodeType__Group_3__1__Impl rule__NodeType__Group_3__2 )
            // InternalXaaS.g:2731:2: rule__NodeType__Group_3__1__Impl rule__NodeType__Group_3__2
            {
            pushFollow(FOLLOW_6);
            rule__NodeType__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_3__1"


    // $ANTLR start "rule__NodeType__Group_3__1__Impl"
    // InternalXaaS.g:2738:1: rule__NodeType__Group_3__1__Impl : ( ':' ) ;
    public final void rule__NodeType__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2742:1: ( ( ':' ) )
            // InternalXaaS.g:2743:1: ( ':' )
            {
            // InternalXaaS.g:2743:1: ( ':' )
            // InternalXaaS.g:2744:2: ':'
            {
             before(grammarAccess.getNodeTypeAccess().getColonKeyword_3_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getColonKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_3__1__Impl"


    // $ANTLR start "rule__NodeType__Group_3__2"
    // InternalXaaS.g:2753:1: rule__NodeType__Group_3__2 : rule__NodeType__Group_3__2__Impl ;
    public final void rule__NodeType__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2757:1: ( rule__NodeType__Group_3__2__Impl )
            // InternalXaaS.g:2758:2: rule__NodeType__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_3__2"


    // $ANTLR start "rule__NodeType__Group_3__2__Impl"
    // InternalXaaS.g:2764:1: rule__NodeType__Group_3__2__Impl : ( ( rule__NodeType__InheritedTypeAssignment_3_2 ) ) ;
    public final void rule__NodeType__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2768:1: ( ( ( rule__NodeType__InheritedTypeAssignment_3_2 ) ) )
            // InternalXaaS.g:2769:1: ( ( rule__NodeType__InheritedTypeAssignment_3_2 ) )
            {
            // InternalXaaS.g:2769:1: ( ( rule__NodeType__InheritedTypeAssignment_3_2 ) )
            // InternalXaaS.g:2770:2: ( rule__NodeType__InheritedTypeAssignment_3_2 )
            {
             before(grammarAccess.getNodeTypeAccess().getInheritedTypeAssignment_3_2()); 
            // InternalXaaS.g:2771:2: ( rule__NodeType__InheritedTypeAssignment_3_2 )
            // InternalXaaS.g:2771:3: rule__NodeType__InheritedTypeAssignment_3_2
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__InheritedTypeAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getNodeTypeAccess().getInheritedTypeAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_3__2__Impl"


    // $ANTLR start "rule__NodeType__Group_4__0"
    // InternalXaaS.g:2780:1: rule__NodeType__Group_4__0 : rule__NodeType__Group_4__0__Impl rule__NodeType__Group_4__1 ;
    public final void rule__NodeType__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2784:1: ( rule__NodeType__Group_4__0__Impl rule__NodeType__Group_4__1 )
            // InternalXaaS.g:2785:2: rule__NodeType__Group_4__0__Impl rule__NodeType__Group_4__1
            {
            pushFollow(FOLLOW_4);
            rule__NodeType__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4__0"


    // $ANTLR start "rule__NodeType__Group_4__0__Impl"
    // InternalXaaS.g:2792:1: rule__NodeType__Group_4__0__Impl : ( 'properties' ) ;
    public final void rule__NodeType__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2796:1: ( ( 'properties' ) )
            // InternalXaaS.g:2797:1: ( 'properties' )
            {
            // InternalXaaS.g:2797:1: ( 'properties' )
            // InternalXaaS.g:2798:2: 'properties'
            {
             before(grammarAccess.getNodeTypeAccess().getPropertiesKeyword_4_0()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getPropertiesKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4__0__Impl"


    // $ANTLR start "rule__NodeType__Group_4__1"
    // InternalXaaS.g:2807:1: rule__NodeType__Group_4__1 : rule__NodeType__Group_4__1__Impl rule__NodeType__Group_4__2 ;
    public final void rule__NodeType__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2811:1: ( rule__NodeType__Group_4__1__Impl rule__NodeType__Group_4__2 )
            // InternalXaaS.g:2812:2: rule__NodeType__Group_4__1__Impl rule__NodeType__Group_4__2
            {
            pushFollow(FOLLOW_24);
            rule__NodeType__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4__1"


    // $ANTLR start "rule__NodeType__Group_4__1__Impl"
    // InternalXaaS.g:2819:1: rule__NodeType__Group_4__1__Impl : ( ':' ) ;
    public final void rule__NodeType__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2823:1: ( ( ':' ) )
            // InternalXaaS.g:2824:1: ( ':' )
            {
            // InternalXaaS.g:2824:1: ( ':' )
            // InternalXaaS.g:2825:2: ':'
            {
             before(grammarAccess.getNodeTypeAccess().getColonKeyword_4_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getColonKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4__1__Impl"


    // $ANTLR start "rule__NodeType__Group_4__2"
    // InternalXaaS.g:2834:1: rule__NodeType__Group_4__2 : rule__NodeType__Group_4__2__Impl ;
    public final void rule__NodeType__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2838:1: ( rule__NodeType__Group_4__2__Impl )
            // InternalXaaS.g:2839:2: rule__NodeType__Group_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4__2"


    // $ANTLR start "rule__NodeType__Group_4__2__Impl"
    // InternalXaaS.g:2845:1: rule__NodeType__Group_4__2__Impl : ( ( rule__NodeType__Group_4_2__0 )* ) ;
    public final void rule__NodeType__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2849:1: ( ( ( rule__NodeType__Group_4_2__0 )* ) )
            // InternalXaaS.g:2850:1: ( ( rule__NodeType__Group_4_2__0 )* )
            {
            // InternalXaaS.g:2850:1: ( ( rule__NodeType__Group_4_2__0 )* )
            // InternalXaaS.g:2851:2: ( rule__NodeType__Group_4_2__0 )*
            {
             before(grammarAccess.getNodeTypeAccess().getGroup_4_2()); 
            // InternalXaaS.g:2852:2: ( rule__NodeType__Group_4_2__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==40||(LA24_0>=44 && LA24_0<=45)||LA24_0==47) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalXaaS.g:2852:3: rule__NodeType__Group_4_2__0
            	    {
            	    pushFollow(FOLLOW_25);
            	    rule__NodeType__Group_4_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getNodeTypeAccess().getGroup_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4__2__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2__0"
    // InternalXaaS.g:2861:1: rule__NodeType__Group_4_2__0 : rule__NodeType__Group_4_2__0__Impl rule__NodeType__Group_4_2__1 ;
    public final void rule__NodeType__Group_4_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2865:1: ( rule__NodeType__Group_4_2__0__Impl rule__NodeType__Group_4_2__1 )
            // InternalXaaS.g:2866:2: rule__NodeType__Group_4_2__0__Impl rule__NodeType__Group_4_2__1
            {
            pushFollow(FOLLOW_24);
            rule__NodeType__Group_4_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2__0"


    // $ANTLR start "rule__NodeType__Group_4_2__0__Impl"
    // InternalXaaS.g:2873:1: rule__NodeType__Group_4_2__0__Impl : ( ( rule__NodeType__Group_4_2_0__0 )? ) ;
    public final void rule__NodeType__Group_4_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2877:1: ( ( ( rule__NodeType__Group_4_2_0__0 )? ) )
            // InternalXaaS.g:2878:1: ( ( rule__NodeType__Group_4_2_0__0 )? )
            {
            // InternalXaaS.g:2878:1: ( ( rule__NodeType__Group_4_2_0__0 )? )
            // InternalXaaS.g:2879:2: ( rule__NodeType__Group_4_2_0__0 )?
            {
             before(grammarAccess.getNodeTypeAccess().getGroup_4_2_0()); 
            // InternalXaaS.g:2880:2: ( rule__NodeType__Group_4_2_0__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==44) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalXaaS.g:2880:3: rule__NodeType__Group_4_2_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__NodeType__Group_4_2_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNodeTypeAccess().getGroup_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2__0__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2__1"
    // InternalXaaS.g:2888:1: rule__NodeType__Group_4_2__1 : rule__NodeType__Group_4_2__1__Impl rule__NodeType__Group_4_2__2 ;
    public final void rule__NodeType__Group_4_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2892:1: ( rule__NodeType__Group_4_2__1__Impl rule__NodeType__Group_4_2__2 )
            // InternalXaaS.g:2893:2: rule__NodeType__Group_4_2__1__Impl rule__NodeType__Group_4_2__2
            {
            pushFollow(FOLLOW_24);
            rule__NodeType__Group_4_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2__1"


    // $ANTLR start "rule__NodeType__Group_4_2__1__Impl"
    // InternalXaaS.g:2900:1: rule__NodeType__Group_4_2__1__Impl : ( ( rule__NodeType__Group_4_2_1__0 )? ) ;
    public final void rule__NodeType__Group_4_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2904:1: ( ( ( rule__NodeType__Group_4_2_1__0 )? ) )
            // InternalXaaS.g:2905:1: ( ( rule__NodeType__Group_4_2_1__0 )? )
            {
            // InternalXaaS.g:2905:1: ( ( rule__NodeType__Group_4_2_1__0 )? )
            // InternalXaaS.g:2906:2: ( rule__NodeType__Group_4_2_1__0 )?
            {
             before(grammarAccess.getNodeTypeAccess().getGroup_4_2_1()); 
            // InternalXaaS.g:2907:2: ( rule__NodeType__Group_4_2_1__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==45) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalXaaS.g:2907:3: rule__NodeType__Group_4_2_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__NodeType__Group_4_2_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNodeTypeAccess().getGroup_4_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2__1__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2__2"
    // InternalXaaS.g:2915:1: rule__NodeType__Group_4_2__2 : rule__NodeType__Group_4_2__2__Impl rule__NodeType__Group_4_2__3 ;
    public final void rule__NodeType__Group_4_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2919:1: ( rule__NodeType__Group_4_2__2__Impl rule__NodeType__Group_4_2__3 )
            // InternalXaaS.g:2920:2: rule__NodeType__Group_4_2__2__Impl rule__NodeType__Group_4_2__3
            {
            pushFollow(FOLLOW_26);
            rule__NodeType__Group_4_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2__2"


    // $ANTLR start "rule__NodeType__Group_4_2__2__Impl"
    // InternalXaaS.g:2927:1: rule__NodeType__Group_4_2__2__Impl : ( ( rule__NodeType__AttributeTypesAssignment_4_2_2 ) ) ;
    public final void rule__NodeType__Group_4_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2931:1: ( ( ( rule__NodeType__AttributeTypesAssignment_4_2_2 ) ) )
            // InternalXaaS.g:2932:1: ( ( rule__NodeType__AttributeTypesAssignment_4_2_2 ) )
            {
            // InternalXaaS.g:2932:1: ( ( rule__NodeType__AttributeTypesAssignment_4_2_2 ) )
            // InternalXaaS.g:2933:2: ( rule__NodeType__AttributeTypesAssignment_4_2_2 )
            {
             before(grammarAccess.getNodeTypeAccess().getAttributeTypesAssignment_4_2_2()); 
            // InternalXaaS.g:2934:2: ( rule__NodeType__AttributeTypesAssignment_4_2_2 )
            // InternalXaaS.g:2934:3: rule__NodeType__AttributeTypesAssignment_4_2_2
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__AttributeTypesAssignment_4_2_2();

            state._fsp--;


            }

             after(grammarAccess.getNodeTypeAccess().getAttributeTypesAssignment_4_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2__2__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2__3"
    // InternalXaaS.g:2942:1: rule__NodeType__Group_4_2__3 : rule__NodeType__Group_4_2__3__Impl ;
    public final void rule__NodeType__Group_4_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2946:1: ( rule__NodeType__Group_4_2__3__Impl )
            // InternalXaaS.g:2947:2: rule__NodeType__Group_4_2__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2__3"


    // $ANTLR start "rule__NodeType__Group_4_2__3__Impl"
    // InternalXaaS.g:2953:1: rule__NodeType__Group_4_2__3__Impl : ( ( rule__NodeType__Group_4_2_3__0 )? ) ;
    public final void rule__NodeType__Group_4_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2957:1: ( ( ( rule__NodeType__Group_4_2_3__0 )? ) )
            // InternalXaaS.g:2958:1: ( ( rule__NodeType__Group_4_2_3__0 )? )
            {
            // InternalXaaS.g:2958:1: ( ( rule__NodeType__Group_4_2_3__0 )? )
            // InternalXaaS.g:2959:2: ( rule__NodeType__Group_4_2_3__0 )?
            {
             before(grammarAccess.getNodeTypeAccess().getGroup_4_2_3()); 
            // InternalXaaS.g:2960:2: ( rule__NodeType__Group_4_2_3__0 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==46) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalXaaS.g:2960:3: rule__NodeType__Group_4_2_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__NodeType__Group_4_2_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNodeTypeAccess().getGroup_4_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2__3__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2_0__0"
    // InternalXaaS.g:2969:1: rule__NodeType__Group_4_2_0__0 : rule__NodeType__Group_4_2_0__0__Impl rule__NodeType__Group_4_2_0__1 ;
    public final void rule__NodeType__Group_4_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2973:1: ( rule__NodeType__Group_4_2_0__0__Impl rule__NodeType__Group_4_2_0__1 )
            // InternalXaaS.g:2974:2: rule__NodeType__Group_4_2_0__0__Impl rule__NodeType__Group_4_2_0__1
            {
            pushFollow(FOLLOW_4);
            rule__NodeType__Group_4_2_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_0__0"


    // $ANTLR start "rule__NodeType__Group_4_2_0__0__Impl"
    // InternalXaaS.g:2981:1: rule__NodeType__Group_4_2_0__0__Impl : ( 'impactOfEnabling' ) ;
    public final void rule__NodeType__Group_4_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:2985:1: ( ( 'impactOfEnabling' ) )
            // InternalXaaS.g:2986:1: ( 'impactOfEnabling' )
            {
            // InternalXaaS.g:2986:1: ( 'impactOfEnabling' )
            // InternalXaaS.g:2987:2: 'impactOfEnabling'
            {
             before(grammarAccess.getNodeTypeAccess().getImpactOfEnablingKeyword_4_2_0_0()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getImpactOfEnablingKeyword_4_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_0__0__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2_0__1"
    // InternalXaaS.g:2996:1: rule__NodeType__Group_4_2_0__1 : rule__NodeType__Group_4_2_0__1__Impl rule__NodeType__Group_4_2_0__2 ;
    public final void rule__NodeType__Group_4_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3000:1: ( rule__NodeType__Group_4_2_0__1__Impl rule__NodeType__Group_4_2_0__2 )
            // InternalXaaS.g:3001:2: rule__NodeType__Group_4_2_0__1__Impl rule__NodeType__Group_4_2_0__2
            {
            pushFollow(FOLLOW_27);
            rule__NodeType__Group_4_2_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_0__1"


    // $ANTLR start "rule__NodeType__Group_4_2_0__1__Impl"
    // InternalXaaS.g:3008:1: rule__NodeType__Group_4_2_0__1__Impl : ( ':' ) ;
    public final void rule__NodeType__Group_4_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3012:1: ( ( ':' ) )
            // InternalXaaS.g:3013:1: ( ':' )
            {
            // InternalXaaS.g:3013:1: ( ':' )
            // InternalXaaS.g:3014:2: ':'
            {
             before(grammarAccess.getNodeTypeAccess().getColonKeyword_4_2_0_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getColonKeyword_4_2_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_0__1__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2_0__2"
    // InternalXaaS.g:3023:1: rule__NodeType__Group_4_2_0__2 : rule__NodeType__Group_4_2_0__2__Impl ;
    public final void rule__NodeType__Group_4_2_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3027:1: ( rule__NodeType__Group_4_2_0__2__Impl )
            // InternalXaaS.g:3028:2: rule__NodeType__Group_4_2_0__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_0__2"


    // $ANTLR start "rule__NodeType__Group_4_2_0__2__Impl"
    // InternalXaaS.g:3034:1: rule__NodeType__Group_4_2_0__2__Impl : ( ( rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2 ) ) ;
    public final void rule__NodeType__Group_4_2_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3038:1: ( ( ( rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2 ) ) )
            // InternalXaaS.g:3039:1: ( ( rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2 ) )
            {
            // InternalXaaS.g:3039:1: ( ( rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2 ) )
            // InternalXaaS.g:3040:2: ( rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2 )
            {
             before(grammarAccess.getNodeTypeAccess().getImpactOfEnablingAssignment_4_2_0_2()); 
            // InternalXaaS.g:3041:2: ( rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2 )
            // InternalXaaS.g:3041:3: rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2();

            state._fsp--;


            }

             after(grammarAccess.getNodeTypeAccess().getImpactOfEnablingAssignment_4_2_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_0__2__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2_1__0"
    // InternalXaaS.g:3050:1: rule__NodeType__Group_4_2_1__0 : rule__NodeType__Group_4_2_1__0__Impl rule__NodeType__Group_4_2_1__1 ;
    public final void rule__NodeType__Group_4_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3054:1: ( rule__NodeType__Group_4_2_1__0__Impl rule__NodeType__Group_4_2_1__1 )
            // InternalXaaS.g:3055:2: rule__NodeType__Group_4_2_1__0__Impl rule__NodeType__Group_4_2_1__1
            {
            pushFollow(FOLLOW_4);
            rule__NodeType__Group_4_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_1__0"


    // $ANTLR start "rule__NodeType__Group_4_2_1__0__Impl"
    // InternalXaaS.g:3062:1: rule__NodeType__Group_4_2_1__0__Impl : ( 'impactOfDisabling' ) ;
    public final void rule__NodeType__Group_4_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3066:1: ( ( 'impactOfDisabling' ) )
            // InternalXaaS.g:3067:1: ( 'impactOfDisabling' )
            {
            // InternalXaaS.g:3067:1: ( 'impactOfDisabling' )
            // InternalXaaS.g:3068:2: 'impactOfDisabling'
            {
             before(grammarAccess.getNodeTypeAccess().getImpactOfDisablingKeyword_4_2_1_0()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getImpactOfDisablingKeyword_4_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_1__0__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2_1__1"
    // InternalXaaS.g:3077:1: rule__NodeType__Group_4_2_1__1 : rule__NodeType__Group_4_2_1__1__Impl rule__NodeType__Group_4_2_1__2 ;
    public final void rule__NodeType__Group_4_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3081:1: ( rule__NodeType__Group_4_2_1__1__Impl rule__NodeType__Group_4_2_1__2 )
            // InternalXaaS.g:3082:2: rule__NodeType__Group_4_2_1__1__Impl rule__NodeType__Group_4_2_1__2
            {
            pushFollow(FOLLOW_27);
            rule__NodeType__Group_4_2_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_1__1"


    // $ANTLR start "rule__NodeType__Group_4_2_1__1__Impl"
    // InternalXaaS.g:3089:1: rule__NodeType__Group_4_2_1__1__Impl : ( ':' ) ;
    public final void rule__NodeType__Group_4_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3093:1: ( ( ':' ) )
            // InternalXaaS.g:3094:1: ( ':' )
            {
            // InternalXaaS.g:3094:1: ( ':' )
            // InternalXaaS.g:3095:2: ':'
            {
             before(grammarAccess.getNodeTypeAccess().getColonKeyword_4_2_1_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getColonKeyword_4_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_1__1__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2_1__2"
    // InternalXaaS.g:3104:1: rule__NodeType__Group_4_2_1__2 : rule__NodeType__Group_4_2_1__2__Impl ;
    public final void rule__NodeType__Group_4_2_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3108:1: ( rule__NodeType__Group_4_2_1__2__Impl )
            // InternalXaaS.g:3109:2: rule__NodeType__Group_4_2_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_1__2"


    // $ANTLR start "rule__NodeType__Group_4_2_1__2__Impl"
    // InternalXaaS.g:3115:1: rule__NodeType__Group_4_2_1__2__Impl : ( ( rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2 ) ) ;
    public final void rule__NodeType__Group_4_2_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3119:1: ( ( ( rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2 ) ) )
            // InternalXaaS.g:3120:1: ( ( rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2 ) )
            {
            // InternalXaaS.g:3120:1: ( ( rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2 ) )
            // InternalXaaS.g:3121:2: ( rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2 )
            {
             before(grammarAccess.getNodeTypeAccess().getImpactOfDisablingAssignment_4_2_1_2()); 
            // InternalXaaS.g:3122:2: ( rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2 )
            // InternalXaaS.g:3122:3: rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2();

            state._fsp--;


            }

             after(grammarAccess.getNodeTypeAccess().getImpactOfDisablingAssignment_4_2_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_1__2__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2_3__0"
    // InternalXaaS.g:3131:1: rule__NodeType__Group_4_2_3__0 : rule__NodeType__Group_4_2_3__0__Impl rule__NodeType__Group_4_2_3__1 ;
    public final void rule__NodeType__Group_4_2_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3135:1: ( rule__NodeType__Group_4_2_3__0__Impl rule__NodeType__Group_4_2_3__1 )
            // InternalXaaS.g:3136:2: rule__NodeType__Group_4_2_3__0__Impl rule__NodeType__Group_4_2_3__1
            {
            pushFollow(FOLLOW_4);
            rule__NodeType__Group_4_2_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_3__0"


    // $ANTLR start "rule__NodeType__Group_4_2_3__0__Impl"
    // InternalXaaS.g:3143:1: rule__NodeType__Group_4_2_3__0__Impl : ( 'constraints' ) ;
    public final void rule__NodeType__Group_4_2_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3147:1: ( ( 'constraints' ) )
            // InternalXaaS.g:3148:1: ( 'constraints' )
            {
            // InternalXaaS.g:3148:1: ( 'constraints' )
            // InternalXaaS.g:3149:2: 'constraints'
            {
             before(grammarAccess.getNodeTypeAccess().getConstraintsKeyword_4_2_3_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getConstraintsKeyword_4_2_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_3__0__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2_3__1"
    // InternalXaaS.g:3158:1: rule__NodeType__Group_4_2_3__1 : rule__NodeType__Group_4_2_3__1__Impl rule__NodeType__Group_4_2_3__2 ;
    public final void rule__NodeType__Group_4_2_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3162:1: ( rule__NodeType__Group_4_2_3__1__Impl rule__NodeType__Group_4_2_3__2 )
            // InternalXaaS.g:3163:2: rule__NodeType__Group_4_2_3__1__Impl rule__NodeType__Group_4_2_3__2
            {
            pushFollow(FOLLOW_6);
            rule__NodeType__Group_4_2_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_3__1"


    // $ANTLR start "rule__NodeType__Group_4_2_3__1__Impl"
    // InternalXaaS.g:3170:1: rule__NodeType__Group_4_2_3__1__Impl : ( ':' ) ;
    public final void rule__NodeType__Group_4_2_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3174:1: ( ( ':' ) )
            // InternalXaaS.g:3175:1: ( ':' )
            {
            // InternalXaaS.g:3175:1: ( ':' )
            // InternalXaaS.g:3176:2: ':'
            {
             before(grammarAccess.getNodeTypeAccess().getColonKeyword_4_2_3_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getNodeTypeAccess().getColonKeyword_4_2_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_3__1__Impl"


    // $ANTLR start "rule__NodeType__Group_4_2_3__2"
    // InternalXaaS.g:3185:1: rule__NodeType__Group_4_2_3__2 : rule__NodeType__Group_4_2_3__2__Impl ;
    public final void rule__NodeType__Group_4_2_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3189:1: ( rule__NodeType__Group_4_2_3__2__Impl )
            // InternalXaaS.g:3190:2: rule__NodeType__Group_4_2_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__Group_4_2_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_3__2"


    // $ANTLR start "rule__NodeType__Group_4_2_3__2__Impl"
    // InternalXaaS.g:3196:1: rule__NodeType__Group_4_2_3__2__Impl : ( ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 ) ) ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )* ) ) ;
    public final void rule__NodeType__Group_4_2_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3200:1: ( ( ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 ) ) ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )* ) ) )
            // InternalXaaS.g:3201:1: ( ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 ) ) ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )* ) )
            {
            // InternalXaaS.g:3201:1: ( ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 ) ) ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )* ) )
            // InternalXaaS.g:3202:2: ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 ) ) ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )* )
            {
            // InternalXaaS.g:3202:2: ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 ) )
            // InternalXaaS.g:3203:3: ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )
            {
             before(grammarAccess.getNodeTypeAccess().getConstraintsAssignment_4_2_3_2()); 
            // InternalXaaS.g:3204:3: ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )
            // InternalXaaS.g:3204:4: rule__NodeType__ConstraintsAssignment_4_2_3_2
            {
            pushFollow(FOLLOW_14);
            rule__NodeType__ConstraintsAssignment_4_2_3_2();

            state._fsp--;


            }

             after(grammarAccess.getNodeTypeAccess().getConstraintsAssignment_4_2_3_2()); 

            }

            // InternalXaaS.g:3207:2: ( ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )* )
            // InternalXaaS.g:3208:3: ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )*
            {
             before(grammarAccess.getNodeTypeAccess().getConstraintsAssignment_4_2_3_2()); 
            // InternalXaaS.g:3209:3: ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )*
            loop28:
            do {
                int alt28=2;
                alt28 = dfa28.predict(input);
                switch (alt28) {
            	case 1 :
            	    // InternalXaaS.g:3209:4: rule__NodeType__ConstraintsAssignment_4_2_3_2
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__NodeType__ConstraintsAssignment_4_2_3_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getNodeTypeAccess().getConstraintsAssignment_4_2_3_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Group_4_2_3__2__Impl"


    // $ANTLR start "rule__Attribute__Group__0"
    // InternalXaaS.g:3219:1: rule__Attribute__Group__0 : rule__Attribute__Group__0__Impl rule__Attribute__Group__1 ;
    public final void rule__Attribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3223:1: ( rule__Attribute__Group__0__Impl rule__Attribute__Group__1 )
            // InternalXaaS.g:3224:2: rule__Attribute__Group__0__Impl rule__Attribute__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Attribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0"


    // $ANTLR start "rule__Attribute__Group__0__Impl"
    // InternalXaaS.g:3231:1: rule__Attribute__Group__0__Impl : ( ( rule__Attribute__NameAssignment_0 ) ) ;
    public final void rule__Attribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3235:1: ( ( ( rule__Attribute__NameAssignment_0 ) ) )
            // InternalXaaS.g:3236:1: ( ( rule__Attribute__NameAssignment_0 ) )
            {
            // InternalXaaS.g:3236:1: ( ( rule__Attribute__NameAssignment_0 ) )
            // InternalXaaS.g:3237:2: ( rule__Attribute__NameAssignment_0 )
            {
             before(grammarAccess.getAttributeAccess().getNameAssignment_0()); 
            // InternalXaaS.g:3238:2: ( rule__Attribute__NameAssignment_0 )
            // InternalXaaS.g:3238:3: rule__Attribute__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0__Impl"


    // $ANTLR start "rule__Attribute__Group__1"
    // InternalXaaS.g:3246:1: rule__Attribute__Group__1 : rule__Attribute__Group__1__Impl rule__Attribute__Group__2 ;
    public final void rule__Attribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3250:1: ( rule__Attribute__Group__1__Impl rule__Attribute__Group__2 )
            // InternalXaaS.g:3251:2: rule__Attribute__Group__1__Impl rule__Attribute__Group__2
            {
            pushFollow(FOLLOW_28);
            rule__Attribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Attribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1"


    // $ANTLR start "rule__Attribute__Group__1__Impl"
    // InternalXaaS.g:3258:1: rule__Attribute__Group__1__Impl : ( ':' ) ;
    public final void rule__Attribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3262:1: ( ( ':' ) )
            // InternalXaaS.g:3263:1: ( ':' )
            {
            // InternalXaaS.g:3263:1: ( ':' )
            // InternalXaaS.g:3264:2: ':'
            {
             before(grammarAccess.getAttributeAccess().getColonKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getAttributeAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__2"
    // InternalXaaS.g:3273:1: rule__Attribute__Group__2 : rule__Attribute__Group__2__Impl ;
    public final void rule__Attribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3277:1: ( rule__Attribute__Group__2__Impl )
            // InternalXaaS.g:3278:2: rule__Attribute__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2"


    // $ANTLR start "rule__Attribute__Group__2__Impl"
    // InternalXaaS.g:3284:1: rule__Attribute__Group__2__Impl : ( ( rule__Attribute__ValueAssignment_2 ) ) ;
    public final void rule__Attribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3288:1: ( ( ( rule__Attribute__ValueAssignment_2 ) ) )
            // InternalXaaS.g:3289:1: ( ( rule__Attribute__ValueAssignment_2 ) )
            {
            // InternalXaaS.g:3289:1: ( ( rule__Attribute__ValueAssignment_2 ) )
            // InternalXaaS.g:3290:2: ( rule__Attribute__ValueAssignment_2 )
            {
             before(grammarAccess.getAttributeAccess().getValueAssignment_2()); 
            // InternalXaaS.g:3291:2: ( rule__Attribute__ValueAssignment_2 )
            // InternalXaaS.g:3291:3: rule__Attribute__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Attribute__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2__Impl"


    // $ANTLR start "rule__Constraint__Group__0"
    // InternalXaaS.g:3300:1: rule__Constraint__Group__0 : rule__Constraint__Group__0__Impl rule__Constraint__Group__1 ;
    public final void rule__Constraint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3304:1: ( rule__Constraint__Group__0__Impl rule__Constraint__Group__1 )
            // InternalXaaS.g:3305:2: rule__Constraint__Group__0__Impl rule__Constraint__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__Constraint__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0"


    // $ANTLR start "rule__Constraint__Group__0__Impl"
    // InternalXaaS.g:3312:1: rule__Constraint__Group__0__Impl : ( ( rule__Constraint__ExpressionsAssignment_0 ) ) ;
    public final void rule__Constraint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3316:1: ( ( ( rule__Constraint__ExpressionsAssignment_0 ) ) )
            // InternalXaaS.g:3317:1: ( ( rule__Constraint__ExpressionsAssignment_0 ) )
            {
            // InternalXaaS.g:3317:1: ( ( rule__Constraint__ExpressionsAssignment_0 ) )
            // InternalXaaS.g:3318:2: ( rule__Constraint__ExpressionsAssignment_0 )
            {
             before(grammarAccess.getConstraintAccess().getExpressionsAssignment_0()); 
            // InternalXaaS.g:3319:2: ( rule__Constraint__ExpressionsAssignment_0 )
            // InternalXaaS.g:3319:3: rule__Constraint__ExpressionsAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__ExpressionsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getExpressionsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0__Impl"


    // $ANTLR start "rule__Constraint__Group__1"
    // InternalXaaS.g:3327:1: rule__Constraint__Group__1 : rule__Constraint__Group__1__Impl rule__Constraint__Group__2 ;
    public final void rule__Constraint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3331:1: ( rule__Constraint__Group__1__Impl rule__Constraint__Group__2 )
            // InternalXaaS.g:3332:2: rule__Constraint__Group__1__Impl rule__Constraint__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Constraint__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1"


    // $ANTLR start "rule__Constraint__Group__1__Impl"
    // InternalXaaS.g:3339:1: rule__Constraint__Group__1__Impl : ( ( rule__Constraint__OperatorAssignment_1 ) ) ;
    public final void rule__Constraint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3343:1: ( ( ( rule__Constraint__OperatorAssignment_1 ) ) )
            // InternalXaaS.g:3344:1: ( ( rule__Constraint__OperatorAssignment_1 ) )
            {
            // InternalXaaS.g:3344:1: ( ( rule__Constraint__OperatorAssignment_1 ) )
            // InternalXaaS.g:3345:2: ( rule__Constraint__OperatorAssignment_1 )
            {
             before(grammarAccess.getConstraintAccess().getOperatorAssignment_1()); 
            // InternalXaaS.g:3346:2: ( rule__Constraint__OperatorAssignment_1 )
            // InternalXaaS.g:3346:3: rule__Constraint__OperatorAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__OperatorAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getOperatorAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1__Impl"


    // $ANTLR start "rule__Constraint__Group__2"
    // InternalXaaS.g:3354:1: rule__Constraint__Group__2 : rule__Constraint__Group__2__Impl rule__Constraint__Group__3 ;
    public final void rule__Constraint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3358:1: ( rule__Constraint__Group__2__Impl rule__Constraint__Group__3 )
            // InternalXaaS.g:3359:2: rule__Constraint__Group__2__Impl rule__Constraint__Group__3
            {
            pushFollow(FOLLOW_30);
            rule__Constraint__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2"


    // $ANTLR start "rule__Constraint__Group__2__Impl"
    // InternalXaaS.g:3366:1: rule__Constraint__Group__2__Impl : ( ':' ) ;
    public final void rule__Constraint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3370:1: ( ( ':' ) )
            // InternalXaaS.g:3371:1: ( ':' )
            {
            // InternalXaaS.g:3371:1: ( ':' )
            // InternalXaaS.g:3372:2: ':'
            {
             before(grammarAccess.getConstraintAccess().getColonKeyword_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getConstraintAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2__Impl"


    // $ANTLR start "rule__Constraint__Group__3"
    // InternalXaaS.g:3381:1: rule__Constraint__Group__3 : rule__Constraint__Group__3__Impl ;
    public final void rule__Constraint__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3385:1: ( rule__Constraint__Group__3__Impl )
            // InternalXaaS.g:3386:2: rule__Constraint__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__3"


    // $ANTLR start "rule__Constraint__Group__3__Impl"
    // InternalXaaS.g:3392:1: rule__Constraint__Group__3__Impl : ( ( rule__Constraint__ExpressionsAssignment_3 ) ) ;
    public final void rule__Constraint__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3396:1: ( ( ( rule__Constraint__ExpressionsAssignment_3 ) ) )
            // InternalXaaS.g:3397:1: ( ( rule__Constraint__ExpressionsAssignment_3 ) )
            {
            // InternalXaaS.g:3397:1: ( ( rule__Constraint__ExpressionsAssignment_3 ) )
            // InternalXaaS.g:3398:2: ( rule__Constraint__ExpressionsAssignment_3 )
            {
             before(grammarAccess.getConstraintAccess().getExpressionsAssignment_3()); 
            // InternalXaaS.g:3399:2: ( rule__Constraint__ExpressionsAssignment_3 )
            // InternalXaaS.g:3399:3: rule__Constraint__ExpressionsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__ExpressionsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getExpressionsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__3__Impl"


    // $ANTLR start "rule__ConstantAttributeType__Group__0"
    // InternalXaaS.g:3408:1: rule__ConstantAttributeType__Group__0 : rule__ConstantAttributeType__Group__0__Impl rule__ConstantAttributeType__Group__1 ;
    public final void rule__ConstantAttributeType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3412:1: ( rule__ConstantAttributeType__Group__0__Impl rule__ConstantAttributeType__Group__1 )
            // InternalXaaS.g:3413:2: rule__ConstantAttributeType__Group__0__Impl rule__ConstantAttributeType__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__ConstantAttributeType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConstantAttributeType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__0"


    // $ANTLR start "rule__ConstantAttributeType__Group__0__Impl"
    // InternalXaaS.g:3420:1: rule__ConstantAttributeType__Group__0__Impl : ( 'constant' ) ;
    public final void rule__ConstantAttributeType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3424:1: ( ( 'constant' ) )
            // InternalXaaS.g:3425:1: ( 'constant' )
            {
            // InternalXaaS.g:3425:1: ( 'constant' )
            // InternalXaaS.g:3426:2: 'constant'
            {
             before(grammarAccess.getConstantAttributeTypeAccess().getConstantKeyword_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getConstantAttributeTypeAccess().getConstantKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__0__Impl"


    // $ANTLR start "rule__ConstantAttributeType__Group__1"
    // InternalXaaS.g:3435:1: rule__ConstantAttributeType__Group__1 : rule__ConstantAttributeType__Group__1__Impl rule__ConstantAttributeType__Group__2 ;
    public final void rule__ConstantAttributeType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3439:1: ( rule__ConstantAttributeType__Group__1__Impl rule__ConstantAttributeType__Group__2 )
            // InternalXaaS.g:3440:2: rule__ConstantAttributeType__Group__1__Impl rule__ConstantAttributeType__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__ConstantAttributeType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConstantAttributeType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__1"


    // $ANTLR start "rule__ConstantAttributeType__Group__1__Impl"
    // InternalXaaS.g:3447:1: rule__ConstantAttributeType__Group__1__Impl : ( ( rule__ConstantAttributeType__NameAssignment_1 ) ) ;
    public final void rule__ConstantAttributeType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3451:1: ( ( ( rule__ConstantAttributeType__NameAssignment_1 ) ) )
            // InternalXaaS.g:3452:1: ( ( rule__ConstantAttributeType__NameAssignment_1 ) )
            {
            // InternalXaaS.g:3452:1: ( ( rule__ConstantAttributeType__NameAssignment_1 ) )
            // InternalXaaS.g:3453:2: ( rule__ConstantAttributeType__NameAssignment_1 )
            {
             before(grammarAccess.getConstantAttributeTypeAccess().getNameAssignment_1()); 
            // InternalXaaS.g:3454:2: ( rule__ConstantAttributeType__NameAssignment_1 )
            // InternalXaaS.g:3454:3: rule__ConstantAttributeType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ConstantAttributeType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getConstantAttributeTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__1__Impl"


    // $ANTLR start "rule__ConstantAttributeType__Group__2"
    // InternalXaaS.g:3462:1: rule__ConstantAttributeType__Group__2 : rule__ConstantAttributeType__Group__2__Impl rule__ConstantAttributeType__Group__3 ;
    public final void rule__ConstantAttributeType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3466:1: ( rule__ConstantAttributeType__Group__2__Impl rule__ConstantAttributeType__Group__3 )
            // InternalXaaS.g:3467:2: rule__ConstantAttributeType__Group__2__Impl rule__ConstantAttributeType__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__ConstantAttributeType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConstantAttributeType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__2"


    // $ANTLR start "rule__ConstantAttributeType__Group__2__Impl"
    // InternalXaaS.g:3474:1: rule__ConstantAttributeType__Group__2__Impl : ( ':' ) ;
    public final void rule__ConstantAttributeType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3478:1: ( ( ':' ) )
            // InternalXaaS.g:3479:1: ( ':' )
            {
            // InternalXaaS.g:3479:1: ( ':' )
            // InternalXaaS.g:3480:2: ':'
            {
             before(grammarAccess.getConstantAttributeTypeAccess().getColonKeyword_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getConstantAttributeTypeAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__2__Impl"


    // $ANTLR start "rule__ConstantAttributeType__Group__3"
    // InternalXaaS.g:3489:1: rule__ConstantAttributeType__Group__3 : rule__ConstantAttributeType__Group__3__Impl rule__ConstantAttributeType__Group__4 ;
    public final void rule__ConstantAttributeType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3493:1: ( rule__ConstantAttributeType__Group__3__Impl rule__ConstantAttributeType__Group__4 )
            // InternalXaaS.g:3494:2: rule__ConstantAttributeType__Group__3__Impl rule__ConstantAttributeType__Group__4
            {
            pushFollow(FOLLOW_4);
            rule__ConstantAttributeType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConstantAttributeType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__3"


    // $ANTLR start "rule__ConstantAttributeType__Group__3__Impl"
    // InternalXaaS.g:3501:1: rule__ConstantAttributeType__Group__3__Impl : ( 'type' ) ;
    public final void rule__ConstantAttributeType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3505:1: ( ( 'type' ) )
            // InternalXaaS.g:3506:1: ( 'type' )
            {
            // InternalXaaS.g:3506:1: ( 'type' )
            // InternalXaaS.g:3507:2: 'type'
            {
             before(grammarAccess.getConstantAttributeTypeAccess().getTypeKeyword_3()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getConstantAttributeTypeAccess().getTypeKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__3__Impl"


    // $ANTLR start "rule__ConstantAttributeType__Group__4"
    // InternalXaaS.g:3516:1: rule__ConstantAttributeType__Group__4 : rule__ConstantAttributeType__Group__4__Impl rule__ConstantAttributeType__Group__5 ;
    public final void rule__ConstantAttributeType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3520:1: ( rule__ConstantAttributeType__Group__4__Impl rule__ConstantAttributeType__Group__5 )
            // InternalXaaS.g:3521:2: rule__ConstantAttributeType__Group__4__Impl rule__ConstantAttributeType__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__ConstantAttributeType__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConstantAttributeType__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__4"


    // $ANTLR start "rule__ConstantAttributeType__Group__4__Impl"
    // InternalXaaS.g:3528:1: rule__ConstantAttributeType__Group__4__Impl : ( ':' ) ;
    public final void rule__ConstantAttributeType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3532:1: ( ( ':' ) )
            // InternalXaaS.g:3533:1: ( ':' )
            {
            // InternalXaaS.g:3533:1: ( ':' )
            // InternalXaaS.g:3534:2: ':'
            {
             before(grammarAccess.getConstantAttributeTypeAccess().getColonKeyword_4()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getConstantAttributeTypeAccess().getColonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__4__Impl"


    // $ANTLR start "rule__ConstantAttributeType__Group__5"
    // InternalXaaS.g:3543:1: rule__ConstantAttributeType__Group__5 : rule__ConstantAttributeType__Group__5__Impl ;
    public final void rule__ConstantAttributeType__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3547:1: ( rule__ConstantAttributeType__Group__5__Impl )
            // InternalXaaS.g:3548:2: rule__ConstantAttributeType__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ConstantAttributeType__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__5"


    // $ANTLR start "rule__ConstantAttributeType__Group__5__Impl"
    // InternalXaaS.g:3554:1: rule__ConstantAttributeType__Group__5__Impl : ( ( rule__ConstantAttributeType__TypeAssignment_5 ) ) ;
    public final void rule__ConstantAttributeType__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3558:1: ( ( ( rule__ConstantAttributeType__TypeAssignment_5 ) ) )
            // InternalXaaS.g:3559:1: ( ( rule__ConstantAttributeType__TypeAssignment_5 ) )
            {
            // InternalXaaS.g:3559:1: ( ( rule__ConstantAttributeType__TypeAssignment_5 ) )
            // InternalXaaS.g:3560:2: ( rule__ConstantAttributeType__TypeAssignment_5 )
            {
             before(grammarAccess.getConstantAttributeTypeAccess().getTypeAssignment_5()); 
            // InternalXaaS.g:3561:2: ( rule__ConstantAttributeType__TypeAssignment_5 )
            // InternalXaaS.g:3561:3: rule__ConstantAttributeType__TypeAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__ConstantAttributeType__TypeAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getConstantAttributeTypeAccess().getTypeAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__Group__5__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__0"
    // InternalXaaS.g:3570:1: rule__CalculatedAttributeType__Group__0 : rule__CalculatedAttributeType__Group__0__Impl rule__CalculatedAttributeType__Group__1 ;
    public final void rule__CalculatedAttributeType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3574:1: ( rule__CalculatedAttributeType__Group__0__Impl rule__CalculatedAttributeType__Group__1 )
            // InternalXaaS.g:3575:2: rule__CalculatedAttributeType__Group__0__Impl rule__CalculatedAttributeType__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__CalculatedAttributeType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__0"


    // $ANTLR start "rule__CalculatedAttributeType__Group__0__Impl"
    // InternalXaaS.g:3582:1: rule__CalculatedAttributeType__Group__0__Impl : ( () ) ;
    public final void rule__CalculatedAttributeType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3586:1: ( ( () ) )
            // InternalXaaS.g:3587:1: ( () )
            {
            // InternalXaaS.g:3587:1: ( () )
            // InternalXaaS.g:3588:2: ()
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getCalculatedAttributeTypeAction_0()); 
            // InternalXaaS.g:3589:2: ()
            // InternalXaaS.g:3589:3: 
            {
            }

             after(grammarAccess.getCalculatedAttributeTypeAccess().getCalculatedAttributeTypeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__0__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__1"
    // InternalXaaS.g:3597:1: rule__CalculatedAttributeType__Group__1 : rule__CalculatedAttributeType__Group__1__Impl rule__CalculatedAttributeType__Group__2 ;
    public final void rule__CalculatedAttributeType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3601:1: ( rule__CalculatedAttributeType__Group__1__Impl rule__CalculatedAttributeType__Group__2 )
            // InternalXaaS.g:3602:2: rule__CalculatedAttributeType__Group__1__Impl rule__CalculatedAttributeType__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__CalculatedAttributeType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__1"


    // $ANTLR start "rule__CalculatedAttributeType__Group__1__Impl"
    // InternalXaaS.g:3609:1: rule__CalculatedAttributeType__Group__1__Impl : ( 'variable' ) ;
    public final void rule__CalculatedAttributeType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3613:1: ( ( 'variable' ) )
            // InternalXaaS.g:3614:1: ( 'variable' )
            {
            // InternalXaaS.g:3614:1: ( 'variable' )
            // InternalXaaS.g:3615:2: 'variable'
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getVariableKeyword_1()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getCalculatedAttributeTypeAccess().getVariableKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__1__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__2"
    // InternalXaaS.g:3624:1: rule__CalculatedAttributeType__Group__2 : rule__CalculatedAttributeType__Group__2__Impl rule__CalculatedAttributeType__Group__3 ;
    public final void rule__CalculatedAttributeType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3628:1: ( rule__CalculatedAttributeType__Group__2__Impl rule__CalculatedAttributeType__Group__3 )
            // InternalXaaS.g:3629:2: rule__CalculatedAttributeType__Group__2__Impl rule__CalculatedAttributeType__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__CalculatedAttributeType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__2"


    // $ANTLR start "rule__CalculatedAttributeType__Group__2__Impl"
    // InternalXaaS.g:3636:1: rule__CalculatedAttributeType__Group__2__Impl : ( ( rule__CalculatedAttributeType__NameAssignment_2 ) ) ;
    public final void rule__CalculatedAttributeType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3640:1: ( ( ( rule__CalculatedAttributeType__NameAssignment_2 ) ) )
            // InternalXaaS.g:3641:1: ( ( rule__CalculatedAttributeType__NameAssignment_2 ) )
            {
            // InternalXaaS.g:3641:1: ( ( rule__CalculatedAttributeType__NameAssignment_2 ) )
            // InternalXaaS.g:3642:2: ( rule__CalculatedAttributeType__NameAssignment_2 )
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getNameAssignment_2()); 
            // InternalXaaS.g:3643:2: ( rule__CalculatedAttributeType__NameAssignment_2 )
            // InternalXaaS.g:3643:3: rule__CalculatedAttributeType__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCalculatedAttributeTypeAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__2__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__3"
    // InternalXaaS.g:3651:1: rule__CalculatedAttributeType__Group__3 : rule__CalculatedAttributeType__Group__3__Impl rule__CalculatedAttributeType__Group__4 ;
    public final void rule__CalculatedAttributeType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3655:1: ( rule__CalculatedAttributeType__Group__3__Impl rule__CalculatedAttributeType__Group__4 )
            // InternalXaaS.g:3656:2: rule__CalculatedAttributeType__Group__3__Impl rule__CalculatedAttributeType__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__CalculatedAttributeType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__3"


    // $ANTLR start "rule__CalculatedAttributeType__Group__3__Impl"
    // InternalXaaS.g:3663:1: rule__CalculatedAttributeType__Group__3__Impl : ( ':' ) ;
    public final void rule__CalculatedAttributeType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3667:1: ( ( ':' ) )
            // InternalXaaS.g:3668:1: ( ':' )
            {
            // InternalXaaS.g:3668:1: ( ':' )
            // InternalXaaS.g:3669:2: ':'
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_3()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__3__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__4"
    // InternalXaaS.g:3678:1: rule__CalculatedAttributeType__Group__4 : rule__CalculatedAttributeType__Group__4__Impl rule__CalculatedAttributeType__Group__5 ;
    public final void rule__CalculatedAttributeType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3682:1: ( rule__CalculatedAttributeType__Group__4__Impl rule__CalculatedAttributeType__Group__5 )
            // InternalXaaS.g:3683:2: rule__CalculatedAttributeType__Group__4__Impl rule__CalculatedAttributeType__Group__5
            {
            pushFollow(FOLLOW_4);
            rule__CalculatedAttributeType__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__4"


    // $ANTLR start "rule__CalculatedAttributeType__Group__4__Impl"
    // InternalXaaS.g:3690:1: rule__CalculatedAttributeType__Group__4__Impl : ( 'type' ) ;
    public final void rule__CalculatedAttributeType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3694:1: ( ( 'type' ) )
            // InternalXaaS.g:3695:1: ( 'type' )
            {
            // InternalXaaS.g:3695:1: ( 'type' )
            // InternalXaaS.g:3696:2: 'type'
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getTypeKeyword_4()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getCalculatedAttributeTypeAccess().getTypeKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__4__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__5"
    // InternalXaaS.g:3705:1: rule__CalculatedAttributeType__Group__5 : rule__CalculatedAttributeType__Group__5__Impl rule__CalculatedAttributeType__Group__6 ;
    public final void rule__CalculatedAttributeType__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3709:1: ( rule__CalculatedAttributeType__Group__5__Impl rule__CalculatedAttributeType__Group__6 )
            // InternalXaaS.g:3710:2: rule__CalculatedAttributeType__Group__5__Impl rule__CalculatedAttributeType__Group__6
            {
            pushFollow(FOLLOW_6);
            rule__CalculatedAttributeType__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__5"


    // $ANTLR start "rule__CalculatedAttributeType__Group__5__Impl"
    // InternalXaaS.g:3717:1: rule__CalculatedAttributeType__Group__5__Impl : ( ':' ) ;
    public final void rule__CalculatedAttributeType__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3721:1: ( ( ':' ) )
            // InternalXaaS.g:3722:1: ( ':' )
            {
            // InternalXaaS.g:3722:1: ( ':' )
            // InternalXaaS.g:3723:2: ':'
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_5()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__5__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__6"
    // InternalXaaS.g:3732:1: rule__CalculatedAttributeType__Group__6 : rule__CalculatedAttributeType__Group__6__Impl rule__CalculatedAttributeType__Group__7 ;
    public final void rule__CalculatedAttributeType__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3736:1: ( rule__CalculatedAttributeType__Group__6__Impl rule__CalculatedAttributeType__Group__7 )
            // InternalXaaS.g:3737:2: rule__CalculatedAttributeType__Group__6__Impl rule__CalculatedAttributeType__Group__7
            {
            pushFollow(FOLLOW_31);
            rule__CalculatedAttributeType__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__6"


    // $ANTLR start "rule__CalculatedAttributeType__Group__6__Impl"
    // InternalXaaS.g:3744:1: rule__CalculatedAttributeType__Group__6__Impl : ( ( rule__CalculatedAttributeType__TypeAssignment_6 ) ) ;
    public final void rule__CalculatedAttributeType__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3748:1: ( ( ( rule__CalculatedAttributeType__TypeAssignment_6 ) ) )
            // InternalXaaS.g:3749:1: ( ( rule__CalculatedAttributeType__TypeAssignment_6 ) )
            {
            // InternalXaaS.g:3749:1: ( ( rule__CalculatedAttributeType__TypeAssignment_6 ) )
            // InternalXaaS.g:3750:2: ( rule__CalculatedAttributeType__TypeAssignment_6 )
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getTypeAssignment_6()); 
            // InternalXaaS.g:3751:2: ( rule__CalculatedAttributeType__TypeAssignment_6 )
            // InternalXaaS.g:3751:3: rule__CalculatedAttributeType__TypeAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__TypeAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getCalculatedAttributeTypeAccess().getTypeAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__6__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__7"
    // InternalXaaS.g:3759:1: rule__CalculatedAttributeType__Group__7 : rule__CalculatedAttributeType__Group__7__Impl rule__CalculatedAttributeType__Group__8 ;
    public final void rule__CalculatedAttributeType__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3763:1: ( rule__CalculatedAttributeType__Group__7__Impl rule__CalculatedAttributeType__Group__8 )
            // InternalXaaS.g:3764:2: rule__CalculatedAttributeType__Group__7__Impl rule__CalculatedAttributeType__Group__8
            {
            pushFollow(FOLLOW_4);
            rule__CalculatedAttributeType__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__7"


    // $ANTLR start "rule__CalculatedAttributeType__Group__7__Impl"
    // InternalXaaS.g:3771:1: rule__CalculatedAttributeType__Group__7__Impl : ( 'equal' ) ;
    public final void rule__CalculatedAttributeType__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3775:1: ( ( 'equal' ) )
            // InternalXaaS.g:3776:1: ( 'equal' )
            {
            // InternalXaaS.g:3776:1: ( 'equal' )
            // InternalXaaS.g:3777:2: 'equal'
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getEqualKeyword_7()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getCalculatedAttributeTypeAccess().getEqualKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__7__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__8"
    // InternalXaaS.g:3786:1: rule__CalculatedAttributeType__Group__8 : rule__CalculatedAttributeType__Group__8__Impl rule__CalculatedAttributeType__Group__9 ;
    public final void rule__CalculatedAttributeType__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3790:1: ( rule__CalculatedAttributeType__Group__8__Impl rule__CalculatedAttributeType__Group__9 )
            // InternalXaaS.g:3791:2: rule__CalculatedAttributeType__Group__8__Impl rule__CalculatedAttributeType__Group__9
            {
            pushFollow(FOLLOW_30);
            rule__CalculatedAttributeType__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__8"


    // $ANTLR start "rule__CalculatedAttributeType__Group__8__Impl"
    // InternalXaaS.g:3798:1: rule__CalculatedAttributeType__Group__8__Impl : ( ':' ) ;
    public final void rule__CalculatedAttributeType__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3802:1: ( ( ':' ) )
            // InternalXaaS.g:3803:1: ( ':' )
            {
            // InternalXaaS.g:3803:1: ( ':' )
            // InternalXaaS.g:3804:2: ':'
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_8()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__8__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__9"
    // InternalXaaS.g:3813:1: rule__CalculatedAttributeType__Group__9 : rule__CalculatedAttributeType__Group__9__Impl rule__CalculatedAttributeType__Group__10 ;
    public final void rule__CalculatedAttributeType__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3817:1: ( rule__CalculatedAttributeType__Group__9__Impl rule__CalculatedAttributeType__Group__10 )
            // InternalXaaS.g:3818:2: rule__CalculatedAttributeType__Group__9__Impl rule__CalculatedAttributeType__Group__10
            {
            pushFollow(FOLLOW_32);
            rule__CalculatedAttributeType__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__9"


    // $ANTLR start "rule__CalculatedAttributeType__Group__9__Impl"
    // InternalXaaS.g:3825:1: rule__CalculatedAttributeType__Group__9__Impl : ( ( rule__CalculatedAttributeType__ExpressionAssignment_9 ) ) ;
    public final void rule__CalculatedAttributeType__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3829:1: ( ( ( rule__CalculatedAttributeType__ExpressionAssignment_9 ) ) )
            // InternalXaaS.g:3830:1: ( ( rule__CalculatedAttributeType__ExpressionAssignment_9 ) )
            {
            // InternalXaaS.g:3830:1: ( ( rule__CalculatedAttributeType__ExpressionAssignment_9 ) )
            // InternalXaaS.g:3831:2: ( rule__CalculatedAttributeType__ExpressionAssignment_9 )
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getExpressionAssignment_9()); 
            // InternalXaaS.g:3832:2: ( rule__CalculatedAttributeType__ExpressionAssignment_9 )
            // InternalXaaS.g:3832:3: rule__CalculatedAttributeType__ExpressionAssignment_9
            {
            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__ExpressionAssignment_9();

            state._fsp--;


            }

             after(grammarAccess.getCalculatedAttributeTypeAccess().getExpressionAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__9__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group__10"
    // InternalXaaS.g:3840:1: rule__CalculatedAttributeType__Group__10 : rule__CalculatedAttributeType__Group__10__Impl ;
    public final void rule__CalculatedAttributeType__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3844:1: ( rule__CalculatedAttributeType__Group__10__Impl )
            // InternalXaaS.g:3845:2: rule__CalculatedAttributeType__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__10"


    // $ANTLR start "rule__CalculatedAttributeType__Group__10__Impl"
    // InternalXaaS.g:3851:1: rule__CalculatedAttributeType__Group__10__Impl : ( ( rule__CalculatedAttributeType__Group_10__0 )? ) ;
    public final void rule__CalculatedAttributeType__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3855:1: ( ( ( rule__CalculatedAttributeType__Group_10__0 )? ) )
            // InternalXaaS.g:3856:1: ( ( rule__CalculatedAttributeType__Group_10__0 )? )
            {
            // InternalXaaS.g:3856:1: ( ( rule__CalculatedAttributeType__Group_10__0 )? )
            // InternalXaaS.g:3857:2: ( rule__CalculatedAttributeType__Group_10__0 )?
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getGroup_10()); 
            // InternalXaaS.g:3858:2: ( rule__CalculatedAttributeType__Group_10__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==48) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalXaaS.g:3858:3: rule__CalculatedAttributeType__Group_10__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CalculatedAttributeType__Group_10__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCalculatedAttributeTypeAccess().getGroup_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group__10__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group_10__0"
    // InternalXaaS.g:3867:1: rule__CalculatedAttributeType__Group_10__0 : rule__CalculatedAttributeType__Group_10__0__Impl rule__CalculatedAttributeType__Group_10__1 ;
    public final void rule__CalculatedAttributeType__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3871:1: ( rule__CalculatedAttributeType__Group_10__0__Impl rule__CalculatedAttributeType__Group_10__1 )
            // InternalXaaS.g:3872:2: rule__CalculatedAttributeType__Group_10__0__Impl rule__CalculatedAttributeType__Group_10__1
            {
            pushFollow(FOLLOW_4);
            rule__CalculatedAttributeType__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group_10__0"


    // $ANTLR start "rule__CalculatedAttributeType__Group_10__0__Impl"
    // InternalXaaS.g:3879:1: rule__CalculatedAttributeType__Group_10__0__Impl : ( 'impactOfUpdating' ) ;
    public final void rule__CalculatedAttributeType__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3883:1: ( ( 'impactOfUpdating' ) )
            // InternalXaaS.g:3884:1: ( 'impactOfUpdating' )
            {
            // InternalXaaS.g:3884:1: ( 'impactOfUpdating' )
            // InternalXaaS.g:3885:2: 'impactOfUpdating'
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getImpactOfUpdatingKeyword_10_0()); 
            match(input,48,FOLLOW_2); 
             after(grammarAccess.getCalculatedAttributeTypeAccess().getImpactOfUpdatingKeyword_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group_10__0__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group_10__1"
    // InternalXaaS.g:3894:1: rule__CalculatedAttributeType__Group_10__1 : rule__CalculatedAttributeType__Group_10__1__Impl rule__CalculatedAttributeType__Group_10__2 ;
    public final void rule__CalculatedAttributeType__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3898:1: ( rule__CalculatedAttributeType__Group_10__1__Impl rule__CalculatedAttributeType__Group_10__2 )
            // InternalXaaS.g:3899:2: rule__CalculatedAttributeType__Group_10__1__Impl rule__CalculatedAttributeType__Group_10__2
            {
            pushFollow(FOLLOW_27);
            rule__CalculatedAttributeType__Group_10__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group_10__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group_10__1"


    // $ANTLR start "rule__CalculatedAttributeType__Group_10__1__Impl"
    // InternalXaaS.g:3906:1: rule__CalculatedAttributeType__Group_10__1__Impl : ( ':' ) ;
    public final void rule__CalculatedAttributeType__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3910:1: ( ( ':' ) )
            // InternalXaaS.g:3911:1: ( ':' )
            {
            // InternalXaaS.g:3911:1: ( ':' )
            // InternalXaaS.g:3912:2: ':'
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_10_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getCalculatedAttributeTypeAccess().getColonKeyword_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group_10__1__Impl"


    // $ANTLR start "rule__CalculatedAttributeType__Group_10__2"
    // InternalXaaS.g:3921:1: rule__CalculatedAttributeType__Group_10__2 : rule__CalculatedAttributeType__Group_10__2__Impl ;
    public final void rule__CalculatedAttributeType__Group_10__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3925:1: ( rule__CalculatedAttributeType__Group_10__2__Impl )
            // InternalXaaS.g:3926:2: rule__CalculatedAttributeType__Group_10__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__Group_10__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group_10__2"


    // $ANTLR start "rule__CalculatedAttributeType__Group_10__2__Impl"
    // InternalXaaS.g:3932:1: rule__CalculatedAttributeType__Group_10__2__Impl : ( ( rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2 ) ) ;
    public final void rule__CalculatedAttributeType__Group_10__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3936:1: ( ( ( rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2 ) ) )
            // InternalXaaS.g:3937:1: ( ( rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2 ) )
            {
            // InternalXaaS.g:3937:1: ( ( rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2 ) )
            // InternalXaaS.g:3938:2: ( rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2 )
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getImpactOfUpdatingAssignment_10_2()); 
            // InternalXaaS.g:3939:2: ( rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2 )
            // InternalXaaS.g:3939:3: rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2
            {
            pushFollow(FOLLOW_2);
            rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2();

            state._fsp--;


            }

             after(grammarAccess.getCalculatedAttributeTypeAccess().getImpactOfUpdatingAssignment_10_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__Group_10__2__Impl"


    // $ANTLR start "rule__BinaryExpression__Group__0"
    // InternalXaaS.g:3948:1: rule__BinaryExpression__Group__0 : rule__BinaryExpression__Group__0__Impl rule__BinaryExpression__Group__1 ;
    public final void rule__BinaryExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3952:1: ( rule__BinaryExpression__Group__0__Impl rule__BinaryExpression__Group__1 )
            // InternalXaaS.g:3953:2: rule__BinaryExpression__Group__0__Impl rule__BinaryExpression__Group__1
            {
            pushFollow(FOLLOW_33);
            rule__BinaryExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BinaryExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__Group__0"


    // $ANTLR start "rule__BinaryExpression__Group__0__Impl"
    // InternalXaaS.g:3960:1: rule__BinaryExpression__Group__0__Impl : ( ( rule__BinaryExpression__ExpressionsAssignment_0 ) ) ;
    public final void rule__BinaryExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3964:1: ( ( ( rule__BinaryExpression__ExpressionsAssignment_0 ) ) )
            // InternalXaaS.g:3965:1: ( ( rule__BinaryExpression__ExpressionsAssignment_0 ) )
            {
            // InternalXaaS.g:3965:1: ( ( rule__BinaryExpression__ExpressionsAssignment_0 ) )
            // InternalXaaS.g:3966:2: ( rule__BinaryExpression__ExpressionsAssignment_0 )
            {
             before(grammarAccess.getBinaryExpressionAccess().getExpressionsAssignment_0()); 
            // InternalXaaS.g:3967:2: ( rule__BinaryExpression__ExpressionsAssignment_0 )
            // InternalXaaS.g:3967:3: rule__BinaryExpression__ExpressionsAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__BinaryExpression__ExpressionsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getBinaryExpressionAccess().getExpressionsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__Group__0__Impl"


    // $ANTLR start "rule__BinaryExpression__Group__1"
    // InternalXaaS.g:3975:1: rule__BinaryExpression__Group__1 : rule__BinaryExpression__Group__1__Impl rule__BinaryExpression__Group__2 ;
    public final void rule__BinaryExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3979:1: ( rule__BinaryExpression__Group__1__Impl rule__BinaryExpression__Group__2 )
            // InternalXaaS.g:3980:2: rule__BinaryExpression__Group__1__Impl rule__BinaryExpression__Group__2
            {
            pushFollow(FOLLOW_30);
            rule__BinaryExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BinaryExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__Group__1"


    // $ANTLR start "rule__BinaryExpression__Group__1__Impl"
    // InternalXaaS.g:3987:1: rule__BinaryExpression__Group__1__Impl : ( ( rule__BinaryExpression__OperatorAssignment_1 ) ) ;
    public final void rule__BinaryExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:3991:1: ( ( ( rule__BinaryExpression__OperatorAssignment_1 ) ) )
            // InternalXaaS.g:3992:1: ( ( rule__BinaryExpression__OperatorAssignment_1 ) )
            {
            // InternalXaaS.g:3992:1: ( ( rule__BinaryExpression__OperatorAssignment_1 ) )
            // InternalXaaS.g:3993:2: ( rule__BinaryExpression__OperatorAssignment_1 )
            {
             before(grammarAccess.getBinaryExpressionAccess().getOperatorAssignment_1()); 
            // InternalXaaS.g:3994:2: ( rule__BinaryExpression__OperatorAssignment_1 )
            // InternalXaaS.g:3994:3: rule__BinaryExpression__OperatorAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__BinaryExpression__OperatorAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBinaryExpressionAccess().getOperatorAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__Group__1__Impl"


    // $ANTLR start "rule__BinaryExpression__Group__2"
    // InternalXaaS.g:4002:1: rule__BinaryExpression__Group__2 : rule__BinaryExpression__Group__2__Impl ;
    public final void rule__BinaryExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4006:1: ( rule__BinaryExpression__Group__2__Impl )
            // InternalXaaS.g:4007:2: rule__BinaryExpression__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BinaryExpression__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__Group__2"


    // $ANTLR start "rule__BinaryExpression__Group__2__Impl"
    // InternalXaaS.g:4013:1: rule__BinaryExpression__Group__2__Impl : ( ( rule__BinaryExpression__ExpressionsAssignment_2 ) ) ;
    public final void rule__BinaryExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4017:1: ( ( ( rule__BinaryExpression__ExpressionsAssignment_2 ) ) )
            // InternalXaaS.g:4018:1: ( ( rule__BinaryExpression__ExpressionsAssignment_2 ) )
            {
            // InternalXaaS.g:4018:1: ( ( rule__BinaryExpression__ExpressionsAssignment_2 ) )
            // InternalXaaS.g:4019:2: ( rule__BinaryExpression__ExpressionsAssignment_2 )
            {
             before(grammarAccess.getBinaryExpressionAccess().getExpressionsAssignment_2()); 
            // InternalXaaS.g:4020:2: ( rule__BinaryExpression__ExpressionsAssignment_2 )
            // InternalXaaS.g:4020:3: rule__BinaryExpression__ExpressionsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__BinaryExpression__ExpressionsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getBinaryExpressionAccess().getExpressionsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__Group__2__Impl"


    // $ANTLR start "rule__AggregationExpression__Group__0"
    // InternalXaaS.g:4029:1: rule__AggregationExpression__Group__0 : rule__AggregationExpression__Group__0__Impl rule__AggregationExpression__Group__1 ;
    public final void rule__AggregationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4033:1: ( rule__AggregationExpression__Group__0__Impl rule__AggregationExpression__Group__1 )
            // InternalXaaS.g:4034:2: rule__AggregationExpression__Group__0__Impl rule__AggregationExpression__Group__1
            {
            pushFollow(FOLLOW_34);
            rule__AggregationExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregationExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__0"


    // $ANTLR start "rule__AggregationExpression__Group__0__Impl"
    // InternalXaaS.g:4041:1: rule__AggregationExpression__Group__0__Impl : ( ( rule__AggregationExpression__OperatorAssignment_0 ) ) ;
    public final void rule__AggregationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4045:1: ( ( ( rule__AggregationExpression__OperatorAssignment_0 ) ) )
            // InternalXaaS.g:4046:1: ( ( rule__AggregationExpression__OperatorAssignment_0 ) )
            {
            // InternalXaaS.g:4046:1: ( ( rule__AggregationExpression__OperatorAssignment_0 ) )
            // InternalXaaS.g:4047:2: ( rule__AggregationExpression__OperatorAssignment_0 )
            {
             before(grammarAccess.getAggregationExpressionAccess().getOperatorAssignment_0()); 
            // InternalXaaS.g:4048:2: ( rule__AggregationExpression__OperatorAssignment_0 )
            // InternalXaaS.g:4048:3: rule__AggregationExpression__OperatorAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__AggregationExpression__OperatorAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAggregationExpressionAccess().getOperatorAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__0__Impl"


    // $ANTLR start "rule__AggregationExpression__Group__1"
    // InternalXaaS.g:4056:1: rule__AggregationExpression__Group__1 : rule__AggregationExpression__Group__1__Impl rule__AggregationExpression__Group__2 ;
    public final void rule__AggregationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4060:1: ( rule__AggregationExpression__Group__1__Impl rule__AggregationExpression__Group__2 )
            // InternalXaaS.g:4061:2: rule__AggregationExpression__Group__1__Impl rule__AggregationExpression__Group__2
            {
            pushFollow(FOLLOW_35);
            rule__AggregationExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregationExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__1"


    // $ANTLR start "rule__AggregationExpression__Group__1__Impl"
    // InternalXaaS.g:4068:1: rule__AggregationExpression__Group__1__Impl : ( '(' ) ;
    public final void rule__AggregationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4072:1: ( ( '(' ) )
            // InternalXaaS.g:4073:1: ( '(' )
            {
            // InternalXaaS.g:4073:1: ( '(' )
            // InternalXaaS.g:4074:2: '('
            {
             before(grammarAccess.getAggregationExpressionAccess().getLeftParenthesisKeyword_1()); 
            match(input,49,FOLLOW_2); 
             after(grammarAccess.getAggregationExpressionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__1__Impl"


    // $ANTLR start "rule__AggregationExpression__Group__2"
    // InternalXaaS.g:4083:1: rule__AggregationExpression__Group__2 : rule__AggregationExpression__Group__2__Impl rule__AggregationExpression__Group__3 ;
    public final void rule__AggregationExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4087:1: ( rule__AggregationExpression__Group__2__Impl rule__AggregationExpression__Group__3 )
            // InternalXaaS.g:4088:2: rule__AggregationExpression__Group__2__Impl rule__AggregationExpression__Group__3
            {
            pushFollow(FOLLOW_36);
            rule__AggregationExpression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregationExpression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__2"


    // $ANTLR start "rule__AggregationExpression__Group__2__Impl"
    // InternalXaaS.g:4095:1: rule__AggregationExpression__Group__2__Impl : ( ( rule__AggregationExpression__DirectionAssignment_2 ) ) ;
    public final void rule__AggregationExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4099:1: ( ( ( rule__AggregationExpression__DirectionAssignment_2 ) ) )
            // InternalXaaS.g:4100:1: ( ( rule__AggregationExpression__DirectionAssignment_2 ) )
            {
            // InternalXaaS.g:4100:1: ( ( rule__AggregationExpression__DirectionAssignment_2 ) )
            // InternalXaaS.g:4101:2: ( rule__AggregationExpression__DirectionAssignment_2 )
            {
             before(grammarAccess.getAggregationExpressionAccess().getDirectionAssignment_2()); 
            // InternalXaaS.g:4102:2: ( rule__AggregationExpression__DirectionAssignment_2 )
            // InternalXaaS.g:4102:3: rule__AggregationExpression__DirectionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__AggregationExpression__DirectionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAggregationExpressionAccess().getDirectionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__2__Impl"


    // $ANTLR start "rule__AggregationExpression__Group__3"
    // InternalXaaS.g:4110:1: rule__AggregationExpression__Group__3 : rule__AggregationExpression__Group__3__Impl rule__AggregationExpression__Group__4 ;
    public final void rule__AggregationExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4114:1: ( rule__AggregationExpression__Group__3__Impl rule__AggregationExpression__Group__4 )
            // InternalXaaS.g:4115:2: rule__AggregationExpression__Group__3__Impl rule__AggregationExpression__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__AggregationExpression__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregationExpression__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__3"


    // $ANTLR start "rule__AggregationExpression__Group__3__Impl"
    // InternalXaaS.g:4122:1: rule__AggregationExpression__Group__3__Impl : ( ',' ) ;
    public final void rule__AggregationExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4126:1: ( ( ',' ) )
            // InternalXaaS.g:4127:1: ( ',' )
            {
            // InternalXaaS.g:4127:1: ( ',' )
            // InternalXaaS.g:4128:2: ','
            {
             before(grammarAccess.getAggregationExpressionAccess().getCommaKeyword_3()); 
            match(input,50,FOLLOW_2); 
             after(grammarAccess.getAggregationExpressionAccess().getCommaKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__3__Impl"


    // $ANTLR start "rule__AggregationExpression__Group__4"
    // InternalXaaS.g:4137:1: rule__AggregationExpression__Group__4 : rule__AggregationExpression__Group__4__Impl rule__AggregationExpression__Group__5 ;
    public final void rule__AggregationExpression__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4141:1: ( rule__AggregationExpression__Group__4__Impl rule__AggregationExpression__Group__5 )
            // InternalXaaS.g:4142:2: rule__AggregationExpression__Group__4__Impl rule__AggregationExpression__Group__5
            {
            pushFollow(FOLLOW_37);
            rule__AggregationExpression__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AggregationExpression__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__4"


    // $ANTLR start "rule__AggregationExpression__Group__4__Impl"
    // InternalXaaS.g:4149:1: rule__AggregationExpression__Group__4__Impl : ( ( rule__AggregationExpression__AttributeTypeAssignment_4 ) ) ;
    public final void rule__AggregationExpression__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4153:1: ( ( ( rule__AggregationExpression__AttributeTypeAssignment_4 ) ) )
            // InternalXaaS.g:4154:1: ( ( rule__AggregationExpression__AttributeTypeAssignment_4 ) )
            {
            // InternalXaaS.g:4154:1: ( ( rule__AggregationExpression__AttributeTypeAssignment_4 ) )
            // InternalXaaS.g:4155:2: ( rule__AggregationExpression__AttributeTypeAssignment_4 )
            {
             before(grammarAccess.getAggregationExpressionAccess().getAttributeTypeAssignment_4()); 
            // InternalXaaS.g:4156:2: ( rule__AggregationExpression__AttributeTypeAssignment_4 )
            // InternalXaaS.g:4156:3: rule__AggregationExpression__AttributeTypeAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__AggregationExpression__AttributeTypeAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getAggregationExpressionAccess().getAttributeTypeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__4__Impl"


    // $ANTLR start "rule__AggregationExpression__Group__5"
    // InternalXaaS.g:4164:1: rule__AggregationExpression__Group__5 : rule__AggregationExpression__Group__5__Impl ;
    public final void rule__AggregationExpression__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4168:1: ( rule__AggregationExpression__Group__5__Impl )
            // InternalXaaS.g:4169:2: rule__AggregationExpression__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AggregationExpression__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__5"


    // $ANTLR start "rule__AggregationExpression__Group__5__Impl"
    // InternalXaaS.g:4175:1: rule__AggregationExpression__Group__5__Impl : ( ')' ) ;
    public final void rule__AggregationExpression__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4179:1: ( ( ')' ) )
            // InternalXaaS.g:4180:1: ( ')' )
            {
            // InternalXaaS.g:4180:1: ( ')' )
            // InternalXaaS.g:4181:2: ')'
            {
             before(grammarAccess.getAggregationExpressionAccess().getRightParenthesisKeyword_5()); 
            match(input,51,FOLLOW_2); 
             after(grammarAccess.getAggregationExpressionAccess().getRightParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__Group__5__Impl"


    // $ANTLR start "rule__NbConnectionExpression__Group__0"
    // InternalXaaS.g:4191:1: rule__NbConnectionExpression__Group__0 : rule__NbConnectionExpression__Group__0__Impl rule__NbConnectionExpression__Group__1 ;
    public final void rule__NbConnectionExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4195:1: ( rule__NbConnectionExpression__Group__0__Impl rule__NbConnectionExpression__Group__1 )
            // InternalXaaS.g:4196:2: rule__NbConnectionExpression__Group__0__Impl rule__NbConnectionExpression__Group__1
            {
            pushFollow(FOLLOW_34);
            rule__NbConnectionExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NbConnectionExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NbConnectionExpression__Group__0"


    // $ANTLR start "rule__NbConnectionExpression__Group__0__Impl"
    // InternalXaaS.g:4203:1: rule__NbConnectionExpression__Group__0__Impl : ( 'NbLink' ) ;
    public final void rule__NbConnectionExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4207:1: ( ( 'NbLink' ) )
            // InternalXaaS.g:4208:1: ( 'NbLink' )
            {
            // InternalXaaS.g:4208:1: ( 'NbLink' )
            // InternalXaaS.g:4209:2: 'NbLink'
            {
             before(grammarAccess.getNbConnectionExpressionAccess().getNbLinkKeyword_0()); 
            match(input,52,FOLLOW_2); 
             after(grammarAccess.getNbConnectionExpressionAccess().getNbLinkKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NbConnectionExpression__Group__0__Impl"


    // $ANTLR start "rule__NbConnectionExpression__Group__1"
    // InternalXaaS.g:4218:1: rule__NbConnectionExpression__Group__1 : rule__NbConnectionExpression__Group__1__Impl rule__NbConnectionExpression__Group__2 ;
    public final void rule__NbConnectionExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4222:1: ( rule__NbConnectionExpression__Group__1__Impl rule__NbConnectionExpression__Group__2 )
            // InternalXaaS.g:4223:2: rule__NbConnectionExpression__Group__1__Impl rule__NbConnectionExpression__Group__2
            {
            pushFollow(FOLLOW_35);
            rule__NbConnectionExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NbConnectionExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NbConnectionExpression__Group__1"


    // $ANTLR start "rule__NbConnectionExpression__Group__1__Impl"
    // InternalXaaS.g:4230:1: rule__NbConnectionExpression__Group__1__Impl : ( '(' ) ;
    public final void rule__NbConnectionExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4234:1: ( ( '(' ) )
            // InternalXaaS.g:4235:1: ( '(' )
            {
            // InternalXaaS.g:4235:1: ( '(' )
            // InternalXaaS.g:4236:2: '('
            {
             before(grammarAccess.getNbConnectionExpressionAccess().getLeftParenthesisKeyword_1()); 
            match(input,49,FOLLOW_2); 
             after(grammarAccess.getNbConnectionExpressionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NbConnectionExpression__Group__1__Impl"


    // $ANTLR start "rule__NbConnectionExpression__Group__2"
    // InternalXaaS.g:4245:1: rule__NbConnectionExpression__Group__2 : rule__NbConnectionExpression__Group__2__Impl rule__NbConnectionExpression__Group__3 ;
    public final void rule__NbConnectionExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4249:1: ( rule__NbConnectionExpression__Group__2__Impl rule__NbConnectionExpression__Group__3 )
            // InternalXaaS.g:4250:2: rule__NbConnectionExpression__Group__2__Impl rule__NbConnectionExpression__Group__3
            {
            pushFollow(FOLLOW_37);
            rule__NbConnectionExpression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NbConnectionExpression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NbConnectionExpression__Group__2"


    // $ANTLR start "rule__NbConnectionExpression__Group__2__Impl"
    // InternalXaaS.g:4257:1: rule__NbConnectionExpression__Group__2__Impl : ( ( rule__NbConnectionExpression__DirectionAssignment_2 ) ) ;
    public final void rule__NbConnectionExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4261:1: ( ( ( rule__NbConnectionExpression__DirectionAssignment_2 ) ) )
            // InternalXaaS.g:4262:1: ( ( rule__NbConnectionExpression__DirectionAssignment_2 ) )
            {
            // InternalXaaS.g:4262:1: ( ( rule__NbConnectionExpression__DirectionAssignment_2 ) )
            // InternalXaaS.g:4263:2: ( rule__NbConnectionExpression__DirectionAssignment_2 )
            {
             before(grammarAccess.getNbConnectionExpressionAccess().getDirectionAssignment_2()); 
            // InternalXaaS.g:4264:2: ( rule__NbConnectionExpression__DirectionAssignment_2 )
            // InternalXaaS.g:4264:3: rule__NbConnectionExpression__DirectionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__NbConnectionExpression__DirectionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getNbConnectionExpressionAccess().getDirectionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NbConnectionExpression__Group__2__Impl"


    // $ANTLR start "rule__NbConnectionExpression__Group__3"
    // InternalXaaS.g:4272:1: rule__NbConnectionExpression__Group__3 : rule__NbConnectionExpression__Group__3__Impl ;
    public final void rule__NbConnectionExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4276:1: ( rule__NbConnectionExpression__Group__3__Impl )
            // InternalXaaS.g:4277:2: rule__NbConnectionExpression__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NbConnectionExpression__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NbConnectionExpression__Group__3"


    // $ANTLR start "rule__NbConnectionExpression__Group__3__Impl"
    // InternalXaaS.g:4283:1: rule__NbConnectionExpression__Group__3__Impl : ( ')' ) ;
    public final void rule__NbConnectionExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4287:1: ( ( ')' ) )
            // InternalXaaS.g:4288:1: ( ')' )
            {
            // InternalXaaS.g:4288:1: ( ')' )
            // InternalXaaS.g:4289:2: ')'
            {
             before(grammarAccess.getNbConnectionExpressionAccess().getRightParenthesisKeyword_3()); 
            match(input,51,FOLLOW_2); 
             after(grammarAccess.getNbConnectionExpressionAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NbConnectionExpression__Group__3__Impl"


    // $ANTLR start "rule__CustomExpression__Group__0"
    // InternalXaaS.g:4299:1: rule__CustomExpression__Group__0 : rule__CustomExpression__Group__0__Impl rule__CustomExpression__Group__1 ;
    public final void rule__CustomExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4303:1: ( rule__CustomExpression__Group__0__Impl rule__CustomExpression__Group__1 )
            // InternalXaaS.g:4304:2: rule__CustomExpression__Group__0__Impl rule__CustomExpression__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__CustomExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CustomExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CustomExpression__Group__0"


    // $ANTLR start "rule__CustomExpression__Group__0__Impl"
    // InternalXaaS.g:4311:1: rule__CustomExpression__Group__0__Impl : ( 'CustomExpression' ) ;
    public final void rule__CustomExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4315:1: ( ( 'CustomExpression' ) )
            // InternalXaaS.g:4316:1: ( 'CustomExpression' )
            {
            // InternalXaaS.g:4316:1: ( 'CustomExpression' )
            // InternalXaaS.g:4317:2: 'CustomExpression'
            {
             before(grammarAccess.getCustomExpressionAccess().getCustomExpressionKeyword_0()); 
            match(input,53,FOLLOW_2); 
             after(grammarAccess.getCustomExpressionAccess().getCustomExpressionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CustomExpression__Group__0__Impl"


    // $ANTLR start "rule__CustomExpression__Group__1"
    // InternalXaaS.g:4326:1: rule__CustomExpression__Group__1 : rule__CustomExpression__Group__1__Impl rule__CustomExpression__Group__2 ;
    public final void rule__CustomExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4330:1: ( rule__CustomExpression__Group__1__Impl rule__CustomExpression__Group__2 )
            // InternalXaaS.g:4331:2: rule__CustomExpression__Group__1__Impl rule__CustomExpression__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__CustomExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CustomExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CustomExpression__Group__1"


    // $ANTLR start "rule__CustomExpression__Group__1__Impl"
    // InternalXaaS.g:4338:1: rule__CustomExpression__Group__1__Impl : ( ':' ) ;
    public final void rule__CustomExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4342:1: ( ( ':' ) )
            // InternalXaaS.g:4343:1: ( ':' )
            {
            // InternalXaaS.g:4343:1: ( ':' )
            // InternalXaaS.g:4344:2: ':'
            {
             before(grammarAccess.getCustomExpressionAccess().getColonKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getCustomExpressionAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CustomExpression__Group__1__Impl"


    // $ANTLR start "rule__CustomExpression__Group__2"
    // InternalXaaS.g:4353:1: rule__CustomExpression__Group__2 : rule__CustomExpression__Group__2__Impl ;
    public final void rule__CustomExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4357:1: ( rule__CustomExpression__Group__2__Impl )
            // InternalXaaS.g:4358:2: rule__CustomExpression__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CustomExpression__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CustomExpression__Group__2"


    // $ANTLR start "rule__CustomExpression__Group__2__Impl"
    // InternalXaaS.g:4364:1: rule__CustomExpression__Group__2__Impl : ( ( rule__CustomExpression__ExpressionAssignment_2 ) ) ;
    public final void rule__CustomExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4368:1: ( ( ( rule__CustomExpression__ExpressionAssignment_2 ) ) )
            // InternalXaaS.g:4369:1: ( ( rule__CustomExpression__ExpressionAssignment_2 ) )
            {
            // InternalXaaS.g:4369:1: ( ( rule__CustomExpression__ExpressionAssignment_2 ) )
            // InternalXaaS.g:4370:2: ( rule__CustomExpression__ExpressionAssignment_2 )
            {
             before(grammarAccess.getCustomExpressionAccess().getExpressionAssignment_2()); 
            // InternalXaaS.g:4371:2: ( rule__CustomExpression__ExpressionAssignment_2 )
            // InternalXaaS.g:4371:3: rule__CustomExpression__ExpressionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__CustomExpression__ExpressionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCustomExpressionAccess().getExpressionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CustomExpression__Group__2__Impl"


    // $ANTLR start "rule__EInt__Group__0"
    // InternalXaaS.g:4380:1: rule__EInt__Group__0 : rule__EInt__Group__0__Impl rule__EInt__Group__1 ;
    public final void rule__EInt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4384:1: ( rule__EInt__Group__0__Impl rule__EInt__Group__1 )
            // InternalXaaS.g:4385:2: rule__EInt__Group__0__Impl rule__EInt__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__EInt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EInt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0"


    // $ANTLR start "rule__EInt__Group__0__Impl"
    // InternalXaaS.g:4392:1: rule__EInt__Group__0__Impl : ( ( '-' )? ) ;
    public final void rule__EInt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4396:1: ( ( ( '-' )? ) )
            // InternalXaaS.g:4397:1: ( ( '-' )? )
            {
            // InternalXaaS.g:4397:1: ( ( '-' )? )
            // InternalXaaS.g:4398:2: ( '-' )?
            {
             before(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 
            // InternalXaaS.g:4399:2: ( '-' )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==15) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalXaaS.g:4399:3: '-'
                    {
                    match(input,15,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0__Impl"


    // $ANTLR start "rule__EInt__Group__1"
    // InternalXaaS.g:4407:1: rule__EInt__Group__1 : rule__EInt__Group__1__Impl ;
    public final void rule__EInt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4411:1: ( rule__EInt__Group__1__Impl )
            // InternalXaaS.g:4412:2: rule__EInt__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EInt__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1"


    // $ANTLR start "rule__EInt__Group__1__Impl"
    // InternalXaaS.g:4418:1: rule__EInt__Group__1__Impl : ( RULE_INT ) ;
    public final void rule__EInt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4422:1: ( ( RULE_INT ) )
            // InternalXaaS.g:4423:1: ( RULE_INT )
            {
            // InternalXaaS.g:4423:1: ( RULE_INT )
            // InternalXaaS.g:4424:2: RULE_INT
            {
             before(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1__Impl"


    // $ANTLR start "rule__RelationshipType__Group__0"
    // InternalXaaS.g:4434:1: rule__RelationshipType__Group__0 : rule__RelationshipType__Group__0__Impl rule__RelationshipType__Group__1 ;
    public final void rule__RelationshipType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4438:1: ( rule__RelationshipType__Group__0__Impl rule__RelationshipType__Group__1 )
            // InternalXaaS.g:4439:2: rule__RelationshipType__Group__0__Impl rule__RelationshipType__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__RelationshipType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__0"


    // $ANTLR start "rule__RelationshipType__Group__0__Impl"
    // InternalXaaS.g:4446:1: rule__RelationshipType__Group__0__Impl : ( ( rule__RelationshipType__NameAssignment_0 ) ) ;
    public final void rule__RelationshipType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4450:1: ( ( ( rule__RelationshipType__NameAssignment_0 ) ) )
            // InternalXaaS.g:4451:1: ( ( rule__RelationshipType__NameAssignment_0 ) )
            {
            // InternalXaaS.g:4451:1: ( ( rule__RelationshipType__NameAssignment_0 ) )
            // InternalXaaS.g:4452:2: ( rule__RelationshipType__NameAssignment_0 )
            {
             before(grammarAccess.getRelationshipTypeAccess().getNameAssignment_0()); 
            // InternalXaaS.g:4453:2: ( rule__RelationshipType__NameAssignment_0 )
            // InternalXaaS.g:4453:3: rule__RelationshipType__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipTypeAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__0__Impl"


    // $ANTLR start "rule__RelationshipType__Group__1"
    // InternalXaaS.g:4461:1: rule__RelationshipType__Group__1 : rule__RelationshipType__Group__1__Impl rule__RelationshipType__Group__2 ;
    public final void rule__RelationshipType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4465:1: ( rule__RelationshipType__Group__1__Impl rule__RelationshipType__Group__2 )
            // InternalXaaS.g:4466:2: rule__RelationshipType__Group__1__Impl rule__RelationshipType__Group__2
            {
            pushFollow(FOLLOW_38);
            rule__RelationshipType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__1"


    // $ANTLR start "rule__RelationshipType__Group__1__Impl"
    // InternalXaaS.g:4473:1: rule__RelationshipType__Group__1__Impl : ( ':' ) ;
    public final void rule__RelationshipType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4477:1: ( ( ':' ) )
            // InternalXaaS.g:4478:1: ( ':' )
            {
            // InternalXaaS.g:4478:1: ( ':' )
            // InternalXaaS.g:4479:2: ':'
            {
             before(grammarAccess.getRelationshipTypeAccess().getColonKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__1__Impl"


    // $ANTLR start "rule__RelationshipType__Group__2"
    // InternalXaaS.g:4488:1: rule__RelationshipType__Group__2 : rule__RelationshipType__Group__2__Impl rule__RelationshipType__Group__3 ;
    public final void rule__RelationshipType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4492:1: ( rule__RelationshipType__Group__2__Impl rule__RelationshipType__Group__3 )
            // InternalXaaS.g:4493:2: rule__RelationshipType__Group__2__Impl rule__RelationshipType__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__RelationshipType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__2"


    // $ANTLR start "rule__RelationshipType__Group__2__Impl"
    // InternalXaaS.g:4500:1: rule__RelationshipType__Group__2__Impl : ( 'valid_source_types' ) ;
    public final void rule__RelationshipType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4504:1: ( ( 'valid_source_types' ) )
            // InternalXaaS.g:4505:1: ( 'valid_source_types' )
            {
            // InternalXaaS.g:4505:1: ( 'valid_source_types' )
            // InternalXaaS.g:4506:2: 'valid_source_types'
            {
             before(grammarAccess.getRelationshipTypeAccess().getValid_source_typesKeyword_2()); 
            match(input,54,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getValid_source_typesKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__2__Impl"


    // $ANTLR start "rule__RelationshipType__Group__3"
    // InternalXaaS.g:4515:1: rule__RelationshipType__Group__3 : rule__RelationshipType__Group__3__Impl rule__RelationshipType__Group__4 ;
    public final void rule__RelationshipType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4519:1: ( rule__RelationshipType__Group__3__Impl rule__RelationshipType__Group__4 )
            // InternalXaaS.g:4520:2: rule__RelationshipType__Group__3__Impl rule__RelationshipType__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__RelationshipType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__3"


    // $ANTLR start "rule__RelationshipType__Group__3__Impl"
    // InternalXaaS.g:4527:1: rule__RelationshipType__Group__3__Impl : ( ':' ) ;
    public final void rule__RelationshipType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4531:1: ( ( ':' ) )
            // InternalXaaS.g:4532:1: ( ':' )
            {
            // InternalXaaS.g:4532:1: ( ':' )
            // InternalXaaS.g:4533:2: ':'
            {
             before(grammarAccess.getRelationshipTypeAccess().getColonKeyword_3()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__3__Impl"


    // $ANTLR start "rule__RelationshipType__Group__4"
    // InternalXaaS.g:4542:1: rule__RelationshipType__Group__4 : rule__RelationshipType__Group__4__Impl rule__RelationshipType__Group__5 ;
    public final void rule__RelationshipType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4546:1: ( rule__RelationshipType__Group__4__Impl rule__RelationshipType__Group__5 )
            // InternalXaaS.g:4547:2: rule__RelationshipType__Group__4__Impl rule__RelationshipType__Group__5
            {
            pushFollow(FOLLOW_39);
            rule__RelationshipType__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__4"


    // $ANTLR start "rule__RelationshipType__Group__4__Impl"
    // InternalXaaS.g:4554:1: rule__RelationshipType__Group__4__Impl : ( ( rule__RelationshipType__SourceAssignment_4 ) ) ;
    public final void rule__RelationshipType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4558:1: ( ( ( rule__RelationshipType__SourceAssignment_4 ) ) )
            // InternalXaaS.g:4559:1: ( ( rule__RelationshipType__SourceAssignment_4 ) )
            {
            // InternalXaaS.g:4559:1: ( ( rule__RelationshipType__SourceAssignment_4 ) )
            // InternalXaaS.g:4560:2: ( rule__RelationshipType__SourceAssignment_4 )
            {
             before(grammarAccess.getRelationshipTypeAccess().getSourceAssignment_4()); 
            // InternalXaaS.g:4561:2: ( rule__RelationshipType__SourceAssignment_4 )
            // InternalXaaS.g:4561:3: rule__RelationshipType__SourceAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__SourceAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipTypeAccess().getSourceAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__4__Impl"


    // $ANTLR start "rule__RelationshipType__Group__5"
    // InternalXaaS.g:4569:1: rule__RelationshipType__Group__5 : rule__RelationshipType__Group__5__Impl rule__RelationshipType__Group__6 ;
    public final void rule__RelationshipType__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4573:1: ( rule__RelationshipType__Group__5__Impl rule__RelationshipType__Group__6 )
            // InternalXaaS.g:4574:2: rule__RelationshipType__Group__5__Impl rule__RelationshipType__Group__6
            {
            pushFollow(FOLLOW_4);
            rule__RelationshipType__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__5"


    // $ANTLR start "rule__RelationshipType__Group__5__Impl"
    // InternalXaaS.g:4581:1: rule__RelationshipType__Group__5__Impl : ( 'valid_target_types' ) ;
    public final void rule__RelationshipType__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4585:1: ( ( 'valid_target_types' ) )
            // InternalXaaS.g:4586:1: ( 'valid_target_types' )
            {
            // InternalXaaS.g:4586:1: ( 'valid_target_types' )
            // InternalXaaS.g:4587:2: 'valid_target_types'
            {
             before(grammarAccess.getRelationshipTypeAccess().getValid_target_typesKeyword_5()); 
            match(input,55,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getValid_target_typesKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__5__Impl"


    // $ANTLR start "rule__RelationshipType__Group__6"
    // InternalXaaS.g:4596:1: rule__RelationshipType__Group__6 : rule__RelationshipType__Group__6__Impl rule__RelationshipType__Group__7 ;
    public final void rule__RelationshipType__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4600:1: ( rule__RelationshipType__Group__6__Impl rule__RelationshipType__Group__7 )
            // InternalXaaS.g:4601:2: rule__RelationshipType__Group__6__Impl rule__RelationshipType__Group__7
            {
            pushFollow(FOLLOW_6);
            rule__RelationshipType__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__6"


    // $ANTLR start "rule__RelationshipType__Group__6__Impl"
    // InternalXaaS.g:4608:1: rule__RelationshipType__Group__6__Impl : ( ':' ) ;
    public final void rule__RelationshipType__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4612:1: ( ( ':' ) )
            // InternalXaaS.g:4613:1: ( ':' )
            {
            // InternalXaaS.g:4613:1: ( ':' )
            // InternalXaaS.g:4614:2: ':'
            {
             before(grammarAccess.getRelationshipTypeAccess().getColonKeyword_6()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getColonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__6__Impl"


    // $ANTLR start "rule__RelationshipType__Group__7"
    // InternalXaaS.g:4623:1: rule__RelationshipType__Group__7 : rule__RelationshipType__Group__7__Impl rule__RelationshipType__Group__8 ;
    public final void rule__RelationshipType__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4627:1: ( rule__RelationshipType__Group__7__Impl rule__RelationshipType__Group__8 )
            // InternalXaaS.g:4628:2: rule__RelationshipType__Group__7__Impl rule__RelationshipType__Group__8
            {
            pushFollow(FOLLOW_40);
            rule__RelationshipType__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__7"


    // $ANTLR start "rule__RelationshipType__Group__7__Impl"
    // InternalXaaS.g:4635:1: rule__RelationshipType__Group__7__Impl : ( ( rule__RelationshipType__TargetAssignment_7 ) ) ;
    public final void rule__RelationshipType__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4639:1: ( ( ( rule__RelationshipType__TargetAssignment_7 ) ) )
            // InternalXaaS.g:4640:1: ( ( rule__RelationshipType__TargetAssignment_7 ) )
            {
            // InternalXaaS.g:4640:1: ( ( rule__RelationshipType__TargetAssignment_7 ) )
            // InternalXaaS.g:4641:2: ( rule__RelationshipType__TargetAssignment_7 )
            {
             before(grammarAccess.getRelationshipTypeAccess().getTargetAssignment_7()); 
            // InternalXaaS.g:4642:2: ( rule__RelationshipType__TargetAssignment_7 )
            // InternalXaaS.g:4642:3: rule__RelationshipType__TargetAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__TargetAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipTypeAccess().getTargetAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__7__Impl"


    // $ANTLR start "rule__RelationshipType__Group__8"
    // InternalXaaS.g:4650:1: rule__RelationshipType__Group__8 : rule__RelationshipType__Group__8__Impl rule__RelationshipType__Group__9 ;
    public final void rule__RelationshipType__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4654:1: ( rule__RelationshipType__Group__8__Impl rule__RelationshipType__Group__9 )
            // InternalXaaS.g:4655:2: rule__RelationshipType__Group__8__Impl rule__RelationshipType__Group__9
            {
            pushFollow(FOLLOW_40);
            rule__RelationshipType__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__8"


    // $ANTLR start "rule__RelationshipType__Group__8__Impl"
    // InternalXaaS.g:4662:1: rule__RelationshipType__Group__8__Impl : ( ( rule__RelationshipType__Group_8__0 )? ) ;
    public final void rule__RelationshipType__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4666:1: ( ( ( rule__RelationshipType__Group_8__0 )? ) )
            // InternalXaaS.g:4667:1: ( ( rule__RelationshipType__Group_8__0 )? )
            {
            // InternalXaaS.g:4667:1: ( ( rule__RelationshipType__Group_8__0 )? )
            // InternalXaaS.g:4668:2: ( rule__RelationshipType__Group_8__0 )?
            {
             before(grammarAccess.getRelationshipTypeAccess().getGroup_8()); 
            // InternalXaaS.g:4669:2: ( rule__RelationshipType__Group_8__0 )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==46) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalXaaS.g:4669:3: rule__RelationshipType__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__RelationshipType__Group_8__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRelationshipTypeAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__8__Impl"


    // $ANTLR start "rule__RelationshipType__Group__9"
    // InternalXaaS.g:4677:1: rule__RelationshipType__Group__9 : rule__RelationshipType__Group__9__Impl rule__RelationshipType__Group__10 ;
    public final void rule__RelationshipType__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4681:1: ( rule__RelationshipType__Group__9__Impl rule__RelationshipType__Group__10 )
            // InternalXaaS.g:4682:2: rule__RelationshipType__Group__9__Impl rule__RelationshipType__Group__10
            {
            pushFollow(FOLLOW_40);
            rule__RelationshipType__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__9"


    // $ANTLR start "rule__RelationshipType__Group__9__Impl"
    // InternalXaaS.g:4689:1: rule__RelationshipType__Group__9__Impl : ( ( rule__RelationshipType__Group_9__0 )? ) ;
    public final void rule__RelationshipType__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4693:1: ( ( ( rule__RelationshipType__Group_9__0 )? ) )
            // InternalXaaS.g:4694:1: ( ( rule__RelationshipType__Group_9__0 )? )
            {
            // InternalXaaS.g:4694:1: ( ( rule__RelationshipType__Group_9__0 )? )
            // InternalXaaS.g:4695:2: ( rule__RelationshipType__Group_9__0 )?
            {
             before(grammarAccess.getRelationshipTypeAccess().getGroup_9()); 
            // InternalXaaS.g:4696:2: ( rule__RelationshipType__Group_9__0 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==56) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalXaaS.g:4696:3: rule__RelationshipType__Group_9__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__RelationshipType__Group_9__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRelationshipTypeAccess().getGroup_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__9__Impl"


    // $ANTLR start "rule__RelationshipType__Group__10"
    // InternalXaaS.g:4704:1: rule__RelationshipType__Group__10 : rule__RelationshipType__Group__10__Impl ;
    public final void rule__RelationshipType__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4708:1: ( rule__RelationshipType__Group__10__Impl )
            // InternalXaaS.g:4709:2: rule__RelationshipType__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__10"


    // $ANTLR start "rule__RelationshipType__Group__10__Impl"
    // InternalXaaS.g:4715:1: rule__RelationshipType__Group__10__Impl : ( ( rule__RelationshipType__Group_10__0 )? ) ;
    public final void rule__RelationshipType__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4719:1: ( ( ( rule__RelationshipType__Group_10__0 )? ) )
            // InternalXaaS.g:4720:1: ( ( rule__RelationshipType__Group_10__0 )? )
            {
            // InternalXaaS.g:4720:1: ( ( rule__RelationshipType__Group_10__0 )? )
            // InternalXaaS.g:4721:2: ( rule__RelationshipType__Group_10__0 )?
            {
             before(grammarAccess.getRelationshipTypeAccess().getGroup_10()); 
            // InternalXaaS.g:4722:2: ( rule__RelationshipType__Group_10__0 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==57) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalXaaS.g:4722:3: rule__RelationshipType__Group_10__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__RelationshipType__Group_10__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRelationshipTypeAccess().getGroup_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group__10__Impl"


    // $ANTLR start "rule__RelationshipType__Group_8__0"
    // InternalXaaS.g:4731:1: rule__RelationshipType__Group_8__0 : rule__RelationshipType__Group_8__0__Impl rule__RelationshipType__Group_8__1 ;
    public final void rule__RelationshipType__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4735:1: ( rule__RelationshipType__Group_8__0__Impl rule__RelationshipType__Group_8__1 )
            // InternalXaaS.g:4736:2: rule__RelationshipType__Group_8__0__Impl rule__RelationshipType__Group_8__1
            {
            pushFollow(FOLLOW_4);
            rule__RelationshipType__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_8__0"


    // $ANTLR start "rule__RelationshipType__Group_8__0__Impl"
    // InternalXaaS.g:4743:1: rule__RelationshipType__Group_8__0__Impl : ( 'constraints' ) ;
    public final void rule__RelationshipType__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4747:1: ( ( 'constraints' ) )
            // InternalXaaS.g:4748:1: ( 'constraints' )
            {
            // InternalXaaS.g:4748:1: ( 'constraints' )
            // InternalXaaS.g:4749:2: 'constraints'
            {
             before(grammarAccess.getRelationshipTypeAccess().getConstraintsKeyword_8_0()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getConstraintsKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_8__0__Impl"


    // $ANTLR start "rule__RelationshipType__Group_8__1"
    // InternalXaaS.g:4758:1: rule__RelationshipType__Group_8__1 : rule__RelationshipType__Group_8__1__Impl rule__RelationshipType__Group_8__2 ;
    public final void rule__RelationshipType__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4762:1: ( rule__RelationshipType__Group_8__1__Impl rule__RelationshipType__Group_8__2 )
            // InternalXaaS.g:4763:2: rule__RelationshipType__Group_8__1__Impl rule__RelationshipType__Group_8__2
            {
            pushFollow(FOLLOW_6);
            rule__RelationshipType__Group_8__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group_8__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_8__1"


    // $ANTLR start "rule__RelationshipType__Group_8__1__Impl"
    // InternalXaaS.g:4770:1: rule__RelationshipType__Group_8__1__Impl : ( ':' ) ;
    public final void rule__RelationshipType__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4774:1: ( ( ':' ) )
            // InternalXaaS.g:4775:1: ( ':' )
            {
            // InternalXaaS.g:4775:1: ( ':' )
            // InternalXaaS.g:4776:2: ':'
            {
             before(grammarAccess.getRelationshipTypeAccess().getColonKeyword_8_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getColonKeyword_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_8__1__Impl"


    // $ANTLR start "rule__RelationshipType__Group_8__2"
    // InternalXaaS.g:4785:1: rule__RelationshipType__Group_8__2 : rule__RelationshipType__Group_8__2__Impl ;
    public final void rule__RelationshipType__Group_8__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4789:1: ( rule__RelationshipType__Group_8__2__Impl )
            // InternalXaaS.g:4790:2: rule__RelationshipType__Group_8__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group_8__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_8__2"


    // $ANTLR start "rule__RelationshipType__Group_8__2__Impl"
    // InternalXaaS.g:4796:1: rule__RelationshipType__Group_8__2__Impl : ( ( rule__RelationshipType__ConstraintsAssignment_8_2 )* ) ;
    public final void rule__RelationshipType__Group_8__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4800:1: ( ( ( rule__RelationshipType__ConstraintsAssignment_8_2 )* ) )
            // InternalXaaS.g:4801:1: ( ( rule__RelationshipType__ConstraintsAssignment_8_2 )* )
            {
            // InternalXaaS.g:4801:1: ( ( rule__RelationshipType__ConstraintsAssignment_8_2 )* )
            // InternalXaaS.g:4802:2: ( rule__RelationshipType__ConstraintsAssignment_8_2 )*
            {
             before(grammarAccess.getRelationshipTypeAccess().getConstraintsAssignment_8_2()); 
            // InternalXaaS.g:4803:2: ( rule__RelationshipType__ConstraintsAssignment_8_2 )*
            loop34:
            do {
                int alt34=2;
                alt34 = dfa34.predict(input);
                switch (alt34) {
            	case 1 :
            	    // InternalXaaS.g:4803:3: rule__RelationshipType__ConstraintsAssignment_8_2
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__RelationshipType__ConstraintsAssignment_8_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

             after(grammarAccess.getRelationshipTypeAccess().getConstraintsAssignment_8_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_8__2__Impl"


    // $ANTLR start "rule__RelationshipType__Group_9__0"
    // InternalXaaS.g:4812:1: rule__RelationshipType__Group_9__0 : rule__RelationshipType__Group_9__0__Impl rule__RelationshipType__Group_9__1 ;
    public final void rule__RelationshipType__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4816:1: ( rule__RelationshipType__Group_9__0__Impl rule__RelationshipType__Group_9__1 )
            // InternalXaaS.g:4817:2: rule__RelationshipType__Group_9__0__Impl rule__RelationshipType__Group_9__1
            {
            pushFollow(FOLLOW_4);
            rule__RelationshipType__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_9__0"


    // $ANTLR start "rule__RelationshipType__Group_9__0__Impl"
    // InternalXaaS.g:4824:1: rule__RelationshipType__Group_9__0__Impl : ( 'impactOfLinking' ) ;
    public final void rule__RelationshipType__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4828:1: ( ( 'impactOfLinking' ) )
            // InternalXaaS.g:4829:1: ( 'impactOfLinking' )
            {
            // InternalXaaS.g:4829:1: ( 'impactOfLinking' )
            // InternalXaaS.g:4830:2: 'impactOfLinking'
            {
             before(grammarAccess.getRelationshipTypeAccess().getImpactOfLinkingKeyword_9_0()); 
            match(input,56,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getImpactOfLinkingKeyword_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_9__0__Impl"


    // $ANTLR start "rule__RelationshipType__Group_9__1"
    // InternalXaaS.g:4839:1: rule__RelationshipType__Group_9__1 : rule__RelationshipType__Group_9__1__Impl rule__RelationshipType__Group_9__2 ;
    public final void rule__RelationshipType__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4843:1: ( rule__RelationshipType__Group_9__1__Impl rule__RelationshipType__Group_9__2 )
            // InternalXaaS.g:4844:2: rule__RelationshipType__Group_9__1__Impl rule__RelationshipType__Group_9__2
            {
            pushFollow(FOLLOW_27);
            rule__RelationshipType__Group_9__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group_9__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_9__1"


    // $ANTLR start "rule__RelationshipType__Group_9__1__Impl"
    // InternalXaaS.g:4851:1: rule__RelationshipType__Group_9__1__Impl : ( ':' ) ;
    public final void rule__RelationshipType__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4855:1: ( ( ':' ) )
            // InternalXaaS.g:4856:1: ( ':' )
            {
            // InternalXaaS.g:4856:1: ( ':' )
            // InternalXaaS.g:4857:2: ':'
            {
             before(grammarAccess.getRelationshipTypeAccess().getColonKeyword_9_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getColonKeyword_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_9__1__Impl"


    // $ANTLR start "rule__RelationshipType__Group_9__2"
    // InternalXaaS.g:4866:1: rule__RelationshipType__Group_9__2 : rule__RelationshipType__Group_9__2__Impl ;
    public final void rule__RelationshipType__Group_9__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4870:1: ( rule__RelationshipType__Group_9__2__Impl )
            // InternalXaaS.g:4871:2: rule__RelationshipType__Group_9__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group_9__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_9__2"


    // $ANTLR start "rule__RelationshipType__Group_9__2__Impl"
    // InternalXaaS.g:4877:1: rule__RelationshipType__Group_9__2__Impl : ( ( rule__RelationshipType__ImpactOfLinkingAssignment_9_2 ) ) ;
    public final void rule__RelationshipType__Group_9__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4881:1: ( ( ( rule__RelationshipType__ImpactOfLinkingAssignment_9_2 ) ) )
            // InternalXaaS.g:4882:1: ( ( rule__RelationshipType__ImpactOfLinkingAssignment_9_2 ) )
            {
            // InternalXaaS.g:4882:1: ( ( rule__RelationshipType__ImpactOfLinkingAssignment_9_2 ) )
            // InternalXaaS.g:4883:2: ( rule__RelationshipType__ImpactOfLinkingAssignment_9_2 )
            {
             before(grammarAccess.getRelationshipTypeAccess().getImpactOfLinkingAssignment_9_2()); 
            // InternalXaaS.g:4884:2: ( rule__RelationshipType__ImpactOfLinkingAssignment_9_2 )
            // InternalXaaS.g:4884:3: rule__RelationshipType__ImpactOfLinkingAssignment_9_2
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__ImpactOfLinkingAssignment_9_2();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipTypeAccess().getImpactOfLinkingAssignment_9_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_9__2__Impl"


    // $ANTLR start "rule__RelationshipType__Group_10__0"
    // InternalXaaS.g:4893:1: rule__RelationshipType__Group_10__0 : rule__RelationshipType__Group_10__0__Impl rule__RelationshipType__Group_10__1 ;
    public final void rule__RelationshipType__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4897:1: ( rule__RelationshipType__Group_10__0__Impl rule__RelationshipType__Group_10__1 )
            // InternalXaaS.g:4898:2: rule__RelationshipType__Group_10__0__Impl rule__RelationshipType__Group_10__1
            {
            pushFollow(FOLLOW_4);
            rule__RelationshipType__Group_10__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group_10__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_10__0"


    // $ANTLR start "rule__RelationshipType__Group_10__0__Impl"
    // InternalXaaS.g:4905:1: rule__RelationshipType__Group_10__0__Impl : ( 'impactOfUnlinking' ) ;
    public final void rule__RelationshipType__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4909:1: ( ( 'impactOfUnlinking' ) )
            // InternalXaaS.g:4910:1: ( 'impactOfUnlinking' )
            {
            // InternalXaaS.g:4910:1: ( 'impactOfUnlinking' )
            // InternalXaaS.g:4911:2: 'impactOfUnlinking'
            {
             before(grammarAccess.getRelationshipTypeAccess().getImpactOfUnlinkingKeyword_10_0()); 
            match(input,57,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getImpactOfUnlinkingKeyword_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_10__0__Impl"


    // $ANTLR start "rule__RelationshipType__Group_10__1"
    // InternalXaaS.g:4920:1: rule__RelationshipType__Group_10__1 : rule__RelationshipType__Group_10__1__Impl rule__RelationshipType__Group_10__2 ;
    public final void rule__RelationshipType__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4924:1: ( rule__RelationshipType__Group_10__1__Impl rule__RelationshipType__Group_10__2 )
            // InternalXaaS.g:4925:2: rule__RelationshipType__Group_10__1__Impl rule__RelationshipType__Group_10__2
            {
            pushFollow(FOLLOW_27);
            rule__RelationshipType__Group_10__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group_10__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_10__1"


    // $ANTLR start "rule__RelationshipType__Group_10__1__Impl"
    // InternalXaaS.g:4932:1: rule__RelationshipType__Group_10__1__Impl : ( ':' ) ;
    public final void rule__RelationshipType__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4936:1: ( ( ':' ) )
            // InternalXaaS.g:4937:1: ( ':' )
            {
            // InternalXaaS.g:4937:1: ( ':' )
            // InternalXaaS.g:4938:2: ':'
            {
             before(grammarAccess.getRelationshipTypeAccess().getColonKeyword_10_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRelationshipTypeAccess().getColonKeyword_10_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_10__1__Impl"


    // $ANTLR start "rule__RelationshipType__Group_10__2"
    // InternalXaaS.g:4947:1: rule__RelationshipType__Group_10__2 : rule__RelationshipType__Group_10__2__Impl ;
    public final void rule__RelationshipType__Group_10__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4951:1: ( rule__RelationshipType__Group_10__2__Impl )
            // InternalXaaS.g:4952:2: rule__RelationshipType__Group_10__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__Group_10__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_10__2"


    // $ANTLR start "rule__RelationshipType__Group_10__2__Impl"
    // InternalXaaS.g:4958:1: rule__RelationshipType__Group_10__2__Impl : ( ( rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2 ) ) ;
    public final void rule__RelationshipType__Group_10__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4962:1: ( ( ( rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2 ) ) )
            // InternalXaaS.g:4963:1: ( ( rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2 ) )
            {
            // InternalXaaS.g:4963:1: ( ( rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2 ) )
            // InternalXaaS.g:4964:2: ( rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2 )
            {
             before(grammarAccess.getRelationshipTypeAccess().getImpactOfUnlinkingAssignment_10_2()); 
            // InternalXaaS.g:4965:2: ( rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2 )
            // InternalXaaS.g:4965:3: rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2
            {
            pushFollow(FOLLOW_2);
            rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2();

            state._fsp--;


            }

             after(grammarAccess.getRelationshipTypeAccess().getImpactOfUnlinkingAssignment_10_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__Group_10__2__Impl"


    // $ANTLR start "rule__XaasYamlLikeTosca__TopologyAssignment_0"
    // InternalXaaS.g:4974:1: rule__XaasYamlLikeTosca__TopologyAssignment_0 : ( ruleTopology ) ;
    public final void rule__XaasYamlLikeTosca__TopologyAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4978:1: ( ( ruleTopology ) )
            // InternalXaaS.g:4979:2: ( ruleTopology )
            {
            // InternalXaaS.g:4979:2: ( ruleTopology )
            // InternalXaaS.g:4980:3: ruleTopology
            {
             before(grammarAccess.getXaasYamlLikeToscaAccess().getTopologyTopologyParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTopology();

            state._fsp--;

             after(grammarAccess.getXaasYamlLikeToscaAccess().getTopologyTopologyParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XaasYamlLikeTosca__TopologyAssignment_0"


    // $ANTLR start "rule__XaasYamlLikeTosca__ConfigurationAssignment_1"
    // InternalXaaS.g:4989:1: rule__XaasYamlLikeTosca__ConfigurationAssignment_1 : ( ruleConfiguration ) ;
    public final void rule__XaasYamlLikeTosca__ConfigurationAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:4993:1: ( ( ruleConfiguration ) )
            // InternalXaaS.g:4994:2: ( ruleConfiguration )
            {
            // InternalXaaS.g:4994:2: ( ruleConfiguration )
            // InternalXaaS.g:4995:3: ruleConfiguration
            {
             before(grammarAccess.getXaasYamlLikeToscaAccess().getConfigurationConfigurationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleConfiguration();

            state._fsp--;

             after(grammarAccess.getXaasYamlLikeToscaAccess().getConfigurationConfigurationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XaasYamlLikeTosca__ConfigurationAssignment_1"


    // $ANTLR start "rule__Configuration__IdentifierAssignment_4"
    // InternalXaaS.g:5004:1: rule__Configuration__IdentifierAssignment_4 : ( ruleEString ) ;
    public final void rule__Configuration__IdentifierAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5008:1: ( ( ruleEString ) )
            // InternalXaaS.g:5009:2: ( ruleEString )
            {
            // InternalXaaS.g:5009:2: ( ruleEString )
            // InternalXaaS.g:5010:3: ruleEString
            {
             before(grammarAccess.getConfigurationAccess().getIdentifierEStringParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getIdentifierEStringParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__IdentifierAssignment_4"


    // $ANTLR start "rule__Configuration__TopologyAssignment_7"
    // InternalXaaS.g:5019:1: rule__Configuration__TopologyAssignment_7 : ( ( ruleEString ) ) ;
    public final void rule__Configuration__TopologyAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5023:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5024:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5024:2: ( ( ruleEString ) )
            // InternalXaaS.g:5025:3: ( ruleEString )
            {
             before(grammarAccess.getConfigurationAccess().getTopologyTopologyCrossReference_7_0()); 
            // InternalXaaS.g:5026:3: ( ruleEString )
            // InternalXaaS.g:5027:4: ruleEString
            {
             before(grammarAccess.getConfigurationAccess().getTopologyTopologyEStringParserRuleCall_7_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getTopologyTopologyEStringParserRuleCall_7_0_1()); 

            }

             after(grammarAccess.getConfigurationAccess().getTopologyTopologyCrossReference_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__TopologyAssignment_7"


    // $ANTLR start "rule__Configuration__NodesAssignment_8"
    // InternalXaaS.g:5038:1: rule__Configuration__NodesAssignment_8 : ( ruleNode ) ;
    public final void rule__Configuration__NodesAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5042:1: ( ( ruleNode ) )
            // InternalXaaS.g:5043:2: ( ruleNode )
            {
            // InternalXaaS.g:5043:2: ( ruleNode )
            // InternalXaaS.g:5044:3: ruleNode
            {
             before(grammarAccess.getConfigurationAccess().getNodesNodeParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleNode();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getNodesNodeParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__NodesAssignment_8"


    // $ANTLR start "rule__Configuration__RelationshipsAssignment_9"
    // InternalXaaS.g:5053:1: rule__Configuration__RelationshipsAssignment_9 : ( ruleRelationship ) ;
    public final void rule__Configuration__RelationshipsAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5057:1: ( ( ruleRelationship ) )
            // InternalXaaS.g:5058:2: ( ruleRelationship )
            {
            // InternalXaaS.g:5058:2: ( ruleRelationship )
            // InternalXaaS.g:5059:3: ruleRelationship
            {
             before(grammarAccess.getConfigurationAccess().getRelationshipsRelationshipParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleRelationship();

            state._fsp--;

             after(grammarAccess.getConfigurationAccess().getRelationshipsRelationshipParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Configuration__RelationshipsAssignment_9"


    // $ANTLR start "rule__Topology__NameAssignment_3"
    // InternalXaaS.g:5068:1: rule__Topology__NameAssignment_3 : ( ruleEString ) ;
    public final void rule__Topology__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5072:1: ( ( ruleEString ) )
            // InternalXaaS.g:5073:2: ( ruleEString )
            {
            // InternalXaaS.g:5073:2: ( ruleEString )
            // InternalXaaS.g:5074:3: ruleEString
            {
             before(grammarAccess.getTopologyAccess().getNameEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getTopologyAccess().getNameEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__NameAssignment_3"


    // $ANTLR start "rule__Topology__NodeTypesAssignment_6"
    // InternalXaaS.g:5083:1: rule__Topology__NodeTypesAssignment_6 : ( ruleNodeType ) ;
    public final void rule__Topology__NodeTypesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5087:1: ( ( ruleNodeType ) )
            // InternalXaaS.g:5088:2: ( ruleNodeType )
            {
            // InternalXaaS.g:5088:2: ( ruleNodeType )
            // InternalXaaS.g:5089:3: ruleNodeType
            {
             before(grammarAccess.getTopologyAccess().getNodeTypesNodeTypeParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleNodeType();

            state._fsp--;

             after(grammarAccess.getTopologyAccess().getNodeTypesNodeTypeParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__NodeTypesAssignment_6"


    // $ANTLR start "rule__Topology__RelationshipTypesAssignment_7_2"
    // InternalXaaS.g:5098:1: rule__Topology__RelationshipTypesAssignment_7_2 : ( ruleRelationshipType ) ;
    public final void rule__Topology__RelationshipTypesAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5102:1: ( ( ruleRelationshipType ) )
            // InternalXaaS.g:5103:2: ( ruleRelationshipType )
            {
            // InternalXaaS.g:5103:2: ( ruleRelationshipType )
            // InternalXaaS.g:5104:3: ruleRelationshipType
            {
             before(grammarAccess.getTopologyAccess().getRelationshipTypesRelationshipTypeParserRuleCall_7_2_0()); 
            pushFollow(FOLLOW_2);
            ruleRelationshipType();

            state._fsp--;

             after(grammarAccess.getTopologyAccess().getRelationshipTypesRelationshipTypeParserRuleCall_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Topology__RelationshipTypesAssignment_7_2"


    // $ANTLR start "rule__Node__NameAssignment_1"
    // InternalXaaS.g:5113:1: rule__Node__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Node__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5117:1: ( ( ruleEString ) )
            // InternalXaaS.g:5118:2: ( ruleEString )
            {
            // InternalXaaS.g:5118:2: ( ruleEString )
            // InternalXaaS.g:5119:3: ruleEString
            {
             before(grammarAccess.getNodeAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getNodeAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__NameAssignment_1"


    // $ANTLR start "rule__Node__TypeAssignment_5"
    // InternalXaaS.g:5128:1: rule__Node__TypeAssignment_5 : ( ( ruleEString ) ) ;
    public final void rule__Node__TypeAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5132:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5133:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5133:2: ( ( ruleEString ) )
            // InternalXaaS.g:5134:3: ( ruleEString )
            {
             before(grammarAccess.getNodeAccess().getTypeNodeTypeCrossReference_5_0()); 
            // InternalXaaS.g:5135:3: ( ruleEString )
            // InternalXaaS.g:5136:4: ruleEString
            {
             before(grammarAccess.getNodeAccess().getTypeNodeTypeEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getNodeAccess().getTypeNodeTypeEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getNodeAccess().getTypeNodeTypeCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__TypeAssignment_5"


    // $ANTLR start "rule__Node__ActivatedAssignment_8"
    // InternalXaaS.g:5147:1: rule__Node__ActivatedAssignment_8 : ( ruleEString ) ;
    public final void rule__Node__ActivatedAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5151:1: ( ( ruleEString ) )
            // InternalXaaS.g:5152:2: ( ruleEString )
            {
            // InternalXaaS.g:5152:2: ( ruleEString )
            // InternalXaaS.g:5153:3: ruleEString
            {
             before(grammarAccess.getNodeAccess().getActivatedEStringParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getNodeAccess().getActivatedEStringParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__ActivatedAssignment_8"


    // $ANTLR start "rule__Node__AttributesAssignment_9_2"
    // InternalXaaS.g:5162:1: rule__Node__AttributesAssignment_9_2 : ( ruleAttribute ) ;
    public final void rule__Node__AttributesAssignment_9_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5166:1: ( ( ruleAttribute ) )
            // InternalXaaS.g:5167:2: ( ruleAttribute )
            {
            // InternalXaaS.g:5167:2: ( ruleAttribute )
            // InternalXaaS.g:5168:3: ruleAttribute
            {
             before(grammarAccess.getNodeAccess().getAttributesAttributeParserRuleCall_9_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getNodeAccess().getAttributesAttributeParserRuleCall_9_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__AttributesAssignment_9_2"


    // $ANTLR start "rule__Relationship__NameAssignment_1"
    // InternalXaaS.g:5177:1: rule__Relationship__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Relationship__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5181:1: ( ( ruleEString ) )
            // InternalXaaS.g:5182:2: ( ruleEString )
            {
            // InternalXaaS.g:5182:2: ( ruleEString )
            // InternalXaaS.g:5183:3: ruleEString
            {
             before(grammarAccess.getRelationshipAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRelationshipAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__NameAssignment_1"


    // $ANTLR start "rule__Relationship__TypeAssignment_5"
    // InternalXaaS.g:5192:1: rule__Relationship__TypeAssignment_5 : ( ( ruleEString ) ) ;
    public final void rule__Relationship__TypeAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5196:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5197:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5197:2: ( ( ruleEString ) )
            // InternalXaaS.g:5198:3: ( ruleEString )
            {
             before(grammarAccess.getRelationshipAccess().getTypeRelationshipTypeCrossReference_5_0()); 
            // InternalXaaS.g:5199:3: ( ruleEString )
            // InternalXaaS.g:5200:4: ruleEString
            {
             before(grammarAccess.getRelationshipAccess().getTypeRelationshipTypeEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRelationshipAccess().getTypeRelationshipTypeEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getRelationshipAccess().getTypeRelationshipTypeCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__TypeAssignment_5"


    // $ANTLR start "rule__Relationship__ConstantAssignment_6_2"
    // InternalXaaS.g:5211:1: rule__Relationship__ConstantAssignment_6_2 : ( ruleEBoolean ) ;
    public final void rule__Relationship__ConstantAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5215:1: ( ( ruleEBoolean ) )
            // InternalXaaS.g:5216:2: ( ruleEBoolean )
            {
            // InternalXaaS.g:5216:2: ( ruleEBoolean )
            // InternalXaaS.g:5217:3: ruleEBoolean
            {
             before(grammarAccess.getRelationshipAccess().getConstantEBooleanParserRuleCall_6_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEBoolean();

            state._fsp--;

             after(grammarAccess.getRelationshipAccess().getConstantEBooleanParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__ConstantAssignment_6_2"


    // $ANTLR start "rule__Relationship__SourceAssignment_7_2"
    // InternalXaaS.g:5226:1: rule__Relationship__SourceAssignment_7_2 : ( ( ruleEString ) ) ;
    public final void rule__Relationship__SourceAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5230:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5231:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5231:2: ( ( ruleEString ) )
            // InternalXaaS.g:5232:3: ( ruleEString )
            {
             before(grammarAccess.getRelationshipAccess().getSourceNodeCrossReference_7_2_0()); 
            // InternalXaaS.g:5233:3: ( ruleEString )
            // InternalXaaS.g:5234:4: ruleEString
            {
             before(grammarAccess.getRelationshipAccess().getSourceNodeEStringParserRuleCall_7_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRelationshipAccess().getSourceNodeEStringParserRuleCall_7_2_0_1()); 

            }

             after(grammarAccess.getRelationshipAccess().getSourceNodeCrossReference_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__SourceAssignment_7_2"


    // $ANTLR start "rule__Relationship__TargetAssignment_8_2"
    // InternalXaaS.g:5245:1: rule__Relationship__TargetAssignment_8_2 : ( ( ruleEString ) ) ;
    public final void rule__Relationship__TargetAssignment_8_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5249:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5250:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5250:2: ( ( ruleEString ) )
            // InternalXaaS.g:5251:3: ( ruleEString )
            {
             before(grammarAccess.getRelationshipAccess().getTargetNodeCrossReference_8_2_0()); 
            // InternalXaaS.g:5252:3: ( ruleEString )
            // InternalXaaS.g:5253:4: ruleEString
            {
             before(grammarAccess.getRelationshipAccess().getTargetNodeEStringParserRuleCall_8_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRelationshipAccess().getTargetNodeEStringParserRuleCall_8_2_0_1()); 

            }

             after(grammarAccess.getRelationshipAccess().getTargetNodeCrossReference_8_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Relationship__TargetAssignment_8_2"


    // $ANTLR start "rule__NodeType__NameAssignment_1"
    // InternalXaaS.g:5264:1: rule__NodeType__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__NodeType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5268:1: ( ( ruleEString ) )
            // InternalXaaS.g:5269:2: ( ruleEString )
            {
            // InternalXaaS.g:5269:2: ( ruleEString )
            // InternalXaaS.g:5270:3: ruleEString
            {
             before(grammarAccess.getNodeTypeAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getNodeTypeAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__NameAssignment_1"


    // $ANTLR start "rule__NodeType__InheritedTypeAssignment_3_2"
    // InternalXaaS.g:5279:1: rule__NodeType__InheritedTypeAssignment_3_2 : ( ( ruleEString ) ) ;
    public final void rule__NodeType__InheritedTypeAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5283:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5284:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5284:2: ( ( ruleEString ) )
            // InternalXaaS.g:5285:3: ( ruleEString )
            {
             before(grammarAccess.getNodeTypeAccess().getInheritedTypeNodeTypeCrossReference_3_2_0()); 
            // InternalXaaS.g:5286:3: ( ruleEString )
            // InternalXaaS.g:5287:4: ruleEString
            {
             before(grammarAccess.getNodeTypeAccess().getInheritedTypeNodeTypeEStringParserRuleCall_3_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getNodeTypeAccess().getInheritedTypeNodeTypeEStringParserRuleCall_3_2_0_1()); 

            }

             after(grammarAccess.getNodeTypeAccess().getInheritedTypeNodeTypeCrossReference_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__InheritedTypeAssignment_3_2"


    // $ANTLR start "rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2"
    // InternalXaaS.g:5298:1: rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2 : ( ruleEInt ) ;
    public final void rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5302:1: ( ( ruleEInt ) )
            // InternalXaaS.g:5303:2: ( ruleEInt )
            {
            // InternalXaaS.g:5303:2: ( ruleEInt )
            // InternalXaaS.g:5304:3: ruleEInt
            {
             before(grammarAccess.getNodeTypeAccess().getImpactOfEnablingEIntParserRuleCall_4_2_0_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getNodeTypeAccess().getImpactOfEnablingEIntParserRuleCall_4_2_0_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2"


    // $ANTLR start "rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2"
    // InternalXaaS.g:5313:1: rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2 : ( ruleEInt ) ;
    public final void rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5317:1: ( ( ruleEInt ) )
            // InternalXaaS.g:5318:2: ( ruleEInt )
            {
            // InternalXaaS.g:5318:2: ( ruleEInt )
            // InternalXaaS.g:5319:3: ruleEInt
            {
             before(grammarAccess.getNodeTypeAccess().getImpactOfDisablingEIntParserRuleCall_4_2_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getNodeTypeAccess().getImpactOfDisablingEIntParserRuleCall_4_2_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2"


    // $ANTLR start "rule__NodeType__AttributeTypesAssignment_4_2_2"
    // InternalXaaS.g:5328:1: rule__NodeType__AttributeTypesAssignment_4_2_2 : ( ruleAttributeType ) ;
    public final void rule__NodeType__AttributeTypesAssignment_4_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5332:1: ( ( ruleAttributeType ) )
            // InternalXaaS.g:5333:2: ( ruleAttributeType )
            {
            // InternalXaaS.g:5333:2: ( ruleAttributeType )
            // InternalXaaS.g:5334:3: ruleAttributeType
            {
             before(grammarAccess.getNodeTypeAccess().getAttributeTypesAttributeTypeParserRuleCall_4_2_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeType();

            state._fsp--;

             after(grammarAccess.getNodeTypeAccess().getAttributeTypesAttributeTypeParserRuleCall_4_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__AttributeTypesAssignment_4_2_2"


    // $ANTLR start "rule__NodeType__ConstraintsAssignment_4_2_3_2"
    // InternalXaaS.g:5343:1: rule__NodeType__ConstraintsAssignment_4_2_3_2 : ( ruleConstraint ) ;
    public final void rule__NodeType__ConstraintsAssignment_4_2_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5347:1: ( ( ruleConstraint ) )
            // InternalXaaS.g:5348:2: ( ruleConstraint )
            {
            // InternalXaaS.g:5348:2: ( ruleConstraint )
            // InternalXaaS.g:5349:3: ruleConstraint
            {
             before(grammarAccess.getNodeTypeAccess().getConstraintsConstraintParserRuleCall_4_2_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getNodeTypeAccess().getConstraintsConstraintParserRuleCall_4_2_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__ConstraintsAssignment_4_2_3_2"


    // $ANTLR start "rule__Attribute__NameAssignment_0"
    // InternalXaaS.g:5358:1: rule__Attribute__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__Attribute__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5362:1: ( ( ruleEString ) )
            // InternalXaaS.g:5363:2: ( ruleEString )
            {
            // InternalXaaS.g:5363:2: ( ruleEString )
            // InternalXaaS.g:5364:3: ruleEString
            {
             before(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__NameAssignment_0"


    // $ANTLR start "rule__Attribute__ValueAssignment_2"
    // InternalXaaS.g:5373:1: rule__Attribute__ValueAssignment_2 : ( ruleEValue ) ;
    public final void rule__Attribute__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5377:1: ( ( ruleEValue ) )
            // InternalXaaS.g:5378:2: ( ruleEValue )
            {
            // InternalXaaS.g:5378:2: ( ruleEValue )
            // InternalXaaS.g:5379:3: ruleEValue
            {
             before(grammarAccess.getAttributeAccess().getValueEValueParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEValue();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getValueEValueParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__ValueAssignment_2"


    // $ANTLR start "rule__Constraint__ExpressionsAssignment_0"
    // InternalXaaS.g:5388:1: rule__Constraint__ExpressionsAssignment_0 : ( ruleAttributeExpression ) ;
    public final void rule__Constraint__ExpressionsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5392:1: ( ( ruleAttributeExpression ) )
            // InternalXaaS.g:5393:2: ( ruleAttributeExpression )
            {
            // InternalXaaS.g:5393:2: ( ruleAttributeExpression )
            // InternalXaaS.g:5394:3: ruleAttributeExpression
            {
             before(grammarAccess.getConstraintAccess().getExpressionsAttributeExpressionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleAttributeExpression();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getExpressionsAttributeExpressionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__ExpressionsAssignment_0"


    // $ANTLR start "rule__Constraint__OperatorAssignment_1"
    // InternalXaaS.g:5403:1: rule__Constraint__OperatorAssignment_1 : ( ruleComparisonOperator ) ;
    public final void rule__Constraint__OperatorAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5407:1: ( ( ruleComparisonOperator ) )
            // InternalXaaS.g:5408:2: ( ruleComparisonOperator )
            {
            // InternalXaaS.g:5408:2: ( ruleComparisonOperator )
            // InternalXaaS.g:5409:3: ruleComparisonOperator
            {
             before(grammarAccess.getConstraintAccess().getOperatorComparisonOperatorEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleComparisonOperator();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getOperatorComparisonOperatorEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__OperatorAssignment_1"


    // $ANTLR start "rule__Constraint__ExpressionsAssignment_3"
    // InternalXaaS.g:5418:1: rule__Constraint__ExpressionsAssignment_3 : ( ruleExpression ) ;
    public final void rule__Constraint__ExpressionsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5422:1: ( ( ruleExpression ) )
            // InternalXaaS.g:5423:2: ( ruleExpression )
            {
            // InternalXaaS.g:5423:2: ( ruleExpression )
            // InternalXaaS.g:5424:3: ruleExpression
            {
             before(grammarAccess.getConstraintAccess().getExpressionsExpressionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getExpressionsExpressionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__ExpressionsAssignment_3"


    // $ANTLR start "rule__ConstantAttributeType__NameAssignment_1"
    // InternalXaaS.g:5433:1: rule__ConstantAttributeType__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__ConstantAttributeType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5437:1: ( ( ruleEString ) )
            // InternalXaaS.g:5438:2: ( ruleEString )
            {
            // InternalXaaS.g:5438:2: ( ruleEString )
            // InternalXaaS.g:5439:3: ruleEString
            {
             before(grammarAccess.getConstantAttributeTypeAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConstantAttributeTypeAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__NameAssignment_1"


    // $ANTLR start "rule__ConstantAttributeType__TypeAssignment_5"
    // InternalXaaS.g:5448:1: rule__ConstantAttributeType__TypeAssignment_5 : ( ruleEString ) ;
    public final void rule__ConstantAttributeType__TypeAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5452:1: ( ( ruleEString ) )
            // InternalXaaS.g:5453:2: ( ruleEString )
            {
            // InternalXaaS.g:5453:2: ( ruleEString )
            // InternalXaaS.g:5454:3: ruleEString
            {
             before(grammarAccess.getConstantAttributeTypeAccess().getTypeEStringParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConstantAttributeTypeAccess().getTypeEStringParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConstantAttributeType__TypeAssignment_5"


    // $ANTLR start "rule__CalculatedAttributeType__NameAssignment_2"
    // InternalXaaS.g:5463:1: rule__CalculatedAttributeType__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__CalculatedAttributeType__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5467:1: ( ( ruleEString ) )
            // InternalXaaS.g:5468:2: ( ruleEString )
            {
            // InternalXaaS.g:5468:2: ( ruleEString )
            // InternalXaaS.g:5469:3: ruleEString
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCalculatedAttributeTypeAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__NameAssignment_2"


    // $ANTLR start "rule__CalculatedAttributeType__TypeAssignment_6"
    // InternalXaaS.g:5478:1: rule__CalculatedAttributeType__TypeAssignment_6 : ( ruleEString ) ;
    public final void rule__CalculatedAttributeType__TypeAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5482:1: ( ( ruleEString ) )
            // InternalXaaS.g:5483:2: ( ruleEString )
            {
            // InternalXaaS.g:5483:2: ( ruleEString )
            // InternalXaaS.g:5484:3: ruleEString
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getTypeEStringParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCalculatedAttributeTypeAccess().getTypeEStringParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__TypeAssignment_6"


    // $ANTLR start "rule__CalculatedAttributeType__ExpressionAssignment_9"
    // InternalXaaS.g:5493:1: rule__CalculatedAttributeType__ExpressionAssignment_9 : ( ruleExpression ) ;
    public final void rule__CalculatedAttributeType__ExpressionAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5497:1: ( ( ruleExpression ) )
            // InternalXaaS.g:5498:2: ( ruleExpression )
            {
            // InternalXaaS.g:5498:2: ( ruleExpression )
            // InternalXaaS.g:5499:3: ruleExpression
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getExpressionExpressionParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getCalculatedAttributeTypeAccess().getExpressionExpressionParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__ExpressionAssignment_9"


    // $ANTLR start "rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2"
    // InternalXaaS.g:5508:1: rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2 : ( ruleEInt ) ;
    public final void rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5512:1: ( ( ruleEInt ) )
            // InternalXaaS.g:5513:2: ( ruleEInt )
            {
            // InternalXaaS.g:5513:2: ( ruleEInt )
            // InternalXaaS.g:5514:3: ruleEInt
            {
             before(grammarAccess.getCalculatedAttributeTypeAccess().getImpactOfUpdatingEIntParserRuleCall_10_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getCalculatedAttributeTypeAccess().getImpactOfUpdatingEIntParserRuleCall_10_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2"


    // $ANTLR start "rule__IntegerValueExpression__ValueAssignment"
    // InternalXaaS.g:5523:1: rule__IntegerValueExpression__ValueAssignment : ( ruleEInt ) ;
    public final void rule__IntegerValueExpression__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5527:1: ( ( ruleEInt ) )
            // InternalXaaS.g:5528:2: ( ruleEInt )
            {
            // InternalXaaS.g:5528:2: ( ruleEInt )
            // InternalXaaS.g:5529:3: ruleEInt
            {
             before(grammarAccess.getIntegerValueExpressionAccess().getValueEIntParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getIntegerValueExpressionAccess().getValueEIntParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerValueExpression__ValueAssignment"


    // $ANTLR start "rule__AttributeExpression__AttributeTypeAssignment"
    // InternalXaaS.g:5538:1: rule__AttributeExpression__AttributeTypeAssignment : ( ( ruleEString ) ) ;
    public final void rule__AttributeExpression__AttributeTypeAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5542:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5543:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5543:2: ( ( ruleEString ) )
            // InternalXaaS.g:5544:3: ( ruleEString )
            {
             before(grammarAccess.getAttributeExpressionAccess().getAttributeTypeAttributeTypeCrossReference_0()); 
            // InternalXaaS.g:5545:3: ( ruleEString )
            // InternalXaaS.g:5546:4: ruleEString
            {
             before(grammarAccess.getAttributeExpressionAccess().getAttributeTypeAttributeTypeEStringParserRuleCall_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAttributeExpressionAccess().getAttributeTypeAttributeTypeEStringParserRuleCall_0_1()); 

            }

             after(grammarAccess.getAttributeExpressionAccess().getAttributeTypeAttributeTypeCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttributeExpression__AttributeTypeAssignment"


    // $ANTLR start "rule__BinaryExpression__ExpressionsAssignment_0"
    // InternalXaaS.g:5557:1: rule__BinaryExpression__ExpressionsAssignment_0 : ( ruleSingelExpression ) ;
    public final void rule__BinaryExpression__ExpressionsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5561:1: ( ( ruleSingelExpression ) )
            // InternalXaaS.g:5562:2: ( ruleSingelExpression )
            {
            // InternalXaaS.g:5562:2: ( ruleSingelExpression )
            // InternalXaaS.g:5563:3: ruleSingelExpression
            {
             before(grammarAccess.getBinaryExpressionAccess().getExpressionsSingelExpressionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleSingelExpression();

            state._fsp--;

             after(grammarAccess.getBinaryExpressionAccess().getExpressionsSingelExpressionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__ExpressionsAssignment_0"


    // $ANTLR start "rule__BinaryExpression__OperatorAssignment_1"
    // InternalXaaS.g:5572:1: rule__BinaryExpression__OperatorAssignment_1 : ( ruleAlgebraicOperator ) ;
    public final void rule__BinaryExpression__OperatorAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5576:1: ( ( ruleAlgebraicOperator ) )
            // InternalXaaS.g:5577:2: ( ruleAlgebraicOperator )
            {
            // InternalXaaS.g:5577:2: ( ruleAlgebraicOperator )
            // InternalXaaS.g:5578:3: ruleAlgebraicOperator
            {
             before(grammarAccess.getBinaryExpressionAccess().getOperatorAlgebraicOperatorEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAlgebraicOperator();

            state._fsp--;

             after(grammarAccess.getBinaryExpressionAccess().getOperatorAlgebraicOperatorEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__OperatorAssignment_1"


    // $ANTLR start "rule__BinaryExpression__ExpressionsAssignment_2"
    // InternalXaaS.g:5587:1: rule__BinaryExpression__ExpressionsAssignment_2 : ( ruleExpression ) ;
    public final void rule__BinaryExpression__ExpressionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5591:1: ( ( ruleExpression ) )
            // InternalXaaS.g:5592:2: ( ruleExpression )
            {
            // InternalXaaS.g:5592:2: ( ruleExpression )
            // InternalXaaS.g:5593:3: ruleExpression
            {
             before(grammarAccess.getBinaryExpressionAccess().getExpressionsExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getBinaryExpressionAccess().getExpressionsExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryExpression__ExpressionsAssignment_2"


    // $ANTLR start "rule__AggregationExpression__OperatorAssignment_0"
    // InternalXaaS.g:5602:1: rule__AggregationExpression__OperatorAssignment_0 : ( ruleAggregationOperator ) ;
    public final void rule__AggregationExpression__OperatorAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5606:1: ( ( ruleAggregationOperator ) )
            // InternalXaaS.g:5607:2: ( ruleAggregationOperator )
            {
            // InternalXaaS.g:5607:2: ( ruleAggregationOperator )
            // InternalXaaS.g:5608:3: ruleAggregationOperator
            {
             before(grammarAccess.getAggregationExpressionAccess().getOperatorAggregationOperatorEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleAggregationOperator();

            state._fsp--;

             after(grammarAccess.getAggregationExpressionAccess().getOperatorAggregationOperatorEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__OperatorAssignment_0"


    // $ANTLR start "rule__AggregationExpression__DirectionAssignment_2"
    // InternalXaaS.g:5617:1: rule__AggregationExpression__DirectionAssignment_2 : ( ruleDirectionKind ) ;
    public final void rule__AggregationExpression__DirectionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5621:1: ( ( ruleDirectionKind ) )
            // InternalXaaS.g:5622:2: ( ruleDirectionKind )
            {
            // InternalXaaS.g:5622:2: ( ruleDirectionKind )
            // InternalXaaS.g:5623:3: ruleDirectionKind
            {
             before(grammarAccess.getAggregationExpressionAccess().getDirectionDirectionKindEnumRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDirectionKind();

            state._fsp--;

             after(grammarAccess.getAggregationExpressionAccess().getDirectionDirectionKindEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__DirectionAssignment_2"


    // $ANTLR start "rule__AggregationExpression__AttributeTypeAssignment_4"
    // InternalXaaS.g:5632:1: rule__AggregationExpression__AttributeTypeAssignment_4 : ( ( ruleEString ) ) ;
    public final void rule__AggregationExpression__AttributeTypeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5636:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5637:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5637:2: ( ( ruleEString ) )
            // InternalXaaS.g:5638:3: ( ruleEString )
            {
             before(grammarAccess.getAggregationExpressionAccess().getAttributeTypeAttributeTypeCrossReference_4_0()); 
            // InternalXaaS.g:5639:3: ( ruleEString )
            // InternalXaaS.g:5640:4: ruleEString
            {
             before(grammarAccess.getAggregationExpressionAccess().getAttributeTypeAttributeTypeEStringParserRuleCall_4_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAggregationExpressionAccess().getAttributeTypeAttributeTypeEStringParserRuleCall_4_0_1()); 

            }

             after(grammarAccess.getAggregationExpressionAccess().getAttributeTypeAttributeTypeCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AggregationExpression__AttributeTypeAssignment_4"


    // $ANTLR start "rule__NbConnectionExpression__DirectionAssignment_2"
    // InternalXaaS.g:5651:1: rule__NbConnectionExpression__DirectionAssignment_2 : ( ruleDirectionKind ) ;
    public final void rule__NbConnectionExpression__DirectionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5655:1: ( ( ruleDirectionKind ) )
            // InternalXaaS.g:5656:2: ( ruleDirectionKind )
            {
            // InternalXaaS.g:5656:2: ( ruleDirectionKind )
            // InternalXaaS.g:5657:3: ruleDirectionKind
            {
             before(grammarAccess.getNbConnectionExpressionAccess().getDirectionDirectionKindEnumRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleDirectionKind();

            state._fsp--;

             after(grammarAccess.getNbConnectionExpressionAccess().getDirectionDirectionKindEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NbConnectionExpression__DirectionAssignment_2"


    // $ANTLR start "rule__CustomExpression__ExpressionAssignment_2"
    // InternalXaaS.g:5666:1: rule__CustomExpression__ExpressionAssignment_2 : ( ruleEString ) ;
    public final void rule__CustomExpression__ExpressionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5670:1: ( ( ruleEString ) )
            // InternalXaaS.g:5671:2: ( ruleEString )
            {
            // InternalXaaS.g:5671:2: ( ruleEString )
            // InternalXaaS.g:5672:3: ruleEString
            {
             before(grammarAccess.getCustomExpressionAccess().getExpressionEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCustomExpressionAccess().getExpressionEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CustomExpression__ExpressionAssignment_2"


    // $ANTLR start "rule__RelationshipType__NameAssignment_0"
    // InternalXaaS.g:5681:1: rule__RelationshipType__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__RelationshipType__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5685:1: ( ( ruleEString ) )
            // InternalXaaS.g:5686:2: ( ruleEString )
            {
            // InternalXaaS.g:5686:2: ( ruleEString )
            // InternalXaaS.g:5687:3: ruleEString
            {
             before(grammarAccess.getRelationshipTypeAccess().getNameEStringParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRelationshipTypeAccess().getNameEStringParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__NameAssignment_0"


    // $ANTLR start "rule__RelationshipType__SourceAssignment_4"
    // InternalXaaS.g:5696:1: rule__RelationshipType__SourceAssignment_4 : ( ( ruleEString ) ) ;
    public final void rule__RelationshipType__SourceAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5700:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5701:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5701:2: ( ( ruleEString ) )
            // InternalXaaS.g:5702:3: ( ruleEString )
            {
             before(grammarAccess.getRelationshipTypeAccess().getSourceNodeTypeCrossReference_4_0()); 
            // InternalXaaS.g:5703:3: ( ruleEString )
            // InternalXaaS.g:5704:4: ruleEString
            {
             before(grammarAccess.getRelationshipTypeAccess().getSourceNodeTypeEStringParserRuleCall_4_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRelationshipTypeAccess().getSourceNodeTypeEStringParserRuleCall_4_0_1()); 

            }

             after(grammarAccess.getRelationshipTypeAccess().getSourceNodeTypeCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__SourceAssignment_4"


    // $ANTLR start "rule__RelationshipType__TargetAssignment_7"
    // InternalXaaS.g:5715:1: rule__RelationshipType__TargetAssignment_7 : ( ( ruleEString ) ) ;
    public final void rule__RelationshipType__TargetAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5719:1: ( ( ( ruleEString ) ) )
            // InternalXaaS.g:5720:2: ( ( ruleEString ) )
            {
            // InternalXaaS.g:5720:2: ( ( ruleEString ) )
            // InternalXaaS.g:5721:3: ( ruleEString )
            {
             before(grammarAccess.getRelationshipTypeAccess().getTargetNodeTypeCrossReference_7_0()); 
            // InternalXaaS.g:5722:3: ( ruleEString )
            // InternalXaaS.g:5723:4: ruleEString
            {
             before(grammarAccess.getRelationshipTypeAccess().getTargetNodeTypeEStringParserRuleCall_7_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRelationshipTypeAccess().getTargetNodeTypeEStringParserRuleCall_7_0_1()); 

            }

             after(grammarAccess.getRelationshipTypeAccess().getTargetNodeTypeCrossReference_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__TargetAssignment_7"


    // $ANTLR start "rule__RelationshipType__ConstraintsAssignment_8_2"
    // InternalXaaS.g:5734:1: rule__RelationshipType__ConstraintsAssignment_8_2 : ( ruleConstraint ) ;
    public final void rule__RelationshipType__ConstraintsAssignment_8_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5738:1: ( ( ruleConstraint ) )
            // InternalXaaS.g:5739:2: ( ruleConstraint )
            {
            // InternalXaaS.g:5739:2: ( ruleConstraint )
            // InternalXaaS.g:5740:3: ruleConstraint
            {
             before(grammarAccess.getRelationshipTypeAccess().getConstraintsConstraintParserRuleCall_8_2_0()); 
            pushFollow(FOLLOW_2);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getRelationshipTypeAccess().getConstraintsConstraintParserRuleCall_8_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__ConstraintsAssignment_8_2"


    // $ANTLR start "rule__RelationshipType__ImpactOfLinkingAssignment_9_2"
    // InternalXaaS.g:5749:1: rule__RelationshipType__ImpactOfLinkingAssignment_9_2 : ( ruleEInt ) ;
    public final void rule__RelationshipType__ImpactOfLinkingAssignment_9_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5753:1: ( ( ruleEInt ) )
            // InternalXaaS.g:5754:2: ( ruleEInt )
            {
            // InternalXaaS.g:5754:2: ( ruleEInt )
            // InternalXaaS.g:5755:3: ruleEInt
            {
             before(grammarAccess.getRelationshipTypeAccess().getImpactOfLinkingEIntParserRuleCall_9_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getRelationshipTypeAccess().getImpactOfLinkingEIntParserRuleCall_9_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__ImpactOfLinkingAssignment_9_2"


    // $ANTLR start "rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2"
    // InternalXaaS.g:5764:1: rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2 : ( ruleEInt ) ;
    public final void rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXaaS.g:5768:1: ( ( ruleEInt ) )
            // InternalXaaS.g:5769:2: ( ruleEInt )
            {
            // InternalXaaS.g:5769:2: ( ruleEInt )
            // InternalXaaS.g:5770:3: ruleEInt
            {
             before(grammarAccess.getRelationshipTypeAccess().getImpactOfUnlinkingEIntParserRuleCall_10_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getRelationshipTypeAccess().getImpactOfUnlinkingEIntParserRuleCall_10_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2"

    // Delegated rules


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA28 dfa28 = new DFA28(this);
    protected DFA34 dfa34 = new DFA34(this);
    static final String dfa_1s = "\36\uffff";
    static final String dfa_2s = "\2\uffff\1\13\2\15\13\uffff\1\15\5\uffff\1\31\4\uffff\1\35\2\uffff";
    static final String dfa_3s = "\1\4\1\6\3\4\4\61\3\uffff\1\5\1\uffff\2\24\1\4\2\62\2\63\2\4\1\42\1\63\1\uffff\1\5\1\4\1\42\1\uffff";
    static final String dfa_4s = "\1\65\1\6\3\71\4\61\3\uffff\1\5\1\uffff\2\25\1\71\2\62\2\63\1\5\1\71\2\63\1\uffff\1\5\1\71\1\63\1\uffff";
    static final String dfa_5s = "\11\uffff\1\6\1\3\1\1\1\uffff\1\2\13\uffff\1\5\3\uffff\1\4";
    static final String dfa_6s = "\36\uffff}>";
    static final String[] dfa_7s = {
            "\1\4\1\3\1\2\10\uffff\1\1\1\uffff\1\5\1\6\1\7\40\uffff\1\10\1\11",
            "\1\2",
            "\2\13\7\uffff\4\12\12\uffff\1\13\5\uffff\1\13\6\uffff\1\13\3\uffff\5\13\7\uffff\2\13",
            "\2\15\7\uffff\4\12\12\uffff\1\15\5\uffff\1\15\1\14\5\uffff\1\15\3\uffff\5\15\7\uffff\2\15",
            "\2\15\7\uffff\4\12\12\uffff\1\15\5\uffff\1\15\6\uffff\1\15\3\uffff\5\15\7\uffff\2\15",
            "\1\16",
            "\1\16",
            "\1\16",
            "\1\17",
            "",
            "",
            "",
            "\1\20",
            "",
            "\1\21\1\22",
            "\1\23\1\24",
            "\2\15\7\uffff\4\12\12\uffff\1\15\5\uffff\1\15\1\14\5\uffff\1\15\3\uffff\5\15\7\uffff\2\15",
            "\1\25",
            "\1\25",
            "\1\26",
            "\1\26",
            "\1\30\1\27",
            "\2\31\7\uffff\4\12\12\uffff\1\31\5\uffff\1\31\6\uffff\1\31\3\uffff\5\31\7\uffff\2\31",
            "\1\32\20\uffff\1\33",
            "\1\33",
            "",
            "\1\34",
            "\2\35\7\uffff\4\12\12\uffff\1\35\5\uffff\1\35\6\uffff\1\35\3\uffff\5\35\7\uffff\2\35",
            "\1\32\20\uffff\1\33",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "737:1: rule__Expression__Alternatives : ( ( ruleIntegerValueExpression ) | ( ruleAttributeExpression ) | ( ruleBinaryExpression ) | ( ruleAggregationExpression ) | ( ruleNbConnectionExpression ) | ( ruleCustomExpression ) );";
        }
    }
    static final String dfa_8s = "\7\uffff";
    static final String dfa_9s = "\1\1\6\uffff";
    static final String dfa_10s = "\1\4\1\uffff\2\26\1\5\1\uffff\1\26";
    static final String dfa_11s = "\1\57\1\uffff\1\42\1\34\1\5\1\uffff\1\42";
    static final String dfa_12s = "\1\uffff\1\2\3\uffff\1\1\1\uffff";
    static final String dfa_13s = "\7\uffff}>";
    static final String[] dfa_14s = {
            "\1\3\1\2\25\uffff\1\1\5\uffff\1\1\6\uffff\1\1\3\uffff\2\1\1\uffff\1\1",
            "",
            "\5\5\1\uffff\1\1\5\uffff\1\4",
            "\5\5\1\uffff\1\1",
            "\1\6",
            "",
            "\5\5\1\uffff\1\1\5\uffff\1\4"
    };

    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final char[] dfa_11 = DFA.unpackEncodedStringToUnsignedChars(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[] dfa_13 = DFA.unpackEncodedString(dfa_13s);
    static final short[][] dfa_14 = unpackEncodedStringArray(dfa_14s);

    class DFA28 extends DFA {

        public DFA28(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 28;
            this.eot = dfa_8;
            this.eof = dfa_9;
            this.min = dfa_10;
            this.max = dfa_11;
            this.accept = dfa_12;
            this.special = dfa_13;
            this.transition = dfa_14;
        }
        public String getDescription() {
            return "()* loopback of 3209:3: ( rule__NodeType__ConstraintsAssignment_4_2_3_2 )*";
        }
    }
    static final String dfa_15s = "\1\71\1\uffff\1\42\1\34\1\5\1\uffff\1\42";
    static final String[] dfa_16s = {
            "\1\3\1\2\25\uffff\1\1\34\uffff\2\1",
            "",
            "\5\5\1\uffff\1\1\5\uffff\1\4",
            "\5\5\1\uffff\1\1",
            "\1\6",
            "",
            "\5\5\1\uffff\1\1\5\uffff\1\4"
    };
    static final char[] dfa_15 = DFA.unpackEncodedStringToUnsignedChars(dfa_15s);
    static final short[][] dfa_16 = unpackEncodedStringArray(dfa_16s);

    class DFA34 extends DFA {

        public DFA34(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 34;
            this.eot = dfa_8;
            this.eof = dfa_9;
            this.min = dfa_10;
            this.max = dfa_15;
            this.accept = dfa_12;
            this.special = dfa_13;
            this.transition = dfa_16;
        }
        public String getDescription() {
            return "()* loopback of 4803:2: ( rule__RelationshipType__ConstraintsAssignment_8_2 )*";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000008800000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000008000000002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000200000030L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000070000000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000084000000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000B10000000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000B10000000002L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000008040L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000008050L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000007C00000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x00300000000E8070L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x000000000001E000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0004000000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0300400000000000L});

}
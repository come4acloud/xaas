/*
 * generated by Xtext 2.11.0
 */
package fr.imta.come4acloud.yml.ide.contentassist.antlr;

import com.google.inject.Inject;
import fr.imta.come4acloud.yml.ide.contentassist.antlr.internal.InternalXaaSParser;
import fr.imta.come4acloud.yml.services.XaaSGrammarAccess;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;

public class XaaSParser extends AbstractContentAssistParser {

	@Inject
	private XaaSGrammarAccess grammarAccess;

	private Map<AbstractElement, String> nameMappings;

	@Override
	protected InternalXaaSParser createParser() {
		InternalXaaSParser result = new InternalXaaSParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getAttributeTypeAccess().getAlternatives(), "rule__AttributeType__Alternatives");
					put(grammarAccess.getExpressionAccess().getAlternatives(), "rule__Expression__Alternatives");
					put(grammarAccess.getSingelExpressionAccess().getAlternatives(), "rule__SingelExpression__Alternatives");
					put(grammarAccess.getEStringAccess().getAlternatives(), "rule__EString__Alternatives");
					put(grammarAccess.getEValueAccess().getAlternatives(), "rule__EValue__Alternatives");
					put(grammarAccess.getEBooleanAccess().getAlternatives(), "rule__EBoolean__Alternatives");
					put(grammarAccess.getAlgebraicOperatorAccess().getAlternatives(), "rule__AlgebraicOperator__Alternatives");
					put(grammarAccess.getAggregationOperatorAccess().getAlternatives(), "rule__AggregationOperator__Alternatives");
					put(grammarAccess.getDirectionKindAccess().getAlternatives(), "rule__DirectionKind__Alternatives");
					put(grammarAccess.getComparisonOperatorAccess().getAlternatives(), "rule__ComparisonOperator__Alternatives");
					put(grammarAccess.getXaasYamlLikeToscaAccess().getGroup(), "rule__XaasYamlLikeTosca__Group__0");
					put(grammarAccess.getConfigurationAccess().getGroup(), "rule__Configuration__Group__0");
					put(grammarAccess.getTopologyAccess().getGroup(), "rule__Topology__Group__0");
					put(grammarAccess.getTopologyAccess().getGroup_7(), "rule__Topology__Group_7__0");
					put(grammarAccess.getEStringAccess().getGroup_0(), "rule__EString__Group_0__0");
					put(grammarAccess.getEStringAccess().getGroup_0_1(), "rule__EString__Group_0_1__0");
					put(grammarAccess.getNodeAccess().getGroup(), "rule__Node__Group__0");
					put(grammarAccess.getNodeAccess().getGroup_9(), "rule__Node__Group_9__0");
					put(grammarAccess.getRelationshipAccess().getGroup(), "rule__Relationship__Group__0");
					put(grammarAccess.getRelationshipAccess().getGroup_6(), "rule__Relationship__Group_6__0");
					put(grammarAccess.getRelationshipAccess().getGroup_7(), "rule__Relationship__Group_7__0");
					put(grammarAccess.getRelationshipAccess().getGroup_8(), "rule__Relationship__Group_8__0");
					put(grammarAccess.getNodeTypeAccess().getGroup(), "rule__NodeType__Group__0");
					put(grammarAccess.getNodeTypeAccess().getGroup_3(), "rule__NodeType__Group_3__0");
					put(grammarAccess.getNodeTypeAccess().getGroup_4(), "rule__NodeType__Group_4__0");
					put(grammarAccess.getNodeTypeAccess().getGroup_4_2(), "rule__NodeType__Group_4_2__0");
					put(grammarAccess.getNodeTypeAccess().getGroup_4_2_0(), "rule__NodeType__Group_4_2_0__0");
					put(grammarAccess.getNodeTypeAccess().getGroup_4_2_1(), "rule__NodeType__Group_4_2_1__0");
					put(grammarAccess.getNodeTypeAccess().getGroup_4_2_3(), "rule__NodeType__Group_4_2_3__0");
					put(grammarAccess.getAttributeAccess().getGroup(), "rule__Attribute__Group__0");
					put(grammarAccess.getConstraintAccess().getGroup(), "rule__Constraint__Group__0");
					put(grammarAccess.getConstantAttributeTypeAccess().getGroup(), "rule__ConstantAttributeType__Group__0");
					put(grammarAccess.getCalculatedAttributeTypeAccess().getGroup(), "rule__CalculatedAttributeType__Group__0");
					put(grammarAccess.getCalculatedAttributeTypeAccess().getGroup_10(), "rule__CalculatedAttributeType__Group_10__0");
					put(grammarAccess.getBinaryExpressionAccess().getGroup(), "rule__BinaryExpression__Group__0");
					put(grammarAccess.getAggregationExpressionAccess().getGroup(), "rule__AggregationExpression__Group__0");
					put(grammarAccess.getNbConnectionExpressionAccess().getGroup(), "rule__NbConnectionExpression__Group__0");
					put(grammarAccess.getCustomExpressionAccess().getGroup(), "rule__CustomExpression__Group__0");
					put(grammarAccess.getEIntAccess().getGroup(), "rule__EInt__Group__0");
					put(grammarAccess.getRelationshipTypeAccess().getGroup(), "rule__RelationshipType__Group__0");
					put(grammarAccess.getRelationshipTypeAccess().getGroup_8(), "rule__RelationshipType__Group_8__0");
					put(grammarAccess.getRelationshipTypeAccess().getGroup_9(), "rule__RelationshipType__Group_9__0");
					put(grammarAccess.getRelationshipTypeAccess().getGroup_10(), "rule__RelationshipType__Group_10__0");
					put(grammarAccess.getXaasYamlLikeToscaAccess().getTopologyAssignment_0(), "rule__XaasYamlLikeTosca__TopologyAssignment_0");
					put(grammarAccess.getXaasYamlLikeToscaAccess().getConfigurationAssignment_1(), "rule__XaasYamlLikeTosca__ConfigurationAssignment_1");
					put(grammarAccess.getConfigurationAccess().getIdentifierAssignment_4(), "rule__Configuration__IdentifierAssignment_4");
					put(grammarAccess.getConfigurationAccess().getTopologyAssignment_7(), "rule__Configuration__TopologyAssignment_7");
					put(grammarAccess.getConfigurationAccess().getNodesAssignment_8(), "rule__Configuration__NodesAssignment_8");
					put(grammarAccess.getConfigurationAccess().getRelationshipsAssignment_9(), "rule__Configuration__RelationshipsAssignment_9");
					put(grammarAccess.getTopologyAccess().getNameAssignment_3(), "rule__Topology__NameAssignment_3");
					put(grammarAccess.getTopologyAccess().getNodeTypesAssignment_6(), "rule__Topology__NodeTypesAssignment_6");
					put(grammarAccess.getTopologyAccess().getRelationshipTypesAssignment_7_2(), "rule__Topology__RelationshipTypesAssignment_7_2");
					put(grammarAccess.getNodeAccess().getNameAssignment_1(), "rule__Node__NameAssignment_1");
					put(grammarAccess.getNodeAccess().getTypeAssignment_5(), "rule__Node__TypeAssignment_5");
					put(grammarAccess.getNodeAccess().getActivatedAssignment_8(), "rule__Node__ActivatedAssignment_8");
					put(grammarAccess.getNodeAccess().getAttributesAssignment_9_2(), "rule__Node__AttributesAssignment_9_2");
					put(grammarAccess.getRelationshipAccess().getNameAssignment_1(), "rule__Relationship__NameAssignment_1");
					put(grammarAccess.getRelationshipAccess().getTypeAssignment_5(), "rule__Relationship__TypeAssignment_5");
					put(grammarAccess.getRelationshipAccess().getConstantAssignment_6_2(), "rule__Relationship__ConstantAssignment_6_2");
					put(grammarAccess.getRelationshipAccess().getSourceAssignment_7_2(), "rule__Relationship__SourceAssignment_7_2");
					put(grammarAccess.getRelationshipAccess().getTargetAssignment_8_2(), "rule__Relationship__TargetAssignment_8_2");
					put(grammarAccess.getNodeTypeAccess().getNameAssignment_1(), "rule__NodeType__NameAssignment_1");
					put(grammarAccess.getNodeTypeAccess().getInheritedTypeAssignment_3_2(), "rule__NodeType__InheritedTypeAssignment_3_2");
					put(grammarAccess.getNodeTypeAccess().getImpactOfEnablingAssignment_4_2_0_2(), "rule__NodeType__ImpactOfEnablingAssignment_4_2_0_2");
					put(grammarAccess.getNodeTypeAccess().getImpactOfDisablingAssignment_4_2_1_2(), "rule__NodeType__ImpactOfDisablingAssignment_4_2_1_2");
					put(grammarAccess.getNodeTypeAccess().getAttributeTypesAssignment_4_2_2(), "rule__NodeType__AttributeTypesAssignment_4_2_2");
					put(grammarAccess.getNodeTypeAccess().getConstraintsAssignment_4_2_3_2(), "rule__NodeType__ConstraintsAssignment_4_2_3_2");
					put(grammarAccess.getAttributeAccess().getNameAssignment_0(), "rule__Attribute__NameAssignment_0");
					put(grammarAccess.getAttributeAccess().getValueAssignment_2(), "rule__Attribute__ValueAssignment_2");
					put(grammarAccess.getConstraintAccess().getExpressionsAssignment_0(), "rule__Constraint__ExpressionsAssignment_0");
					put(grammarAccess.getConstraintAccess().getOperatorAssignment_1(), "rule__Constraint__OperatorAssignment_1");
					put(grammarAccess.getConstraintAccess().getExpressionsAssignment_3(), "rule__Constraint__ExpressionsAssignment_3");
					put(grammarAccess.getConstantAttributeTypeAccess().getNameAssignment_1(), "rule__ConstantAttributeType__NameAssignment_1");
					put(grammarAccess.getConstantAttributeTypeAccess().getTypeAssignment_5(), "rule__ConstantAttributeType__TypeAssignment_5");
					put(grammarAccess.getCalculatedAttributeTypeAccess().getNameAssignment_2(), "rule__CalculatedAttributeType__NameAssignment_2");
					put(grammarAccess.getCalculatedAttributeTypeAccess().getTypeAssignment_6(), "rule__CalculatedAttributeType__TypeAssignment_6");
					put(grammarAccess.getCalculatedAttributeTypeAccess().getExpressionAssignment_9(), "rule__CalculatedAttributeType__ExpressionAssignment_9");
					put(grammarAccess.getCalculatedAttributeTypeAccess().getImpactOfUpdatingAssignment_10_2(), "rule__CalculatedAttributeType__ImpactOfUpdatingAssignment_10_2");
					put(grammarAccess.getIntegerValueExpressionAccess().getValueAssignment(), "rule__IntegerValueExpression__ValueAssignment");
					put(grammarAccess.getAttributeExpressionAccess().getAttributeTypeAssignment(), "rule__AttributeExpression__AttributeTypeAssignment");
					put(grammarAccess.getBinaryExpressionAccess().getExpressionsAssignment_0(), "rule__BinaryExpression__ExpressionsAssignment_0");
					put(grammarAccess.getBinaryExpressionAccess().getOperatorAssignment_1(), "rule__BinaryExpression__OperatorAssignment_1");
					put(grammarAccess.getBinaryExpressionAccess().getExpressionsAssignment_2(), "rule__BinaryExpression__ExpressionsAssignment_2");
					put(grammarAccess.getAggregationExpressionAccess().getOperatorAssignment_0(), "rule__AggregationExpression__OperatorAssignment_0");
					put(grammarAccess.getAggregationExpressionAccess().getDirectionAssignment_2(), "rule__AggregationExpression__DirectionAssignment_2");
					put(grammarAccess.getAggregationExpressionAccess().getAttributeTypeAssignment_4(), "rule__AggregationExpression__AttributeTypeAssignment_4");
					put(grammarAccess.getNbConnectionExpressionAccess().getDirectionAssignment_2(), "rule__NbConnectionExpression__DirectionAssignment_2");
					put(grammarAccess.getCustomExpressionAccess().getExpressionAssignment_2(), "rule__CustomExpression__ExpressionAssignment_2");
					put(grammarAccess.getRelationshipTypeAccess().getNameAssignment_0(), "rule__RelationshipType__NameAssignment_0");
					put(grammarAccess.getRelationshipTypeAccess().getSourceAssignment_4(), "rule__RelationshipType__SourceAssignment_4");
					put(grammarAccess.getRelationshipTypeAccess().getTargetAssignment_7(), "rule__RelationshipType__TargetAssignment_7");
					put(grammarAccess.getRelationshipTypeAccess().getConstraintsAssignment_8_2(), "rule__RelationshipType__ConstraintsAssignment_8_2");
					put(grammarAccess.getRelationshipTypeAccess().getImpactOfLinkingAssignment_9_2(), "rule__RelationshipType__ImpactOfLinkingAssignment_9_2");
					put(grammarAccess.getRelationshipTypeAccess().getImpactOfUnlinkingAssignment_10_2(), "rule__RelationshipType__ImpactOfUnlinkingAssignment_10_2");
				}
			};
		}
		return nameMappings.get(element);
	}
			
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public XaaSGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(XaaSGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}

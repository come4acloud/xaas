package fr.imta.come4acloud.yml.ui.handler;

import java.net.URL;
import java.util.ArrayList; 
import java.util.Arrays; 
import java.util.List; 

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.generator.GeneratorContext;
import org.eclipse.xtext.generator.IGenerator2;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class GenerationHandler extends AbstractHandler implements IHandler {
    
    @Inject
    private IGenerator2 generator;
 
    @Inject
    private Provider<EclipseResourceFileSystemAccess> fileAccessProvider;
     
    @Inject
    IResourceDescriptions resourceDescriptions;
     
    @Inject
    IResourceSetProvider resourceSetProvider;
     
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
         
        ISelection selection = HandlerUtil.getCurrentSelection(event);
        if (selection instanceof IStructuredSelection) {
            IStructuredSelection structuredSelection = (IStructuredSelection) selection;
            Object firstElement = structuredSelection.getFirstElement();
            if (firstElement instanceof IFile) {
                IFile file = (IFile) firstElement;
                IProject project = file.getProject();
                IFolder srcGenFolder = project.getFolder("src-gen");
              //convert the project to java project
                IJavaProject javaProject = JavaCore.create(project);
                if (!srcGenFolder.exists()) {
                    try {
                    	//add src-gen folde
                        srcGenFolder.create(true, true, new NullProgressMonitor());
                        
                      
                        
                        List<IClasspathEntry> classpathEntries = new ArrayList<IClasspathEntry>();
                        
                      //add src-gen folder to the classpath entries
                        IClasspathEntry srcEntry = JavaCore.newSourceEntry(srcGenFolder.getFullPath());
                        classpathEntries.add(srcEntry);
                        //get library container path
                        URL delegateLocation = GenerationHandler.class.getProtectionDomain().getCodeSource().getLocation();
                        IPath libPath = new Path(delegateLocation.getPath());
                      //add choco solver library to the classpath entries 
                        IClasspathEntry chocoLibrary = JavaCore.newLibraryEntry(
                        	    new Path(libPath.append("lib/choco-solver-4.0.2.jar").toString()), // library location
                        	    new Path(libPath.append("lib/choco-solver-4.0.2-sources.jar").toString()), // source archive location
                        	    new Path("src"), // source archive root path
                        	    true); // exported
                        classpathEntries.add(chocoLibrary);
                      //add choco solver dependencies to the classpath entries
                        IClasspathEntry chocoDependencies = JavaCore.newLibraryEntry(
                        	    new Path(libPath.append("lib/choco-solver-4.0.2-with-dependencies.jar").toString()), 
                        	    null, // no source
                        	    null, // no source
                        	    false); // not exported
                        classpathEntries.add(chocoDependencies);
                      //add XaaS library to the classpath entries
                        IClasspathEntry XaaSLibrary = JavaCore.newLibraryEntry(
                        	    new Path(libPath.append("lib/XaaSv1.0.0.jar").toString()), 
                        	    null, // no source
                        	    null, // no source
                        	    false); // not exported
                        classpathEntries.add(XaaSLibrary);
                        
                        classpathEntries.addAll(Arrays.asList(javaProject.getRawClasspath()));
    					javaProject.setRawClasspath(classpathEntries.toArray(new IClasspathEntry[classpathEntries.size()]), new NullProgressMonitor());
    					 
                    } catch (CoreException e) {
                        return null;
                    }
                }
            	
            	
                final EclipseResourceFileSystemAccess fsa = fileAccessProvider.get();
                
//                fsa.setProject(project);
                fsa.setOutputPath(srcGenFolder.getFullPath().toString());
//                fsa.setMonitor(new NullProgressMonitor());  
                
                URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
                ResourceSet rs = resourceSetProvider.get(project);
                Resource r = rs.getResource(uri, true);
                generator.doGenerate(r, fsa, new GeneratorContext());
                 
            }
        }
        return null;
    }
 
    @Override
    public boolean isEnabled() {
        return true;
    }
 
}

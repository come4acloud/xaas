/**
 */
package xaas;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.AttributeType#getNodeType <em>Node Type</em>}</li>
 *   <li>{@link xaas.AttributeType#getName <em>Name</em>}</li>
 *   <li>{@link xaas.AttributeType#getType <em>Type</em>}</li>
 *   <li>{@link xaas.AttributeType#getImpactOfUpdating <em>Impact Of Updating</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getAttributeType()
 * @model abstract="true"
 * @generated
 */
public interface AttributeType extends EObject {
	/**
	 * Returns the value of the '<em><b>Node Type</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link xaas.NodeType#getAttributeTypes <em>Attribute Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Type</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Type</em>' container reference.
	 * @see #setNodeType(NodeType)
	 * @see xaas.XaasPackage#getAttributeType_NodeType()
	 * @see xaas.NodeType#getAttributeTypes
	 * @model opposite="attributeTypes" required="true" transient="false"
	 * @generated
	 */
	NodeType getNodeType();

	/**
	 * Sets the value of the '{@link xaas.AttributeType#getNodeType <em>Node Type</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Type</em>' container reference.
	 * @see #getNodeType()
	 * @generated
	 */
	void setNodeType(NodeType value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see xaas.XaasPackage#getAttributeType_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link xaas.AttributeType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see xaas.XaasPackage#getAttributeType_Type()
	 * @model required="true"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link xaas.AttributeType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Impact Of Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Impact Of Updating</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Impact Of Updating</em>' attribute.
	 * @see #setImpactOfUpdating(int)
	 * @see xaas.XaasPackage#getAttributeType_ImpactOfUpdating()
	 * @model
	 * @generated
	 */
	int getImpactOfUpdating();

	/**
	 * Sets the value of the '{@link xaas.AttributeType#getImpactOfUpdating <em>Impact Of Updating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Impact Of Updating</em>' attribute.
	 * @see #getImpactOfUpdating()
	 * @generated
	 */
	void setImpactOfUpdating(int value);

} // AttributeType

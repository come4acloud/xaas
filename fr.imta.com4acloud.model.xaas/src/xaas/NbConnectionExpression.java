/**
 */
package xaas;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nb Connection Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.NbConnectionExpression#getDirection <em>Direction</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getNbConnectionExpression()
 * @model
 * @generated
 */
public interface NbConnectionExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link xaas.DirectionKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see xaas.DirectionKind
	 * @see #setDirection(DirectionKind)
	 * @see xaas.XaasPackage#getNbConnectionExpression_Direction()
	 * @model required="true"
	 * @generated
	 */
	DirectionKind getDirection();

	/**
	 * Sets the value of the '{@link xaas.NbConnectionExpression#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see xaas.DirectionKind
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(DirectionKind value);

} // NbConnectionExpression

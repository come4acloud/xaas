/**
 */
package xaas;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.BinaryExpression#getOperator <em>Operator</em>}</li>
 *   <li>{@link xaas.BinaryExpression#getExpressions <em>Expressions</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getBinaryExpression()
 * @model
 * @generated
 */
public interface BinaryExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link xaas.AlgebraicOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see xaas.AlgebraicOperator
	 * @see #setOperator(AlgebraicOperator)
	 * @see xaas.XaasPackage#getBinaryExpression_Operator()
	 * @model required="true"
	 * @generated
	 */
	AlgebraicOperator getOperator();

	/**
	 * Sets the value of the '{@link xaas.BinaryExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see xaas.AlgebraicOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(AlgebraicOperator value);

	/**
	 * Returns the value of the '<em><b>Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.Expression}.
	 * It is bidirectional and its opposite is '{@link xaas.Expression#getBinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expressions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expressions</em>' containment reference list.
	 * @see xaas.XaasPackage#getBinaryExpression_Expressions()
	 * @see xaas.Expression#getBinaryExpression
	 * @model opposite="binaryExpression" containment="true" lower="2" upper="2"
	 * @generated
	 */
	EList<Expression> getExpressions();

} // BinaryExpression

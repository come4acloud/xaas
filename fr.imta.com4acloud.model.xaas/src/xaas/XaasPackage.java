/**
 */
package xaas;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see xaas.XaasFactory
 * @model kind="package"
 * @generated
 */
public interface XaasPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "xaas";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/xaas";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "xaas";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	XaasPackage eINSTANCE = xaas.impl.XaasPackageImpl.init();

	/**
	 * The meta object id for the '{@link xaas.impl.TopologyImpl <em>Topology</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.TopologyImpl
	 * @see xaas.impl.XaasPackageImpl#getTopology()
	 * @generated
	 */
	int TOPOLOGY = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Node Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY__NODE_TYPES = 1;

	/**
	 * The feature id for the '<em><b>Relationship Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY__RELATIONSHIP_TYPES = 2;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY__CONSTRAINTS = 3;

	/**
	 * The number of structural features of the '<em>Topology</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Topology</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOPOLOGY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.NodeTypeImpl <em>Node Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.NodeTypeImpl
	 * @see xaas.impl.XaasPackageImpl#getNodeType()
	 * @generated
	 */
	int NODE_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Topology</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE__TOPOLOGY = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Inherited Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE__INHERITED_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Impact Of Enabling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE__IMPACT_OF_ENABLING = 3;

	/**
	 * The feature id for the '<em><b>Impact Of Disabling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE__IMPACT_OF_DISABLING = 4;

	/**
	 * The feature id for the '<em><b>Attribute Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE__ATTRIBUTE_TYPES = 5;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE__CONSTRAINTS = 6;

	/**
	 * The number of structural features of the '<em>Node Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Node Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.AttributeTypeImpl <em>Attribute Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.AttributeTypeImpl
	 * @see xaas.impl.XaasPackageImpl#getAttributeType()
	 * @generated
	 */
	int ATTRIBUTE_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_TYPE__NODE_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_TYPE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_TYPE__TYPE = 2;

	/**
	 * The feature id for the '<em><b>Impact Of Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_TYPE__IMPACT_OF_UPDATING = 3;

	/**
	 * The number of structural features of the '<em>Attribute Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Attribute Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.ConstantAttributeTypeImpl <em>Constant Attribute Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.ConstantAttributeTypeImpl
	 * @see xaas.impl.XaasPackageImpl#getConstantAttributeType()
	 * @generated
	 */
	int CONSTANT_ATTRIBUTE_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_ATTRIBUTE_TYPE__NODE_TYPE = ATTRIBUTE_TYPE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_ATTRIBUTE_TYPE__NAME = ATTRIBUTE_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_ATTRIBUTE_TYPE__TYPE = ATTRIBUTE_TYPE__TYPE;

	/**
	 * The feature id for the '<em><b>Impact Of Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_ATTRIBUTE_TYPE__IMPACT_OF_UPDATING = ATTRIBUTE_TYPE__IMPACT_OF_UPDATING;

	/**
	 * The number of structural features of the '<em>Constant Attribute Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_ATTRIBUTE_TYPE_FEATURE_COUNT = ATTRIBUTE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Constant Attribute Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_ATTRIBUTE_TYPE_OPERATION_COUNT = ATTRIBUTE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link xaas.impl.CalculatedAttributeTypeImpl <em>Calculated Attribute Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.CalculatedAttributeTypeImpl
	 * @see xaas.impl.XaasPackageImpl#getCalculatedAttributeType()
	 * @generated
	 */
	int CALCULATED_ATTRIBUTE_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATED_ATTRIBUTE_TYPE__NODE_TYPE = ATTRIBUTE_TYPE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATED_ATTRIBUTE_TYPE__NAME = ATTRIBUTE_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATED_ATTRIBUTE_TYPE__TYPE = ATTRIBUTE_TYPE__TYPE;

	/**
	 * The feature id for the '<em><b>Impact Of Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATED_ATTRIBUTE_TYPE__IMPACT_OF_UPDATING = ATTRIBUTE_TYPE__IMPACT_OF_UPDATING;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATED_ATTRIBUTE_TYPE__EXPRESSION = ATTRIBUTE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Calculated Attribute Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATED_ATTRIBUTE_TYPE_FEATURE_COUNT = ATTRIBUTE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Calculated Attribute Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATED_ATTRIBUTE_TYPE_OPERATION_COUNT = ATTRIBUTE_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link xaas.impl.RelationshipTypeImpl <em>Relationship Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.RelationshipTypeImpl
	 * @see xaas.impl.XaasPackageImpl#getRelationshipType()
	 * @generated
	 */
	int RELATIONSHIP_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Topology</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_TYPE__TOPOLOGY = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_TYPE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_TYPE__SOURCE = 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_TYPE__TARGET = 3;

	/**
	 * The feature id for the '<em><b>Impact Of Linking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_TYPE__IMPACT_OF_LINKING = 4;

	/**
	 * The feature id for the '<em><b>Impact Of Unlinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_TYPE__IMPACT_OF_UNLINKING = 5;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_TYPE__CONSTRAINTS = 6;

	/**
	 * The number of structural features of the '<em>Relationship Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_TYPE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Relationship Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.ConstraintImpl <em>Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.ConstraintImpl
	 * @see xaas.impl.XaasPackageImpl#getConstraint()
	 * @generated
	 */
	int CONSTRAINT = 6;

	/**
	 * The feature id for the '<em><b>Topology</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__TOPOLOGY = 0;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__NODE_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Relationship Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__RELATIONSHIP_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__OPERATOR = 3;

	/**
	 * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__EXPRESSIONS = 4;

	/**
	 * The number of structural features of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.ExpressionImpl
	 * @see xaas.impl.XaasPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Calculated Attribute Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__CALCULATED_ATTRIBUTE_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__CONSTRAINT = 1;

	/**
	 * The feature id for the '<em><b>Binary Expression</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__BINARY_EXPRESSION = 2;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.IntegerValueExpressionImpl <em>Integer Value Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.IntegerValueExpressionImpl
	 * @see xaas.impl.XaasPackageImpl#getIntegerValueExpression()
	 * @generated
	 */
	int INTEGER_VALUE_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Calculated Attribute Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_EXPRESSION__CALCULATED_ATTRIBUTE_TYPE = EXPRESSION__CALCULATED_ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_EXPRESSION__CONSTRAINT = EXPRESSION__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Binary Expression</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_EXPRESSION__BINARY_EXPRESSION = EXPRESSION__BINARY_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_EXPRESSION__VALUE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Value Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Integer Value Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_VALUE_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link xaas.impl.AttributeExpressionImpl <em>Attribute Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.AttributeExpressionImpl
	 * @see xaas.impl.XaasPackageImpl#getAttributeExpression()
	 * @generated
	 */
	int ATTRIBUTE_EXPRESSION = 9;

	/**
	 * The feature id for the '<em><b>Calculated Attribute Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_EXPRESSION__CALCULATED_ATTRIBUTE_TYPE = EXPRESSION__CALCULATED_ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_EXPRESSION__CONSTRAINT = EXPRESSION__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Binary Expression</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_EXPRESSION__BINARY_EXPRESSION = EXPRESSION__BINARY_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_EXPRESSION__ATTRIBUTE_TYPE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Attribute Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link xaas.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.BinaryExpressionImpl
	 * @see xaas.impl.XaasPackageImpl#getBinaryExpression()
	 * @generated
	 */
	int BINARY_EXPRESSION = 10;

	/**
	 * The feature id for the '<em><b>Calculated Attribute Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__CALCULATED_ATTRIBUTE_TYPE = EXPRESSION__CALCULATED_ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__CONSTRAINT = EXPRESSION__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Binary Expression</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__BINARY_EXPRESSION = EXPRESSION__BINARY_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__EXPRESSIONS = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link xaas.impl.AggregationExpressionImpl <em>Aggregation Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.AggregationExpressionImpl
	 * @see xaas.impl.XaasPackageImpl#getAggregationExpression()
	 * @generated
	 */
	int AGGREGATION_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>Calculated Attribute Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATION_EXPRESSION__CALCULATED_ATTRIBUTE_TYPE = EXPRESSION__CALCULATED_ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATION_EXPRESSION__CONSTRAINT = EXPRESSION__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Binary Expression</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATION_EXPRESSION__BINARY_EXPRESSION = EXPRESSION__BINARY_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATION_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATION_EXPRESSION__DIRECTION = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATION_EXPRESSION__ATTRIBUTE_TYPE = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Aggregation Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Aggregation Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGGREGATION_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link xaas.impl.NbConnectionExpressionImpl <em>Nb Connection Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.NbConnectionExpressionImpl
	 * @see xaas.impl.XaasPackageImpl#getNbConnectionExpression()
	 * @generated
	 */
	int NB_CONNECTION_EXPRESSION = 12;

	/**
	 * The feature id for the '<em><b>Calculated Attribute Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_CONNECTION_EXPRESSION__CALCULATED_ATTRIBUTE_TYPE = EXPRESSION__CALCULATED_ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_CONNECTION_EXPRESSION__CONSTRAINT = EXPRESSION__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Binary Expression</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_CONNECTION_EXPRESSION__BINARY_EXPRESSION = EXPRESSION__BINARY_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_CONNECTION_EXPRESSION__DIRECTION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Nb Connection Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_CONNECTION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Nb Connection Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NB_CONNECTION_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link xaas.impl.CustomExpressionImpl <em>Custom Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.CustomExpressionImpl
	 * @see xaas.impl.XaasPackageImpl#getCustomExpression()
	 * @generated
	 */
	int CUSTOM_EXPRESSION = 13;

	/**
	 * The feature id for the '<em><b>Calculated Attribute Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_EXPRESSION__CALCULATED_ATTRIBUTE_TYPE = EXPRESSION__CALCULATED_ATTRIBUTE_TYPE;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_EXPRESSION__CONSTRAINT = EXPRESSION__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Binary Expression</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_EXPRESSION__BINARY_EXPRESSION = EXPRESSION__BINARY_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_EXPRESSION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Custom Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Custom Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link xaas.impl.ConfigurationImpl <em>Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.ConfigurationImpl
	 * @see xaas.impl.XaasPackageImpl#getConfiguration()
	 * @generated
	 */
	int CONFIGURATION = 14;

	/**
	 * The feature id for the '<em><b>Topology</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__TOPOLOGY = 0;

	/**
	 * The feature id for the '<em><b>Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__IDENTIFIER = 1;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__NODES = 2;

	/**
	 * The feature id for the '<em><b>Relationships</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__RELATIONSHIPS = 3;

	/**
	 * The number of structural features of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.NodeImpl
	 * @see xaas.impl.XaasPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 15;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__NAME = 2;

	/**
	 * The feature id for the '<em><b>Activated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__ACTIVATED = 3;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__ATTRIBUTES = 4;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.AttributeImpl <em>Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.AttributeImpl
	 * @see xaas.impl.XaasPackageImpl#getAttribute()
	 * @generated
	 */
	int ATTRIBUTE = 16;

	/**
	 * The feature id for the '<em><b>Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__NODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__VALUE = 2;

	/**
	 * The number of structural features of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.RelationshipImpl <em>Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.RelationshipImpl
	 * @see xaas.impl.XaasPackageImpl#getRelationship()
	 * @generated
	 */
	int RELATIONSHIP = 17;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__NAME = 2;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__CONSTANT = 3;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__SOURCE = 4;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP__TARGET = 5;

	/**
	 * The number of structural features of the '<em>Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RELATIONSHIP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.impl.XaasYamlLikeToscaImpl <em>Yaml Like Tosca</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.impl.XaasYamlLikeToscaImpl
	 * @see xaas.impl.XaasPackageImpl#getXaasYamlLikeTosca()
	 * @generated
	 */
	int XAAS_YAML_LIKE_TOSCA = 18;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAAS_YAML_LIKE_TOSCA__CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Topology</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAAS_YAML_LIKE_TOSCA__TOPOLOGY = 1;

	/**
	 * The number of structural features of the '<em>Yaml Like Tosca</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAAS_YAML_LIKE_TOSCA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Yaml Like Tosca</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XAAS_YAML_LIKE_TOSCA_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link xaas.ComparisonOperator <em>Comparison Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.ComparisonOperator
	 * @see xaas.impl.XaasPackageImpl#getComparisonOperator()
	 * @generated
	 */
	int COMPARISON_OPERATOR = 19;

	/**
	 * The meta object id for the '{@link xaas.AggregationOperator <em>Aggregation Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.AggregationOperator
	 * @see xaas.impl.XaasPackageImpl#getAggregationOperator()
	 * @generated
	 */
	int AGGREGATION_OPERATOR = 20;

	/**
	 * The meta object id for the '{@link xaas.AlgebraicOperator <em>Algebraic Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.AlgebraicOperator
	 * @see xaas.impl.XaasPackageImpl#getAlgebraicOperator()
	 * @generated
	 */
	int ALGEBRAIC_OPERATOR = 21;

	/**
	 * The meta object id for the '{@link xaas.DirectionKind <em>Direction Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see xaas.DirectionKind
	 * @see xaas.impl.XaasPackageImpl#getDirectionKind()
	 * @generated
	 */
	int DIRECTION_KIND = 22;


	/**
	 * Returns the meta object for class '{@link xaas.Topology <em>Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Topology</em>'.
	 * @see xaas.Topology
	 * @generated
	 */
	EClass getTopology();

	/**
	 * Returns the meta object for the attribute '{@link xaas.Topology#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see xaas.Topology#getName()
	 * @see #getTopology()
	 * @generated
	 */
	EAttribute getTopology_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.Topology#getNodeTypes <em>Node Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Node Types</em>'.
	 * @see xaas.Topology#getNodeTypes()
	 * @see #getTopology()
	 * @generated
	 */
	EReference getTopology_NodeTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.Topology#getRelationshipTypes <em>Relationship Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relationship Types</em>'.
	 * @see xaas.Topology#getRelationshipTypes()
	 * @see #getTopology()
	 * @generated
	 */
	EReference getTopology_RelationshipTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.Topology#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see xaas.Topology#getConstraints()
	 * @see #getTopology()
	 * @generated
	 */
	EReference getTopology_Constraints();

	/**
	 * Returns the meta object for class '{@link xaas.NodeType <em>Node Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Type</em>'.
	 * @see xaas.NodeType
	 * @generated
	 */
	EClass getNodeType();

	/**
	 * Returns the meta object for the container reference '{@link xaas.NodeType#getTopology <em>Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Topology</em>'.
	 * @see xaas.NodeType#getTopology()
	 * @see #getNodeType()
	 * @generated
	 */
	EReference getNodeType_Topology();

	/**
	 * Returns the meta object for the attribute '{@link xaas.NodeType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see xaas.NodeType#getName()
	 * @see #getNodeType()
	 * @generated
	 */
	EAttribute getNodeType_Name();

	/**
	 * Returns the meta object for the reference '{@link xaas.NodeType#getInheritedType <em>Inherited Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Inherited Type</em>'.
	 * @see xaas.NodeType#getInheritedType()
	 * @see #getNodeType()
	 * @generated
	 */
	EReference getNodeType_InheritedType();

	/**
	 * Returns the meta object for the attribute '{@link xaas.NodeType#getImpactOfEnabling <em>Impact Of Enabling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Impact Of Enabling</em>'.
	 * @see xaas.NodeType#getImpactOfEnabling()
	 * @see #getNodeType()
	 * @generated
	 */
	EAttribute getNodeType_ImpactOfEnabling();

	/**
	 * Returns the meta object for the attribute '{@link xaas.NodeType#getImpactOfDisabling <em>Impact Of Disabling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Impact Of Disabling</em>'.
	 * @see xaas.NodeType#getImpactOfDisabling()
	 * @see #getNodeType()
	 * @generated
	 */
	EAttribute getNodeType_ImpactOfDisabling();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.NodeType#getAttributeTypes <em>Attribute Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attribute Types</em>'.
	 * @see xaas.NodeType#getAttributeTypes()
	 * @see #getNodeType()
	 * @generated
	 */
	EReference getNodeType_AttributeTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.NodeType#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see xaas.NodeType#getConstraints()
	 * @see #getNodeType()
	 * @generated
	 */
	EReference getNodeType_Constraints();

	/**
	 * Returns the meta object for class '{@link xaas.AttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Type</em>'.
	 * @see xaas.AttributeType
	 * @generated
	 */
	EClass getAttributeType();

	/**
	 * Returns the meta object for the container reference '{@link xaas.AttributeType#getNodeType <em>Node Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Node Type</em>'.
	 * @see xaas.AttributeType#getNodeType()
	 * @see #getAttributeType()
	 * @generated
	 */
	EReference getAttributeType_NodeType();

	/**
	 * Returns the meta object for the attribute '{@link xaas.AttributeType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see xaas.AttributeType#getName()
	 * @see #getAttributeType()
	 * @generated
	 */
	EAttribute getAttributeType_Name();

	/**
	 * Returns the meta object for the attribute '{@link xaas.AttributeType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see xaas.AttributeType#getType()
	 * @see #getAttributeType()
	 * @generated
	 */
	EAttribute getAttributeType_Type();

	/**
	 * Returns the meta object for the attribute '{@link xaas.AttributeType#getImpactOfUpdating <em>Impact Of Updating</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Impact Of Updating</em>'.
	 * @see xaas.AttributeType#getImpactOfUpdating()
	 * @see #getAttributeType()
	 * @generated
	 */
	EAttribute getAttributeType_ImpactOfUpdating();

	/**
	 * Returns the meta object for class '{@link xaas.ConstantAttributeType <em>Constant Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Attribute Type</em>'.
	 * @see xaas.ConstantAttributeType
	 * @generated
	 */
	EClass getConstantAttributeType();

	/**
	 * Returns the meta object for class '{@link xaas.CalculatedAttributeType <em>Calculated Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Calculated Attribute Type</em>'.
	 * @see xaas.CalculatedAttributeType
	 * @generated
	 */
	EClass getCalculatedAttributeType();

	/**
	 * Returns the meta object for the containment reference '{@link xaas.CalculatedAttributeType#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see xaas.CalculatedAttributeType#getExpression()
	 * @see #getCalculatedAttributeType()
	 * @generated
	 */
	EReference getCalculatedAttributeType_Expression();

	/**
	 * Returns the meta object for class '{@link xaas.RelationshipType <em>Relationship Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relationship Type</em>'.
	 * @see xaas.RelationshipType
	 * @generated
	 */
	EClass getRelationshipType();

	/**
	 * Returns the meta object for the container reference '{@link xaas.RelationshipType#getTopology <em>Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Topology</em>'.
	 * @see xaas.RelationshipType#getTopology()
	 * @see #getRelationshipType()
	 * @generated
	 */
	EReference getRelationshipType_Topology();

	/**
	 * Returns the meta object for the attribute '{@link xaas.RelationshipType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see xaas.RelationshipType#getName()
	 * @see #getRelationshipType()
	 * @generated
	 */
	EAttribute getRelationshipType_Name();

	/**
	 * Returns the meta object for the reference '{@link xaas.RelationshipType#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see xaas.RelationshipType#getSource()
	 * @see #getRelationshipType()
	 * @generated
	 */
	EReference getRelationshipType_Source();

	/**
	 * Returns the meta object for the reference '{@link xaas.RelationshipType#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see xaas.RelationshipType#getTarget()
	 * @see #getRelationshipType()
	 * @generated
	 */
	EReference getRelationshipType_Target();

	/**
	 * Returns the meta object for the attribute '{@link xaas.RelationshipType#getImpactOfLinking <em>Impact Of Linking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Impact Of Linking</em>'.
	 * @see xaas.RelationshipType#getImpactOfLinking()
	 * @see #getRelationshipType()
	 * @generated
	 */
	EAttribute getRelationshipType_ImpactOfLinking();

	/**
	 * Returns the meta object for the attribute '{@link xaas.RelationshipType#getImpactOfUnlinking <em>Impact Of Unlinking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Impact Of Unlinking</em>'.
	 * @see xaas.RelationshipType#getImpactOfUnlinking()
	 * @see #getRelationshipType()
	 * @generated
	 */
	EAttribute getRelationshipType_ImpactOfUnlinking();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.RelationshipType#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see xaas.RelationshipType#getConstraints()
	 * @see #getRelationshipType()
	 * @generated
	 */
	EReference getRelationshipType_Constraints();

	/**
	 * Returns the meta object for class '{@link xaas.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint</em>'.
	 * @see xaas.Constraint
	 * @generated
	 */
	EClass getConstraint();

	/**
	 * Returns the meta object for the container reference '{@link xaas.Constraint#getTopology <em>Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Topology</em>'.
	 * @see xaas.Constraint#getTopology()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_Topology();

	/**
	 * Returns the meta object for the container reference '{@link xaas.Constraint#getNodeType <em>Node Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Node Type</em>'.
	 * @see xaas.Constraint#getNodeType()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_NodeType();

	/**
	 * Returns the meta object for the container reference '{@link xaas.Constraint#getRelationshipType <em>Relationship Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Relationship Type</em>'.
	 * @see xaas.Constraint#getRelationshipType()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_RelationshipType();

	/**
	 * Returns the meta object for the attribute '{@link xaas.Constraint#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see xaas.Constraint#getOperator()
	 * @see #getConstraint()
	 * @generated
	 */
	EAttribute getConstraint_Operator();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.Constraint#getExpressions <em>Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expressions</em>'.
	 * @see xaas.Constraint#getExpressions()
	 * @see #getConstraint()
	 * @generated
	 */
	EReference getConstraint_Expressions();

	/**
	 * Returns the meta object for class '{@link xaas.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see xaas.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for the container reference '{@link xaas.Expression#getCalculatedAttributeType <em>Calculated Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Calculated Attribute Type</em>'.
	 * @see xaas.Expression#getCalculatedAttributeType()
	 * @see #getExpression()
	 * @generated
	 */
	EReference getExpression_CalculatedAttributeType();

	/**
	 * Returns the meta object for the container reference '{@link xaas.Expression#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Constraint</em>'.
	 * @see xaas.Expression#getConstraint()
	 * @see #getExpression()
	 * @generated
	 */
	EReference getExpression_Constraint();

	/**
	 * Returns the meta object for the container reference '{@link xaas.Expression#getBinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Binary Expression</em>'.
	 * @see xaas.Expression#getBinaryExpression()
	 * @see #getExpression()
	 * @generated
	 */
	EReference getExpression_BinaryExpression();

	/**
	 * Returns the meta object for class '{@link xaas.IntegerValueExpression <em>Integer Value Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Value Expression</em>'.
	 * @see xaas.IntegerValueExpression
	 * @generated
	 */
	EClass getIntegerValueExpression();

	/**
	 * Returns the meta object for the attribute '{@link xaas.IntegerValueExpression#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see xaas.IntegerValueExpression#getValue()
	 * @see #getIntegerValueExpression()
	 * @generated
	 */
	EAttribute getIntegerValueExpression_Value();

	/**
	 * Returns the meta object for class '{@link xaas.AttributeExpression <em>Attribute Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Expression</em>'.
	 * @see xaas.AttributeExpression
	 * @generated
	 */
	EClass getAttributeExpression();

	/**
	 * Returns the meta object for the reference '{@link xaas.AttributeExpression#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Type</em>'.
	 * @see xaas.AttributeExpression#getAttributeType()
	 * @see #getAttributeExpression()
	 * @generated
	 */
	EReference getAttributeExpression_AttributeType();

	/**
	 * Returns the meta object for class '{@link xaas.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Expression</em>'.
	 * @see xaas.BinaryExpression
	 * @generated
	 */
	EClass getBinaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link xaas.BinaryExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see xaas.BinaryExpression#getOperator()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EAttribute getBinaryExpression_Operator();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.BinaryExpression#getExpressions <em>Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expressions</em>'.
	 * @see xaas.BinaryExpression#getExpressions()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_Expressions();

	/**
	 * Returns the meta object for class '{@link xaas.AggregationExpression <em>Aggregation Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aggregation Expression</em>'.
	 * @see xaas.AggregationExpression
	 * @generated
	 */
	EClass getAggregationExpression();

	/**
	 * Returns the meta object for the attribute '{@link xaas.AggregationExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see xaas.AggregationExpression#getOperator()
	 * @see #getAggregationExpression()
	 * @generated
	 */
	EAttribute getAggregationExpression_Operator();

	/**
	 * Returns the meta object for the attribute '{@link xaas.AggregationExpression#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see xaas.AggregationExpression#getDirection()
	 * @see #getAggregationExpression()
	 * @generated
	 */
	EAttribute getAggregationExpression_Direction();

	/**
	 * Returns the meta object for the reference '{@link xaas.AggregationExpression#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Type</em>'.
	 * @see xaas.AggregationExpression#getAttributeType()
	 * @see #getAggregationExpression()
	 * @generated
	 */
	EReference getAggregationExpression_AttributeType();

	/**
	 * Returns the meta object for class '{@link xaas.NbConnectionExpression <em>Nb Connection Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nb Connection Expression</em>'.
	 * @see xaas.NbConnectionExpression
	 * @generated
	 */
	EClass getNbConnectionExpression();

	/**
	 * Returns the meta object for the attribute '{@link xaas.NbConnectionExpression#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see xaas.NbConnectionExpression#getDirection()
	 * @see #getNbConnectionExpression()
	 * @generated
	 */
	EAttribute getNbConnectionExpression_Direction();

	/**
	 * Returns the meta object for class '{@link xaas.CustomExpression <em>Custom Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Expression</em>'.
	 * @see xaas.CustomExpression
	 * @generated
	 */
	EClass getCustomExpression();

	/**
	 * Returns the meta object for the attribute '{@link xaas.CustomExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see xaas.CustomExpression#getExpression()
	 * @see #getCustomExpression()
	 * @generated
	 */
	EAttribute getCustomExpression_Expression();

	/**
	 * Returns the meta object for class '{@link xaas.Configuration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration</em>'.
	 * @see xaas.Configuration
	 * @generated
	 */
	EClass getConfiguration();

	/**
	 * Returns the meta object for the reference '{@link xaas.Configuration#getTopology <em>Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Topology</em>'.
	 * @see xaas.Configuration#getTopology()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_Topology();

	/**
	 * Returns the meta object for the attribute '{@link xaas.Configuration#getIdentifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Identifier</em>'.
	 * @see xaas.Configuration#getIdentifier()
	 * @see #getConfiguration()
	 * @generated
	 */
	EAttribute getConfiguration_Identifier();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.Configuration#getNodes <em>Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Nodes</em>'.
	 * @see xaas.Configuration#getNodes()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_Nodes();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.Configuration#getRelationships <em>Relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relationships</em>'.
	 * @see xaas.Configuration#getRelationships()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_Relationships();

	/**
	 * Returns the meta object for class '{@link xaas.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see xaas.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for the container reference '{@link xaas.Node#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Configuration</em>'.
	 * @see xaas.Node#getConfiguration()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_Configuration();

	/**
	 * Returns the meta object for the reference '{@link xaas.Node#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see xaas.Node#getType()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_Type();

	/**
	 * Returns the meta object for the attribute '{@link xaas.Node#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see xaas.Node#getName()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_Name();

	/**
	 * Returns the meta object for the attribute '{@link xaas.Node#getActivated <em>Activated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activated</em>'.
	 * @see xaas.Node#getActivated()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_Activated();

	/**
	 * Returns the meta object for the containment reference list '{@link xaas.Node#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see xaas.Node#getAttributes()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_Attributes();

	/**
	 * Returns the meta object for class '{@link xaas.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute</em>'.
	 * @see xaas.Attribute
	 * @generated
	 */
	EClass getAttribute();

	/**
	 * Returns the meta object for the container reference '{@link xaas.Attribute#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Node</em>'.
	 * @see xaas.Attribute#getNode()
	 * @see #getAttribute()
	 * @generated
	 */
	EReference getAttribute_Node();

	/**
	 * Returns the meta object for the attribute '{@link xaas.Attribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see xaas.Attribute#getName()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Name();

	/**
	 * Returns the meta object for the attribute '{@link xaas.Attribute#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see xaas.Attribute#getValue()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Value();

	/**
	 * Returns the meta object for class '{@link xaas.Relationship <em>Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Relationship</em>'.
	 * @see xaas.Relationship
	 * @generated
	 */
	EClass getRelationship();

	/**
	 * Returns the meta object for the container reference '{@link xaas.Relationship#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Configuration</em>'.
	 * @see xaas.Relationship#getConfiguration()
	 * @see #getRelationship()
	 * @generated
	 */
	EReference getRelationship_Configuration();

	/**
	 * Returns the meta object for the reference '{@link xaas.Relationship#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see xaas.Relationship#getType()
	 * @see #getRelationship()
	 * @generated
	 */
	EReference getRelationship_Type();

	/**
	 * Returns the meta object for the attribute '{@link xaas.Relationship#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see xaas.Relationship#getName()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_Name();

	/**
	 * Returns the meta object for the attribute '{@link xaas.Relationship#isConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see xaas.Relationship#isConstant()
	 * @see #getRelationship()
	 * @generated
	 */
	EAttribute getRelationship_Constant();

	/**
	 * Returns the meta object for the reference '{@link xaas.Relationship#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see xaas.Relationship#getSource()
	 * @see #getRelationship()
	 * @generated
	 */
	EReference getRelationship_Source();

	/**
	 * Returns the meta object for the reference '{@link xaas.Relationship#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see xaas.Relationship#getTarget()
	 * @see #getRelationship()
	 * @generated
	 */
	EReference getRelationship_Target();

	/**
	 * Returns the meta object for class '{@link xaas.XaasYamlLikeTosca <em>Yaml Like Tosca</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Yaml Like Tosca</em>'.
	 * @see xaas.XaasYamlLikeTosca
	 * @generated
	 */
	EClass getXaasYamlLikeTosca();

	/**
	 * Returns the meta object for the containment reference '{@link xaas.XaasYamlLikeTosca#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Configuration</em>'.
	 * @see xaas.XaasYamlLikeTosca#getConfiguration()
	 * @see #getXaasYamlLikeTosca()
	 * @generated
	 */
	EReference getXaasYamlLikeTosca_Configuration();

	/**
	 * Returns the meta object for the containment reference '{@link xaas.XaasYamlLikeTosca#getTopology <em>Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Topology</em>'.
	 * @see xaas.XaasYamlLikeTosca#getTopology()
	 * @see #getXaasYamlLikeTosca()
	 * @generated
	 */
	EReference getXaasYamlLikeTosca_Topology();

	/**
	 * Returns the meta object for enum '{@link xaas.ComparisonOperator <em>Comparison Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Comparison Operator</em>'.
	 * @see xaas.ComparisonOperator
	 * @generated
	 */
	EEnum getComparisonOperator();

	/**
	 * Returns the meta object for enum '{@link xaas.AggregationOperator <em>Aggregation Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Aggregation Operator</em>'.
	 * @see xaas.AggregationOperator
	 * @generated
	 */
	EEnum getAggregationOperator();

	/**
	 * Returns the meta object for enum '{@link xaas.AlgebraicOperator <em>Algebraic Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Algebraic Operator</em>'.
	 * @see xaas.AlgebraicOperator
	 * @generated
	 */
	EEnum getAlgebraicOperator();

	/**
	 * Returns the meta object for enum '{@link xaas.DirectionKind <em>Direction Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Direction Kind</em>'.
	 * @see xaas.DirectionKind
	 * @generated
	 */
	EEnum getDirectionKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	XaasFactory getXaasFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link xaas.impl.TopologyImpl <em>Topology</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.TopologyImpl
		 * @see xaas.impl.XaasPackageImpl#getTopology()
		 * @generated
		 */
		EClass TOPOLOGY = eINSTANCE.getTopology();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOPOLOGY__NAME = eINSTANCE.getTopology_Name();

		/**
		 * The meta object literal for the '<em><b>Node Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGY__NODE_TYPES = eINSTANCE.getTopology_NodeTypes();

		/**
		 * The meta object literal for the '<em><b>Relationship Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGY__RELATIONSHIP_TYPES = eINSTANCE.getTopology_RelationshipTypes();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOPOLOGY__CONSTRAINTS = eINSTANCE.getTopology_Constraints();

		/**
		 * The meta object literal for the '{@link xaas.impl.NodeTypeImpl <em>Node Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.NodeTypeImpl
		 * @see xaas.impl.XaasPackageImpl#getNodeType()
		 * @generated
		 */
		EClass NODE_TYPE = eINSTANCE.getNodeType();

		/**
		 * The meta object literal for the '<em><b>Topology</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_TYPE__TOPOLOGY = eINSTANCE.getNodeType_Topology();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE_TYPE__NAME = eINSTANCE.getNodeType_Name();

		/**
		 * The meta object literal for the '<em><b>Inherited Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_TYPE__INHERITED_TYPE = eINSTANCE.getNodeType_InheritedType();

		/**
		 * The meta object literal for the '<em><b>Impact Of Enabling</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE_TYPE__IMPACT_OF_ENABLING = eINSTANCE.getNodeType_ImpactOfEnabling();

		/**
		 * The meta object literal for the '<em><b>Impact Of Disabling</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE_TYPE__IMPACT_OF_DISABLING = eINSTANCE.getNodeType_ImpactOfDisabling();

		/**
		 * The meta object literal for the '<em><b>Attribute Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_TYPE__ATTRIBUTE_TYPES = eINSTANCE.getNodeType_AttributeTypes();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_TYPE__CONSTRAINTS = eINSTANCE.getNodeType_Constraints();

		/**
		 * The meta object literal for the '{@link xaas.impl.AttributeTypeImpl <em>Attribute Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.AttributeTypeImpl
		 * @see xaas.impl.XaasPackageImpl#getAttributeType()
		 * @generated
		 */
		EClass ATTRIBUTE_TYPE = eINSTANCE.getAttributeType();

		/**
		 * The meta object literal for the '<em><b>Node Type</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_TYPE__NODE_TYPE = eINSTANCE.getAttributeType_NodeType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_TYPE__NAME = eINSTANCE.getAttributeType_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_TYPE__TYPE = eINSTANCE.getAttributeType_Type();

		/**
		 * The meta object literal for the '<em><b>Impact Of Updating</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_TYPE__IMPACT_OF_UPDATING = eINSTANCE.getAttributeType_ImpactOfUpdating();

		/**
		 * The meta object literal for the '{@link xaas.impl.ConstantAttributeTypeImpl <em>Constant Attribute Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.ConstantAttributeTypeImpl
		 * @see xaas.impl.XaasPackageImpl#getConstantAttributeType()
		 * @generated
		 */
		EClass CONSTANT_ATTRIBUTE_TYPE = eINSTANCE.getConstantAttributeType();

		/**
		 * The meta object literal for the '{@link xaas.impl.CalculatedAttributeTypeImpl <em>Calculated Attribute Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.CalculatedAttributeTypeImpl
		 * @see xaas.impl.XaasPackageImpl#getCalculatedAttributeType()
		 * @generated
		 */
		EClass CALCULATED_ATTRIBUTE_TYPE = eINSTANCE.getCalculatedAttributeType();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALCULATED_ATTRIBUTE_TYPE__EXPRESSION = eINSTANCE.getCalculatedAttributeType_Expression();

		/**
		 * The meta object literal for the '{@link xaas.impl.RelationshipTypeImpl <em>Relationship Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.RelationshipTypeImpl
		 * @see xaas.impl.XaasPackageImpl#getRelationshipType()
		 * @generated
		 */
		EClass RELATIONSHIP_TYPE = eINSTANCE.getRelationshipType();

		/**
		 * The meta object literal for the '<em><b>Topology</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP_TYPE__TOPOLOGY = eINSTANCE.getRelationshipType_Topology();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP_TYPE__NAME = eINSTANCE.getRelationshipType_Name();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP_TYPE__SOURCE = eINSTANCE.getRelationshipType_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP_TYPE__TARGET = eINSTANCE.getRelationshipType_Target();

		/**
		 * The meta object literal for the '<em><b>Impact Of Linking</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP_TYPE__IMPACT_OF_LINKING = eINSTANCE.getRelationshipType_ImpactOfLinking();

		/**
		 * The meta object literal for the '<em><b>Impact Of Unlinking</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP_TYPE__IMPACT_OF_UNLINKING = eINSTANCE.getRelationshipType_ImpactOfUnlinking();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP_TYPE__CONSTRAINTS = eINSTANCE.getRelationshipType_Constraints();

		/**
		 * The meta object literal for the '{@link xaas.impl.ConstraintImpl <em>Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.ConstraintImpl
		 * @see xaas.impl.XaasPackageImpl#getConstraint()
		 * @generated
		 */
		EClass CONSTRAINT = eINSTANCE.getConstraint();

		/**
		 * The meta object literal for the '<em><b>Topology</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT__TOPOLOGY = eINSTANCE.getConstraint_Topology();

		/**
		 * The meta object literal for the '<em><b>Node Type</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT__NODE_TYPE = eINSTANCE.getConstraint_NodeType();

		/**
		 * The meta object literal for the '<em><b>Relationship Type</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT__RELATIONSHIP_TYPE = eINSTANCE.getConstraint_RelationshipType();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTRAINT__OPERATOR = eINSTANCE.getConstraint_Operator();

		/**
		 * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT__EXPRESSIONS = eINSTANCE.getConstraint_Expressions();

		/**
		 * The meta object literal for the '{@link xaas.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.ExpressionImpl
		 * @see xaas.impl.XaasPackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '<em><b>Calculated Attribute Type</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION__CALCULATED_ATTRIBUTE_TYPE = eINSTANCE.getExpression_CalculatedAttributeType();

		/**
		 * The meta object literal for the '<em><b>Constraint</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION__CONSTRAINT = eINSTANCE.getExpression_Constraint();

		/**
		 * The meta object literal for the '<em><b>Binary Expression</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPRESSION__BINARY_EXPRESSION = eINSTANCE.getExpression_BinaryExpression();

		/**
		 * The meta object literal for the '{@link xaas.impl.IntegerValueExpressionImpl <em>Integer Value Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.IntegerValueExpressionImpl
		 * @see xaas.impl.XaasPackageImpl#getIntegerValueExpression()
		 * @generated
		 */
		EClass INTEGER_VALUE_EXPRESSION = eINSTANCE.getIntegerValueExpression();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_VALUE_EXPRESSION__VALUE = eINSTANCE.getIntegerValueExpression_Value();

		/**
		 * The meta object literal for the '{@link xaas.impl.AttributeExpressionImpl <em>Attribute Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.AttributeExpressionImpl
		 * @see xaas.impl.XaasPackageImpl#getAttributeExpression()
		 * @generated
		 */
		EClass ATTRIBUTE_EXPRESSION = eINSTANCE.getAttributeExpression();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_EXPRESSION__ATTRIBUTE_TYPE = eINSTANCE.getAttributeExpression_AttributeType();

		/**
		 * The meta object literal for the '{@link xaas.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.BinaryExpressionImpl
		 * @see xaas.impl.XaasPackageImpl#getBinaryExpression()
		 * @generated
		 */
		EClass BINARY_EXPRESSION = eINSTANCE.getBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_EXPRESSION__OPERATOR = eINSTANCE.getBinaryExpression_Operator();

		/**
		 * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__EXPRESSIONS = eINSTANCE.getBinaryExpression_Expressions();

		/**
		 * The meta object literal for the '{@link xaas.impl.AggregationExpressionImpl <em>Aggregation Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.AggregationExpressionImpl
		 * @see xaas.impl.XaasPackageImpl#getAggregationExpression()
		 * @generated
		 */
		EClass AGGREGATION_EXPRESSION = eINSTANCE.getAggregationExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGGREGATION_EXPRESSION__OPERATOR = eINSTANCE.getAggregationExpression_Operator();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGGREGATION_EXPRESSION__DIRECTION = eINSTANCE.getAggregationExpression_Direction();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGGREGATION_EXPRESSION__ATTRIBUTE_TYPE = eINSTANCE.getAggregationExpression_AttributeType();

		/**
		 * The meta object literal for the '{@link xaas.impl.NbConnectionExpressionImpl <em>Nb Connection Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.NbConnectionExpressionImpl
		 * @see xaas.impl.XaasPackageImpl#getNbConnectionExpression()
		 * @generated
		 */
		EClass NB_CONNECTION_EXPRESSION = eINSTANCE.getNbConnectionExpression();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NB_CONNECTION_EXPRESSION__DIRECTION = eINSTANCE.getNbConnectionExpression_Direction();

		/**
		 * The meta object literal for the '{@link xaas.impl.CustomExpressionImpl <em>Custom Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.CustomExpressionImpl
		 * @see xaas.impl.XaasPackageImpl#getCustomExpression()
		 * @generated
		 */
		EClass CUSTOM_EXPRESSION = eINSTANCE.getCustomExpression();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOM_EXPRESSION__EXPRESSION = eINSTANCE.getCustomExpression_Expression();

		/**
		 * The meta object literal for the '{@link xaas.impl.ConfigurationImpl <em>Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.ConfigurationImpl
		 * @see xaas.impl.XaasPackageImpl#getConfiguration()
		 * @generated
		 */
		EClass CONFIGURATION = eINSTANCE.getConfiguration();

		/**
		 * The meta object literal for the '<em><b>Topology</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__TOPOLOGY = eINSTANCE.getConfiguration_Topology();

		/**
		 * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONFIGURATION__IDENTIFIER = eINSTANCE.getConfiguration_Identifier();

		/**
		 * The meta object literal for the '<em><b>Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__NODES = eINSTANCE.getConfiguration_Nodes();

		/**
		 * The meta object literal for the '<em><b>Relationships</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__RELATIONSHIPS = eINSTANCE.getConfiguration_Relationships();

		/**
		 * The meta object literal for the '{@link xaas.impl.NodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.NodeImpl
		 * @see xaas.impl.XaasPackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__CONFIGURATION = eINSTANCE.getNode_Configuration();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__TYPE = eINSTANCE.getNode_Type();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__NAME = eINSTANCE.getNode_Name();

		/**
		 * The meta object literal for the '<em><b>Activated</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__ACTIVATED = eINSTANCE.getNode_Activated();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__ATTRIBUTES = eINSTANCE.getNode_Attributes();

		/**
		 * The meta object literal for the '{@link xaas.impl.AttributeImpl <em>Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.AttributeImpl
		 * @see xaas.impl.XaasPackageImpl#getAttribute()
		 * @generated
		 */
		EClass ATTRIBUTE = eINSTANCE.getAttribute();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE__NODE = eINSTANCE.getAttribute_Node();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__NAME = eINSTANCE.getAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__VALUE = eINSTANCE.getAttribute_Value();

		/**
		 * The meta object literal for the '{@link xaas.impl.RelationshipImpl <em>Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.RelationshipImpl
		 * @see xaas.impl.XaasPackageImpl#getRelationship()
		 * @generated
		 */
		EClass RELATIONSHIP = eINSTANCE.getRelationship();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP__CONFIGURATION = eINSTANCE.getRelationship_Configuration();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP__TYPE = eINSTANCE.getRelationship_Type();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__NAME = eINSTANCE.getRelationship_Name();

		/**
		 * The meta object literal for the '<em><b>Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RELATIONSHIP__CONSTANT = eINSTANCE.getRelationship_Constant();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP__SOURCE = eINSTANCE.getRelationship_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RELATIONSHIP__TARGET = eINSTANCE.getRelationship_Target();

		/**
		 * The meta object literal for the '{@link xaas.impl.XaasYamlLikeToscaImpl <em>Yaml Like Tosca</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.impl.XaasYamlLikeToscaImpl
		 * @see xaas.impl.XaasPackageImpl#getXaasYamlLikeTosca()
		 * @generated
		 */
		EClass XAAS_YAML_LIKE_TOSCA = eINSTANCE.getXaasYamlLikeTosca();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XAAS_YAML_LIKE_TOSCA__CONFIGURATION = eINSTANCE.getXaasYamlLikeTosca_Configuration();

		/**
		 * The meta object literal for the '<em><b>Topology</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XAAS_YAML_LIKE_TOSCA__TOPOLOGY = eINSTANCE.getXaasYamlLikeTosca_Topology();

		/**
		 * The meta object literal for the '{@link xaas.ComparisonOperator <em>Comparison Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.ComparisonOperator
		 * @see xaas.impl.XaasPackageImpl#getComparisonOperator()
		 * @generated
		 */
		EEnum COMPARISON_OPERATOR = eINSTANCE.getComparisonOperator();

		/**
		 * The meta object literal for the '{@link xaas.AggregationOperator <em>Aggregation Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.AggregationOperator
		 * @see xaas.impl.XaasPackageImpl#getAggregationOperator()
		 * @generated
		 */
		EEnum AGGREGATION_OPERATOR = eINSTANCE.getAggregationOperator();

		/**
		 * The meta object literal for the '{@link xaas.AlgebraicOperator <em>Algebraic Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.AlgebraicOperator
		 * @see xaas.impl.XaasPackageImpl#getAlgebraicOperator()
		 * @generated
		 */
		EEnum ALGEBRAIC_OPERATOR = eINSTANCE.getAlgebraicOperator();

		/**
		 * The meta object literal for the '{@link xaas.DirectionKind <em>Direction Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see xaas.DirectionKind
		 * @see xaas.impl.XaasPackageImpl#getDirectionKind()
		 * @generated
		 */
		EEnum DIRECTION_KIND = eINSTANCE.getDirectionKind();

	}

} //XaasPackage

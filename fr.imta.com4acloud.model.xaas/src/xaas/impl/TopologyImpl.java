/**
 */
package xaas.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import xaas.Constraint;
import xaas.NodeType;
import xaas.RelationshipType;
import xaas.Topology;
import xaas.XaasPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Topology</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link xaas.impl.TopologyImpl#getName <em>Name</em>}</li>
 *   <li>{@link xaas.impl.TopologyImpl#getNodeTypes <em>Node Types</em>}</li>
 *   <li>{@link xaas.impl.TopologyImpl#getRelationshipTypes <em>Relationship Types</em>}</li>
 *   <li>{@link xaas.impl.TopologyImpl#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TopologyImpl extends MinimalEObjectImpl.Container implements Topology {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNodeTypes() <em>Node Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<NodeType> nodeTypes;

	/**
	 * The cached value of the '{@link #getRelationshipTypes() <em>Relationship Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelationshipTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<RelationshipType> relationshipTypes;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<Constraint> constraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TopologyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XaasPackage.Literals.TOPOLOGY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.TOPOLOGY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NodeType> getNodeTypes() {
		if (nodeTypes == null) {
			nodeTypes = new EObjectContainmentWithInverseEList<NodeType>(NodeType.class, this, XaasPackage.TOPOLOGY__NODE_TYPES, XaasPackage.NODE_TYPE__TOPOLOGY);
		}
		return nodeTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RelationshipType> getRelationshipTypes() {
		if (relationshipTypes == null) {
			relationshipTypes = new EObjectContainmentWithInverseEList<RelationshipType>(RelationshipType.class, this, XaasPackage.TOPOLOGY__RELATIONSHIP_TYPES, XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY);
		}
		return relationshipTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Constraint> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentWithInverseEList<Constraint>(Constraint.class, this, XaasPackage.TOPOLOGY__CONSTRAINTS, XaasPackage.CONSTRAINT__TOPOLOGY);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.TOPOLOGY__NODE_TYPES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getNodeTypes()).basicAdd(otherEnd, msgs);
			case XaasPackage.TOPOLOGY__RELATIONSHIP_TYPES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRelationshipTypes()).basicAdd(otherEnd, msgs);
			case XaasPackage.TOPOLOGY__CONSTRAINTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConstraints()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.TOPOLOGY__NODE_TYPES:
				return ((InternalEList<?>)getNodeTypes()).basicRemove(otherEnd, msgs);
			case XaasPackage.TOPOLOGY__RELATIONSHIP_TYPES:
				return ((InternalEList<?>)getRelationshipTypes()).basicRemove(otherEnd, msgs);
			case XaasPackage.TOPOLOGY__CONSTRAINTS:
				return ((InternalEList<?>)getConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case XaasPackage.TOPOLOGY__NAME:
				return getName();
			case XaasPackage.TOPOLOGY__NODE_TYPES:
				return getNodeTypes();
			case XaasPackage.TOPOLOGY__RELATIONSHIP_TYPES:
				return getRelationshipTypes();
			case XaasPackage.TOPOLOGY__CONSTRAINTS:
				return getConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case XaasPackage.TOPOLOGY__NAME:
				setName((String)newValue);
				return;
			case XaasPackage.TOPOLOGY__NODE_TYPES:
				getNodeTypes().clear();
				getNodeTypes().addAll((Collection<? extends NodeType>)newValue);
				return;
			case XaasPackage.TOPOLOGY__RELATIONSHIP_TYPES:
				getRelationshipTypes().clear();
				getRelationshipTypes().addAll((Collection<? extends RelationshipType>)newValue);
				return;
			case XaasPackage.TOPOLOGY__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection<? extends Constraint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case XaasPackage.TOPOLOGY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case XaasPackage.TOPOLOGY__NODE_TYPES:
				getNodeTypes().clear();
				return;
			case XaasPackage.TOPOLOGY__RELATIONSHIP_TYPES:
				getRelationshipTypes().clear();
				return;
			case XaasPackage.TOPOLOGY__CONSTRAINTS:
				getConstraints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case XaasPackage.TOPOLOGY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case XaasPackage.TOPOLOGY__NODE_TYPES:
				return nodeTypes != null && !nodeTypes.isEmpty();
			case XaasPackage.TOPOLOGY__RELATIONSHIP_TYPES:
				return relationshipTypes != null && !relationshipTypes.isEmpty();
			case XaasPackage.TOPOLOGY__CONSTRAINTS:
				return constraints != null && !constraints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //TopologyImpl

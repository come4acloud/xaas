/**
 */
package xaas.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import xaas.AttributeType;
import xaas.Constraint;
import xaas.NodeType;
import xaas.Topology;
import xaas.XaasPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link xaas.impl.NodeTypeImpl#getTopology <em>Topology</em>}</li>
 *   <li>{@link xaas.impl.NodeTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link xaas.impl.NodeTypeImpl#getInheritedType <em>Inherited Type</em>}</li>
 *   <li>{@link xaas.impl.NodeTypeImpl#getImpactOfEnabling <em>Impact Of Enabling</em>}</li>
 *   <li>{@link xaas.impl.NodeTypeImpl#getImpactOfDisabling <em>Impact Of Disabling</em>}</li>
 *   <li>{@link xaas.impl.NodeTypeImpl#getAttributeTypes <em>Attribute Types</em>}</li>
 *   <li>{@link xaas.impl.NodeTypeImpl#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NodeTypeImpl extends MinimalEObjectImpl.Container implements NodeType {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInheritedType() <em>Inherited Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInheritedType()
	 * @generated
	 * @ordered
	 */
	protected NodeType inheritedType;

	/**
	 * The default value of the '{@link #getImpactOfEnabling() <em>Impact Of Enabling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfEnabling()
	 * @generated
	 * @ordered
	 */
	protected static final int IMPACT_OF_ENABLING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getImpactOfEnabling() <em>Impact Of Enabling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfEnabling()
	 * @generated
	 * @ordered
	 */
	protected int impactOfEnabling = IMPACT_OF_ENABLING_EDEFAULT;

	/**
	 * The default value of the '{@link #getImpactOfDisabling() <em>Impact Of Disabling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfDisabling()
	 * @generated
	 * @ordered
	 */
	protected static final int IMPACT_OF_DISABLING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getImpactOfDisabling() <em>Impact Of Disabling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfDisabling()
	 * @generated
	 * @ordered
	 */
	protected int impactOfDisabling = IMPACT_OF_DISABLING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAttributeTypes() <em>Attribute Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeType> attributeTypes;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<Constraint> constraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XaasPackage.Literals.NODE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Topology getTopology() {
		if (eContainerFeatureID() != XaasPackage.NODE_TYPE__TOPOLOGY) return null;
		return (Topology)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTopology(Topology newTopology, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTopology, XaasPackage.NODE_TYPE__TOPOLOGY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopology(Topology newTopology) {
		if (newTopology != eInternalContainer() || (eContainerFeatureID() != XaasPackage.NODE_TYPE__TOPOLOGY && newTopology != null)) {
			if (EcoreUtil.isAncestor(this, newTopology))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTopology != null)
				msgs = ((InternalEObject)newTopology).eInverseAdd(this, XaasPackage.TOPOLOGY__NODE_TYPES, Topology.class, msgs);
			msgs = basicSetTopology(newTopology, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.NODE_TYPE__TOPOLOGY, newTopology, newTopology));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.NODE_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType getInheritedType() {
		if (inheritedType != null && inheritedType.eIsProxy()) {
			InternalEObject oldInheritedType = (InternalEObject)inheritedType;
			inheritedType = (NodeType)eResolveProxy(oldInheritedType);
			if (inheritedType != oldInheritedType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, XaasPackage.NODE_TYPE__INHERITED_TYPE, oldInheritedType, inheritedType));
			}
		}
		return inheritedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType basicGetInheritedType() {
		return inheritedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInheritedType(NodeType newInheritedType) {
		NodeType oldInheritedType = inheritedType;
		inheritedType = newInheritedType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.NODE_TYPE__INHERITED_TYPE, oldInheritedType, inheritedType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getImpactOfEnabling() {
		return impactOfEnabling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpactOfEnabling(int newImpactOfEnabling) {
		int oldImpactOfEnabling = impactOfEnabling;
		impactOfEnabling = newImpactOfEnabling;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.NODE_TYPE__IMPACT_OF_ENABLING, oldImpactOfEnabling, impactOfEnabling));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getImpactOfDisabling() {
		return impactOfDisabling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpactOfDisabling(int newImpactOfDisabling) {
		int oldImpactOfDisabling = impactOfDisabling;
		impactOfDisabling = newImpactOfDisabling;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.NODE_TYPE__IMPACT_OF_DISABLING, oldImpactOfDisabling, impactOfDisabling));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeType> getAttributeTypes() {
		if (attributeTypes == null) {
			attributeTypes = new EObjectContainmentWithInverseEList<AttributeType>(AttributeType.class, this, XaasPackage.NODE_TYPE__ATTRIBUTE_TYPES, XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE);
		}
		return attributeTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Constraint> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentWithInverseEList<Constraint>(Constraint.class, this, XaasPackage.NODE_TYPE__CONSTRAINTS, XaasPackage.CONSTRAINT__NODE_TYPE);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.NODE_TYPE__TOPOLOGY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTopology((Topology)otherEnd, msgs);
			case XaasPackage.NODE_TYPE__ATTRIBUTE_TYPES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAttributeTypes()).basicAdd(otherEnd, msgs);
			case XaasPackage.NODE_TYPE__CONSTRAINTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConstraints()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.NODE_TYPE__TOPOLOGY:
				return basicSetTopology(null, msgs);
			case XaasPackage.NODE_TYPE__ATTRIBUTE_TYPES:
				return ((InternalEList<?>)getAttributeTypes()).basicRemove(otherEnd, msgs);
			case XaasPackage.NODE_TYPE__CONSTRAINTS:
				return ((InternalEList<?>)getConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case XaasPackage.NODE_TYPE__TOPOLOGY:
				return eInternalContainer().eInverseRemove(this, XaasPackage.TOPOLOGY__NODE_TYPES, Topology.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case XaasPackage.NODE_TYPE__TOPOLOGY:
				return getTopology();
			case XaasPackage.NODE_TYPE__NAME:
				return getName();
			case XaasPackage.NODE_TYPE__INHERITED_TYPE:
				if (resolve) return getInheritedType();
				return basicGetInheritedType();
			case XaasPackage.NODE_TYPE__IMPACT_OF_ENABLING:
				return getImpactOfEnabling();
			case XaasPackage.NODE_TYPE__IMPACT_OF_DISABLING:
				return getImpactOfDisabling();
			case XaasPackage.NODE_TYPE__ATTRIBUTE_TYPES:
				return getAttributeTypes();
			case XaasPackage.NODE_TYPE__CONSTRAINTS:
				return getConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case XaasPackage.NODE_TYPE__TOPOLOGY:
				setTopology((Topology)newValue);
				return;
			case XaasPackage.NODE_TYPE__NAME:
				setName((String)newValue);
				return;
			case XaasPackage.NODE_TYPE__INHERITED_TYPE:
				setInheritedType((NodeType)newValue);
				return;
			case XaasPackage.NODE_TYPE__IMPACT_OF_ENABLING:
				setImpactOfEnabling((Integer)newValue);
				return;
			case XaasPackage.NODE_TYPE__IMPACT_OF_DISABLING:
				setImpactOfDisabling((Integer)newValue);
				return;
			case XaasPackage.NODE_TYPE__ATTRIBUTE_TYPES:
				getAttributeTypes().clear();
				getAttributeTypes().addAll((Collection<? extends AttributeType>)newValue);
				return;
			case XaasPackage.NODE_TYPE__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection<? extends Constraint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case XaasPackage.NODE_TYPE__TOPOLOGY:
				setTopology((Topology)null);
				return;
			case XaasPackage.NODE_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case XaasPackage.NODE_TYPE__INHERITED_TYPE:
				setInheritedType((NodeType)null);
				return;
			case XaasPackage.NODE_TYPE__IMPACT_OF_ENABLING:
				setImpactOfEnabling(IMPACT_OF_ENABLING_EDEFAULT);
				return;
			case XaasPackage.NODE_TYPE__IMPACT_OF_DISABLING:
				setImpactOfDisabling(IMPACT_OF_DISABLING_EDEFAULT);
				return;
			case XaasPackage.NODE_TYPE__ATTRIBUTE_TYPES:
				getAttributeTypes().clear();
				return;
			case XaasPackage.NODE_TYPE__CONSTRAINTS:
				getConstraints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case XaasPackage.NODE_TYPE__TOPOLOGY:
				return getTopology() != null;
			case XaasPackage.NODE_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case XaasPackage.NODE_TYPE__INHERITED_TYPE:
				return inheritedType != null;
			case XaasPackage.NODE_TYPE__IMPACT_OF_ENABLING:
				return impactOfEnabling != IMPACT_OF_ENABLING_EDEFAULT;
			case XaasPackage.NODE_TYPE__IMPACT_OF_DISABLING:
				return impactOfDisabling != IMPACT_OF_DISABLING_EDEFAULT;
			case XaasPackage.NODE_TYPE__ATTRIBUTE_TYPES:
				return attributeTypes != null && !attributeTypes.isEmpty();
			case XaasPackage.NODE_TYPE__CONSTRAINTS:
				return constraints != null && !constraints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", impactOfEnabling: ");
		result.append(impactOfEnabling);
		result.append(", impactOfDisabling: ");
		result.append(impactOfDisabling);
		result.append(')');
		return result.toString();
	}

} //NodeTypeImpl

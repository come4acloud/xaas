/**
 */
package xaas.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import xaas.AttributeType;
import xaas.NodeType;
import xaas.XaasPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link xaas.impl.AttributeTypeImpl#getNodeType <em>Node Type</em>}</li>
 *   <li>{@link xaas.impl.AttributeTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link xaas.impl.AttributeTypeImpl#getType <em>Type</em>}</li>
 *   <li>{@link xaas.impl.AttributeTypeImpl#getImpactOfUpdating <em>Impact Of Updating</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AttributeTypeImpl extends MinimalEObjectImpl.Container implements AttributeType {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getImpactOfUpdating() <em>Impact Of Updating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfUpdating()
	 * @generated
	 * @ordered
	 */
	protected static final int IMPACT_OF_UPDATING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getImpactOfUpdating() <em>Impact Of Updating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfUpdating()
	 * @generated
	 * @ordered
	 */
	protected int impactOfUpdating = IMPACT_OF_UPDATING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XaasPackage.Literals.ATTRIBUTE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType getNodeType() {
		if (eContainerFeatureID() != XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE) return null;
		return (NodeType)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNodeType(NodeType newNodeType, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newNodeType, XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNodeType(NodeType newNodeType) {
		if (newNodeType != eInternalContainer() || (eContainerFeatureID() != XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE && newNodeType != null)) {
			if (EcoreUtil.isAncestor(this, newNodeType))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newNodeType != null)
				msgs = ((InternalEObject)newNodeType).eInverseAdd(this, XaasPackage.NODE_TYPE__ATTRIBUTE_TYPES, NodeType.class, msgs);
			msgs = basicSetNodeType(newNodeType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE, newNodeType, newNodeType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.ATTRIBUTE_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.ATTRIBUTE_TYPE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getImpactOfUpdating() {
		return impactOfUpdating;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpactOfUpdating(int newImpactOfUpdating) {
		int oldImpactOfUpdating = impactOfUpdating;
		impactOfUpdating = newImpactOfUpdating;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.ATTRIBUTE_TYPE__IMPACT_OF_UPDATING, oldImpactOfUpdating, impactOfUpdating));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetNodeType((NodeType)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE:
				return basicSetNodeType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE:
				return eInternalContainer().eInverseRemove(this, XaasPackage.NODE_TYPE__ATTRIBUTE_TYPES, NodeType.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE:
				return getNodeType();
			case XaasPackage.ATTRIBUTE_TYPE__NAME:
				return getName();
			case XaasPackage.ATTRIBUTE_TYPE__TYPE:
				return getType();
			case XaasPackage.ATTRIBUTE_TYPE__IMPACT_OF_UPDATING:
				return getImpactOfUpdating();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE:
				setNodeType((NodeType)newValue);
				return;
			case XaasPackage.ATTRIBUTE_TYPE__NAME:
				setName((String)newValue);
				return;
			case XaasPackage.ATTRIBUTE_TYPE__TYPE:
				setType((String)newValue);
				return;
			case XaasPackage.ATTRIBUTE_TYPE__IMPACT_OF_UPDATING:
				setImpactOfUpdating((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE:
				setNodeType((NodeType)null);
				return;
			case XaasPackage.ATTRIBUTE_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case XaasPackage.ATTRIBUTE_TYPE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case XaasPackage.ATTRIBUTE_TYPE__IMPACT_OF_UPDATING:
				setImpactOfUpdating(IMPACT_OF_UPDATING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case XaasPackage.ATTRIBUTE_TYPE__NODE_TYPE:
				return getNodeType() != null;
			case XaasPackage.ATTRIBUTE_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case XaasPackage.ATTRIBUTE_TYPE__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case XaasPackage.ATTRIBUTE_TYPE__IMPACT_OF_UPDATING:
				return impactOfUpdating != IMPACT_OF_UPDATING_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", type: ");
		result.append(type);
		result.append(", impactOfUpdating: ");
		result.append(impactOfUpdating);
		result.append(')');
		return result.toString();
	}

} //AttributeTypeImpl

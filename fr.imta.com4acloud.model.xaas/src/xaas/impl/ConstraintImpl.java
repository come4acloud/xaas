/**
 */
package xaas.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import xaas.ComparisonOperator;
import xaas.Constraint;
import xaas.Expression;
import xaas.NodeType;
import xaas.RelationshipType;
import xaas.Topology;
import xaas.XaasPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link xaas.impl.ConstraintImpl#getTopology <em>Topology</em>}</li>
 *   <li>{@link xaas.impl.ConstraintImpl#getNodeType <em>Node Type</em>}</li>
 *   <li>{@link xaas.impl.ConstraintImpl#getRelationshipType <em>Relationship Type</em>}</li>
 *   <li>{@link xaas.impl.ConstraintImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link xaas.impl.ConstraintImpl#getExpressions <em>Expressions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConstraintImpl extends MinimalEObjectImpl.Container implements Constraint {
	/**
	 * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected static final ComparisonOperator OPERATOR_EDEFAULT = ComparisonOperator.EQUAL;

	/**
	 * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected ComparisonOperator operator = OPERATOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExpressions() <em>Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> expressions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XaasPackage.Literals.CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Topology getTopology() {
		if (eContainerFeatureID() != XaasPackage.CONSTRAINT__TOPOLOGY) return null;
		return (Topology)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTopology(Topology newTopology, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTopology, XaasPackage.CONSTRAINT__TOPOLOGY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopology(Topology newTopology) {
		if (newTopology != eInternalContainer() || (eContainerFeatureID() != XaasPackage.CONSTRAINT__TOPOLOGY && newTopology != null)) {
			if (EcoreUtil.isAncestor(this, newTopology))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTopology != null)
				msgs = ((InternalEObject)newTopology).eInverseAdd(this, XaasPackage.TOPOLOGY__CONSTRAINTS, Topology.class, msgs);
			msgs = basicSetTopology(newTopology, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.CONSTRAINT__TOPOLOGY, newTopology, newTopology));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType getNodeType() {
		if (eContainerFeatureID() != XaasPackage.CONSTRAINT__NODE_TYPE) return null;
		return (NodeType)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNodeType(NodeType newNodeType, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newNodeType, XaasPackage.CONSTRAINT__NODE_TYPE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNodeType(NodeType newNodeType) {
		if (newNodeType != eInternalContainer() || (eContainerFeatureID() != XaasPackage.CONSTRAINT__NODE_TYPE && newNodeType != null)) {
			if (EcoreUtil.isAncestor(this, newNodeType))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newNodeType != null)
				msgs = ((InternalEObject)newNodeType).eInverseAdd(this, XaasPackage.NODE_TYPE__CONSTRAINTS, NodeType.class, msgs);
			msgs = basicSetNodeType(newNodeType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.CONSTRAINT__NODE_TYPE, newNodeType, newNodeType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipType getRelationshipType() {
		if (eContainerFeatureID() != XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE) return null;
		return (RelationshipType)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelationshipType(RelationshipType newRelationshipType, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newRelationshipType, XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelationshipType(RelationshipType newRelationshipType) {
		if (newRelationshipType != eInternalContainer() || (eContainerFeatureID() != XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE && newRelationshipType != null)) {
			if (EcoreUtil.isAncestor(this, newRelationshipType))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newRelationshipType != null)
				msgs = ((InternalEObject)newRelationshipType).eInverseAdd(this, XaasPackage.RELATIONSHIP_TYPE__CONSTRAINTS, RelationshipType.class, msgs);
			msgs = basicSetRelationshipType(newRelationshipType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE, newRelationshipType, newRelationshipType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonOperator getOperator() {
		return operator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperator(ComparisonOperator newOperator) {
		ComparisonOperator oldOperator = operator;
		operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.CONSTRAINT__OPERATOR, oldOperator, operator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getExpressions() {
		if (expressions == null) {
			expressions = new EObjectContainmentWithInverseEList<Expression>(Expression.class, this, XaasPackage.CONSTRAINT__EXPRESSIONS, XaasPackage.EXPRESSION__CONSTRAINT);
		}
		return expressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.CONSTRAINT__TOPOLOGY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTopology((Topology)otherEnd, msgs);
			case XaasPackage.CONSTRAINT__NODE_TYPE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetNodeType((NodeType)otherEnd, msgs);
			case XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetRelationshipType((RelationshipType)otherEnd, msgs);
			case XaasPackage.CONSTRAINT__EXPRESSIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExpressions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.CONSTRAINT__TOPOLOGY:
				return basicSetTopology(null, msgs);
			case XaasPackage.CONSTRAINT__NODE_TYPE:
				return basicSetNodeType(null, msgs);
			case XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE:
				return basicSetRelationshipType(null, msgs);
			case XaasPackage.CONSTRAINT__EXPRESSIONS:
				return ((InternalEList<?>)getExpressions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case XaasPackage.CONSTRAINT__TOPOLOGY:
				return eInternalContainer().eInverseRemove(this, XaasPackage.TOPOLOGY__CONSTRAINTS, Topology.class, msgs);
			case XaasPackage.CONSTRAINT__NODE_TYPE:
				return eInternalContainer().eInverseRemove(this, XaasPackage.NODE_TYPE__CONSTRAINTS, NodeType.class, msgs);
			case XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE:
				return eInternalContainer().eInverseRemove(this, XaasPackage.RELATIONSHIP_TYPE__CONSTRAINTS, RelationshipType.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case XaasPackage.CONSTRAINT__TOPOLOGY:
				return getTopology();
			case XaasPackage.CONSTRAINT__NODE_TYPE:
				return getNodeType();
			case XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE:
				return getRelationshipType();
			case XaasPackage.CONSTRAINT__OPERATOR:
				return getOperator();
			case XaasPackage.CONSTRAINT__EXPRESSIONS:
				return getExpressions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case XaasPackage.CONSTRAINT__TOPOLOGY:
				setTopology((Topology)newValue);
				return;
			case XaasPackage.CONSTRAINT__NODE_TYPE:
				setNodeType((NodeType)newValue);
				return;
			case XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE:
				setRelationshipType((RelationshipType)newValue);
				return;
			case XaasPackage.CONSTRAINT__OPERATOR:
				setOperator((ComparisonOperator)newValue);
				return;
			case XaasPackage.CONSTRAINT__EXPRESSIONS:
				getExpressions().clear();
				getExpressions().addAll((Collection<? extends Expression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case XaasPackage.CONSTRAINT__TOPOLOGY:
				setTopology((Topology)null);
				return;
			case XaasPackage.CONSTRAINT__NODE_TYPE:
				setNodeType((NodeType)null);
				return;
			case XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE:
				setRelationshipType((RelationshipType)null);
				return;
			case XaasPackage.CONSTRAINT__OPERATOR:
				setOperator(OPERATOR_EDEFAULT);
				return;
			case XaasPackage.CONSTRAINT__EXPRESSIONS:
				getExpressions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case XaasPackage.CONSTRAINT__TOPOLOGY:
				return getTopology() != null;
			case XaasPackage.CONSTRAINT__NODE_TYPE:
				return getNodeType() != null;
			case XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE:
				return getRelationshipType() != null;
			case XaasPackage.CONSTRAINT__OPERATOR:
				return operator != OPERATOR_EDEFAULT;
			case XaasPackage.CONSTRAINT__EXPRESSIONS:
				return expressions != null && !expressions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operator: ");
		result.append(operator);
		result.append(')');
		return result.toString();
	}

} //ConstraintImpl

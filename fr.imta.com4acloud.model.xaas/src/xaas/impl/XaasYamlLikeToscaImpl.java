/**
 */
package xaas.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import xaas.Configuration;
import xaas.Topology;
import xaas.XaasPackage;
import xaas.XaasYamlLikeTosca;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Yaml Like Tosca</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link xaas.impl.XaasYamlLikeToscaImpl#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link xaas.impl.XaasYamlLikeToscaImpl#getTopology <em>Topology</em>}</li>
 * </ul>
 *
 * @generated
 */
public class XaasYamlLikeToscaImpl extends MinimalEObjectImpl.Container implements XaasYamlLikeTosca {
	/**
	 * The cached value of the '{@link #getConfiguration() <em>Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfiguration()
	 * @generated
	 * @ordered
	 */
	protected Configuration configuration;

	/**
	 * The cached value of the '{@link #getTopology() <em>Topology</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopology()
	 * @generated
	 * @ordered
	 */
	protected Topology topology;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XaasYamlLikeToscaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XaasPackage.Literals.XAAS_YAML_LIKE_TOSCA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getConfiguration() {
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConfiguration(Configuration newConfiguration, NotificationChain msgs) {
		Configuration oldConfiguration = configuration;
		configuration = newConfiguration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XaasPackage.XAAS_YAML_LIKE_TOSCA__CONFIGURATION, oldConfiguration, newConfiguration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfiguration(Configuration newConfiguration) {
		if (newConfiguration != configuration) {
			NotificationChain msgs = null;
			if (configuration != null)
				msgs = ((InternalEObject)configuration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XaasPackage.XAAS_YAML_LIKE_TOSCA__CONFIGURATION, null, msgs);
			if (newConfiguration != null)
				msgs = ((InternalEObject)newConfiguration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XaasPackage.XAAS_YAML_LIKE_TOSCA__CONFIGURATION, null, msgs);
			msgs = basicSetConfiguration(newConfiguration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.XAAS_YAML_LIKE_TOSCA__CONFIGURATION, newConfiguration, newConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Topology getTopology() {
		return topology;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTopology(Topology newTopology, NotificationChain msgs) {
		Topology oldTopology = topology;
		topology = newTopology;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XaasPackage.XAAS_YAML_LIKE_TOSCA__TOPOLOGY, oldTopology, newTopology);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopology(Topology newTopology) {
		if (newTopology != topology) {
			NotificationChain msgs = null;
			if (topology != null)
				msgs = ((InternalEObject)topology).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XaasPackage.XAAS_YAML_LIKE_TOSCA__TOPOLOGY, null, msgs);
			if (newTopology != null)
				msgs = ((InternalEObject)newTopology).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XaasPackage.XAAS_YAML_LIKE_TOSCA__TOPOLOGY, null, msgs);
			msgs = basicSetTopology(newTopology, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.XAAS_YAML_LIKE_TOSCA__TOPOLOGY, newTopology, newTopology));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__CONFIGURATION:
				return basicSetConfiguration(null, msgs);
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__TOPOLOGY:
				return basicSetTopology(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__CONFIGURATION:
				return getConfiguration();
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__TOPOLOGY:
				return getTopology();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__CONFIGURATION:
				setConfiguration((Configuration)newValue);
				return;
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__TOPOLOGY:
				setTopology((Topology)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__CONFIGURATION:
				setConfiguration((Configuration)null);
				return;
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__TOPOLOGY:
				setTopology((Topology)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__CONFIGURATION:
				return configuration != null;
			case XaasPackage.XAAS_YAML_LIKE_TOSCA__TOPOLOGY:
				return topology != null;
		}
		return super.eIsSet(featureID);
	}

} //XaasYamlLikeToscaImpl

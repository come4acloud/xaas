/**
 */
package xaas.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import xaas.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class XaasFactoryImpl extends EFactoryImpl implements XaasFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static XaasFactory init() {
		try {
			XaasFactory theXaasFactory = (XaasFactory)EPackage.Registry.INSTANCE.getEFactory(XaasPackage.eNS_URI);
			if (theXaasFactory != null) {
				return theXaasFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new XaasFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XaasFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case XaasPackage.TOPOLOGY: return createTopology();
			case XaasPackage.NODE_TYPE: return createNodeType();
			case XaasPackage.CONSTANT_ATTRIBUTE_TYPE: return createConstantAttributeType();
			case XaasPackage.CALCULATED_ATTRIBUTE_TYPE: return createCalculatedAttributeType();
			case XaasPackage.RELATIONSHIP_TYPE: return createRelationshipType();
			case XaasPackage.CONSTRAINT: return createConstraint();
			case XaasPackage.INTEGER_VALUE_EXPRESSION: return createIntegerValueExpression();
			case XaasPackage.ATTRIBUTE_EXPRESSION: return createAttributeExpression();
			case XaasPackage.BINARY_EXPRESSION: return createBinaryExpression();
			case XaasPackage.AGGREGATION_EXPRESSION: return createAggregationExpression();
			case XaasPackage.NB_CONNECTION_EXPRESSION: return createNbConnectionExpression();
			case XaasPackage.CUSTOM_EXPRESSION: return createCustomExpression();
			case XaasPackage.CONFIGURATION: return createConfiguration();
			case XaasPackage.NODE: return createNode();
			case XaasPackage.ATTRIBUTE: return createAttribute();
			case XaasPackage.RELATIONSHIP: return createRelationship();
			case XaasPackage.XAAS_YAML_LIKE_TOSCA: return createXaasYamlLikeTosca();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case XaasPackage.COMPARISON_OPERATOR:
				return createComparisonOperatorFromString(eDataType, initialValue);
			case XaasPackage.AGGREGATION_OPERATOR:
				return createAggregationOperatorFromString(eDataType, initialValue);
			case XaasPackage.ALGEBRAIC_OPERATOR:
				return createAlgebraicOperatorFromString(eDataType, initialValue);
			case XaasPackage.DIRECTION_KIND:
				return createDirectionKindFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case XaasPackage.COMPARISON_OPERATOR:
				return convertComparisonOperatorToString(eDataType, instanceValue);
			case XaasPackage.AGGREGATION_OPERATOR:
				return convertAggregationOperatorToString(eDataType, instanceValue);
			case XaasPackage.ALGEBRAIC_OPERATOR:
				return convertAlgebraicOperatorToString(eDataType, instanceValue);
			case XaasPackage.DIRECTION_KIND:
				return convertDirectionKindToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Topology createTopology() {
		TopologyImpl topology = new TopologyImpl();
		return topology;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType createNodeType() {
		NodeTypeImpl nodeType = new NodeTypeImpl();
		return nodeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantAttributeType createConstantAttributeType() {
		ConstantAttributeTypeImpl constantAttributeType = new ConstantAttributeTypeImpl();
		return constantAttributeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CalculatedAttributeType createCalculatedAttributeType() {
		CalculatedAttributeTypeImpl calculatedAttributeType = new CalculatedAttributeTypeImpl();
		return calculatedAttributeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelationshipType createRelationshipType() {
		RelationshipTypeImpl relationshipType = new RelationshipTypeImpl();
		return relationshipType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint createConstraint() {
		ConstraintImpl constraint = new ConstraintImpl();
		return constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerValueExpression createIntegerValueExpression() {
		IntegerValueExpressionImpl integerValueExpression = new IntegerValueExpressionImpl();
		return integerValueExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeExpression createAttributeExpression() {
		AttributeExpressionImpl attributeExpression = new AttributeExpressionImpl();
		return attributeExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryExpression createBinaryExpression() {
		BinaryExpressionImpl binaryExpression = new BinaryExpressionImpl();
		return binaryExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AggregationExpression createAggregationExpression() {
		AggregationExpressionImpl aggregationExpression = new AggregationExpressionImpl();
		return aggregationExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NbConnectionExpression createNbConnectionExpression() {
		NbConnectionExpressionImpl nbConnectionExpression = new NbConnectionExpressionImpl();
		return nbConnectionExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomExpression createCustomExpression() {
		CustomExpressionImpl customExpression = new CustomExpressionImpl();
		return customExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration createConfiguration() {
		ConfigurationImpl configuration = new ConfigurationImpl();
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node createNode() {
		NodeImpl node = new NodeImpl();
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute createAttribute() {
		AttributeImpl attribute = new AttributeImpl();
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Relationship createRelationship() {
		RelationshipImpl relationship = new RelationshipImpl();
		return relationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XaasYamlLikeTosca createXaasYamlLikeTosca() {
		XaasYamlLikeToscaImpl xaasYamlLikeTosca = new XaasYamlLikeToscaImpl();
		return xaasYamlLikeTosca;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonOperator createComparisonOperatorFromString(EDataType eDataType, String initialValue) {
		ComparisonOperator result = ComparisonOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertComparisonOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AggregationOperator createAggregationOperatorFromString(EDataType eDataType, String initialValue) {
		AggregationOperator result = AggregationOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAggregationOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlgebraicOperator createAlgebraicOperatorFromString(EDataType eDataType, String initialValue) {
		AlgebraicOperator result = AlgebraicOperator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAlgebraicOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirectionKind createDirectionKindFromString(EDataType eDataType, String initialValue) {
		DirectionKind result = DirectionKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDirectionKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XaasPackage getXaasPackage() {
		return (XaasPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static XaasPackage getPackage() {
		return XaasPackage.eINSTANCE;
	}

} //XaasFactoryImpl

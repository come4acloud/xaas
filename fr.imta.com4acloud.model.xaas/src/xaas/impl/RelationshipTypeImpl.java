/**
 */
package xaas.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import xaas.Constraint;
import xaas.NodeType;
import xaas.RelationshipType;
import xaas.Topology;
import xaas.XaasPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Relationship Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link xaas.impl.RelationshipTypeImpl#getTopology <em>Topology</em>}</li>
 *   <li>{@link xaas.impl.RelationshipTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link xaas.impl.RelationshipTypeImpl#getSource <em>Source</em>}</li>
 *   <li>{@link xaas.impl.RelationshipTypeImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link xaas.impl.RelationshipTypeImpl#getImpactOfLinking <em>Impact Of Linking</em>}</li>
 *   <li>{@link xaas.impl.RelationshipTypeImpl#getImpactOfUnlinking <em>Impact Of Unlinking</em>}</li>
 *   <li>{@link xaas.impl.RelationshipTypeImpl#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RelationshipTypeImpl extends MinimalEObjectImpl.Container implements RelationshipType {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected NodeType source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected NodeType target;

	/**
	 * The default value of the '{@link #getImpactOfLinking() <em>Impact Of Linking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfLinking()
	 * @generated
	 * @ordered
	 */
	protected static final int IMPACT_OF_LINKING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getImpactOfLinking() <em>Impact Of Linking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfLinking()
	 * @generated
	 * @ordered
	 */
	protected int impactOfLinking = IMPACT_OF_LINKING_EDEFAULT;

	/**
	 * The default value of the '{@link #getImpactOfUnlinking() <em>Impact Of Unlinking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfUnlinking()
	 * @generated
	 * @ordered
	 */
	protected static final int IMPACT_OF_UNLINKING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getImpactOfUnlinking() <em>Impact Of Unlinking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpactOfUnlinking()
	 * @generated
	 * @ordered
	 */
	protected int impactOfUnlinking = IMPACT_OF_UNLINKING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<Constraint> constraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RelationshipTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XaasPackage.Literals.RELATIONSHIP_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Topology getTopology() {
		if (eContainerFeatureID() != XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY) return null;
		return (Topology)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTopology(Topology newTopology, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTopology, XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopology(Topology newTopology) {
		if (newTopology != eInternalContainer() || (eContainerFeatureID() != XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY && newTopology != null)) {
			if (EcoreUtil.isAncestor(this, newTopology))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTopology != null)
				msgs = ((InternalEObject)newTopology).eInverseAdd(this, XaasPackage.TOPOLOGY__RELATIONSHIP_TYPES, Topology.class, msgs);
			msgs = basicSetTopology(newTopology, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY, newTopology, newTopology));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.RELATIONSHIP_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = (NodeType)eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, XaasPackage.RELATIONSHIP_TYPE__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(NodeType newSource) {
		NodeType oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.RELATIONSHIP_TYPE__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (NodeType)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, XaasPackage.RELATIONSHIP_TYPE__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(NodeType newTarget) {
		NodeType oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.RELATIONSHIP_TYPE__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getImpactOfLinking() {
		return impactOfLinking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpactOfLinking(int newImpactOfLinking) {
		int oldImpactOfLinking = impactOfLinking;
		impactOfLinking = newImpactOfLinking;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_LINKING, oldImpactOfLinking, impactOfLinking));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getImpactOfUnlinking() {
		return impactOfUnlinking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpactOfUnlinking(int newImpactOfUnlinking) {
		int oldImpactOfUnlinking = impactOfUnlinking;
		impactOfUnlinking = newImpactOfUnlinking;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_UNLINKING, oldImpactOfUnlinking, impactOfUnlinking));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Constraint> getConstraints() {
		if (constraints == null) {
			constraints = new EObjectContainmentWithInverseEList<Constraint>(Constraint.class, this, XaasPackage.RELATIONSHIP_TYPE__CONSTRAINTS, XaasPackage.CONSTRAINT__RELATIONSHIP_TYPE);
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTopology((Topology)otherEnd, msgs);
			case XaasPackage.RELATIONSHIP_TYPE__CONSTRAINTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConstraints()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY:
				return basicSetTopology(null, msgs);
			case XaasPackage.RELATIONSHIP_TYPE__CONSTRAINTS:
				return ((InternalEList<?>)getConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY:
				return eInternalContainer().eInverseRemove(this, XaasPackage.TOPOLOGY__RELATIONSHIP_TYPES, Topology.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY:
				return getTopology();
			case XaasPackage.RELATIONSHIP_TYPE__NAME:
				return getName();
			case XaasPackage.RELATIONSHIP_TYPE__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case XaasPackage.RELATIONSHIP_TYPE__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_LINKING:
				return getImpactOfLinking();
			case XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_UNLINKING:
				return getImpactOfUnlinking();
			case XaasPackage.RELATIONSHIP_TYPE__CONSTRAINTS:
				return getConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY:
				setTopology((Topology)newValue);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__NAME:
				setName((String)newValue);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__SOURCE:
				setSource((NodeType)newValue);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__TARGET:
				setTarget((NodeType)newValue);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_LINKING:
				setImpactOfLinking((Integer)newValue);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_UNLINKING:
				setImpactOfUnlinking((Integer)newValue);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__CONSTRAINTS:
				getConstraints().clear();
				getConstraints().addAll((Collection<? extends Constraint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY:
				setTopology((Topology)null);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__SOURCE:
				setSource((NodeType)null);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__TARGET:
				setTarget((NodeType)null);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_LINKING:
				setImpactOfLinking(IMPACT_OF_LINKING_EDEFAULT);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_UNLINKING:
				setImpactOfUnlinking(IMPACT_OF_UNLINKING_EDEFAULT);
				return;
			case XaasPackage.RELATIONSHIP_TYPE__CONSTRAINTS:
				getConstraints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case XaasPackage.RELATIONSHIP_TYPE__TOPOLOGY:
				return getTopology() != null;
			case XaasPackage.RELATIONSHIP_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case XaasPackage.RELATIONSHIP_TYPE__SOURCE:
				return source != null;
			case XaasPackage.RELATIONSHIP_TYPE__TARGET:
				return target != null;
			case XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_LINKING:
				return impactOfLinking != IMPACT_OF_LINKING_EDEFAULT;
			case XaasPackage.RELATIONSHIP_TYPE__IMPACT_OF_UNLINKING:
				return impactOfUnlinking != IMPACT_OF_UNLINKING_EDEFAULT;
			case XaasPackage.RELATIONSHIP_TYPE__CONSTRAINTS:
				return constraints != null && !constraints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", impactOfLinking: ");
		result.append(impactOfLinking);
		result.append(", impactOfUnlinking: ");
		result.append(impactOfUnlinking);
		result.append(')');
		return result.toString();
	}

} //RelationshipTypeImpl

/**
 */
package xaas.impl;

import org.eclipse.emf.ecore.EClass;

import xaas.ConstantAttributeType;
import xaas.XaasPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constant Attribute Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ConstantAttributeTypeImpl extends AttributeTypeImpl implements ConstantAttributeType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstantAttributeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XaasPackage.Literals.CONSTANT_ATTRIBUTE_TYPE;
	}

} //ConstantAttributeTypeImpl

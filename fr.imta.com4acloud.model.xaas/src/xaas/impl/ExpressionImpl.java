/**
 */
package xaas.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import xaas.BinaryExpression;
import xaas.CalculatedAttributeType;
import xaas.Constraint;
import xaas.Expression;
import xaas.XaasPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link xaas.impl.ExpressionImpl#getCalculatedAttributeType <em>Calculated Attribute Type</em>}</li>
 *   <li>{@link xaas.impl.ExpressionImpl#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link xaas.impl.ExpressionImpl#getBinaryExpression <em>Binary Expression</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ExpressionImpl extends MinimalEObjectImpl.Container implements Expression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XaasPackage.Literals.EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CalculatedAttributeType getCalculatedAttributeType() {
		if (eContainerFeatureID() != XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE) return null;
		return (CalculatedAttributeType)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCalculatedAttributeType(CalculatedAttributeType newCalculatedAttributeType, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCalculatedAttributeType, XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCalculatedAttributeType(CalculatedAttributeType newCalculatedAttributeType) {
		if (newCalculatedAttributeType != eInternalContainer() || (eContainerFeatureID() != XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE && newCalculatedAttributeType != null)) {
			if (EcoreUtil.isAncestor(this, newCalculatedAttributeType))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCalculatedAttributeType != null)
				msgs = ((InternalEObject)newCalculatedAttributeType).eInverseAdd(this, XaasPackage.CALCULATED_ATTRIBUTE_TYPE__EXPRESSION, CalculatedAttributeType.class, msgs);
			msgs = basicSetCalculatedAttributeType(newCalculatedAttributeType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE, newCalculatedAttributeType, newCalculatedAttributeType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint getConstraint() {
		if (eContainerFeatureID() != XaasPackage.EXPRESSION__CONSTRAINT) return null;
		return (Constraint)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstraint(Constraint newConstraint, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newConstraint, XaasPackage.EXPRESSION__CONSTRAINT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraint(Constraint newConstraint) {
		if (newConstraint != eInternalContainer() || (eContainerFeatureID() != XaasPackage.EXPRESSION__CONSTRAINT && newConstraint != null)) {
			if (EcoreUtil.isAncestor(this, newConstraint))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newConstraint != null)
				msgs = ((InternalEObject)newConstraint).eInverseAdd(this, XaasPackage.CONSTRAINT__EXPRESSIONS, Constraint.class, msgs);
			msgs = basicSetConstraint(newConstraint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.EXPRESSION__CONSTRAINT, newConstraint, newConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryExpression getBinaryExpression() {
		if (eContainerFeatureID() != XaasPackage.EXPRESSION__BINARY_EXPRESSION) return null;
		return (BinaryExpression)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBinaryExpression(BinaryExpression newBinaryExpression, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newBinaryExpression, XaasPackage.EXPRESSION__BINARY_EXPRESSION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBinaryExpression(BinaryExpression newBinaryExpression) {
		if (newBinaryExpression != eInternalContainer() || (eContainerFeatureID() != XaasPackage.EXPRESSION__BINARY_EXPRESSION && newBinaryExpression != null)) {
			if (EcoreUtil.isAncestor(this, newBinaryExpression))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newBinaryExpression != null)
				msgs = ((InternalEObject)newBinaryExpression).eInverseAdd(this, XaasPackage.BINARY_EXPRESSION__EXPRESSIONS, BinaryExpression.class, msgs);
			msgs = basicSetBinaryExpression(newBinaryExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, XaasPackage.EXPRESSION__BINARY_EXPRESSION, newBinaryExpression, newBinaryExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCalculatedAttributeType((CalculatedAttributeType)otherEnd, msgs);
			case XaasPackage.EXPRESSION__CONSTRAINT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetConstraint((Constraint)otherEnd, msgs);
			case XaasPackage.EXPRESSION__BINARY_EXPRESSION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetBinaryExpression((BinaryExpression)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE:
				return basicSetCalculatedAttributeType(null, msgs);
			case XaasPackage.EXPRESSION__CONSTRAINT:
				return basicSetConstraint(null, msgs);
			case XaasPackage.EXPRESSION__BINARY_EXPRESSION:
				return basicSetBinaryExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE:
				return eInternalContainer().eInverseRemove(this, XaasPackage.CALCULATED_ATTRIBUTE_TYPE__EXPRESSION, CalculatedAttributeType.class, msgs);
			case XaasPackage.EXPRESSION__CONSTRAINT:
				return eInternalContainer().eInverseRemove(this, XaasPackage.CONSTRAINT__EXPRESSIONS, Constraint.class, msgs);
			case XaasPackage.EXPRESSION__BINARY_EXPRESSION:
				return eInternalContainer().eInverseRemove(this, XaasPackage.BINARY_EXPRESSION__EXPRESSIONS, BinaryExpression.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE:
				return getCalculatedAttributeType();
			case XaasPackage.EXPRESSION__CONSTRAINT:
				return getConstraint();
			case XaasPackage.EXPRESSION__BINARY_EXPRESSION:
				return getBinaryExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE:
				setCalculatedAttributeType((CalculatedAttributeType)newValue);
				return;
			case XaasPackage.EXPRESSION__CONSTRAINT:
				setConstraint((Constraint)newValue);
				return;
			case XaasPackage.EXPRESSION__BINARY_EXPRESSION:
				setBinaryExpression((BinaryExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE:
				setCalculatedAttributeType((CalculatedAttributeType)null);
				return;
			case XaasPackage.EXPRESSION__CONSTRAINT:
				setConstraint((Constraint)null);
				return;
			case XaasPackage.EXPRESSION__BINARY_EXPRESSION:
				setBinaryExpression((BinaryExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case XaasPackage.EXPRESSION__CALCULATED_ATTRIBUTE_TYPE:
				return getCalculatedAttributeType() != null;
			case XaasPackage.EXPRESSION__CONSTRAINT:
				return getConstraint() != null;
			case XaasPackage.EXPRESSION__BINARY_EXPRESSION:
				return getBinaryExpression() != null;
		}
		return super.eIsSet(featureID);
	}

} //ExpressionImpl

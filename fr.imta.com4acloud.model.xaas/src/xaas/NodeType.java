/**
 */
package xaas;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.NodeType#getTopology <em>Topology</em>}</li>
 *   <li>{@link xaas.NodeType#getName <em>Name</em>}</li>
 *   <li>{@link xaas.NodeType#getInheritedType <em>Inherited Type</em>}</li>
 *   <li>{@link xaas.NodeType#getImpactOfEnabling <em>Impact Of Enabling</em>}</li>
 *   <li>{@link xaas.NodeType#getImpactOfDisabling <em>Impact Of Disabling</em>}</li>
 *   <li>{@link xaas.NodeType#getAttributeTypes <em>Attribute Types</em>}</li>
 *   <li>{@link xaas.NodeType#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getNodeType()
 * @model
 * @generated
 */
public interface NodeType extends EObject {
	/**
	 * Returns the value of the '<em><b>Topology</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link xaas.Topology#getNodeTypes <em>Node Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Topology</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Topology</em>' container reference.
	 * @see #setTopology(Topology)
	 * @see xaas.XaasPackage#getNodeType_Topology()
	 * @see xaas.Topology#getNodeTypes
	 * @model opposite="nodeTypes" required="true" transient="false"
	 * @generated
	 */
	Topology getTopology();

	/**
	 * Sets the value of the '{@link xaas.NodeType#getTopology <em>Topology</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Topology</em>' container reference.
	 * @see #getTopology()
	 * @generated
	 */
	void setTopology(Topology value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see xaas.XaasPackage#getNodeType_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link xaas.NodeType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Inherited Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inherited Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inherited Type</em>' reference.
	 * @see #setInheritedType(NodeType)
	 * @see xaas.XaasPackage#getNodeType_InheritedType()
	 * @model
	 * @generated
	 */
	NodeType getInheritedType();

	/**
	 * Sets the value of the '{@link xaas.NodeType#getInheritedType <em>Inherited Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inherited Type</em>' reference.
	 * @see #getInheritedType()
	 * @generated
	 */
	void setInheritedType(NodeType value);

	/**
	 * Returns the value of the '<em><b>Impact Of Enabling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Impact Of Enabling</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Impact Of Enabling</em>' attribute.
	 * @see #setImpactOfEnabling(int)
	 * @see xaas.XaasPackage#getNodeType_ImpactOfEnabling()
	 * @model
	 * @generated
	 */
	int getImpactOfEnabling();

	/**
	 * Sets the value of the '{@link xaas.NodeType#getImpactOfEnabling <em>Impact Of Enabling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Impact Of Enabling</em>' attribute.
	 * @see #getImpactOfEnabling()
	 * @generated
	 */
	void setImpactOfEnabling(int value);

	/**
	 * Returns the value of the '<em><b>Impact Of Disabling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Impact Of Disabling</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Impact Of Disabling</em>' attribute.
	 * @see #setImpactOfDisabling(int)
	 * @see xaas.XaasPackage#getNodeType_ImpactOfDisabling()
	 * @model
	 * @generated
	 */
	int getImpactOfDisabling();

	/**
	 * Sets the value of the '{@link xaas.NodeType#getImpactOfDisabling <em>Impact Of Disabling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Impact Of Disabling</em>' attribute.
	 * @see #getImpactOfDisabling()
	 * @generated
	 */
	void setImpactOfDisabling(int value);

	/**
	 * Returns the value of the '<em><b>Attribute Types</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.AttributeType}.
	 * It is bidirectional and its opposite is '{@link xaas.AttributeType#getNodeType <em>Node Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Types</em>' containment reference list.
	 * @see xaas.XaasPackage#getNodeType_AttributeTypes()
	 * @see xaas.AttributeType#getNodeType
	 * @model opposite="nodeType" containment="true"
	 * @generated
	 */
	EList<AttributeType> getAttributeTypes();

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.Constraint}.
	 * It is bidirectional and its opposite is '{@link xaas.Constraint#getNodeType <em>Node Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see xaas.XaasPackage#getNodeType_Constraints()
	 * @see xaas.Constraint#getNodeType
	 * @model opposite="nodeType" containment="true"
	 * @generated
	 */
	EList<Constraint> getConstraints();

} // NodeType

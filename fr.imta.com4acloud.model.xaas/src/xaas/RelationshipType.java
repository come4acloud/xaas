/**
 */
package xaas;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relationship Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.RelationshipType#getTopology <em>Topology</em>}</li>
 *   <li>{@link xaas.RelationshipType#getName <em>Name</em>}</li>
 *   <li>{@link xaas.RelationshipType#getSource <em>Source</em>}</li>
 *   <li>{@link xaas.RelationshipType#getTarget <em>Target</em>}</li>
 *   <li>{@link xaas.RelationshipType#getImpactOfLinking <em>Impact Of Linking</em>}</li>
 *   <li>{@link xaas.RelationshipType#getImpactOfUnlinking <em>Impact Of Unlinking</em>}</li>
 *   <li>{@link xaas.RelationshipType#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getRelationshipType()
 * @model
 * @generated
 */
public interface RelationshipType extends EObject {
	/**
	 * Returns the value of the '<em><b>Topology</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link xaas.Topology#getRelationshipTypes <em>Relationship Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Topology</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Topology</em>' container reference.
	 * @see #setTopology(Topology)
	 * @see xaas.XaasPackage#getRelationshipType_Topology()
	 * @see xaas.Topology#getRelationshipTypes
	 * @model opposite="relationshipTypes" required="true" transient="false"
	 * @generated
	 */
	Topology getTopology();

	/**
	 * Sets the value of the '{@link xaas.RelationshipType#getTopology <em>Topology</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Topology</em>' container reference.
	 * @see #getTopology()
	 * @generated
	 */
	void setTopology(Topology value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see xaas.XaasPackage#getRelationshipType_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link xaas.RelationshipType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(NodeType)
	 * @see xaas.XaasPackage#getRelationshipType_Source()
	 * @model required="true"
	 * @generated
	 */
	NodeType getSource();

	/**
	 * Sets the value of the '{@link xaas.RelationshipType#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(NodeType value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(NodeType)
	 * @see xaas.XaasPackage#getRelationshipType_Target()
	 * @model required="true"
	 * @generated
	 */
	NodeType getTarget();

	/**
	 * Sets the value of the '{@link xaas.RelationshipType#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(NodeType value);

	/**
	 * Returns the value of the '<em><b>Impact Of Linking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Impact Of Linking</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Impact Of Linking</em>' attribute.
	 * @see #setImpactOfLinking(int)
	 * @see xaas.XaasPackage#getRelationshipType_ImpactOfLinking()
	 * @model
	 * @generated
	 */
	int getImpactOfLinking();

	/**
	 * Sets the value of the '{@link xaas.RelationshipType#getImpactOfLinking <em>Impact Of Linking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Impact Of Linking</em>' attribute.
	 * @see #getImpactOfLinking()
	 * @generated
	 */
	void setImpactOfLinking(int value);

	/**
	 * Returns the value of the '<em><b>Impact Of Unlinking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Impact Of Unlinking</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Impact Of Unlinking</em>' attribute.
	 * @see #setImpactOfUnlinking(int)
	 * @see xaas.XaasPackage#getRelationshipType_ImpactOfUnlinking()
	 * @model
	 * @generated
	 */
	int getImpactOfUnlinking();

	/**
	 * Sets the value of the '{@link xaas.RelationshipType#getImpactOfUnlinking <em>Impact Of Unlinking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Impact Of Unlinking</em>' attribute.
	 * @see #getImpactOfUnlinking()
	 * @generated
	 */
	void setImpactOfUnlinking(int value);

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.Constraint}.
	 * It is bidirectional and its opposite is '{@link xaas.Constraint#getRelationshipType <em>Relationship Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see xaas.XaasPackage#getRelationshipType_Constraints()
	 * @see xaas.Constraint#getRelationshipType
	 * @model opposite="relationshipType" containment="true"
	 * @generated
	 */
	EList<Constraint> getConstraints();

} // RelationshipType

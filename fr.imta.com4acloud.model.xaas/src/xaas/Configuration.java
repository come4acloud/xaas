/**
 */
package xaas;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.Configuration#getTopology <em>Topology</em>}</li>
 *   <li>{@link xaas.Configuration#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link xaas.Configuration#getNodes <em>Nodes</em>}</li>
 *   <li>{@link xaas.Configuration#getRelationships <em>Relationships</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getConfiguration()
 * @model
 * @generated
 */
public interface Configuration extends EObject {
	/**
	 * Returns the value of the '<em><b>Topology</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Topology</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Topology</em>' reference.
	 * @see #setTopology(Topology)
	 * @see xaas.XaasPackage#getConfiguration_Topology()
	 * @model required="true"
	 * @generated
	 */
	Topology getTopology();

	/**
	 * Sets the value of the '{@link xaas.Configuration#getTopology <em>Topology</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Topology</em>' reference.
	 * @see #getTopology()
	 * @generated
	 */
	void setTopology(Topology value);

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' attribute.
	 * @see #setIdentifier(String)
	 * @see xaas.XaasPackage#getConfiguration_Identifier()
	 * @model required="true"
	 * @generated
	 */
	String getIdentifier();

	/**
	 * Sets the value of the '{@link xaas.Configuration#getIdentifier <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifier</em>' attribute.
	 * @see #getIdentifier()
	 * @generated
	 */
	void setIdentifier(String value);

	/**
	 * Returns the value of the '<em><b>Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.Node}.
	 * It is bidirectional and its opposite is '{@link xaas.Node#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodes</em>' containment reference list.
	 * @see xaas.XaasPackage#getConfiguration_Nodes()
	 * @see xaas.Node#getConfiguration
	 * @model opposite="configuration" containment="true"
	 * @generated
	 */
	EList<Node> getNodes();

	/**
	 * Returns the value of the '<em><b>Relationships</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.Relationship}.
	 * It is bidirectional and its opposite is '{@link xaas.Relationship#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationships</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationships</em>' containment reference list.
	 * @see xaas.XaasPackage#getConfiguration_Relationships()
	 * @see xaas.Relationship#getConfiguration
	 * @model opposite="configuration" containment="true"
	 * @generated
	 */
	EList<Relationship> getRelationships();

} // Configuration

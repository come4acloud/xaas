/**
 */
package xaas;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aggregation Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.AggregationExpression#getOperator <em>Operator</em>}</li>
 *   <li>{@link xaas.AggregationExpression#getDirection <em>Direction</em>}</li>
 *   <li>{@link xaas.AggregationExpression#getAttributeType <em>Attribute Type</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getAggregationExpression()
 * @model
 * @generated
 */
public interface AggregationExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link xaas.AggregationOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see xaas.AggregationOperator
	 * @see #setOperator(AggregationOperator)
	 * @see xaas.XaasPackage#getAggregationExpression_Operator()
	 * @model required="true"
	 * @generated
	 */
	AggregationOperator getOperator();

	/**
	 * Sets the value of the '{@link xaas.AggregationExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see xaas.AggregationOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(AggregationOperator value);

	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link xaas.DirectionKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see xaas.DirectionKind
	 * @see #setDirection(DirectionKind)
	 * @see xaas.XaasPackage#getAggregationExpression_Direction()
	 * @model required="true"
	 * @generated
	 */
	DirectionKind getDirection();

	/**
	 * Sets the value of the '{@link xaas.AggregationExpression#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see xaas.DirectionKind
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(DirectionKind value);

	/**
	 * Returns the value of the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Type</em>' reference.
	 * @see #setAttributeType(AttributeType)
	 * @see xaas.XaasPackage#getAggregationExpression_AttributeType()
	 * @model required="true"
	 * @generated
	 */
	AttributeType getAttributeType();

	/**
	 * Sets the value of the '{@link xaas.AggregationExpression#getAttributeType <em>Attribute Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Type</em>' reference.
	 * @see #getAttributeType()
	 * @generated
	 */
	void setAttributeType(AttributeType value);

} // AggregationExpression

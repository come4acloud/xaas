/**
 */
package xaas;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Yaml Like Tosca</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.XaasYamlLikeTosca#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link xaas.XaasYamlLikeTosca#getTopology <em>Topology</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getXaasYamlLikeTosca()
 * @model
 * @generated
 */
public interface XaasYamlLikeTosca extends EObject {
	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' containment reference.
	 * @see #setConfiguration(Configuration)
	 * @see xaas.XaasPackage#getXaasYamlLikeTosca_Configuration()
	 * @model containment="true"
	 * @generated
	 */
	Configuration getConfiguration();

	/**
	 * Sets the value of the '{@link xaas.XaasYamlLikeTosca#getConfiguration <em>Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' containment reference.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(Configuration value);

	/**
	 * Returns the value of the '<em><b>Topology</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Topology</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Topology</em>' containment reference.
	 * @see #setTopology(Topology)
	 * @see xaas.XaasPackage#getXaasYamlLikeTosca_Topology()
	 * @model containment="true"
	 * @generated
	 */
	Topology getTopology();

	/**
	 * Sets the value of the '{@link xaas.XaasYamlLikeTosca#getTopology <em>Topology</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Topology</em>' containment reference.
	 * @see #getTopology()
	 * @generated
	 */
	void setTopology(Topology value);

} // XaasYamlLikeTosca

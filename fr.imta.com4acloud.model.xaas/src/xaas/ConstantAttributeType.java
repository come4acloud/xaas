/**
 */
package xaas;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant Attribute Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see xaas.XaasPackage#getConstantAttributeType()
 * @model
 * @generated
 */
public interface ConstantAttributeType extends AttributeType {
} // ConstantAttributeType

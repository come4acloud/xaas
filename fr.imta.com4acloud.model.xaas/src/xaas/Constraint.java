/**
 */
package xaas;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.Constraint#getTopology <em>Topology</em>}</li>
 *   <li>{@link xaas.Constraint#getNodeType <em>Node Type</em>}</li>
 *   <li>{@link xaas.Constraint#getRelationshipType <em>Relationship Type</em>}</li>
 *   <li>{@link xaas.Constraint#getOperator <em>Operator</em>}</li>
 *   <li>{@link xaas.Constraint#getExpressions <em>Expressions</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getConstraint()
 * @model
 * @generated
 */
public interface Constraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Topology</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link xaas.Topology#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Topology</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Topology</em>' container reference.
	 * @see #setTopology(Topology)
	 * @see xaas.XaasPackage#getConstraint_Topology()
	 * @see xaas.Topology#getConstraints
	 * @model opposite="constraints" transient="false"
	 * @generated
	 */
	Topology getTopology();

	/**
	 * Sets the value of the '{@link xaas.Constraint#getTopology <em>Topology</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Topology</em>' container reference.
	 * @see #getTopology()
	 * @generated
	 */
	void setTopology(Topology value);

	/**
	 * Returns the value of the '<em><b>Node Type</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link xaas.NodeType#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Type</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Type</em>' container reference.
	 * @see #setNodeType(NodeType)
	 * @see xaas.XaasPackage#getConstraint_NodeType()
	 * @see xaas.NodeType#getConstraints
	 * @model opposite="constraints" transient="false"
	 * @generated
	 */
	NodeType getNodeType();

	/**
	 * Sets the value of the '{@link xaas.Constraint#getNodeType <em>Node Type</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Type</em>' container reference.
	 * @see #getNodeType()
	 * @generated
	 */
	void setNodeType(NodeType value);

	/**
	 * Returns the value of the '<em><b>Relationship Type</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link xaas.RelationshipType#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationship Type</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationship Type</em>' container reference.
	 * @see #setRelationshipType(RelationshipType)
	 * @see xaas.XaasPackage#getConstraint_RelationshipType()
	 * @see xaas.RelationshipType#getConstraints
	 * @model opposite="constraints" transient="false"
	 * @generated
	 */
	RelationshipType getRelationshipType();

	/**
	 * Sets the value of the '{@link xaas.Constraint#getRelationshipType <em>Relationship Type</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relationship Type</em>' container reference.
	 * @see #getRelationshipType()
	 * @generated
	 */
	void setRelationshipType(RelationshipType value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link xaas.ComparisonOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see xaas.ComparisonOperator
	 * @see #setOperator(ComparisonOperator)
	 * @see xaas.XaasPackage#getConstraint_Operator()
	 * @model required="true"
	 * @generated
	 */
	ComparisonOperator getOperator();

	/**
	 * Sets the value of the '{@link xaas.Constraint#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see xaas.ComparisonOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(ComparisonOperator value);

	/**
	 * Returns the value of the '<em><b>Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.Expression}.
	 * It is bidirectional and its opposite is '{@link xaas.Expression#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expressions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expressions</em>' containment reference list.
	 * @see xaas.XaasPackage#getConstraint_Expressions()
	 * @see xaas.Expression#getConstraint
	 * @model opposite="constraint" containment="true" lower="2" upper="2"
	 * @generated
	 */
	EList<Expression> getExpressions();

} // Constraint

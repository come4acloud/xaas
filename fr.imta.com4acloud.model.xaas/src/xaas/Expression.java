/**
 */
package xaas;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.Expression#getCalculatedAttributeType <em>Calculated Attribute Type</em>}</li>
 *   <li>{@link xaas.Expression#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link xaas.Expression#getBinaryExpression <em>Binary Expression</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getExpression()
 * @model abstract="true"
 * @generated
 */
public interface Expression extends EObject {
	/**
	 * Returns the value of the '<em><b>Calculated Attribute Type</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link xaas.CalculatedAttributeType#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Attribute Type</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Attribute Type</em>' container reference.
	 * @see #setCalculatedAttributeType(CalculatedAttributeType)
	 * @see xaas.XaasPackage#getExpression_CalculatedAttributeType()
	 * @see xaas.CalculatedAttributeType#getExpression
	 * @model opposite="expression" transient="false"
	 * @generated
	 */
	CalculatedAttributeType getCalculatedAttributeType();

	/**
	 * Sets the value of the '{@link xaas.Expression#getCalculatedAttributeType <em>Calculated Attribute Type</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Calculated Attribute Type</em>' container reference.
	 * @see #getCalculatedAttributeType()
	 * @generated
	 */
	void setCalculatedAttributeType(CalculatedAttributeType value);

	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link xaas.Constraint#getExpressions <em>Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' container reference.
	 * @see #setConstraint(Constraint)
	 * @see xaas.XaasPackage#getExpression_Constraint()
	 * @see xaas.Constraint#getExpressions
	 * @model opposite="expressions" transient="false"
	 * @generated
	 */
	Constraint getConstraint();

	/**
	 * Sets the value of the '{@link xaas.Expression#getConstraint <em>Constraint</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraint</em>' container reference.
	 * @see #getConstraint()
	 * @generated
	 */
	void setConstraint(Constraint value);

	/**
	 * Returns the value of the '<em><b>Binary Expression</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link xaas.BinaryExpression#getExpressions <em>Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binary Expression</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binary Expression</em>' container reference.
	 * @see #setBinaryExpression(BinaryExpression)
	 * @see xaas.XaasPackage#getExpression_BinaryExpression()
	 * @see xaas.BinaryExpression#getExpressions
	 * @model opposite="expressions" transient="false"
	 * @generated
	 */
	BinaryExpression getBinaryExpression();

	/**
	 * Sets the value of the '{@link xaas.Expression#getBinaryExpression <em>Binary Expression</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binary Expression</em>' container reference.
	 * @see #getBinaryExpression()
	 * @generated
	 */
	void setBinaryExpression(BinaryExpression value);

} // Expression

/**
 */
package xaas;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Calculated Attribute Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.CalculatedAttributeType#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getCalculatedAttributeType()
 * @model
 * @generated
 */
public interface CalculatedAttributeType extends AttributeType {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link xaas.Expression#getCalculatedAttributeType <em>Calculated Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(Expression)
	 * @see xaas.XaasPackage#getCalculatedAttributeType_Expression()
	 * @see xaas.Expression#getCalculatedAttributeType
	 * @model opposite="calculatedAttributeType" containment="true" required="true"
	 * @generated
	 */
	Expression getExpression();

	/**
	 * Sets the value of the '{@link xaas.CalculatedAttributeType#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(Expression value);

} // CalculatedAttributeType

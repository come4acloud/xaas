/**
 */
package xaas;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Topology</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link xaas.Topology#getName <em>Name</em>}</li>
 *   <li>{@link xaas.Topology#getNodeTypes <em>Node Types</em>}</li>
 *   <li>{@link xaas.Topology#getRelationshipTypes <em>Relationship Types</em>}</li>
 *   <li>{@link xaas.Topology#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see xaas.XaasPackage#getTopology()
 * @model
 * @generated
 */
public interface Topology extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see xaas.XaasPackage#getTopology_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link xaas.Topology#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Node Types</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.NodeType}.
	 * It is bidirectional and its opposite is '{@link xaas.NodeType#getTopology <em>Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Types</em>' containment reference list.
	 * @see xaas.XaasPackage#getTopology_NodeTypes()
	 * @see xaas.NodeType#getTopology
	 * @model opposite="topology" containment="true"
	 * @generated
	 */
	EList<NodeType> getNodeTypes();

	/**
	 * Returns the value of the '<em><b>Relationship Types</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.RelationshipType}.
	 * It is bidirectional and its opposite is '{@link xaas.RelationshipType#getTopology <em>Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationship Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationship Types</em>' containment reference list.
	 * @see xaas.XaasPackage#getTopology_RelationshipTypes()
	 * @see xaas.RelationshipType#getTopology
	 * @model opposite="topology" containment="true"
	 * @generated
	 */
	EList<RelationshipType> getRelationshipTypes();

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link xaas.Constraint}.
	 * It is bidirectional and its opposite is '{@link xaas.Constraint#getTopology <em>Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see xaas.XaasPackage#getTopology_Constraints()
	 * @see xaas.Constraint#getTopology
	 * @model opposite="topology" containment="true"
	 * @generated
	 */
	EList<Constraint> getConstraints();

} // Topology

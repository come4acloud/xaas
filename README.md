
# CoMe4ACloud

CoMe4ACloud is an Atlanstic2020 funded project whose objective is to provide an end-to-end solution for autonomic Cloud services. To that end, we rely on techniques of Constraint Programming so as a decision-making tool and Model-driven Engineering to ease the automatic generation of the so-called autonomic managers as well as their communication with the managed system. For further information, please read [the project description](https://come4acloud.github.io/).


## Prerequisite:

* [Install Eclipse](https://www.eclipse.org/downloads/?)
* [Install Xtext](https://eclipse.org/Xtext/)
* [Install ATL/EMFTVM](https://wiki.eclipse.org/ATL/EMFTVM#Installation)
* [Install OpenTOSCA](http://www.opentosca.org/) (Optional)

## Setup in Eclipse
1. Download or clone [XaaS Projects](https://gitlab.inria.fr/come4acloud/xaas/repository/archive.zip?ref=master).
2. Import <span style="color: green"> XaaS Projects </span> into Eclipse.


## Run CoMe4ACloud
1. Right click on <span style="color: green">fr.imta.come4acloud.dsl.xaas</span> project -> Run As -> Eclipse Application.
A new Eclipse IDE instance will be started.
<br /><br />
2. In the new Eclipse IDE, create Java project.
<br /><br />
3. Create a new file with <span style="color: green">.xaas</span> extention in <span style="color: green">src</span> folder on the created Java project.
<br /><br />
4. Write the implement of your cloud computing topology and configuration. An example of [Moodle](https://moodle.org/) application will be found in [Moodle](https://gitlab.inria.fr/come4acloud/xaas/tree/master/Moodle) project.
<br /><br />
5. Right click on your <span style="color: green">.xaas</span> file -> Come4ACloud -> Generate Code to generate the source code that are representing your cloud computing topology and configuration.
The generated source code should be founded in <span style="color: green">src-gen</span> folder in your Java project.
Moreover, the required dependencies was injected in your project class path.
<br /><br />
6. Right click in your Java project -> Run As -> Run Configurations… . Create a new java configuration. Add <span style="color: green">controler.Controler</span> as main class. 

7. In your Java configuration, choose <span style="color: green">Argument</span> tab, add the following parameters into <span style="color: green">Program argument</span>:
	* The name of current configuration.
	* The name of next configuration generated from constraint solver.
	* The maximum time analyse required to stop searching for a solution by constraint solver.
    * A boolean value (<span style="color: green">true</span> | <span style="color: green">false</span>) if you need to search for a soluation from current configuration.

## Demo
For more information, two demo videos can be found [here](http://hyperurl.co/come4acloud) and [here](http://hyperurl.co/come4acloud_runtime).

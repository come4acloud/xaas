package fr.imta.come4acloud.model.transformation;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.m2m.atl.emftvm.EmftvmFactory;
import org.eclipse.m2m.atl.emftvm.ExecEnv;
import org.eclipse.m2m.atl.emftvm.Metamodel;
import org.eclipse.m2m.atl.emftvm.Model;
import org.eclipse.m2m.atl.emftvm.impl.resource.EMFTVMResourceFactoryImpl;
import org.eclipse.m2m.atl.emftvm.util.DefaultModuleResolver;
import org.eclipse.m2m.atl.emftvm.util.ModuleResolver;
import org.eclipse.m2m.atl.emftvm.util.TimingData;

/**
 * An off-the-shelf launcher for ATL/EMFTVM transformations
 * @author Victor Guana - guana@ualberta.ca
 * University of Alberta - SSRG Lab.
 * Edmonton, Alberta. Canada
 * Using code examples from: https://wiki.eclipse.org/ATL/EMFTVM
 */
public class ATLLauncherEMFTVM {
	
		// Some constants for quick initialization and testing.
		public final static String IN_METAMODEL = "resources/TOSCA.ecore";
		public final static String IN_METAMODEL_NAME = "TOSCA";
		public final static String OUT_METAMODEL = "resources/XaaS.ecore";
		public final static String OUT_METAMODEL_NAME = "XaaS";
		
		public final static String OUT_MODEL = "resources/Conf1.xmi";
		public final static String IN_MODEL = "resources/iaas-updated.xmi";
		
		public final static String TRANSFORMATION_DIR = "resources/";
		public final static String TRANSFORMATION_MODULE= "XaaS2TOSCA";
		
		public final static String XAAS2TOSCA_TRANSFORMATION_MODULE= "XaaS2TOSCA";
		public final static String TOSCA2XAAS_TRANSFORMATION_MODULE= "TOSCA2XaaS";
		
		//Main transformation launch method
		public void launch(String inMetamodelPath, String inModelPath, String outMetamodelPath,
				String outModelPath, String transformationDir, String transformationModule){
			
			/* 
			 * Creates the execution environment where the transformation is going to be executed,
			 * you could use an execution pool if you want to run multiple transformations in parallel,
			 * but for the purpose of the example let's keep it simple.
			 */
			ExecEnv env = EmftvmFactory.eINSTANCE.createExecEnv();
			ResourceSet rs = new ResourceSetImpl();

			rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
			rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
			rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("emftvm", new EMFTVMResourceFactoryImpl());
			
			try {
				
				// Load metamodels
				Metamodel inMetamodel = EmftvmFactory.eINSTANCE.createMetamodel();
				Resource inputMetamodel = rs.createResource(URI.createFileURI(inMetamodelPath));
				inputMetamodel.load(null);
				EPackage inputMetamodelPackage = (EPackage) inputMetamodel.getContents().get(0);
				System.out.println(inputMetamodelPackage);
				rs.getPackageRegistry().put(
					inputMetamodelPackage.getNsURI(), 
					inputMetamodelPackage);
				inMetamodel.setResource(inputMetamodel);
				env.registerMetaModel(IN_METAMODEL_NAME, inMetamodel);
				
				Metamodel outMetamodel = EmftvmFactory.eINSTANCE.createMetamodel();
				Resource outputMetamodel = rs.createResource(URI.createFileURI(outMetamodelPath));
				outputMetamodel.load(null);
				EPackage outputMetamodelPackage = (EPackage) outputMetamodel.getContents().get(0);
				System.out.println(outputMetamodelPackage);
				rs.getPackageRegistry().put(
					outputMetamodelPackage.getNsURI(), 
					outputMetamodelPackage);
				outMetamodel.setResource(outputMetamodel);
				env.registerMetaModel(OUT_METAMODEL_NAME, outMetamodel);
			
				// Load models
				Model inModel = EmftvmFactory.eINSTANCE.createModel();
				Resource myTOSCAmodel = rs.createResource(URI.createFileURI(inModelPath));
				Map<String, Object> loadOptions = new HashMap<String, Object>();
				loadOptions.put(XMLResource.OPTION_EXTENDED_META_DATA, Boolean.TRUE);
				myTOSCAmodel.load(loadOptions);
				inModel.setResource(myTOSCAmodel);
				env.registerInputModel("IN", inModel);
				
				Model outModel = EmftvmFactory.eINSTANCE.createModel();
				outModel.setResource(rs.createResource(URI.createURI(outModelPath)));
				env.registerOutputModel("OUT", outModel);
				
				/*
				 *  Load and run the transformation module
				 *  Point at the directory your transformations are stored, the ModuleResolver will 
				 *  look for the .emftvm file corresponding to the module you want to load and run
				 */
				ModuleResolver mr = new DefaultModuleResolver(transformationDir, rs);
				TimingData td = new TimingData();
				env.loadModule(mr, transformationModule);
				td.finishLoading();
				env.run(td);
				td.finish();
			
				outModel.getResource().save(Collections.emptyMap());
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
		
		/*
		 *  A test main method, I'm using constants so I can quickly change the case study by simply
		 *  modifying the header of the class.
		 */	
		public static void Transform(String inModel, String outModel, String module){
			ATLLauncherEMFTVM l = new ATLLauncherEMFTVM();
			l.launch(IN_METAMODEL, IN_MODEL, OUT_METAMODEL, OUT_MODEL, TRANSFORMATION_DIR, TRANSFORMATION_MODULE);
		}
}


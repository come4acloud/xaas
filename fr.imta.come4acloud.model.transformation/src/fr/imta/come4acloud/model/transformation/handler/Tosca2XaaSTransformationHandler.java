package fr.imta.come4acloud.model.transformation.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;

public class Tosca2XaaSTransformationHandler  extends AbstractHandler implements IHandler{

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		//Opens a open-file dialog to prompt the user for a TOSCA file name
		FileDialog dialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);
		dialog.setOverwrite(true);
		dialog.setFilterExtensions(new String[] {"*.xml", "*.csar"});
		String toscaFile = dialog.open();
		
		return null;
	}

}

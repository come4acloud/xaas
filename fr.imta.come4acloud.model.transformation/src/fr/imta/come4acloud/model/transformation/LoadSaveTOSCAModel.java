package fr.imta.come4acloud.model.transformation;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.eclipse.camf.tosca.util.ToscaResourceFactoryImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;

public class LoadSaveTOSCAModel {

	public static void main(String[] args) {
		try {
		
		 /*
		  * Set input/output files paths (relative) 
		  */
		 //
		 // "Error" in input XML file from Zak:
		 //			"NodeTypeProperties" not existing in current TOSCA metamodel in Ecore,
		 //			"PropertiesDefinition" is used instead...
		 //
		 URI inputXMLfileURI = URI.createFileURI("resources/iaas-updated.xml");
		 URI outputXMIfileURI = URI.createFileURI("resources/iaas-updated.xmi");
		 
		 /*
		  * Configure ResourceSet
		  */
		 ResourceSet resourceSet = new ResourceSetImpl();
		 resourceSet.getResourceFactoryRegistry().
			getExtensionToFactoryMap().
			put("ecore", new EcoreResourceFactoryImpl());
		 resourceSet.getResourceFactoryRegistry().
			getExtensionToFactoryMap().
			put("xmi", new XMIResourceFactoryImpl());	 
		 resourceSet.getResourceFactoryRegistry().
		 	getExtensionToFactoryMap().
		 	put("xml", new XMLResourceFactoryImpl());
		 	//put("xml", new TOSCAResourceFactoryImpl());
		
		 /*
		  * Load TOSCA metamodel
		  */
		 //EPackage TOSCAPackage = EPackage.Registry.INSTANCE.getEPackage("http://docs.oasis-open.org/tosca/ns/2011/12");
		 Resource TOSCAMetamodel = resourceSet.createResource(URI.createFileURI("resources/TOSCA.ecore"));
		 TOSCAMetamodel.load(null);
		 EPackage TOSCAPackage = (EPackage) TOSCAMetamodel.getContents().get(0);
		 resourceSet.getPackageRegistry().put(
				 TOSCAPackage.getNsURI(), 
				 TOSCAPackage);
		 
		 /*
		  * Load TOSCA model (from XML file) as a XML Resource
		  */
		 Resource myTOSCAmodel = resourceSet.createResource(inputXMLfileURI);
		 Map<String, Object> loadOptions = new HashMap<String, Object>();
		 loadOptions.put(XMLResource.OPTION_EXTENDED_META_DATA, Boolean.TRUE);
		 //loadOptions.put(XMLResource.OPTION_USE_ENCODED_ATTRIBUTE_STYLE, Boolean.TRUE);
		 //loadOptions.put(XMLResource.OPTION_USE_LEXICAL_HANDLER, Boolean.TRUE);
		 myTOSCAmodel.load(loadOptions);
	     
		 myTOSCAmodel.setURI(URI.createFileURI("resources/iaas-updated-copy.xml"));
	     myTOSCAmodel.save(null);
	     
		 /*
		  * Copy it into a new TOSCA model as a XMI Resource
		  */
	     Resource myTOSCAmodelXMI = resourceSet.createResource(outputXMIfileURI);
	     EObject myTOSCAmodelRootElement = myTOSCAmodel.getContents().get(0);
	     //printModelContent(myTOSCAmodelRootElement);
	     myTOSCAmodelXMI.getContents().add(myTOSCAmodelRootElement);
	     
	     /*
	      * Save the TOSCA model (into an XMI file)
	      */
	     Map<String, Object> saveOptions = new HashMap<String, Object>();
	     saveOptions.put(XMLResource.OPTION_EXTENDED_META_DATA, Boolean.TRUE);
	     //saveOptions.put(XMLResource.OPTION_USE_ENCODED_ATTRIBUTE_STYLE, Boolean.TRUE);
	     //saveOptions.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
	     myTOSCAmodelXMI.save(saveOptions);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void printModelContent(EObject rootModelElement) {
		EObject subModelElement = rootModelElement.eContents().get(0);
		System.out.println("ROOT ELEMENT - " + rootModelElement.eClass().getName());
		System.out.println("	" +
				subModelElement.eClass().getName() + " - " + 
				subModelElement.eGet(subModelElement.eClass().getEStructuralFeature("name")));
		@SuppressWarnings("unchecked")
		List<EObject> nodeTypes = (List<EObject>) subModelElement.eGet(subModelElement.eClass().getEStructuralFeature("nodeType"));
		for(EObject nodeType : nodeTypes) {
			System.out.println("		" + 
					nodeType.eClass().getName() + " - " + 
					nodeType.eGet(nodeType.eClass().getEStructuralFeature("name"))
					);
		}
	}

}

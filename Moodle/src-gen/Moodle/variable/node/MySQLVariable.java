package Moodle.variable.node;

import abstractsystem.variable.node.*;
import abstractsystem.state.attribute.*;
import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import enumerations.*;
import Moodle.state.node.*;
import Moodle.state.attribute.*;
import org.chocosolver.solver.*;


public class MySQLVariable extends InternalComponentVariable {
	
	public MySQLVariable(MySQL n, Model m, int nb_nodes) {
		super(n, m, nb_nodes);
	}
	
	//prepare link constraints (link constraint type & link constraint node)
	@Override
	protected void prepareLinkConstraints(){
		super.prepareLinkConstraints();

		getLinkConstraintsDescriptor().addClasse(TypeLinkConstraint.mandatory, EQcomparator.equal, MoodleApplication.class, Sens.from);
		getLinkConstraintsDescriptor().addClasse(TypeLinkConstraint.mandatory, EQcomparator.equal, Worker.class, Sens.to);
		
	
		
	}
}

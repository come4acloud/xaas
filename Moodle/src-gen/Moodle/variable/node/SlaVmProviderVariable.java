package Moodle.variable.node;

import abstractsystem.variable.node.*;
import abstractsystem.state.attribute.*;
import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import enumerations.*;
import Moodle.state.node.*;
import Moodle.state.attribute.*;
import org.chocosolver.solver.*;


public class SlaVmProviderVariable extends SLAProviderVariable {
	
	public SlaVmProviderVariable(SlaVmProvider n, Model m, int nb_nodes) {
		super(n, m, nb_nodes);
	}
	
	//prepare link constraints (link constraint type & link constraint node)
	@Override
	protected void prepareLinkConstraints(){
		super.prepareLinkConstraints();

		getLinkConstraintsDescriptor().addClasse(TypeLinkConstraint.mandatory, EQcomparator.equal, VmApache.class, Sens.from);
		getLinkConstraintsDescriptor().addClasse(TypeLinkConstraint.mandatory, EQcomparator.equal, RootProvider.class, Sens.to);
		
	
		
	}
}

package Moodle.variable.node;

import abstractsystem.variable.node.*;
import abstractsystem.state.attribute.*;
import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import enumerations.*;
import Moodle.state.node.*;
import Moodle.state.attribute.*;
import org.chocosolver.solver.*;


public class ClientWebAppSlaVariable extends SLAClientVariable {
	
	public ClientWebAppSlaVariable(ClientWebAppSla n, Model m, int nb_nodes) {
		super(n, m, nb_nodes);
	}
	
	//prepare link constraints (link constraint type & link constraint node)
	@Override
	protected void prepareLinkConstraints(){
		super.prepareLinkConstraints();

		getLinkConstraintsDescriptor().addClasse(TypeLinkConstraint.mandatory, EQcomparator.equal, RootClient.class, Sens.from);
		getLinkConstraintsDescriptor().addClasse(TypeLinkConstraint.mandatory, EQcomparator.equal, MoodleWebAService.class, Sens.to);
		
		getLinkConstraintsDescriptor().addIDNode(TypeLinkConstraint.mandatory, EQcomparator.equal, ((AttributeIDNode) node.getAttributeByTypeName(Attribute.getNameType(ID_MoodleWebAService.class))).getValue(), Sens.to); 
	
		
	}
}

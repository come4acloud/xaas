package Moodle.variable.attribute;

import org.chocosolver.solver.*;
import abstractsystem.variable.attribute.*;
import java.util.*;
import org.chocosolver.solver.constraints.*;
import abstractsystem.state.node.*;
import enumerations.*;
import abstractsystem.constraints.*;
import Moodle.state.attribute.*;

public  class VmCostVariable  extends AttributeIntegerVariable{
	public VmCostVariable(VmCost a, int min, int max, Model m) {
		super(a, min, max, m);
	}
	
	@Override
	public List<Constraint> buildConstraints(Model m, List<Node> nodes){
		List<Constraint> constraints = new ArrayList<Constraint>();
		//last id is 5
		AttributeConstraint exp6 = new BinnaryExpression(TypeOperatorBinnaryExpression.prod, new AggregationExpression(this.getAttribute().getHost(), TypeAggregationOperator.sum, "Moodle.state.attribute.VmCpuCapacity", TypeSens.pred), new BinnaryExpression(TypeOperatorBinnaryExpression.sum, new NameAttributeExpression(this.getAttribute().getHost(), "Moodle.state.attribute.VmCpuUnitPrice"), new BinnaryExpression(TypeOperatorBinnaryExpression.prod, new AggregationExpression(this.getAttribute().getHost(), TypeAggregationOperator.sum, "Moodle.state.attribute.VmRamCapacity", TypeSens.pred), new NameAttributeExpression(this.getAttribute().getHost(), "Moodle.state.attribute.VmRamUnitPrice"))));
		constraints	.addAll(exp6.build(m,nodes));
		constraints.add(m.arithm(getValueVar(), "=", exp6.getValueVar()));
		return constraints;
	}
}

package Moodle.variable.attribute;

import org.chocosolver.solver.*;
import abstractsystem.variable.attribute.*;
import java.util.*;
import org.chocosolver.solver.constraints.*;
import abstractsystem.state.node.*;
import enumerations.*;
import abstractsystem.constraints.*;
import Moodle.state.attribute.*;

public  class AppResponseTimeVariable  extends AttributeIntegerVariable{
	public AppResponseTimeVariable(AppResponseTime a, int min, int max, Model m) {
		super(a, min, max, m);
	}
	
	@Override
	public List<Constraint> buildConstraints(Model m, List<Node> nodes){
		List<Constraint> constraints = new ArrayList<Constraint>();
		//last id is 0
		AttributeConstraint exp1 = new AggregationExpression(this.getAttribute().getHost(), TypeAggregationOperator.sum, "Moodle.state.attribute.TierResponseTime", TypeSens.pred);
		constraints	.addAll(exp1.build(m,nodes));
		constraints.add(m.arithm(getValueVar(), "=", exp1.getValueVar()));
		return constraints;
	}
}

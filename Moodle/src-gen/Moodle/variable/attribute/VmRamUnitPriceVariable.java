package Moodle.variable.attribute;

import org.chocosolver.solver.*;
import abstractsystem.variable.attribute.*;
import java.util.*;
import org.chocosolver.solver.constraints.*;
import abstractsystem.state.node.*;
import enumerations.*;
import abstractsystem.constraints.*;
import Moodle.state.attribute.*;

public  class VmRamUnitPriceVariable  extends AttributeIntegerFixedVariable{
	public VmRamUnitPriceVariable(VmRamUnitPrice a, int min, int max, Model m) {
		super(a, m);
	}
	
	@Override
	public List<Constraint> buildConstraints(Model m, List<Node> nodes){
		List<Constraint> constraints = new ArrayList<Constraint>();
		return constraints;
	}
}

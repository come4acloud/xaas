package Moodle.variable.attribute;

import org.chocosolver.solver.*;
import abstractsystem.variable.attribute.*;
import java.util.*;
import org.chocosolver.solver.constraints.*;
import abstractsystem.state.node.*;
import enumerations.*;
import abstractsystem.constraints.*;
import Moodle.state.attribute.*;

public  class ClientCostVariable  extends AttributeIntegerFixedVariable{
	public ClientCostVariable(ClientCost a, int min, int max, Model m) {
		super(a, m);
	}
	
	@Override
	public List<Constraint> buildConstraints(Model m, List<Node> nodes){
		List<Constraint> constraints = new ArrayList<Constraint>();
		//last id is 7
		AttributeConstraint exp8 = new BinnaryExpression(TypeOperatorBinnaryExpression.minus, new NameAttributeExpression(this.getAttribute().getHost(), "Moodle.state.attribute.b"), new BinnaryExpression(TypeOperatorBinnaryExpression.prod, new AggregationExpression(this.getAttribute().getHost(), TypeAggregationOperator.sum, "Moodle.state.attribute.RequiredResponseTime", TypeSens.succ), new NameAttributeExpression(this.getAttribute().getHost(), "Moodle.state.attribute.a")));
		constraints.addAll(exp8.build(m,nodes));
		constraints.add(m.arithm(getValueVar(), ">=", exp8.getValueVar()));
		return constraints;
	}
}

package Moodle.variable.attribute;

import org.chocosolver.solver.*;
import abstractsystem.variable.attribute.*;
import java.util.*;
import org.chocosolver.solver.constraints.*;
import abstractsystem.state.node.*;
import enumerations.*;
import abstractsystem.constraints.*;
import Moodle.state.attribute.*;

public  class TierResponseTimeVariable  extends AttributeIntegerVariable{
	public TierResponseTimeVariable(TierResponseTime a, int min, int max, Model m) {
		super(a, min, max, m);
	}
	
	@Override
	public List<Constraint> buildConstraints(Model m, List<Node> nodes){
		List<Constraint> constraints = new ArrayList<Constraint>();
		//last id is 2
		AttributeConstraint exp3 = new BinnaryExpression(TypeOperatorBinnaryExpression.div, new NameAttributeExpression(this.getAttribute().getHost(), "Moodle.state.attribute.Workload"), new NbLinkExpression(this.getAttribute().getHost(), TypeSens.succ));
		constraints	.addAll(exp3.build(m,nodes));
		constraints.add(m.arithm(getValueVar(), "=", exp3.getValueVar()));
		return constraints;
	}
}

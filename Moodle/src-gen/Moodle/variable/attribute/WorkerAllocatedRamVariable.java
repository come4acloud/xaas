package Moodle.variable.attribute;

import org.chocosolver.solver.*;
import abstractsystem.variable.attribute.*;
import java.util.*;
import org.chocosolver.solver.constraints.*;
import abstractsystem.state.node.*;
import enumerations.*;
import abstractsystem.constraints.*;
import Moodle.state.attribute.*;

public  class WorkerAllocatedRamVariable  extends AttributeIntegerVariable{
	public WorkerAllocatedRamVariable(WorkerAllocatedRam a, int min, int max, Model m) {
		super(a, min, max, m);
	}
	
	@Override
	public List<Constraint> buildConstraints(Model m, List<Node> nodes){
		List<Constraint> constraints = new ArrayList<Constraint>();
		//last id is 4
		AttributeConstraint exp5 = new AggregationExpression(this.getAttribute().getHost(), TypeAggregationOperator.sum, "Moodle.state.attribute.VmRamCapacity", TypeSens.succ);
		constraints	.addAll(exp5.build(m,nodes));
		constraints.add(m.arithm(getValueVar(), "=", exp5.getValueVar()));
		return constraints;
	}
}

package Moodle.variable.attribute;

import org.chocosolver.solver.*;
import abstractsystem.variable.attribute.*;
import java.util.*;
import org.chocosolver.solver.constraints.*;
import abstractsystem.state.node.*;
import enumerations.*;
import abstractsystem.constraints.*;
import Moodle.state.attribute.*;

public  class WorkerAllocatedCpuVariable  extends AttributeIntegerVariable{
	public WorkerAllocatedCpuVariable(WorkerAllocatedCpu a, int min, int max, Model m) {
		super(a, min, max, m);
	}
	
	@Override
	public List<Constraint> buildConstraints(Model m, List<Node> nodes){
		List<Constraint> constraints = new ArrayList<Constraint>();
		//last id is 3
		AttributeConstraint exp4 = new AggregationExpression(this.getAttribute().getHost(), TypeAggregationOperator.sum, "Moodle.state.attribute.VmCpuCapacity", TypeSens.succ);
		constraints	.addAll(exp4.build(m,nodes));
		constraints.add(m.arithm(getValueVar(), "=", exp4.getValueVar()));
		return constraints;
	}
}

package Moodle.state.node;

import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import java.util.*;
import Moodle.variable.node.*;
import org.chocosolver.solver.*;

public class SlaVmProvider extends SLAProvider{
	
	private SlaVmProviderVariable variable=null;
	
	public SlaVmProvider(IDNode idNode, IDType idtype, Set<IDNode> preds, Set<IDNode> succs, IDInternal idNodeInternal, Map<String,Object> atts) {
		super(idNode,idtype, preds, succs, idNodeInternal, atts);	
	}
	
	protected SlaVmProviderVariable getSLAProviderVariable(Model m, int nb_nodes){
		if(variable == null){
			variable= new SlaVmProviderVariable(this, m, nb_nodes);
		}
		
		return variable;
	}
}

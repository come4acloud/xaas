package Moodle.state.node;

import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import java.util.*;
import Moodle.variable.node.*;
import org.chocosolver.solver.*;

public class MySQL extends InternalComponent{
	
	private MySQLVariable variable=null;
	
	public MySQL(IDNode idNode, IDType idtype, Set<IDNode> preds, Set<IDNode> succs, IDInternal idNodeInternal, Map<String,Object> atts) {
		super(idNode,idtype, preds, succs, idNodeInternal, atts);	
	}
	
	protected MySQLVariable getInternalComponentVariable(Model m, int nb_nodes){
		if(variable == null){
			variable= new MySQLVariable(this, m, nb_nodes);
		}
		
		return variable;
	}
}

package Moodle.state.node;

import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import java.util.*;
import Moodle.variable.node.*;
import org.chocosolver.solver.*;

public class MoodleWebAService extends ServiceClient{
	
	private MoodleWebAServiceVariable variable=null;
	
	public MoodleWebAService(IDNode idNode, IDType idtype, Set<IDNode> preds, Set<IDNode> succs, IDInternal idNodeInternal, Map<String,Object> atts) {
		super(idNode,idtype, preds, succs, idNodeInternal, atts);	
	}
	
	protected MoodleWebAServiceVariable getServiceClientVariable(Model m, int nb_nodes){
		if(variable == null){
			variable= new MoodleWebAServiceVariable(this, m, nb_nodes);
		}
		
		return variable;
	}
}

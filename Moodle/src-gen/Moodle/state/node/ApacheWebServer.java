package Moodle.state.node;

import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import java.util.*;
import Moodle.variable.node.*;
import org.chocosolver.solver.*;

public class ApacheWebServer extends InternalComponent{
	
	private ApacheWebServerVariable variable=null;
	
	public ApacheWebServer(IDNode idNode, IDType idtype, Set<IDNode> preds, Set<IDNode> succs, IDInternal idNodeInternal, Map<String,Object> atts) {
		super(idNode,idtype, preds, succs, idNodeInternal, atts);	
	}
	
	protected ApacheWebServerVariable getInternalComponentVariable(Model m, int nb_nodes){
		if(variable == null){
			variable= new ApacheWebServerVariable(this, m, nb_nodes);
		}
		
		return variable;
	}
}

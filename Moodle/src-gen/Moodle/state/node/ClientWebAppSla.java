package Moodle.state.node;

import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import java.util.*;
import Moodle.variable.node.*;
import org.chocosolver.solver.*;

public class ClientWebAppSla extends SLAClient{
	
	private ClientWebAppSlaVariable variable=null;
	
	public ClientWebAppSla(IDNode idNode, IDType idtype, Set<IDNode> preds, Set<IDNode> succs, IDInternal idNodeInternal, Map<String,Object> atts) {
		super(idNode,idtype, preds, succs, idNodeInternal, atts);	
	}
	
	protected ClientWebAppSlaVariable getSLAClientVariable(Model m, int nb_nodes){
		if(variable == null){
			variable= new ClientWebAppSlaVariable(this, m, nb_nodes);
		}
		
		return variable;
	}
}

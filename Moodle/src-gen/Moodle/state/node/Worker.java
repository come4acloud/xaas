package Moodle.state.node;

import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import java.util.*;
import Moodle.variable.node.*;
import org.chocosolver.solver.*;

public class Worker extends InternalComponent{
	
	private WorkerVariable variable=null;
	
	public Worker(IDNode idNode, IDType idtype, Set<IDNode> preds, Set<IDNode> succs, IDInternal idNodeInternal, Map<String,Object> atts) {
		super(idNode,idtype, preds, succs, idNodeInternal, atts);	
	}
	
	protected WorkerVariable getInternalComponentVariable(Model m, int nb_nodes){
		if(variable == null){
			variable= new WorkerVariable(this, m, nb_nodes);
		}
		
		return variable;
	}
}

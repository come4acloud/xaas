package Moodle.state.attribute;

import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import abstractsystem.state.attribute.*;
import org.chocosolver.solver.variables.*;
import org.chocosolver.solver.*;
import Moodle.variable.attribute.*;

public class WorkerAllocatedRam extends AttributeInteger{
	
	private WorkerAllocatedRamVariable variable=null;
	
	public WorkerAllocatedRam(Node n, Integer i){
		super(n,i);
	}
	
	@Override
	public WorkerAllocatedRamVariable getVariable(Model m, int nb_nodes) {
		if(variable == null){
			variable=new WorkerAllocatedRamVariable(this, 0, IntVar.MAX_INT_BOUND, m);
		}
		
		return variable;
	}
}

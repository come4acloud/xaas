package Moodle.state.attribute;

import abstractsystem.state.node.*;
import abstractsystem.state.node.id.*;
import abstractsystem.state.attribute.*;

public class ID_MoodleWebAService extends AttributeIDNode{
	
	public ID_MoodleWebAService(Node n, IDNode i){
		super(n,i);
	}
}

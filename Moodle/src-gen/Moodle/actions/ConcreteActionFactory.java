package Moodle.actions;

import abstractsystem.state.node.id.*;
import controler.Knowledge;
import abstractsystem.action.*;

public class ConcreteActionFactory extends DefaultActionFactory implements ActionFactory{
	
	@Override
	public Action newEnableAction(IDNode idNode) {
		return super.newEnableAction(idNode);
	}	
	
	@Override
	public Action newDisableAction(IDNode idNode) {
		return super.newDisableAction(idNode);
	}
	
	@Override
	public Action newUnlinkAction(IDNode idnodefrom, IDNode idnodeto) {
		return super.newUnlinkAction(idnodefrom,idnodeto);
	}
	
	@Override
	public Action newLinkAction(IDNode idnodefrom, IDNode idnodeto) {
		return super.newLinkAction(idnodefrom,idnodeto);
	}
	
	@Override
	public Action newModifyAttributeAction(IDNode idNode, String name_attribute, int old_value, int new_value) {
		return super.newModifyAttributeAction(idNode,name_attribute,old_value, new_value);
	}			
}

